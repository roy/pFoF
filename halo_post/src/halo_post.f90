!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2020 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Post treatment for pFoF halos
!! @brief
!! 
!! @author Fabrice Roy

!> Post treatment for pFoF halos
!------------------------------------------------------------------------------------------------------------------------------------

program halo_post
  
  use fortran_hdf5_manage_interface_m
  use halo_post_parameters_m
  use iso_fortran_env, only : OUTPUT_UNIT
  use mpi

  implicit none

  integer :: mpierr

  call mpi_init(mpierr)
  call hdf5_init()

  write(OUTPUT_UNIT,'(a)') 'Halo post-treatment'

  call hdf5_finalize()
  call mpi_finalize(mpierr)

end program halo_post
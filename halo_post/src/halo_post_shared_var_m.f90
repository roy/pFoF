!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2020 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Global variables for halo_post
!! @brief
!! 
!! @author Fabrice Roy

!> Global variables for halo_post

module halo_post_shared_var_m

  use timer_m, only : timer_t
  use mpi_process_m, only : mpi_process_t

  implicit none

  private

  public :: mpi_process, &
       timer

  type(mpi_process_t) :: mpi_process
  type(timer_t) :: timer

end module halo_post_shared_var_m

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2020 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Class for halo_post input parameters
!! @brief
!! 
!! @author Fabrice Roy

!> Class for halo_post input parameters
!----------------------------------------------------------------------------------------------------------------------------------

module halo_post_parameters_m

  use error_handling_m
  use iso_fortran_env
  use mpi
  use halo_post_shared_var_m

  implicit none

  private

  public :: halo_post_parameters_t

  integer, parameter :: LEN_STRING = 256
  integer :: mpi_type_php

  type halo_post_parameters_t
    integer :: files_number
    integer :: halos_number
    character(len=LEN_STRING) :: data_directory
    character(len=LEN_STRING), allocatable, dimension(:) :: file_names
    integer, allocatable, dimension(:) :: halo_ids
  contains
    procedure :: Write => Write_halo_post_parameters
    procedure :: Read => Read_halo_post_parameters
    procedure :: Read_hdf5 => Read_hdf5_halo_post_parameters
    procedure :: Write_hdf5 => Write_hdf5_halo_post_parameters
  end type halo_post_parameters_t

contains
  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Read_halo_post_parameters(this)
   
    class(halo_post_parameters_t), intent(out) :: this

    integer :: alloc_stat
    character(len=LEN_STRING) :: data_directory
    character(ERR_MSG_LEN) :: error_message
    character(len=LEN_STRING), allocatable, dimension(:) :: file_names
    integer :: files_number
    integer, dimension(:), allocatable :: halo_ids
    integer :: halos_number
    integer :: ioerr
    integer :: l_string
    integer :: mpierr
    character(len=:), allocatable :: parameters_filename
    integer :: parameters_unit
    integer :: status

    namelist / input / data_directory, files_number, halos_number
    namelist / files / file_names
    namelist / halos / halo_ids

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_halo_post_parameters begins on process', mpi_process%rank
#endif

    call timer%Set_ref()
    if (mpi_process%rank==0) then
      if( command_argument_count() == 0 ) then
        parameters_filename = 'halo_post.nml'
      else
        call get_command_argument(1, length=l_string, status=status)
        allocate(character(l_string)::parameters_filename)
        call get_command_argument(1, parameters_filename, status=status)
        if (status /= 0) then
          write(ERROR_UNIT,*) 'Error in Read_halo_post_parameters: get_command_argument for argument 1 failed'
          call mpi_abort(MPI_COMM_WORLD, status, mpierr)
        end if
      end if
      write(OUTPUT_UNIT,'(a,a)') 'Parameters read from input file ', trim(parameters_filename)

      ! read input parameters
      open(newunit=parameters_unit, file=parameters_filename, status='old', action='read', iostat=ioerr, iomsg=error_message)
      if( ioerr /= 0 ) call IO_error('open file '//parameters_filename, 'Read_halo_post_parameters', &  
        error_message, ioerr, mpi_process%rank)

      read(parameters_unit, nml=input, iostat=ioerr, iomsg=error_message)
      if(ioerr /= 0) call IO_error('read input namelist', 'Read_halo_post_parameters', &
        error_message, ioerr, mpi_process%rank)

      if(files_number /= 0) then
        allocate(file_names(files_number), stat=alloc_stat, errmsg=error_message)
        if(alloc_stat /= 0) then
           call Allocate_error('file_names','Read_halo_post_parameters', error_message, alloc_stat, mpi_process%rank)
        end if
        read(parameters_unit, nml=files, iostat=ioerr, iomsg=error_message)
        if(ioerr /= 0) call IO_error('read files namelist', 'Read_halo_post_parameters', &
          error_message, ioerr, mpi_process%rank)
      end if

      if(halos_number /= 0) then
        allocate(halo_ids(halos_number), stat=alloc_stat, errmsg=error_message)
        if(alloc_stat /= 0) then
           call Allocate_error('halo_ids','Read_halo_post_parameters', error_message, alloc_stat, mpi_process%rank)
        end if
          read(parameters_unit, nml=halos, iostat=ioerr, iomsg=error_message)
          if(ioerr /= 0) call IO_error('read halos namelist', 'Read_halo_post_parameters', &
            error_message, ioerr, mpi_process%rank)
      end if
              
      close(parameters_unit)

      this%data_directory = data_directory
      this%files_number = files_number
      this%halos_number = halos_number
      if(halos_number /= 0) then
        call move_alloc(halo_ids, this%halo_ids)
      end if
      if(files_number /= 0) then
        call move_alloc(file_names,this%file_names)
      end if
  
    end if
    call timer%Inc_inp()

!    call Init_mpi_type_a2cp(mpi_type_a2cp, this)
!    call mpi_bcast(this, 1, mpi_type_a2cp, 0, MPI_COMM_WORLD, mpierr)
!    call timer%Inc_comm()

!    if(mpi_process%rank==0) call this%Print(OUTPUT_UNIT)
    call timer%Inc_out()

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_amr2cell_hdf5_parameter ends on process', mpi_process%rank
#endif

  end subroutine Read_halo_post_parameters

!----------------------------------------------------------------------------------------------------------------------------------
  subroutine Write_halo_post_parameters(this, unit)

    class(halo_post_parameters_t),intent(in) :: this
    integer,intent(in) :: unit
  

  end subroutine Write_halo_post_parameters

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Read_hdf5_halo_post_parameters(this, unit)

    class(halo_post_parameters_t),intent(in) :: this
    integer,intent(in) :: unit
  
    
  end subroutine Read_hdf5_halo_post_parameters

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Write_hdf5_halo_post_parameters(this, unit)

    class(halo_post_parameters_t),intent(in) :: this
    integer,intent(in) :: unit
  
    
  end subroutine Write_hdf5_halo_post_parameters

end module halo_post_parameters_m
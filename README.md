# pFoF
## Parallel Friend of Friend halo finder

pFoF is a Fortran software package based on a distributed implementation of the Friends-of-Friends halo finder algorithm.

It can be used to analyze output from the RAMSES code. It has also been succesfully used to analyze output from the Gadget code. 

This parallel Friend of Friend implementation is based on a sequential implementation written by Edouard Audit (Maison de la Simulation).

It has been written by Fabrice Roy and Vincent Bouillot -- CNRS / LUTH (Observatoire de Paris).

mail: fabrice.roy@obspm.fr

The tools and amr2cube have been written by Fabrice Roy.

The stable version can be found in the trunk directory.

There are 2 different versions of pFoF: pfof_snap for RAMSES snapshots, pfof_cone for RAMSES lightcones. 


The reference documentation can be found [here](https://roy.pages.obspm.fr/pFoF). 
## Install

pFoF needs:
* MPI: it has been tested with OpenMPI and IntelMPI
* HDF5: parallel version with support of Fortran 2003
* the Fortran HDF5 Interface library compiled with MPI support

Fortran HDF5 Interface is available here: https://gitlab.obspm.fr/roy/fortran-hdf5-interface

It comes with some GNU Make makefiles. You should edit the Make.inc file to edit the options according to your configuration.

## Test

Go to pFoF/trunk/pfof_snap/src.
Build the code.
Copy the executable (default name: pfof_snap) to the test directory.
Run pfof_snap with 
mpirun -np 8 ./pfof_snap

## Run

## Copyright and License
pFoF is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
@see COPYING

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Convert ramses_output to an array of cells
!! @brief
!!
!! @author Fabrice Roy

!> Convert ramses_output to an array of cells
!------------------------------------------------------------------------------------------------------------------------------------

module ramses_output_to_cells_m

  use shared_variables_m

  implicit none

  private

  public :: Ramses_output_to_cells

contains

  subroutine Ramses_output_to_cells(cells, ramses_output)

    use cell_m, only : cell_t
    use cells_array_m, only : cells_array_t
    use constants_m, only : IDKIND
    use error_handling_m, only : Allocate_error, ERR_MSG_LEN
    use iso_fortran_env, only : ERROR_UNIT
    use mpi
    use ramses_output_m, only : ramses_output_t

    type(cells_array_t), intent(out) :: cells
    class(ramses_output_t), intent(in) :: ramses_output

    integer(kind=IDKIND) :: cells_number
    integer :: iboundary
    integer :: icell, icell_level
    integer :: idim
    integer :: ifile
    integer :: ilevel
    integer :: nboundary
    integer :: ncpu
    integer :: nlevelmax
    integer :: nps, nthp
    integer :: size
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Ramses_output_to_cells begins on process', mpi_process%rank
#endif

    call timer%Set_ref()      
    cells_number = 0
    do ifile = ramses_output%first_file, ramses_output%last_file
       cells_number = cells_number + ramses_output%amr_datas(ifile)%ncells_total 
    end do

#ifdef DEBUG
    write(ERROR_UNIT,*) 'number of cells on process ',mpi_process%rank, ' : ', cells_number
#endif

    nthp = ramses_output%hydro_datas(ramses_output%first_file)%n_non_thermal_pressures
    nps = ramses_output%hydro_datas(ramses_output%first_file)%n_passive_scalars
    call cells%Allocate(nthp, nps, cells_number)



    icell = 1
    icell_level = 1

    nlevelmax=ramses_output%amr_header%nlevel_max
    nboundary=ramses_output%amr_header%nboundary
    ncpu=ramses_output%amr_header%ncpu

    do ifile = ramses_output%first_file, ramses_output%last_file ! ramses_output%first_file+2
       do ilevel = 1, nlevelmax
          do iboundary = 1, nboundary+ncpu
             if(iboundary<=ncpu) then
                size = ramses_output%amr_datas(ifile)%numbl(iboundary,ilevel)
             else
                size = ramses_output%amr_datas(ifile)%numbb(iboundary-ncpu,ilevel)
             end if
             cells%elts( icell_level : icell_level + size - 1 )%level = ilevel
             icell_level = icell_level + size
          end do
       end do
       cells%elts( icell : icell + ramses_output%amr_datas(ifile)%ncells_total - 1 )%process_id = -1
       cells%elts( icell : icell + ramses_output%amr_datas(ifile)%ncells_total - 1 )%cube_id = -1       
       cells%elts( icell : icell + ramses_output%amr_datas(ifile)%ncells_total - 1 )%identity = &
            ramses_output%amr_datas(ifile)%grid_index(:)
       do idim = 1,3
          cells%elts( icell : icell + ramses_output%amr_datas(ifile)%ncells_total - 1 )%position(idim) = &
               ramses_output%amr_datas(ifile)%grid_center(:,idim)
          cells%elts( icell : icell + ramses_output%amr_datas(ifile)%ncells_total - 1 )%velocity_field(idim) = &
               ramses_output%hydro_datas(ifile)%velocity_field(:,idim)
       end do
       cells%elts( icell : icell + ramses_output%amr_datas(ifile)%ncells_total - 1 )%density = &
            ramses_output%hydro_datas(ifile)%density(:)
       cells%elts( icell : icell + ramses_output%amr_datas(ifile)%ncells_total - 1 )%thermal_pressure = &
            ramses_output%hydro_datas(ifile)%thermal_pressure(:)
       do idim = 1,ramses_output%hydro_datas(ifile)%n_non_thermal_pressures
          cells%elts( icell : icell + ramses_output%amr_datas(ifile)%ncells_total - 1 )%non_thermal_pressures(idim) = &
               ramses_output%hydro_datas(ifile)%non_thermal_pressures(:,idim)
       end do
       do idim = 1,ramses_output%hydro_datas(ifile)%n_passive_scalars
          cells%elts( icell : icell + ramses_output%amr_datas(ifile)%ncells_total - 1 )%passive_scalars(idim) = &
               ramses_output%hydro_datas(ifile)%passive_scalars(:,idim)
       end do
       icell = icell + ramses_output%amr_datas(ifile)%ncells_total
    end do

    call timer%Inc_comp()
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Ramses_output_to_cells ends on process', mpi_process%rank
#endif

  end subroutine Ramses_output_to_cells

end module ramses_output_to_cells_m

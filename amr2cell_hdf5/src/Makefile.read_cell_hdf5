include ../../Make.inc

EXE = read_cell_hdf5.exe

COMMONDIR = ../../common/src/
OBJDIR=../../obj/
MODDIR=../../mod/
VPATH= $(COMMONDIR):.

SRC = char_utils_m.f90			\
	error_handling_m.f90		\
	constants_m.f90			\
	mpi_process_m.f90		\
	timer_m.f90 			\
    mpi_utils_m.f90     \
	shared_variables_m.f90 		\
	index_m.f90			\
	ramses_info_m.f90 		\
	ramses_hydro_descriptor_m.f90	\
	amr2cell_hdf5_parameters_m.f90	\
	cell_m.f90			\
	cells_array_m.f90		\
	cube_m.f90			\
	cubes_array_m.f90		\
	ramses_hydro_header_m.f90	\
	ramses_hydro_data_m.f90		\
	ramses_amr_header_m.f90 	\
	ramses_amr_data_m.f90		\
	ramses_output_m.f90		\
	ramses_output_to_cells_m.f90	\
	read_cell_hdf5.f90

OBJS_NOPREFIX=$(SRC:%.f90=%.o)
OBJS=$(addprefix $(OBJDIR), $(OBJS_NOPREFIX))

# variable containing if git is used or not
ifeq ($(wildcard ../../.git/HEAD ../../.git/index),)
	OBJ_GIT =
	USE_GIT =
else
	USE_GIT = $(shell which git)
	OBJ_GIT = ../../.git/HEAD ../../.git/index
endif


$(OBJDIR)%.o:%.f90
	@echo "--------------------------------"
	@echo building $*.o with parameters \
	FC=$(FC), FCFLAGS=$(FCFLAGS)
	$(FC) -c $(FCFLAGS) $< -o $@

$(MODDIR)%.mod:%.f90
	@echo "--------------------------------"
	@echo building $*.mod with parameters \
	FC=$(FC), FCFLAGS=$(FCFLAGS)
	$(FC) -c $(FCFLAGS) $<


all: directories release
release: $(EXE)
debug: FCFLAGS = $(DEBUGFLAGS)
debug: $(EXE)

$(EXE) : gitversion $(OBJS)
	@echo "--------------------------------"
	@echo $(ROOT_PFOF)
	@echo building $(EXE) with parameters \
	FC=$(FC), LDFLAGS=$(LDFLAGS)
	$(FC) -g -o $(EXE) $(OBJS) $(LDFLAGS)

cleanall :
	@echo "--------------------------------"
	@echo cleaning compiled objects, modules and executable
	rm -f $(OBJDIR)*.o $(MODDIR)*.mod *~ $(EXE) $(COMMONDIR)/gitversion.h

clean:
	@echo "--------------------------------"
	@echo cleaning executable
	rm -f  $(EXE)


# create file for git version
gitversion: $(OBJ_GIT)
ifneq ($(USE_GIT), )
	@cd $(COMMONDIR) ;\
	echo "#define GITVERSION \"$(shell git rev-parse HEAD)\"" > gitversion.h
else
	@cd $(COMMONDIR) ;\
	echo "#define GITVERSION \"no_version_found\"" > gitversion.h
endif

directories: ${MODDIR} ${OBJDIR}

${OBJDIR}:
	${MKDIR_P} $@

${MODDIR}:
	${MKDIR_P} $@


.PHONY: gitversion clean cleanall directories all

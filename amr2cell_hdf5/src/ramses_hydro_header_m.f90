!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Class for Ramses hydro header
!! @brief
!! 
!! @author Fabrice Roy

!> Class for Ramses hydro header
!------------------------------------------------------------------------------------------------------------------------------------

module ramses_hydro_header_m

  use constants_m, only : DP
  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use shared_variables_m

  implicit none

  private
  public :: ramses_hydro_header_t

  type ramses_hydro_header_t
     integer(kind=4) :: ncpu
     integer(kind=4) :: nvar
     integer(kind=4) :: ndim
     integer(kind=4) :: nlevel_max
     integer(kind=4) :: nboundary
     real(kind=DP) :: gamma
   contains
     procedure :: Print => Print_ramses_hydro_header
     procedure :: Read => Read_ramses_hydro_header
  end type ramses_hydro_header_t

contains

  !----------------------------------------------------------------------------------------------------------------------------------
  !> Read header from RAMSES hydro file
  subroutine Print_ramses_hydro_header(this)

    class(ramses_hydro_header_t), intent(in) :: this

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Print_ramses_hydro_header begins on process', mpi_process%rank
#endif
    call timer%Set_ref()

    write(OUTPUT_UNIT,'(a)') ' '
    write(OUTPUT_UNIT,'(a)') '=========================================================== '
    write(OUTPUT_UNIT,'(a)') 'hydro file header: '
    write(OUTPUT_UNIT,'(a,i0)') 'number of cpu: ', this%ncpu
    write(OUTPUT_UNIT,'(a,i0)') 'number of variables: ', this%nvar
    write(OUTPUT_UNIT,'(a,i0)') 'number of dimensions: ', this%ndim
    write(OUTPUT_UNIT,'(a,i0)') 'AMR max level: ', this%nlevel_max
    write(OUTPUT_UNIT,'(a,i0)') 'number of boundaries: ', this%nboundary
    write(OUTPUT_UNIT,'(a,e23.16)') 'gamma: ', this%gamma
    write(OUTPUT_UNIT,'(a)') '=========================================================== '
    write(OUTPUT_UNIT,'(a)') ' '

    call timer%Inc_out()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Print_ramses_hydro_header ends on process', mpi_process%rank
#endif

  end subroutine Print_ramses_hydro_header


  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Read_ramses_hydro_header(this, filename)

    use error_handling_m, only : Allocate_error, IO_error, ERR_MSG_LEN

    class(ramses_hydro_header_t), intent(out) :: this
    character(len=*), intent(in) :: filename

    integer :: ioerr
    integer :: unit_hydro
    character(len=ERR_MSG_LEN), allocatable :: error_message

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_ramses_hydro_header begins on process', mpi_process%rank
#endif
    call timer%Set_ref()    

    open(newunit=unit_hydro, file=filename, status='old', action='read', form='unformatted', iostat = ioerr, iomsg = error_message)
    if( ioerr > 0 ) then
       call IO_error('open file '//filename, 'Read_ramses_hydro_header', error_message, ioerr, mpi_process%rank)
    end if

    read(unit_hydro, iostat=ioerr, iomsg=error_message) this%ncpu
    if(ioerr/=0) call IO_error('read this%ncpu', 'Read_ramses_hydro_header', error_message, ioerr, mpi_process%rank)

    read(unit_hydro, iostat=ioerr, iomsg=error_message) this%nvar
    if(ioerr/=0) call IO_error('read this%nvar', 'Read_ramses_hydro_header', error_message, ioerr, mpi_process%rank)

    read(unit_hydro, iostat=ioerr, iomsg=error_message) this%ndim
    if(ioerr/=0) call IO_error('read this%ndim', 'Read_ramses_hydro_header', error_message, ioerr, mpi_process%rank)

    read(unit_hydro, iostat=ioerr, iomsg=error_message) this%nlevel_max
    if(ioerr/=0) call IO_error('read this%nlevel_max', 'Read_ramses_hydro_header', error_message, ioerr, mpi_process%rank)

    read(unit_hydro, iostat=ioerr, iomsg=error_message) this%nboundary
    if(ioerr/=0) call IO_error('read this%nboundary', 'Read_ramses_hydro_header', error_message, ioerr, mpi_process%rank)

    read(unit_hydro, iostat=ioerr, iomsg=error_message) this%gamma
    if(ioerr/=0) call IO_error('read this%gamma', 'Read_ramses_hydro_header', error_message, ioerr, mpi_process%rank)

    close(unit_hydro)

    call timer%Inc_inp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_ramses_hydro_header ends on process', mpi_process%rank
#endif

  end subroutine Read_ramses_hydro_header

end module ramses_hydro_header_m

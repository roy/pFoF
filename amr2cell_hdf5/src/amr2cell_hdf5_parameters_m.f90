!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Class for amr2cell_hdf5 input parameters
!! @brief
!! 
!! @author Fabrice Roy

!> Class for amr2cell_hdf5 input parameters
!----------------------------------------------------------------------------------------------------------------------------------

module amr2cell_hdf5_parameters_m

  use error_handling_m
  use iso_fortran_env
  use mpi
  use shared_variables_m

  implicit none

  private

  public :: amr2cell_hdf5_parameters_t

  integer, parameter :: LEN_STRING = 256

  type amr2cell_hdf5_parameters_t
     character(len=LEN_STRING) :: data_dir
     character(len=LEN_STRING) :: amr_filename
     character(len=LEN_STRING) :: hydro_filename
     character(len=LEN_STRING) :: hydro_descriptor_filename
     character(len=LEN_STRING) :: info_filename
     character(len=LEN_STRING) :: cube_filename
     integer :: cube_level
     integer :: cell_level_min
     integer :: cell_level_max
   contains
     procedure :: Check => Check_amr2cell_hdf5_parameters
     procedure :: Print => Print_amr2cell_hdf5_parameters
     procedure :: Read => Read_amr2cell_hdf5_parameters
     procedure :: Read_hdf5 => Read_hdf5_amr2cell_hdf5_parameters
     procedure :: Write_hdf5 => Write_hdf5_amr2cell_hdf5_parameters
  end type amr2cell_hdf5_parameters_t

contains
  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Check_amr2cell_hdf5_parameters(this, ramses_info)

    use char_utils_m, only : Int_to_char5
    use constants_m, only : FILENAME_LEN
    use ramses_info_m, only : ramses_info_t

    class(amr2cell_hdf5_parameters_t), intent(inout) :: this
    type(ramses_info_t), intent(in) :: ramses_info

    character(len=FILENAME_LEN) :: filename
    integer :: ncubes
    logical :: data_dir_exists
    logical :: amr_file_exists
    logical :: hydro_file_exists
    logical :: hydro_descriptor_file_exists
    logical :: info_file_exists

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Check_amr2cell_hdf5_parameters begins on process', mpi_process%rank
#endif

    ! check if cube numbers is >= process numbers
    ncubes = (2**(this%cube_level-1))**3
    if( ncubes < mpi_process%comm%size ) then
       write(ERROR_UNIT, '(a,i0,a,i0,a)') 'You are using ',mpi_process%comm%size,' processes and you want to write ', ncubes,' cubes.'
       call Algorithmic_error('Check_amr2cell_hdf5_parameters', 'Number of processes used should be <= number of cubes written.', 1000, mpi_process%rank)
    end if

    ! check cell_level_min and cell_level_max
    if( this%cell_level_min == 0 .or. this%cell_level_min < ramses_info%levelmin ) then
      this%cell_level_min = ramses_info%levelmin
      write(OUTPUT_UNIT,'(a,i0)') 'cell_level_min was undefined or < Ramses level_min ; its value has been adjusted to Ramses level_min = ', ramses_info%levelmin
    end if

    if( this%cell_level_max == 0 .or. this%cell_level_max > ramses_info%levelmax ) then
      this%cell_level_max = ramses_info%levelmax
      write(OUTPUT_UNIT,'(a,i0)') 'cell_level_max was undefined or > Ramses level_max ; its value has been adjusted to Ramses level_max = ', ramses_info%levelmax
    end if

    ! test the existance of the files
#ifndef INTEL
    inquire(file=this%data_dir, exist=data_dir_exists)
#else
    inquire(directory=this%data_dir, exist=data_dir_exists )
#endif
    if(.not. data_dir_exists) then
       write(ERROR_UNIT, '(a)') 'Data directory = '//trim(this%data_dir)
       call Algorithmic_error('Check_amr2cell_hdf5_parameters', 'This directory does not exist. Check data_dir parameter.', 1001, mpi_process%rank)
    end if

    filename=trim(this%data_dir)//trim(this%amr_filename)//Int_to_char5(1)
    inquire(file=filename, exist=amr_file_exists )
    if(.not. amr_file_exists) then
       write(ERROR_UNIT, '(a)') '1st amr file = '//filename
       call Algorithmic_error('Check_amr2cell_hdf5_parameters', 'This file does not exist. Check amr_filename parameter.', 1001, mpi_process%rank)
    end if

    filename=trim(this%data_dir)//trim(this%hydro_filename)//Int_to_char5(1)
    inquire(file=filename, exist=hydro_file_exists )
    if(.not. hydro_file_exists) then
       write(ERROR_UNIT, '(a)') '1st hydro file = '//filename
       call Algorithmic_error('Check_amr2cell_hdf5_parameters', 'This file does not exist. Check hydro_filename parameter.', 1001, mpi_process%rank)
    end if

    filename=trim(this%data_dir)//trim(this%info_filename)
    inquire(file=filename, exist=info_file_exists )
    if(.not. info_file_exists) then
       write(ERROR_UNIT, '(a)') 'info file = '//filename
       call Algorithmic_error('Check_amr2cell_hdf5_parameters', 'This file does not exist. Check info_filename parameter.', 1001, mpi_process%rank)
    end if

    filename=trim(this%data_dir)//trim(this%hydro_descriptor_filename)
    inquire(file=filename, exist=hydro_descriptor_file_exists )
    if(.not. hydro_descriptor_file_exists) then
       write(ERROR_UNIT, '(a)') 'hydro descriptor file = '//filename
       call Algorithmic_error('Check_amr2cell_hdf5_parameters', 'This file does not exist. Check hydro_descriptor_filename parameter.', 1001, mpi_process%rank)
    end if

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Check_amr2cell_hdf5_parameters ends on process', mpi_process%rank
#endif

  end subroutine Check_amr2cell_hdf5_parameters

  !--------------------------------------------------------------------------------------------------------------------------------
  subroutine Pack_a2c_parameters(parameters, memory_buffer, buffer_len)

    use iso_fortran_env, only: ERROR_UNIT, INT32
    use mpi

    integer, intent(out) :: buffer_len
    character, dimension(:), allocatable, intent(out) :: memory_buffer
    type(amr2cell_hdf5_parameters_t), intent(in) :: parameters

    integer :: buffer_pos
    integer :: mpierr
    integer :: tmp_len

    character(*), parameter :: ROUTINE_NAME = 'Pack_p2ch_parameters'

#ifndef NDEBUG
    write (ERROR_UNIT, '(a,i0)') ROUTINE_NAME//' begins on process', mpi_process%rank
#endif

    buffer_len = 6*LEN_STRING
    call mpi_pack_size(1, MPI_INTEGER, MPI_COMM_WORLD, tmp_len, mpierr)
    buffer_len = buffer_len + 3*tmp_len
    allocate (memory_buffer(0:buffer_len - 1))

    buffer_pos = 0
    if (mpi_process%rank == 0) then
      call mpi_pack(parameters%data_dir, LEN_STRING, MPI_CHARACTER, &
                    memory_buffer, buffer_len, buffer_pos, MPI_COMM_WORLD, mpierr)
      call mpi_pack(parameters%amr_filename, LEN_STRING, MPI_CHARACTER, &
                    memory_buffer, buffer_len, buffer_pos, MPI_COMM_WORLD, mpierr)
      call mpi_pack(parameters%hydro_filename, LEN_STRING, MPI_CHARACTER, &
                    memory_buffer, buffer_len, buffer_pos, MPI_COMM_WORLD, mpierr)
      call mpi_pack(parameters%hydro_descriptor_filename, LEN_STRING, MPI_CHARACTER, &
                    memory_buffer, buffer_len, buffer_pos, MPI_COMM_WORLD, mpierr)
      call mpi_pack(parameters%info_filename, LEN_STRING, MPI_CHARACTER, &
                    memory_buffer, buffer_len, buffer_pos, MPI_COMM_WORLD, mpierr)
      call mpi_pack(parameters%cube_filename, LEN_STRING, MPI_CHARACTER, &
                    memory_buffer, buffer_len, buffer_pos, MPI_COMM_WORLD, mpierr)
      call mpi_pack(parameters%cube_level, 1, MPI_INTEGER, &
                    memory_buffer, buffer_len, buffer_pos, MPI_COMM_WORLD, mpierr)
      call mpi_pack(parameters%cell_level_min, 1, MPI_INTEGER, &
                    memory_buffer, buffer_len, buffer_pos, MPI_COMM_WORLD, mpierr)
      call mpi_pack(parameters%cell_level_max, 1, MPI_INTEGER, &
                    memory_buffer, buffer_len, buffer_pos, MPI_COMM_WORLD, mpierr)
    end if

#ifndef NDEBUG
    write (ERROR_UNIT, '(a,i0)') ROUTINE_NAME//' ends on process', mpi_process%rank
#endif

  end subroutine Pack_a2c_parameters

  !--------------------------------------------------------------------------------------------------------------------------------
  subroutine Unpack_a2c_parameters(parameters, memory_buffer, buffer_len)

    use iso_fortran_env, only: ERROR_UNIT, INT32
    use mpi

    integer, intent(in) :: buffer_len
    character, dimension(*), intent(in) :: memory_buffer
    type(amr2cell_hdf5_parameters_t), intent(inout) :: parameters

    integer :: buffer_pos
    integer :: mpierr

    character(*), parameter :: ROUTINE_NAME = 'Unpack_p2ch_parameters'

#ifndef NDEBUG
    write (ERROR_UNIT, '(a,i0)') ROUTINE_NAME//' begins on process', mpi_process%rank
#endif

    buffer_pos = 0

    if (mpi_process%rank /= 0) then  
      call mpi_unpack(memory_buffer, buffer_len, buffer_pos, parameters%data_dir, &
      LEN_STRING, MPI_CHARACTER, MPI_COMM_WORLD, mpierr)
      call mpi_unpack(memory_buffer, buffer_len, buffer_pos, parameters%amr_filename, &
      LEN_STRING, MPI_CHARACTER, MPI_COMM_WORLD, mpierr)
      call mpi_unpack(memory_buffer, buffer_len, buffer_pos, parameters%hydro_filename, &
      LEN_STRING, MPI_CHARACTER, MPI_COMM_WORLD, mpierr)
      call mpi_unpack(memory_buffer, buffer_len, buffer_pos, parameters%hydro_descriptor_filename, &
      LEN_STRING, MPI_CHARACTER, MPI_COMM_WORLD, mpierr)
      call mpi_unpack(memory_buffer, buffer_len, buffer_pos, parameters%info_filename, &
      LEN_STRING, MPI_CHARACTER, MPI_COMM_WORLD, mpierr)
      call mpi_unpack(memory_buffer, buffer_len, buffer_pos, parameters%cube_filename, &
      LEN_STRING, MPI_CHARACTER, MPI_COMM_WORLD, mpierr)
      call mpi_unpack(memory_buffer, buffer_len, buffer_pos, parameters%cube_level, &
      1, MPI_INTEGER, MPI_COMM_WORLD, mpierr)
      call mpi_unpack(memory_buffer, buffer_len, buffer_pos, parameters%cell_level_min, &
      1, MPI_INTEGER, MPI_COMM_WORLD, mpierr)
      call mpi_unpack(memory_buffer, buffer_len, buffer_pos, parameters%cell_level_max, &
      1, MPI_INTEGER, MPI_COMM_WORLD, mpierr)
    end if

#ifndef NDEBUG
    write (ERROR_UNIT, '(a,i0)') ROUTINE_NAME//' ends on process', mpi_process%rank
#endif

  end subroutine Unpack_a2c_parameters

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Print_amr2cell_hdf5_parameters(this, unit_number)

    class(amr2cell_hdf5_parameters_t), intent(in) :: this
    integer, intent(in) :: unit_number

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Print_amr2cell_hdf5_parameters begins on process', mpi_process%rank
#endif

    write(unit_number,'(a,a)') '---------------------------------------------------------------------------------'
    write(unit_number,'(a)') 'Input parameters:'
    write(unit_number,'(a,a)') ' - input data directory: ', trim(this%data_dir)
    write(unit_number,'(a,a)') ' - amr filename: ', trim(this%amr_filename)
    write(unit_number,'(a,a)') ' - hydro filename: ', trim(this%hydro_filename)
    write(unit_number,'(a,a)') ' - hydro descriptor filename: ', trim(this%hydro_descriptor_filename)
    write(unit_number,'(a,a)') ' - info filename: ', trim(this%info_filename)
    write(unit_number,*) ''
    write(unit_number,'(a)') 'Output parameters:'    
    write(unit_number,'(a,a)') ' - cube filename: ', trim(this%cube_filename)
    write(unit_number,'(a,i0)') ' - cube level: ', this%cube_level
    write(unit_number,'(a,i0)') ' - minimum cell level: ', this%cell_level_min
    write(unit_number,'(a,i0)') ' - maximum cell level: ', this%cell_level_max
    write(unit_number,'(a,a)') '---------------------------------------------------------------------------------'

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Print_amr2cell_hdf5_parameters ends on process', mpi_process%rank
#endif

  end subroutine Print_amr2cell_hdf5_parameters

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Read_amr2cell_hdf5_parameters(this)

    class(amr2cell_hdf5_parameters_t), intent(out) :: this

    character(len=LEN_STRING) :: data_directory, amr_filename, hydro_filename, info_filename, &
         hydro_descriptor_filename
    integer :: buffer_len
    character(len=LEN_STRING) :: cube_filename
    integer :: cell_level_max
    integer :: cell_level_min
    integer :: cube_level
    character(ERR_MSG_LEN) :: error_message
    integer :: ioerr
    character(len=:), allocatable :: input
    integer :: l_string
    character, allocatable, dimension(:) :: memory_buffer
    integer :: mpierr
    character(len=:), allocatable :: parameters_filename
    integer :: parameters_unit
    integer :: status

    namelist / input_parameters / data_directory, amr_filename, hydro_filename, info_filename, &
         hydro_descriptor_filename
    namelist / output_parameters / cube_level, cell_level_min, cell_level_max, cube_filename

    cell_level_max = 0
    cell_level_min = 0

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_amr2cell_hdf5_parameters begins on process', mpi_process%rank
#endif

    call timer%Set_ref()
    if (mpi_process%rank==0) then
       if( command_argument_count() == 0 ) then
          parameters_filename = 'amr2cell_hdf5.nml'
       else
          call get_command_argument(1, length=l_string, status=status)
          allocate(character(l_string)::parameters_filename)
          call get_command_argument(1, parameters_filename, status=status)
          if (status /= 0) then
             write(ERROR_UNIT,*) 'Error in Read_amr2cell_hdf5_parameters: get_command_argument for argument 1 failed'
             call mpi_abort(MPI_COMM_WORLD, status, mpierr)
          end if
       end if
       write(OUTPUT_UNIT,'(a,a)') 'Parameters read from input file ', trim(parameters_filename)

       ! read input parameters
       open(newunit=parameters_unit, file=parameters_filename, status='old', action='read', iostat=ioerr, iomsg=error_message)
       if( ioerr /= 0 ) call IO_error('open file '//parameters_filename, 'Init_ramses_output', &  
            error_message, ioerr, mpi_process%rank)

       read(parameters_unit, nml=input_parameters, iostat=ioerr, iomsg=error_message)
       if(ioerr /= 0) call IO_error('read input_parameters namelist', 'Read_amr2cell_hdf5_parameters', &
            error_message, ioerr, mpi_process%rank)

       read(parameters_unit, nml=output_parameters, iostat=ioerr, iomsg=error_message)
       if(ioerr /= 0) call IO_error('read output_parameters namelist', 'Read_amr2cell_hdf5_parameters', &
            error_message, ioerr, mpi_process%rank)

       close(parameters_unit)

       this%data_dir = data_directory
       this%amr_filename = amr_filename
       this%hydro_filename = hydro_filename
       this%hydro_descriptor_filename = hydro_descriptor_filename
       this%info_filename = info_filename
       this%cube_filename = cube_filename
       this%cube_level = cube_level
       this%cell_level_min = cell_level_min
       this%cell_level_max = cell_level_max
    end if
    call timer%Inc_inp()

    call Pack_a2c_parameters(this, memory_buffer, buffer_len)
    call mpi_bcast(memory_buffer, buffer_len, MPI_PACKED, 0, MPI_COMM_WORLD, mpierr)
    call Unpack_a2c_parameters(this, memory_buffer, buffer_len)
    call timer%Inc_comm()

    if(mpi_process%rank==0) call this%Print(OUTPUT_UNIT)
    call timer%Inc_out()

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_amr2cell_hdf5_parameter ends on process', mpi_process%rank
#endif

  end subroutine Read_amr2cell_hdf5_parameters

!----------------------------------------------------------------------------------------------------------------------------------
  subroutine Read_hdf5_amr2cell_hdf5_parameters(this, hdf5_id, mpi_process)

    use fortran_hdf5_constants_m, only : H5_STR_LEN
    use fortran_hdf5_manage_groups_m, only : Hdf5_close_group, Hdf5_open_group
    use fortran_hdf5_read_attribute_m, only : Hdf5_read_attr
    use hdf5, only : HID_T
    use mpi_process_m, only : mpi_process_t

    integer(HID_T), intent(in) :: hdf5_id
    type(mpi_process_t), intent(in) :: mpi_process
    class(amr2cell_hdf5_parameters_t), intent(out) :: this

    character(len=H5_STR_LEN) :: groupname
    character(len=H5_STR_LEN) :: aname
    integer(kind=HID_T) :: gr_id
   
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_hdf5_amr2cell_hdf5_parameters begins on process', mpi_process%rank
#endif

    groupname = 'amr2cell_hdf5_parameters'
    call Hdf5_open_group(hdf5_id,groupname,gr_id)

    aname = 'data_dir'
    call Hdf5_read_attr(gr_id,aname,LEN_STRING,this%data_dir)
    aname = 'amr_filename'
    call Hdf5_read_attr(gr_id,aname,LEN_STRING,this%amr_filename)
    aname = 'hydro_filename'
    call Hdf5_read_attr(gr_id,aname,LEN_STRING,this%hydro_filename)
    aname = 'hydro_descriptor_filename'
    call Hdf5_read_attr(gr_id,aname,LEN_STRING,this%hydro_descriptor_filename)
    aname = 'info_filename'
    call Hdf5_read_attr(gr_id,aname,LEN_STRING,this%info_filename)
    aname = 'cube_filename'
    call Hdf5_read_attr(gr_id,aname,LEN_STRING,this%cube_filename)
    aname = 'cube_level'
    call Hdf5_read_attr(gr_id,aname,this%cube_level)
    aname = 'cell_level_min'
    call Hdf5_read_attr(gr_id,aname,this%cell_level_min)
    aname = 'cell_level_max'
    call Hdf5_read_attr(gr_id,aname,this%cell_level_max)

    call Hdf5_close_group(gr_id)


#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_hdf5_amr2cell_hdf5_parameters ends on process', mpi_process%rank
#endif

  end subroutine Read_hdf5_amr2cell_hdf5_parameters

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Write_hdf5_amr2cell_hdf5_parameters(this, hdf5_id, mpi_process)

    use fortran_hdf5_constants_m, only : H5_STR_LEN
    use fortran_hdf5_manage_groups_m, only : Hdf5_close_group, Hdf5_create_group
    use fortran_hdf5_write_attribute_m, only : Hdf5_write_attr
    use hdf5, only : HID_T
    use mpi_process_m, only : mpi_process_t

    integer(HID_T), intent(in) :: hdf5_id
    type(mpi_process_t), intent(in) :: mpi_process
    class(amr2cell_hdf5_parameters_t), intent(in) :: this

    character(len=H5_STR_LEN) :: groupname
    character(len=H5_STR_LEN) :: aname
    integer(kind=HID_T) :: gr_id
   
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Write_hdf5_amr2cell_hdf5_parameters begins on process', mpi_process%rank
#endif

    groupname = 'amr2cell_hdf5_parameters'
    call Hdf5_create_group(hdf5_id,groupname,gr_id)

    aname = 'data_dir'
    call Hdf5_write_attr(gr_id,aname,this%data_dir)
    aname = 'amr_filename'
    call Hdf5_write_attr(gr_id,aname,this%amr_filename)
    aname = 'hydro_filename'
    call Hdf5_write_attr(gr_id,aname,this%hydro_filename)
    aname = 'hydro_descriptor_filename'
    call Hdf5_write_attr(gr_id,aname,this%hydro_descriptor_filename)
    aname = 'info_filename'
    call Hdf5_write_attr(gr_id,aname,this%info_filename)
    aname = 'cube_filename'
    call Hdf5_write_attr(gr_id,aname,this%cube_filename)
    aname = 'cube_level'
    call Hdf5_write_attr(gr_id,aname,this%cube_level)
    aname = 'cell_level_min'
    call Hdf5_write_attr(gr_id,aname,this%cell_level_min)
    aname = 'cell_level_max'
    call Hdf5_write_attr(gr_id,aname,this%cell_level_max)

    call Hdf5_close_group(gr_id)


#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Write_hdf5_amr2cell_hdf5_parameters ends on process', mpi_process%rank
#endif

  end subroutine Write_hdf5_amr2cell_hdf5_parameters

end module amr2cell_hdf5_parameters_m


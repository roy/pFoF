!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Class for Ramses hydro descriptor
!! @brief
!! 
!! @author Fabrice Roy

!> Class for Ramses hydro descriptor
!------------------------------------------------------------------------------------------------------------------------------------

module ramses_hydro_descriptor_m

  use char_utils_m, only : string_t
  use constants_m, only : DP
  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use shared_variables_m

  implicit none

  private
  public :: ramses_hydro_descriptor_t

  type ramses_hydro_descriptor_t
     integer(kind=4) :: nvar
     type(string_t), allocatable, dimension(:) :: variable_names
     integer(kind=4) :: n_non_thermal_pressures
     integer(kind=4) :: n_passive_scalars
   contains
     procedure :: Print => Print_ramses_hydro_descriptor
     procedure :: Read => Read_ramses_hydro_descriptor
     procedure :: Read_hdf5 => Read_hdf5_ramses_hydro_descriptor
     procedure :: Write_hdf5 => Write_hdf5_ramses_hydro_descriptor
  end type ramses_hydro_descriptor_t

contains

  !----------------------------------------------------------------------------------------------------------------------------------
  !> Read header from RAMSES hydro file
  subroutine Print_ramses_hydro_descriptor(this)

    class(ramses_hydro_descriptor_t), intent(in) :: this

    integer :: ivar

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Print_ramses_hydro_descriptor begins on process', mpi_process%rank
#endif
    call timer%Set_ref()

    write(OUTPUT_UNIT,'(a)') ' '
    write(OUTPUT_UNIT,'(a)') '=========================================================== '
    write(OUTPUT_UNIT,'(a)') 'hydro file descriptor: '
    write(OUTPUT_UNIT,'(a,i0)') 'number of hydro variables: ', this%nvar
    write(OUTPUT_UNIT,'(a,i0)') 'number of non thermal pressures: ', this%n_non_thermal_pressures
    write(OUTPUT_UNIT,'(a,i0)') 'number of passive scalars: ', this%n_passive_scalars
    do ivar = 1, this%nvar
       write(OUTPUT_UNIT,'(a,i0,a,a)') 'name of hydro variables#',ivar,':',this%variable_names(ivar)%string
    end do
    write(OUTPUT_UNIT,'(a)') '=========================================================== '
    write(OUTPUT_UNIT,'(a)') ' '

    call timer%Inc_out()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Print_ramses_hydro_descriptor ends on process', mpi_process%rank
#endif

  end subroutine Print_ramses_hydro_descriptor

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Read_ramses_hydro_descriptor(this, filename)

    use char_utils_m, only : Int_to_char5
    use error_handling_m, only : Allocate_error, IO_error, ERR_MSG_LEN
    use mpi

    class(ramses_hydro_descriptor_t), intent(out) :: this
    character(len=*), intent(in) :: filename

    integer :: ioerr
    integer :: unit_hydro
    character(len=ERR_MSG_LEN) :: error_message
    character(len=14) :: tmpchar
    character(len=22) :: name
    integer :: alloc_stat
    integer :: ivar

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_ramses_hydro_descriptor begins on process', mpi_process%rank
#endif
    call timer%Set_ref()    

    open(newunit=unit_hydro, file=filename, status='old', action='read', form='formatted', iostat = ioerr, iomsg = error_message)
    if( ioerr > 0 ) then
       call IO_error('open file '//filename, 'Read_ramses_hydro_descriptor', error_message, ioerr, mpi_process%rank)
    end if

    read(unit_hydro, '(a14,i10)', iostat=ioerr, iomsg=error_message) tmpchar, this%nvar
    if(ioerr/=0) call IO_error('read this%nvar', 'Read_ramses_hydro_descriptor', error_message, ioerr, mpi_process%rank)

    allocate(this%variable_names(this%nvar), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%variable_names','Read_ramses_hydro_descriptor', &
            error_message, alloc_stat, mpi_process%rank)
    end if

    do ivar = 1, this%nvar
       read(unit_hydro, '(a14, a)', iostat=ioerr, iomsg=error_message) tmpchar, name
       this%variable_names(ivar)%string = trim(name)
       if(ioerr/=0) call IO_error('read this%nvar('//Int_to_char5(ivar)//')', 'Read_ramses_hydro_descriptor', &
            error_message, ioerr, mpi_process%rank)
    end do

    close(unit_hydro)

    this%n_non_thermal_pressures = 0
    this%n_passive_scalars = 0
    do ivar = 1, this%nvar
       if(this%variable_names(ivar)%string(1:1)=='n') this%n_non_thermal_pressures =+ 1
       if(this%variable_names(ivar)%string(1:1)=='p' ) this%n_passive_scalars =+ 1
    end do

    call timer%Inc_inp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_ramses_hydro_descriptor ends on process', mpi_process%rank
#endif

  end subroutine Read_ramses_hydro_descriptor

!------------------------------------------------------------------------------------------------------------------------------------
  subroutine Read_hdf5_ramses_hydro_descriptor(this, hdf5_id)

    use error_handling_m, only : Allocate_error, IO_error, ERR_MSG_LEN
    use fortran_hdf5_constants_m, only : H5_STR_LEN
    use fortran_hdf5_manage_groups_m, only : Hdf5_close_group, Hdf5_open_group
    use fortran_hdf5_read_attribute_m, only : Hdf5_read_attr
    use hdf5, only : HID_T

    integer(HID_T), intent(in) :: hdf5_id
    class(ramses_hydro_descriptor_t), intent(out) :: this

    integer :: alloc_stat
    character(len=H5_STR_LEN) :: aname
    character(len=ERR_MSG_LEN) :: error_message
    integer(kind=HID_T) :: gr_id
    character(len=H5_STR_LEN) :: groupname
    integer(kind=4) :: iname
    character(len=H5_STR_LEN), allocatable, dimension(:) :: tmpchar

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_hdf5_ramses_hydro_descriptor begins on process', mpi_process%rank
#endif

    call timer%Set_ref()

    groupname = 'ramses_hydro_descriptor_info'
    call Hdf5_open_group(hdf5_id,groupname,gr_id)

    aname = 'nvar' 
    call Hdf5_read_attr(gr_id, aname, this%nvar)
    aname = 'n_non_thermal_pressures' 
    call Hdf5_read_attr(gr_id, aname, this%n_non_thermal_pressures)
    aname = 'n_passive_scalars' 
    call Hdf5_read_attr(gr_id, aname, this%n_passive_scalars)
    aname = 'variable_names'
    allocate(tmpchar(this%nvar), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('tmpchar','Read_hdf5_ramses_hydro_descriptor', &
            error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(this%variable_names(this%nvar), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%variable_names','Read_hdf5_ramses_hydro_descriptor', &
            error_message, alloc_stat, mpi_process%rank)
    end if
    call Hdf5_read_attr(gr_id, aname, H5_STR_LEN, this%nvar, tmpchar)
    do iname = 1, this%nvar
      this%variable_names(iname)%string = tmpchar(iname)
    end do
call Hdf5_close_group(gr_id)

call timer%Inc_inp()

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_hdf5_ramses_hydro_descriptor ends on process', mpi_process%rank
#endif

  end subroutine Read_hdf5_ramses_hydro_descriptor

!------------------------------------------------------------------------------------------------------------------------------------      
  subroutine Write_hdf5_ramses_hydro_descriptor(this, hdf5_id)

    use error_handling_m, only : Allocate_error, IO_error, ERR_MSG_LEN
    use fortran_hdf5_constants_m, only : H5_STR_LEN
    use fortran_hdf5_manage_groups_m, only : Hdf5_close_group, Hdf5_create_group
    use fortran_hdf5_write_attribute_m, only : Hdf5_write_attr
    use hdf5, only : HID_T

    class(ramses_hydro_descriptor_t), intent(in) :: this
    integer(HID_T), intent(in) :: hdf5_id

    integer :: alloc_stat
    character(len=H5_STR_LEN) :: aname
    character(len=ERR_MSG_LEN) :: error_message
    integer(kind=HID_T) :: gr_id
    character(len=H5_STR_LEN) :: groupname
    integer(kind=4) :: iname
    character(len=H5_STR_LEN), allocatable, dimension(:) :: tmpchar

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Write_hdf5_ramses_hydro_descriptor begins on process', mpi_process%rank
#endif

    call timer%Set_ref()

    groupname = 'ramses_hydro_descriptor_info'
    call Hdf5_create_group(hdf5_id,groupname,gr_id)
    
    aname = 'nvar' 
    call Hdf5_write_attr(gr_id, aname, this%nvar)
    aname = 'n_non_thermal_pressures' 
    call Hdf5_write_attr(gr_id, aname, this%n_non_thermal_pressures)
    aname = 'n_passive_scalars' 
    call Hdf5_write_attr(gr_id, aname, this%n_passive_scalars)
    aname = 'variable_names'
    allocate(tmpchar(this%nvar), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('tmpchar','Write_hdf5_ramses_hydro_descriptor', &
            error_message, alloc_stat, mpi_process%rank)
    end if    
    do iname = 1, this%nvar
      tmpchar(iname) = this%variable_names(iname)%string
    end do
    call Hdf5_write_attr(gr_id, aname, this%nvar, tmpchar)

    call Hdf5_close_group(gr_id)

    call timer%Inc_out()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Write_hdf5_ramses_hydro_descriptor ends on process', mpi_process%rank
#endif

  end subroutine Write_hdf5_ramses_hydro_descriptor

end module ramses_hydro_descriptor_m

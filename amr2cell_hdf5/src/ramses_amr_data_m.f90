!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Class for Ramses AMR data
!! @brief
!!
!! @author Fabrice Roy

!> Class for Ramses AMR data
!------------------------------------------------------------------------------------------------------------------------------------

module ramses_amr_data_m

  use constants_m, only : DP, QDP
  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use shared_variables_m

  implicit none

  private
  public :: ramses_amr_data_t

  type ramses_amr_data_t
     integer(kind=4) :: ngrid_level
     integer, allocatable, dimension(:) :: ncells
     integer :: ncells_total
     integer :: twotondim
     integer :: headf, tailf, numbf, used_mem, used_mem_tot
     integer :: nbinodes, ncoarse, ndomain
     character(len=80) :: ordering
     integer, allocatable, dimension(:) :: grid_index, next_index, prev_index, father_index
     integer, allocatable, dimension(:,:) :: neighbor_index
     real(DP), allocatable, dimension(:,:) :: grid_center
     integer, allocatable, dimension(:,:) :: headl, taill, numbl, numbtot
     integer, allocatable, dimension(:,:) :: headb, tailb, numbb
     real(DP), allocatable, dimension(:) :: bisec_wall
     integer, allocatable, dimension(:,:) :: bisec_next
     integer, allocatable, dimension(:) :: bisec_indx
     real(DP), allocatable, dimension(:,:) :: bisec_cpubox_max, bisec_cpubox_min
     real(QDP), allocatable, dimension(:) :: bound_key
     integer, allocatable, dimension(:) :: refinement_map, cpu_map
     integer, allocatable, dimension(:,:) :: son
   contains
     procedure :: Deallocate => Deallocate_ramses_amr_data
     procedure :: Read => Read_ramses_amr_data
     procedure :: Test => Test_ramses_amr_tree
  end type ramses_amr_data_t

contains
  !----------------------------------------------------------------------------------------------------------------------------------
  !> Deallocate arrays in ramses_amr_data_t
  subroutine Deallocate_ramses_amr_data(this)

    use error_handling_m, only : Deallocate_error, ERR_MSG_LEN

    class(ramses_amr_data_t), intent(out) :: this

    character(len=ERR_MSG_LEN) :: error_message
    integer :: alloc_stat

#ifdef DEBUG2
    write(ERROR_UNIT,'(a,i0)') 'Deallocate_ramses_amr_data begins on process', mpi_process%rank
#endif
    call timer%Set_ref()

    if(allocated(this%grid_index)) then
       write(ERROR_UNIT,*) 'Deallocate grid_index'
       deallocate(this%grid_index, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%grid_index','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%next_index)) then
       deallocate(this%next_index, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%next_index','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%prev_index)) then
       deallocate(this%prev_index, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%prev_index','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%father_index)) then
       deallocate(this%father_index, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%father_index','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%neighbor_index)) then
       deallocate(this%neighbor_index, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%neighbor_index','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%grid_center)) then
       deallocate(this%grid_center, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%grid_center','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%headl)) then
       deallocate(this%headl, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%headl','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%taill)) then
       deallocate(this%taill, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%taill','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%numbl)) then
       deallocate(this%numbl, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%numbl','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%numbtot)) then
       deallocate(this%numbtot, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%numbtot','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%headb)) then
       deallocate(this%headb, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%headb','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%tailb)) then
       deallocate(this%tailb, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%tailb','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%numbb)) then
       deallocate(this%numbb, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%numbb','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%bisec_wall)) then
       deallocate(this%bisec_wall, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%bisec_wall','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%bisec_next)) then
       deallocate(this%bisec_next, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%bisec_next','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%bisec_indx)) then
       deallocate(this%bisec_indx, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%bisec_indx','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%bisec_cpubox_max)) then
       deallocate(this%bisec_cpubox_max, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%bisec_cpubox_max','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%bisec_cpubox_min)) then
       deallocate(this%bisec_cpubox_min, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%bisec_cpubox_min','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%bound_key)) then
       deallocate(this%bound_key, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%bound_key','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%son)) then
       deallocate(this%son, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%son','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%refinement_map)) then
       deallocate(this%refinement_map, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%refinement_map','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if
    if(allocated(this%cpu_map)) then
       deallocate(this%cpu_map, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%cpu_map','Deallocate_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
    end if

    call timer%Inc_comp()
#ifdef DEBUG2
    write(ERROR_UNIT,'(a,i0)') 'Deallocate_ramses_amr_data ends on process', mpi_process%rank
#endif

  end subroutine Deallocate_ramses_amr_data


  !----------------------------------------------------------------------------------------------------------------------------------
  !> Read data from RAMSES amr file
  subroutine Read_ramses_amr_data(this, ramses_amr_header, filename)

    use ramses_amr_header_m, only : ramses_amr_header_t
    use error_handling_m, only : Allocate_error, IO_error, ERR_MSG_LEN
    use mpi

    class(ramses_amr_data_t), intent(out) :: this
    type(ramses_amr_header_t), intent(inout) :: ramses_amr_header
    character(len=*), intent(in) :: filename

    integer :: ioerr
    integer :: unit_amr
    character(len=ERR_MSG_LEN) :: error_message
    integer :: alloc_stat

    integer :: noutput, iout, ifout
    real(DP), allocatable, dimension(:) :: tout, aout
    real(DP), allocatable, dimension(:) :: dtold, dtnew
    real(DP) :: omega_b, omega_k, omega_l, omega_m
    real(DP) :: rho_tot
    real(DP) :: aexp, aexp_ini, aexp_old, boxlen_ini
    integer :: nstep, nstep_coarse
    real(DP) :: epot_tot_int, epot_tot_old, h0, const, hexp, mass_sph, mass_tot_0, time
    integer :: ilevel, iboundary
    integer :: size, index_begin, index_end, index2_begin, index2_end
    integer :: idim

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_ramses_amr_data begins on process', mpi_process%rank
#endif
    call timer%Set_ref()

    open(newunit=unit_amr, file=filename, status='old', action='read', form='unformatted', iostat = ioerr, iomsg = error_message)
    if( ioerr > 0 ) then
       call IO_error('open file '//filename, 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    end if

    read(unit_amr, iostat=ioerr, iomsg=error_message) ramses_amr_header%ncpu
    if(ioerr/=0) call IO_error('read ramses_amr_header%ncpu', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) ramses_amr_header%ndim
    if(ioerr/=0) call IO_error('read ramses_amr_header%ndim', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) ramses_amr_header%nx, ramses_amr_header%ny, ramses_amr_header%nz
    if(ioerr/=0) call IO_error('read ramses_amr_header%nx/ny/nz', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) ramses_amr_header%nlevel_max
    if(ioerr/=0) call IO_error('read ramses_amr_header%nlevel_max', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) ramses_amr_header%ngrid_max
    if(ioerr/=0) call IO_error('read ramses_amr_header%ngrid_max', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) ramses_amr_header%nboundary
    if(ioerr/=0) call IO_error('read ramses_amr_header%nboundary', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) ramses_amr_header%ngrid_current
    if(ioerr/=0) call IO_error('read ramses_amr_header%ngrid_current', 'Read_ramses_amr_data', &
         error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) ramses_amr_header%boxlen
    if(ioerr/=0) call IO_error('read ramses_amr_header%boxlen', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)

    read(unit_amr, iostat=ioerr, iomsg=error_message) noutput, iout, ifout
    if(ioerr/=0) call IO_error('read noutput/iout/ifout', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)

    allocate(tout(noutput), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('tout','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(aout(noutput), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('aout','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if

    read(unit_amr, iostat=ioerr, iomsg=error_message) tout
    if(ioerr/=0) call IO_error('read tout', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) aout
    if(ioerr/=0) call IO_error('read aout', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) time
    if(ioerr/=0) call IO_error('read time', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)

    allocate(dtold(ramses_amr_header%nlevel_max), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('dtold','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(dtnew(ramses_amr_header%nlevel_max), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('dtnew','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if

    read(unit_amr, iostat=ioerr, iomsg=error_message) dtold
    if(ioerr/=0) call IO_error('read dtold', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) dtnew
    if(ioerr/=0) call IO_error('read dtnew', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) nstep, nstep_coarse
    if(ioerr/=0) call IO_error('read nstep/nstep_coarse', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) const, mass_tot_0, rho_tot
    if(ioerr/=0) call IO_error('read const/mass_tot_0/rho_tot', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) omega_m, omega_l, omega_k, omega_b, h0, aexp_ini, boxlen_ini
    if(ioerr/=0) call IO_error('read omega_m, omega_l, omega_k, omega_b, h0, aexp_ini, boxlen_ini', &
         'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) aexp, hexp, aexp_old, epot_tot_int, epot_tot_old
    if(ioerr/=0) call IO_error('read aexp, hexp, aexp_old, epot_tot_int, epot_tot_old', &
         'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) mass_sph
    if(ioerr/=0) call IO_error('read mass_sph', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)

    ! allocations
    allocate(this%headl(ramses_amr_header%ncpu, ramses_amr_header%nlevel_max), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%headl','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(this%taill(ramses_amr_header%ncpu, ramses_amr_header%nlevel_max), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%taill','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(this%numbl(ramses_amr_header%ncpu, ramses_amr_header%nlevel_max), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%numbl','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(this%numbtot(10, ramses_amr_header%nlevel_max), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%numbtot','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if

    read(unit_amr, iostat=ioerr, iomsg=error_message) this%headl
    if(ioerr/=0) call IO_error('read this%headl', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) this%taill
    if(ioerr/=0) call IO_error('read this%taill', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) this%numbl
    if(ioerr/=0) call IO_error('read this%numbl', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) this%numbtot
    if(ioerr/=0) call IO_error('read this%numbtot', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)

    if(ramses_amr_header%nboundary > 0) then
       allocate(this%headb(ramses_amr_header%nboundary, ramses_amr_header%nlevel_max), stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Allocate_error('this%headb','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
       allocate(this%tailb(ramses_amr_header%nboundary, ramses_amr_header%nlevel_max), stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Allocate_error('this%tailb','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
       allocate(this%numbb(ramses_amr_header%nboundary, ramses_amr_header%nlevel_max), stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Allocate_error('this%numbb','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if

       read(unit_amr, iostat=ioerr, iomsg=error_message) this%headb
       if(ioerr/=0) call IO_error('read this%headb', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
       read(unit_amr, iostat=ioerr, iomsg=error_message) this%tailb
       if(ioerr/=0) call IO_error('read this%tailb', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
       read(unit_amr, iostat=ioerr, iomsg=error_message) this%numbb
       if(ioerr/=0) call IO_error('read this%numbb', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    end if

    read(unit_amr, iostat=ioerr, iomsg=error_message) this%headf, this%tailf, this%numbf, this%used_mem, this%used_mem_tot
    if(ioerr/=0) call IO_error('read this%headf, this%tailf, this%numbf, this%used_mem, this%used_mem_tot', &
         'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)

    read(unit_amr, iostat=ioerr, iomsg=error_message) this%ordering
    if(ioerr/=0) call IO_error('read this%ordering', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)

    if(this%ordering == 'bisection') then
       this%nbinodes = 2**(ceiling(log(dble(ramses_amr_header%ncpu))/log(2.0_4))+1)-1 ! cf RAMSES, file init_amr.f90

       allocate(this%bisec_wall(this%nbinodes), stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Allocate_error('this%bisec_wall','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
       allocate(this%bisec_next(this%nbinodes,2), stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Allocate_error('this%bisec_next','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
       allocate(this%bisec_indx(this%nbinodes), stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Allocate_error('this%bisec_indx','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
       allocate(this%bisec_cpubox_min(ramses_amr_header%ncpu,ramses_amr_header%ndim), stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Allocate_error('this%bisec_cpubox_min','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
       allocate(this%bisec_cpubox_max(ramses_amr_header%ncpu, ramses_amr_header%ndim), stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Allocate_error('this%bisec_cpubox_max','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if

       read(unit_amr, iostat=ioerr, iomsg=error_message) this%bisec_wall
       if(ioerr/=0) call IO_error('read this%bisec_wall', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
       read(unit_amr, iostat=ioerr, iomsg=error_message) this%bisec_next
       if(ioerr/=0) call IO_error('read this%bisec_next', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
       read(unit_amr, iostat=ioerr, iomsg=error_message) this%bisec_indx
       if(ioerr/=0) call IO_error('read this%bisec_indx', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
       read(unit_amr, iostat=ioerr, iomsg=error_message) this%bisec_cpubox_min
       if(ioerr/=0) call IO_error('read this%bisec_cpubox_min', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
       read(unit_amr, iostat=ioerr, iomsg=error_message) this%bisec_cpubox_max
       if(ioerr/=0) call IO_error('read this%bisec_cpubox_max', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    else
       ! we consider overload=1 i.e. ndomain=ncpu
       this%ndomain = ramses_amr_header%ncpu
       allocate(this%bound_key(0:this%ndomain), stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Allocate_error('this%bound_key','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
       end if
       read(unit_amr, iostat=ioerr, iomsg=error_message) this%bound_key
       if(ioerr/=0) call IO_error('read this%bound_key', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)

    end if

    ! compute total size
    allocate(this%ncells(ramses_amr_header%nlevel_max), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%ncells','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    this%ncells=0
    do ilevel = 1, ramses_amr_header%nlevel_max
       do iboundary = 1, ramses_amr_header%ncpu
          this%ncells(ilevel) = this%ncells(ilevel) + this%numbl(iboundary,ilevel)
       end do
       do iboundary = 1, ramses_amr_header%nboundary
          this%ncells(ilevel) = this%ncells(ilevel) + this%numbb(iboundary, ilevel)
       end do
    end do
    this%ncells_total = sum(this%ncells)

    ! initialize ncoarse
    this%ncoarse = ramses_amr_header%nx*ramses_amr_header%ny*ramses_amr_header%nz
    ! initialize twotondim
    this%twotondim = 2**ramses_amr_header%ndim

    allocate(this%son(this%ncells_total,this%twotondim), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%son','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(this%refinement_map(this%ncoarse+this%twotondim*this%ncells_total), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%refinement_map','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(this%cpu_map(this%ncoarse+this%twotondim*this%ncells_total), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%cpu_map','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if

    ! test: skip lines for coarse grid
    read(unit_amr, iostat=ioerr, iomsg=error_message) 
    if(ioerr/=0) call IO_error('read this%son', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) 
    if(ioerr/=0) call IO_error('read this%refinement_map', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message)  
    if(ioerr/=0) call IO_error('read this%cpu_map', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)

    allocate(this%grid_center(this%ncells_total,ramses_amr_header%ndim), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%grid_center','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(this%grid_index(this%ncells_total), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%grid_index','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(this%next_index(this%ncells_total), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%next_index','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(this%prev_index(this%ncells_total), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%prev_index','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(this%father_index(this%ncells_total), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%father_index','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(this%neighbor_index(this%ncells_total, 2*ramses_amr_header%ndim), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%neighbor_index','Read_ramses_amr_data', error_message, alloc_stat, mpi_process%rank)
    end if

    index_end = 0
    index2_end = this%ncoarse
    dolevel: do ilevel = 1, ramses_amr_header%nlevel_max

       doboundary: do iboundary = 1, ramses_amr_header%nboundary+ramses_amr_header%ncpu
          if(iboundary<=ramses_amr_header%ncpu) then
             size = this%numbl(iboundary,ilevel)
          else
             size = this%numbb(iboundary-ramses_amr_header%ncpu,ilevel)
          end if
          notempty: if(size/=0) then
             index_begin = index_end + 1
             index_end = index_end + size
             ! read grid_index
             read(unit_amr, iostat=ioerr, iomsg=error_message) this%grid_index(index_begin:index_end)
             if(ioerr/=0) then
                write(ERROR_UNIT,*) 'Error: read this%grid_index for ilevel=', ilevel, ' and iboundary=', iboundary
                call IO_error('read this%grid_index', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
             end if
             ! read next_index
             read(unit_amr, iostat=ioerr, iomsg=error_message) this%next_index(index_begin:index_end)
             if(ioerr/=0) then
                write(ERROR_UNIT,*) 'Error: read this%next_index for ilevel=', ilevel, ' and iboundary=', iboundary
                call IO_error('read this%next_index', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
             end if
             ! read prev_index
             read(unit_amr, iostat=ioerr, iomsg=error_message) this%prev_index(index_begin:index_end)
             if(ioerr/=0) then
                write(ERROR_UNIT,*) 'Error: read this%prev_index for ilevel=', ilevel, ' and iboundary=', iboundary
                call IO_error('read this%prev_index', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
             end if
             ! read grid_center
             do idim = 1, ramses_amr_header%ndim
                read(unit_amr, iostat=ioerr, iomsg=error_message) this%grid_center(index_begin:index_end,idim)
                if(ioerr/=0) then
                   write(ERROR_UNIT,*) 'Error: read this%grid_center for idim=',idim,', ilevel=', ilevel, &
                        ' and iboundary=', iboundary
                   call IO_error('read this%grid_center', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
                end if
             end do
             ! read father_index
             read(unit_amr, iostat=ioerr, iomsg=error_message) this%father_index(index_begin:index_end)
             if(ioerr/=0) then
                write(ERROR_UNIT,*) 'Error: read this%father_index for ilevel=', ilevel, ' and iboundary=', iboundary
                call IO_error('read this%father_index', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
             end if
             ! read neighbor_index
             do idim = 1, 2*ramses_amr_header%ndim
                read(unit_amr, iostat=ioerr, iomsg=error_message) this%neighbor_index(index_begin:index_end, idim)
                if(ioerr/=0) then
                   write(ERROR_UNIT,*) 'Error: read this%neighbor_index for idim=',idim,', ilevel=', ilevel, &
                        ' and iboundary=', iboundary
                   call IO_error('read this%neighbor_index', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
                end if
             end do
             ! read son
             do idim = 1, this%twotondim
                read(unit_amr, iostat=ioerr, iomsg=error_message) this%son(index_begin:index_end, idim)
                if(ioerr/=0) then
                   write(ERROR_UNIT,*) 'Error: read this%son for idim=',idim,', ilevel=', ilevel, &
                        ' and iboundary=', iboundary
                   call IO_error('read this%son', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
                end if
             end do
             ! read cpu_map
             do idim = 1, this%twotondim
                index2_begin = index2_end + (idim-1)*size + 1
                read(unit_amr, iostat=ioerr, iomsg=error_message) this%cpu_map(index2_begin:index2_begin+size-1)
                if(ioerr/=0) then
                   write(ERROR_UNIT,*) 'Error: read this%cpu_map for idim=',idim,', ilevel=', ilevel, &
                        ' and iboundary=', iboundary
                   call IO_error('read this%cpu_map', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
                end if
             end do
             ! read refinement map
             do idim = 1, this%twotondim
                index2_begin = index2_end + (idim-1)*size + 1
                read(unit_amr, iostat=ioerr, iomsg=error_message) this%refinement_map(index2_begin:index2_begin+size-1)
                if(ioerr/=0) then
                   write(ERROR_UNIT,*) 'Error: read this%refinement_map for idim=',idim,', ilevel=', ilevel, &
                        ' and iboundary=', iboundary
                   call IO_error('read this%refinement_map', 'Read_ramses_amr_data', error_message, ioerr, mpi_process%rank)
                end if
             end do
             index2_end = index2_end + this%twotondim*size
          end if notempty
       end do doboundary

    end do dolevel

    close(unit_amr)

    call timer%Inc_inp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_ramses_amr_data ends on process', mpi_process%rank
#endif

  end subroutine Read_ramses_amr_data

  !------------------------------------------------------------------------------------------------------------------------------------
  subroutine Test_ramses_amr_tree(this)

    class(ramses_amr_data_t), intent(in) :: this

    integer :: inode, i, root

    root = 3
    write(OUTPUT_UNIT,*) 'node=',root, ': ',this%grid_center(root,:)
    write(OUTPUT_UNIT,*) 'sons: ',this%son(root,:)
    do i = 1, 8
       inode = this%son(root,i)
       write(OUTPUT_UNIT,*) 'node=',inode, ': ',this%grid_center(inode,:)
       write(OUTPUT_UNIT,*) 'sons: ',this%son(inode,:)

    end do

  end subroutine Test_ramses_amr_tree

end module ramses_amr_data_m

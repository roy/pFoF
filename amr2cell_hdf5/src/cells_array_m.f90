!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Class for an array of cells
!! @brief
!! 
!! @author Fabrice Roy

!> Class for an array of cells
!------------------------------------------------------------------------------------------------------------------------------------

module cells_array_m

  use cell_m, only : cell_t, Init_cell_mpi_type
  use constants_m, only : IDKIND
  use error_handling_m, only : Algorithmic_error, Allocate_error, ERR_MSG_LEN
  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use shared_variables_m

  implicit none

  private
  public :: cells_array_t

  type cells_array_t
     integer(kind=IDKIND) :: ncells
     type(cell_t(:,:)), allocatable, dimension(:) :: elts
   contains
     procedure Allocate => Allocate_cells_array
     procedure Count_cells
     procedure Deallocate => Deallocate_cells_array
     procedure Distribute => Distribute_cells_array
     procedure Clean => Clean_unused_cells 
     procedure Read_hdf5 => Read_hdf5_cells_array
     procedure, private :: Remove_duplicate_cells
     procedure, private :: Remove_large_cells
     procedure, private :: Remove_small_cells
     procedure Sort => Sort_cells_array
     procedure, private :: Sort_heapify_cells
     procedure, private :: Sort_sink_cells
     procedure, private :: Sort_swap_cells
     procedure :: Write_hdf5 => Write_hdf5_cells_array
  end type cells_array_t

contains

  !------------------------------------------------------------------------------------------------------------------------------------

  subroutine Allocate_cells_array(this, nthp, nps, ncells)

    class(cells_array_t), intent(inout) :: this
    integer, intent(in) :: nthp, nps
    integer(kind=IDKIND), intent(in) :: ncells

    character(len=ERR_MSG_LEN) :: error_message
    integer :: alloc_stat

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Allocate_cells_array begins on process', mpi_process%rank
#endif

    call timer%Set_ref()
    this%ncells = ncells
    allocate(cell_t(nthp,nps) :: this%elts(ncells), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%elts','Allocate_cells_array', error_message, alloc_stat, mpi_process%rank)
    end if
    call timer%Inc_comp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Allocate_cells_array ends on process', mpi_process%rank
#endif

  end subroutine Allocate_cells_array

  !------------------------------------------------------------------------------------------------------------------------------------
  subroutine Clean_unused_cells(this, amr2cell_hdf5_parameters)
    
    use amr2cell_hdf5_parameters_m, only : amr2cell_hdf5_parameters_t
    
    type(amr2cell_hdf5_parameters_t), intent(in) :: amr2cell_hdf5_parameters
    class(cells_array_t),intent(inout) :: this

    call this%Sort()
    call this%Remove_duplicate_cells()
    call this%Remove_large_cells(amr2cell_hdf5_parameters%cell_level_min)
    call this%Remove_small_cells(amr2cell_hdf5_parameters%cell_level_max)

  end subroutine Clean_unused_cells

  !------------------------------------------------------------------------------------------------------------------------------------

  subroutine Count_cells(this, local_cells_per_cube, global_cells_per_cube, dims)

    use constants_m, only : DP
    use index_m, only : Position_to_id
    use mpi

    class(cells_array_t), intent(inout) :: this
    integer, dimension(:), allocatable, intent(out) :: local_cells_per_cube
    integer, dimension(:), allocatable, intent(out) :: global_cells_per_cube
    integer, dimension(3), intent(in) :: dims

    integer :: alloc_stat
    character(len=ERR_MSG_LEN) :: error_message
    integer :: icell
    integer :: icube
    real(DP), dimension(3) :: lengths
    integer :: mpierr
    integer :: ncubes 

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Count_cells begins on process ', mpi_process%rank
#endif

    ! first sort and remove large cells

    call timer%Set_ref()
    ncubes = dims(1)*dims(2)*dims(3)
    allocate(local_cells_per_cube(ncubes), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('local_cells_per_cube','Count_cells', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(global_cells_per_cube(ncubes), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('global_cells_per_cube','Count_cells', error_message, alloc_stat, mpi_process%rank)
    end if

    local_cells_per_cube = 0
    global_cells_per_cube = 0
    lengths = 1._DP/dims

    do icell = 1, this%ncells
       icube = Position_to_id(dims,lengths,this%elts(icell)%position)
       if(icube > ncubes) then
          write(ERROR_UNIT,'(a,i0,a,i0,a,3f8.3)') &
               'Error: icube=',icube, ' for cell ',icell, ' with position ', this%elts(icell)%position
          call Algorithmic_error('Count_cells','icube > ncubes', 101, mpi_process%rank)
       end if
       local_cells_per_cube(icube) = local_cells_per_cube(icube)+1
       ! save cube id in process_id_array: it will be converted to process rank later
       this%elts(icell)%cube_id = icube
    end do
    call timer%Inc_comp()

    ! reduce cells_per_cube to get the global number of cells on each cube 
    call mpi_allreduce(local_cells_per_cube, global_cells_per_cube, ncubes, MPI_INTEGER, MPI_SUM, mpi_process%comm%name, mpierr)
    call timer%Inc_comm()

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Count_cells ends on process ', mpi_process%rank
#endif

  end subroutine Count_cells


  !------------------------------------------------------------------------------------------------------------------------------------

  subroutine Deallocate_cells_array(this)

    class(cells_array_t), intent(out) :: this

  end subroutine Deallocate_cells_array


  !------------------------------------------------------------------------------------------------------------------------------------

  subroutine Distribute_cells_array(this, cube_to_process)

    use mpi

    class(cells_array_t), intent(inout) :: this
    integer, dimension(:), intent(in) :: cube_to_process

    integer(kind=4) :: cell_mpi_t_size
    integer(kind=MPI_ADDRESS_KIND) :: cells_window_dim
    integer :: alloc_stat
    integer :: cell_mpi_t
    type(cells_array_t) :: cells_array_to_send
    integer :: cells_window
    integer :: cube_level
    integer :: dest
    integer :: disp_recv
    integer :: disp_send
    integer, allocatable, dimension(:) :: displacement_recv
    integer, allocatable, dimension(:) :: displacement_send
    character(len=ERR_MSG_LEN) :: error_message
    integer :: icell
    integer :: icube
    integer :: ind
    integer :: iprocess
    integer(IDKIND) :: nc
    integer :: nntp, nps
    integer :: mpierr
    integer, dimension(MPI_STATUS_SIZE) :: mpistat
    integer :: my_ncells
    integer, allocatable, dimension(:) :: ncells_to_send
    integer, allocatable, dimension(:) :: ncells_to_recv
    integer :: prov
    integer :: request_recv
    integer :: request_send
    type(cells_array_t) :: tmp_cells_array

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Distribute_cells_array begins on process ', mpi_process%rank
#endif

    call timer%Set_ref()
    ! check to which cube belongs a cell
    ! set process_id to the rank of the process which will write the cube
    allocate(ncells_to_send(mpi_process%comm%size), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('ncells_to_send','Distribute_cells_array', error_message, alloc_stat, mpi_process%rank)
    end if

    ncells_to_send = 0
    do icell = 1, this%ncells
       icube = this%elts(icell)%cube_id
       iprocess = cube_to_process(icube)
       this%elts(icell)%process_id = iprocess
       ncells_to_send(iprocess+1) = ncells_to_send(iprocess+1) + 1
    end do

    ! sort against the process_id 
    call this%Sort()

    allocate(ncells_to_recv(mpi_process%comm%size),stat=alloc_stat,errmsg=error_message)
    if ( alloc_stat /= 0 ) then
       call Allocate_error('ncells_to_recv','Distribute_cells_array',error_message, alloc_stat, mpi_process%rank)
    end if
    call timer%Inc_comp()

    call mpi_alltoall(ncells_to_send,1,MPI_INTEGER,ncells_to_recv,1,MPI_INT,MPI_COMM_WORLD,mpierr)
    call timer%Inc_comm()

    my_ncells = ncells_to_send(mpi_process%rank+1)

    ! allocate tmp_cells_array: will become this
    nntp = this%elts(1)%n_non_thermal_pressures
    nps = this%elts(1)%n_passive_scalars
    nc = sum(ncells_to_recv)
    call tmp_cells_array%Allocate(nntp,nps,nc)

    ! compute displacements
    allocate(displacement_recv(mpi_process%comm%size), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then    
       call Allocate_error('displacement_recv','Distribute_cells_array', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(displacement_send(mpi_process%comm%size), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then    
       call Allocate_error('displacement_send','Distribute_cells_array', error_message, alloc_stat, mpi_process%rank)
    end if

    displacement_recv(1) = 0
    do iprocess = 2, mpi_process%comm%size
       displacement_recv(iprocess) = displacement_recv(iprocess-1) + ncells_to_recv(iprocess-1)
    end do
    displacement_send(1) = 0
    do iprocess = 2, mpi_process%comm%size
       displacement_send(iprocess) = displacement_send(iprocess-1) + ncells_to_send(iprocess-1)
    end do

    ! copy local cells to tmp
    disp_recv = displacement_recv(mpi_process%rank+1)
    disp_send = displacement_send(mpi_process%rank+1)
    tmp_cells_array%elts(disp_recv+1:disp_recv+my_ncells) = this%elts(disp_send+1:disp_send+my_ncells)
    call timer%Inc_comp()

    ! init mpi_cell_t for communications
    call Init_cell_mpi_type(cell_mpi_t, this%elts(1:2)) 

    do iprocess = 1, mpi_process%comm%size-1
       dest = mod(mpi_process%rank + iprocess, mpi_process%comm%size)
       prov = mod(mpi_process%rank - iprocess + mpi_process%comm%size, mpi_process%comm%size)
       if(ncells_to_send(dest+1) /=0 ) then
          disp_send = displacement_send(dest+1)
          call mpi_isend(this%elts(1+disp_send),ncells_to_send(dest+1), cell_mpi_t,dest,dest,mpi_process%comm%name,request_send,mpierr)
       end if
       if(ncells_to_recv(prov+1)/=0) then
          disp_recv = displacement_recv(prov+1)
          call mpi_irecv(tmp_cells_array%elts(1+disp_recv),ncells_to_recv(prov+1),cell_mpi_t,prov,mpi_process%rank,mpi_process%comm%name,request_recv,mpierr)
       end if

       if(ncells_to_send(dest+1)/=0) call mpi_wait(request_send,mpistat,mpierr)
       if(ncells_to_recv(prov+1)/=0) call mpi_wait(request_recv,mpistat,mpierr)

    end do
    call timer%Inc_comm()

    if(allocated(this%elts)) then
       call this%Deallocate()
    end if
    call move_alloc(tmp_cells_array%elts,this%elts)
    this%ncells=tmp_cells_array%ncells

    ! check
    do icell=1, this%ncells
       if(this%elts(icell)%process_id /= mpi_process%rank) then
          call this%elts(icell)%print(OUTPUT_UNIT)
          call Algorithmic_error('Distribute_cells_array','this%elts()%process_id /= mpi_process%rank', 102, mpi_process%rank)
       end if
    end do

    call timer%Inc_comp()

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Distribute_cells_array ends on process ', mpi_process%rank
#endif

  end subroutine Distribute_cells_array

  !------------------------------------------------------------------------------------------------------------------------------------
  subroutine Read_hdf5_cells_array(this, hdf5_id, ramses_hydro_descriptor)

    use char_utils_m, only : int_to_char12
    use fortran_hdf5_constants_m
    use fortran_hdf5_manage_groups_m
    use fortran_hdf5_read_attribute_m
    use fortran_hdf5_read_data_m
    use hdf5
    use ramses_hydro_descriptor_m, only : ramses_hydro_descriptor_t

    class(cells_array_t),intent(out) :: this
    integer(kind=hid_t),intent(in) :: hdf5_id
    type(ramses_hydro_descriptor_t), intent(in) :: ramses_hydro_descriptor

    character(len=H5_STR_LEN) :: attr_name
    character(len=H5_STR_LEN) :: groupname
    integer(kind=hid_t) :: cell_gr_id
    integer :: icell
    integer(IDKIND) :: ncells
    integer(kind=4) :: nps
    integer(kind=4) :: nthp

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_hdf5_cells_array begins on process', mpi_process%rank
#endif
    ! read number of cells in this array
    attr_name = 'ncells'
    call Hdf5_read_data(hdf5_id,attr_name, ncells)

    call this%Allocate(ramses_hydro_descriptor%n_non_thermal_pressures,ramses_hydro_descriptor%n_passive_scalars,ncells)

    do icell = 1, this%ncells
       groupname = 'cell_'//int_to_char12(icell)
       call Hdf5_open_group(hdf5_id, groupname, cell_gr_id)
       call this%elts(icell)%Read_hdf5(cell_gr_id)
       call Hdf5_close_group(cell_gr_id)
    end do

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_hdf5_cells_array ends on process', mpi_process%rank
#endif

  end subroutine Read_hdf5_cells_array

  !------------------------------------------------------------------------------------------------------------------------------------
  subroutine Remove_duplicate_cells(this)

    class(cells_array_t), intent(inout) :: this

    integer :: icell
    integer, parameter :: MAX_LVL=50
    integer :: n_duplicates
    type(cell_t(:,:)), allocatable :: ref_cell
    type(cells_array_t) :: tmp
    integer :: nntp, nps
    integer(kind=IDKIND) :: nc
    character(len=ERR_MSG_LEN) :: error_message
    integer :: alloc_stat

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Remove_duplicate_cells begins on process ', mpi_process%rank
    write(ERROR_UNIT,'(a,i0)') 'number of cells with duplicates: ', this%ncells
#endif
    call timer%Set_ref()

    n_duplicates = 0
    allocate(ref_cell, source=this%elts(1), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('ref_cell','Remove_duplicate_cells', error_message, alloc_stat, mpi_process%rank)
    end if
    ! 1st cell is reference cell 
    ref_cell = this%elts(1)
    ! loop over cells
    do icell = 2, this%ncells
       if( this%elts(icell)%Is_eq(ref_cell) ) then
          this%elts(icell)%level = MAX_LVL
          n_duplicates = n_duplicates+1
       else
          ref_cell =  this%elts(icell)
       end if
    end do

    call this%Sort()

    nntp = this%elts(1)%n_non_thermal_pressures
    nps = this%elts(1)%n_passive_scalars
    nc = this%ncells-n_duplicates
    call tmp%Allocate(nntp, nps, nc)
    tmp%elts(1:tmp%ncells) = this%elts(1:tmp%ncells)

    if(allocated(this%elts)) then
       call this%Deallocate()
    end if
    call move_alloc(tmp%elts,this%elts)
    this%ncells=tmp%ncells

    call timer%Inc_comp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'number of cells without duplicates: ', this%ncells
    write(ERROR_UNIT,'(a,i0)') 'Remove_duplicate_cells ends on process ', mpi_process%rank
#endif

  end subroutine Remove_duplicate_cells

  !------------------------------------------------------------------------------------------------------------------------------------
  subroutine Remove_large_cells(this, level_min)

    class(cells_array_t),intent(inout)  :: this
    integer ,intent(in) ::  level_min

    integer :: icell
    integer :: n_large
    integer(kind=IDKIND) :: nc
    integer :: nntp, nps
    type(cells_array_t) :: tmp

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Remove_large_cells begins on process ', mpi_process%rank
#endif
    call timer%Set_ref()

    n_large = 0
    do icell = 1, this%ncells
       if(this%elts(icell)%level < level_min ) then
          n_large = n_large + 1
       else
          exit
       end if
    enddo

    nntp = this%elts(1)%n_non_thermal_pressures
    nps = this%elts(1)%n_passive_scalars
    nc = this%ncells - n_large
    call tmp%Allocate(nntp, nps, nc)
    tmp%elts(1:tmp%ncells) = this%elts(n_large+1:this%ncells)

    if(allocated(this%elts)) then
       call this%Deallocate()
    end if
    call move_alloc(tmp%elts,this%elts)
    this%ncells=tmp%ncells

    call timer%Inc_comp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'number of cells after large ones removed: ', this%ncells
    write(ERROR_UNIT,'(a,i0)') 'Remove_large_cells ends on process ', mpi_process%rank
#endif

  end subroutine Remove_large_cells

  !------------------------------------------------------------------------------------------------------------------------------------
  subroutine Remove_small_cells(this, level_max)

    class(cells_array_t),intent(inout)  :: this
    integer, intent(in) ::  level_max

    integer :: icell
    integer :: n_small
    integer(kind=IDKIND) :: nc
    integer :: nntp, nps
    type(cells_array_t) :: tmp

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Remove_small_cells begins on process ', mpi_process%rank
#endif
    call timer%Set_ref()

    n_small = 0
    do icell = 1, this%ncells
       if(this%elts(icell)%level > level_max ) then
          n_small = n_small + 1
       else
          exit
       end if
    enddo

    nntp = this%elts(1)%n_non_thermal_pressures
    nps = this%elts(1)%n_passive_scalars
    nc = this%ncells - n_small
    call tmp%Allocate(nntp, nps, nc)
    tmp%elts(1:tmp%ncells) = this%elts(1:tmp%ncells)

    if(allocated(this%elts)) then
       call this%Deallocate()
    end if
    call move_alloc(tmp%elts,this%elts)
    this%ncells=tmp%ncells

    call timer%Inc_comp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'number of cells after small ones removed: ', this%ncells
    write(ERROR_UNIT,'(a,i0)') 'Remove_small_cells ends on process ', mpi_process%rank
#endif

  end subroutine Remove_small_cells

  !------------------------------------------------------------------------------------------------------------------------------------

  subroutine Sort_cells_array(this)

    class(cells_array_t), intent(inout) :: this

    ! Local variables
    integer(kind=IDKIND) :: i,size

#ifdef DEBUGSORT
    write(ERROR_UNIT,'(a,i0)') 'Sort_cells_array begins on process ', mpi_process%rank
#endif
    call timer%Set_ref()

    do i = this%ncells/2, 1, -1
       call this%Sort_sink_cells(this%ncells, i)
    end do

    do i = this%ncells, 2, -1
       call this%Sort_swap_cells(1,i)
       call this%Sort_sink_cells(i-1, 1)
    end do

    call timer%Inc_comp()
#ifdef DEBUGSORT
    write(ERROR_UNIT,'(a,i0)') 'Sort_cells_array ends on process ', mpi_process%rank
#endif

  end subroutine Sort_cells_array


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine Sort_heapify_cells(this,size)

    class(cells_array_t), intent(inout) :: this
    integer(kind=IDKIND),intent(out)         :: size

    ! Local variables
    integer(kind=IDKIND) :: i

#ifdef DEBUGSORT
    write(ERROR_UNIT,'(a,i0)') 'Sort_heapify_cells begins on process ', mpi_process%rank, size
#endif

    size = this%ncells
    do i = this%ncells/2, 1, -1
       call this%Sort_sink_cells(size,i)
    end do

#ifdef DEBUGSORT
    write(ERROR_UNIT,'(a,i0)') 'Sort_heapify_cells ends on process ', mpi_process%rank
#endif

  end subroutine Sort_heapify_cells


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine Sort_sink_cells(this,size,i)

    class(cells_array_t), intent(inout) :: this
    integer(kind=IDKIND),intent(in) :: i,size

    ! Local variables
    integer(kind=IDKIND) :: l, r, max

#ifdef DEBUGSORT
    write(ERROR_UNIT,'(a,i0,x,i0,x,i0)') 'Sort_sink_cells begins on process ', mpi_process%rank, size, i
#endif

    l = 2*i
    r = 2*i+1
    max = i
    if( (l <= size) ) then
       if( this%elts(l)%Is_sup(this%elts(i)) ) then
          max = l
       else          
          max = i
       end if
    end if
    if( (r <= size) ) then
       if ( this%elts(r)%Is_sup(this%elts(max))  ) then
          max = r
       end if
    end if
    if( max /= i ) then
       call this%Sort_swap_cells(i,max)
       call this%Sort_sink_cells(size,max)
    end if

#ifdef DEBUGSORT
    write(ERROR_UNIT,'(a,i0)') 'Sort_sink_cells ends on process ', mpi_process%rank
#endif

  end subroutine Sort_sink_cells


  !=======================================================================
  !> Swap routine for heapsort
  subroutine Sort_swap_cells(this,i,j)

    class(cells_array_t), intent(inout) :: this
    integer(kind=IDKIND), intent(in) :: i,j

    ! Local variables
    type(cell_t(:,:)), allocatable, save :: tmp

#ifdef DEBUGSORT
    write(ERROR_UNIT,'(a,i0,x,i0,x,i0)') 'Sort_swap_cells begins on process ', mpi_process%rank, i, j
#endif

    if(.not.allocated(tmp)) then
       allocate(tmp, source=this%elts(i))
    else
       tmp = this%elts(i)
    end if

    this%elts(i) = this%elts(j)
    this%elts(j) = tmp

#ifdef DEBUGSORT
    call this%elts(i)%Print(ERROR_UNIT)
    call this%elts(j)%Print(ERROR_UNIT)
    write(ERROR_UNIT,'(a,i0)') 'Sort_swap_cells ends on process ', mpi_process%rank
#endif

  end subroutine Sort_swap_cells

  !------------------------------------------------------------------------------------------------------------------------------------
  subroutine Write_hdf5_cells_array(this, hdf5_id)

    use char_utils_m, only : int_to_char12
    use fortran_hdf5_constants_m
    use fortran_hdf5_manage_groups_m
    use fortran_hdf5_write_attribute_m
    use fortran_hdf5_write_data_m
    use hdf5


    class(cells_array_t),intent(in) :: this
    integer(kind=hid_t),intent(in) :: hdf5_id

    character(len=H5_STR_LEN) :: attr_name
    character(len=H5_STR_LEN) :: groupname
    integer(kind=hid_t) :: cell_gr_id
    integer :: icell

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Write_hdf5_cells_array begins on process', mpi_process%rank
#endif
    ! write number of cells in this array: it could be an integer(8) so write as data
    attr_name = 'ncells'
    call Hdf5_write_data(hdf5_id, attr_name, this%ncells)

    do icell = 1, this%ncells
       groupname = 'cell_'//int_to_char12(icell)
       call Hdf5_create_group(hdf5_id, groupname, cell_gr_id)
       call this%elts(icell)%Write_hdf5(cell_gr_id)
       call Hdf5_close_group(cell_gr_id)
    end do

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Write_hdf5_cells_array ends on process', mpi_process%rank
#endif

  end subroutine Write_hdf5_cells_array

end module cells_array_m

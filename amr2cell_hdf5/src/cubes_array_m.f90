!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Class for an array of cubes output
!! @brief
!! 
!! @author Fabrice Roy

!> Class for an array of cubes output
!----------------------------------------------------------------------------------------------------------------------------------
module cubes_array_m

  use amr2cell_hdf5_parameters_m
  use cube_m, only : cube_t
  use error_handling_m
  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use ramses_info_m
  use shared_variables_m

  implicit none

  private

  public :: cubes_array_t

  type cubes_array_t
     integer, dimension(3) :: dims
     integer :: ncubes
     type(cube_t), allocatable, dimension(:) :: elts
   contains
     procedure :: Assign => Assign_cubes_to_process
     procedure :: Associate => Associate_cells_to_cubes
     procedure :: Init => Init_cubes_array
     procedure :: Write => Write_cubes_array
  end type cubes_array_t

contains
  !----------------------------------------------------------------------------------------------------------------------------------

  subroutine Assign_cubes_to_process(this, cube_to_process, cells_per_cube)

    use mpi

    class(cubes_array_t), intent(inout) :: this
    integer, dimension(:), allocatable, intent(out) :: cube_to_process
    integer, dimension(*), intent(in) :: cells_per_cube

    integer :: alloc_stat
    character(len=ERR_MSG_LEN) :: error_message
    integer :: icube
    integer :: iproc
    integer, dimension(:,:), allocatable :: maxs_ranks
    integer :: nb_receiver
    integer :: nb_sender
    integer, allocatable, dimension(:,:) :: ncells_cube_proc
    integer, dimension(:), allocatable :: ncube_per_process
    integer :: mpierr
    integer :: my_cube
    integer :: ncube_left
    integer, allocatable, dimension(:) :: receiver_id
    integer, allocatable, dimension(:) :: sender_id

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Assign_cubes_to_process begins on process', mpi_process%rank
#endif

    call timer%Set_ref()
    allocate(maxs_ranks(2,this%ncubes), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('maxs_ranks','Assign_cubes_to_process', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(cube_to_process(this%ncubes), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('cube_to_process','Assign_cubes_to_process', error_message, alloc_stat, mpi_process%rank)
    end if
    allocate(ncells_cube_proc(this%ncubes,mpi_process%comm%size), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('cube_to_process','Assign_cubes_to_process', error_message, alloc_stat, mpi_process%rank)
    end if
    call timer%Inc_comp()    
    call mpi_allgather(cells_per_cube , this%ncubes, MPI_INTEGER, ncells_cube_proc, this%ncubes, MPI_INTEGER,mpi_process%comm%name, mpierr)
    call timer%Inc_comm()
    ncube_left = this%ncubes
    do while (ncube_left > 0)
       do iproc = 1, mpi_process%comm%size
          icube = maxloc(ncells_cube_proc(:,iproc),dim=1)
          cube_to_process(icube) = iproc-1
          this%elts(icube)%process_rank = iproc-1
          ncells_cube_proc(icube,:) = 0
          ncube_left = ncube_left - 1
       end do
    end do
    call timer%Inc_comp()    
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Assign_cubes_to_process ends on process', mpi_process%rank
#endif

  end subroutine Assign_cubes_to_process

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Associate_cells_to_cubes(this, cells_array, global_cells_per_cube)

    use cells_array_m, only : cells_array_t
    use constants_m, only : IDKIND

    class(cubes_array_t),intent(inout) :: this
    type(cells_array_t),intent(inout) :: cells_array
    integer, dimension(*), intent(in) :: global_cells_per_cube

    integer :: first_cell
    integer :: icell
    integer :: icube
    integer :: last_cell
    integer(kind=IDKIND) :: ncells
    integer :: nps, nthp

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Associate_cells_to_cubes begins on process', mpi_process%rank
#endif

    call timer%Set_ref()
    first_cell = 1
    ! allocate cells_array for each cube
    do icube = 1, this%ncubes
       if( this%elts(icube)%process_rank == mpi_process%rank ) then
          ncells = global_cells_per_cube(icube)
          nps = cells_array%elts(1)%n_passive_scalars
          nthp = cells_array%elts(1)%n_non_thermal_pressures
          call this%elts(icube)%cells_array%Allocate(nthp,nps,ncells)
          last_cell = first_cell + ncells - 1
          this%elts(icube)%cells_array%elts(1:ncells) = cells_array%elts(first_cell:last_cell)
          first_cell = last_cell + 1
       end if
    end do

    call cells_array%Deallocate()

    call timer%Inc_comp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Associate_cells_to_cubes ends on process', mpi_process%rank
#endif

  end subroutine Associate_cells_to_cubes

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Init_cubes_array(this, amr2cell_hdf5_parameters, ramses_info)

    class(cubes_array_t), intent(out) :: this
    type(amr2cell_hdf5_parameters_t), intent(in) :: amr2cell_hdf5_parameters
    type(ramses_info_t), intent(in) :: ramses_info

    integer(kind=4) :: alloc_stat
    character(len=ERR_MSG_LEN) :: error_message
    integer :: icube

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Init_cubes_array begins on process', mpi_process%rank
#endif

    call timer%Set_ref()
    this%dims = 2 ** (amr2cell_hdf5_parameters%cube_level-1)
    this%ncubes = this%dims(1) * this%dims(2) * this%dims(3) 
    allocate(this%elts(this%ncubes), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%elts','Init_cubes_array', error_message, alloc_stat, mpi_process%rank)
    end if

    do icube = 1, this%ncubes
       call this%elts(icube)%Init(amr2cell_hdf5_parameters, icube, ramses_info)
    end do

    call timer%Inc_comp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Init_cubes_array ends on process', mpi_process%rank
#endif 

  end subroutine Init_cubes_array

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Write_cubes_array(this,amr2cell_hdf5_parameters, ramses_hydro_descriptor)

    use amr2cell_hdf5_parameters_m, only : amr2cell_hdf5_parameters_t
    use ramses_hydro_descriptor_m, only : ramses_hydro_descriptor_t

    type(amr2cell_hdf5_parameters_t), intent(in) :: amr2cell_hdf5_parameters
    type(ramses_hydro_descriptor_t), intent(in) :: ramses_hydro_descriptor
    class(cubes_array_t), intent(in) :: this

    integer :: icube

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Write_cubes_array begins on process', mpi_process%rank
#endif

    call timer%Set_ref()

    do icube = 1, this%ncubes
       if ( this%elts(icube)%process_rank == mpi_process%rank ) then
          call this%elts(icube)%Write_hdf5(amr2cell_hdf5_parameters, ramses_hydro_descriptor)
       end if
    end do

    call timer%Inc_out()

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Write_cubes_array ends on process', mpi_process%rank
#endif

  end subroutine Write_cubes_array

end module cubes_array_m

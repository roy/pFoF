!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Contains program that convert binary hydro and amr output files from RAMSES to HDF5 'cube' files
!! @brief
!! 
!! @author Fabrice Roy

!> Contains program that convert binary hydro and amr output files from RAMSES to HDF5 'cube' files
!------------------------------------------------------------------------------------------------------------------------------------

program amr2cell_hdf5

  use amr2cell_hdf5_parameters_m, only : amr2cell_hdf5_parameters_t
  use error_handling_m, only : Allocate_error, ERR_MSG_LEN
  use cell_m, only : cell_t
  use cells_array_m, only : cells_array_t
  use constants_m, only : IDKIND
  use cubes_array_m, only : cubes_array_t
  use fortran_hdf5_manage_interface_m
  use iso_fortran_env, only : OUTPUT_UNIT
  use mpi
  use ramses_info_m, only : ramses_info_t
  use ramses_output_m, only : ramses_output_t
  use ramses_output_to_cells_m, only : ramses_output_to_cells
  use shared_variables_m

  implicit none

  type(amr2cell_hdf5_parameters_t) :: amr2cell_hdf5_parameters
  type(cells_array_t) :: cells_array
  integer, allocatable, dimension(:) :: cube_to_process
  type(cubes_array_t) :: cubes_array
  integer, allocatable, dimension(:) :: global_cells_per_cube
  integer, allocatable, dimension(:) :: local_cells_per_cube
  integer :: mpierr
  type(ramses_info_t) :: ramses_info
  type(ramses_output_t) :: ramses_output

  call mpi_init(mpierr)
  call hdf5_init()

  call timer%Init()

  call mpi_process%Init()

  call mpi_process%Print()

  call amr2cell_hdf5_parameters%Read()

  call ramses_info%Read(trim(amr2cell_hdf5_parameters%data_dir)//trim(amr2cell_hdf5_parameters%info_filename), mpi_process)

  call amr2cell_hdf5_parameters%Check(ramses_info)

  call ramses_output%Init(amr2cell_hdf5_parameters)

  call ramses_output%Read()

  call Ramses_output_to_cells(cells_array, ramses_output)

  call ramses_output%Deallocate()

  call cells_array%Clean(amr2cell_hdf5_parameters)

  call cubes_array%Init(amr2cell_hdf5_parameters, ramses_info)

  call cells_array%Count_cells(local_cells_per_cube, global_cells_per_cube, cubes_array%dims)

  call cubes_array%Assign(cube_to_process, local_cells_per_cube)

  call cells_array%Distribute(cube_to_process)

  call cubes_array%Associate(cells_array, global_cells_per_cube)

  call cubes_array%Write(amr2cell_hdf5_parameters, ramses_output%hydro_descriptor)

  call timer%Finalize()
  if ( mpi_process%rank == 0 ) then
     call timer%Print(OUTPUT_UNIT)
  end if

  call hdf5_finalize()
  call mpi_finalize(mpierr)

end program amr2cell_hdf5

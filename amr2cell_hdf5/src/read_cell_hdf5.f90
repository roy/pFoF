!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Program that reads HDF5 'amr_cube' files
!! @brief
!! 
!! @author Fabrice Roy

!> Program that reads HDF5 'amr_cube' files
!------------------------------------------------------------------------------------------------------------------------------------

program read_cell_hdf5
        
    use amr2cell_hdf5_parameters_m, only : amr2cell_hdf5_parameters_t
    use error_handling_m, only : Allocate_error, ERR_MSG_LEN
    use cell_m, only : cell_t
    use cells_array_m, only : cells_array_t
    use char_utils_m
    use constants_m, only : IDKIND
    use cube_m, only : cube_t
    use fortran_hdf5_manage_interface_m
    use fortran_hdf5_constants_m
    use iso_fortran_env, only : OUTPUT_UNIT
    use mpi
    use mpi_utils_m
    use ramses_info_m, only : ramses_info_t
    use ramses_output_m, only : ramses_output_t
    use ramses_output_to_cells_m, only : ramses_output_to_cells
    use shared_variables_m
  
    implicit none
  
    type(amr2cell_hdf5_parameters_t) :: amr2cell_hdf5_parameters
    type(cells_array_t) :: cells_array
    type(cube_t) :: cube
    character(H5_FILENAME_LEN) :: filename
    integer(4) :: first_file, last_file, nfile
    integer :: ifile
    integer :: mpierr
  
    call mpi_init(mpierr)
    call hdf5_init()
    call timer%Init()

    call mpi_process%Init()

    nfile = 8

    call Mpi_distribution(first_file, last_file, nfile, mpi_process)
    
    do ifile = first_file, last_file
        filename = 'cube_amr_hydro'//Int_to_char5(ifile)//'.h5'
        call cube%Read_hdf5(filename, amr2cell_hdf5_parameters)
    end do

    call timer%Finalize()
    call timer%Print(OUTPUT_UNIT)
  
    call hdf5_finalize()
    call mpi_finalize(mpierr)
  
end program read_cell_hdf5
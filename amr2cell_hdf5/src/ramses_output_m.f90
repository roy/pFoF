!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Class for complete RAMSES output (without particles) 
!! @brief
!! 
!! @author Fabrice Roy

!> Class for complete RAMSES output (without particles)
!----------------------------------------------------------------------------------------------------------------------------------
module ramses_output_m

  use error_handling_m, only : ERR_MSG_LEN, Allocate_error, Deallocate_error, IO_error
  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use ramses_amr_data_m, only : ramses_amr_data_t
  use ramses_amr_header_m, only : ramses_amr_header_t
  use ramses_hydro_data_m, only : ramses_hydro_data_t
  use ramses_hydro_descriptor_m, only : ramses_hydro_descriptor_t
  use ramses_hydro_header_m, only : ramses_hydro_header_t
  use ramses_info_m, only : ramses_info_t
  use shared_variables_m

  implicit none

  private
  public :: ramses_output_t

  type ramses_output_t
     integer :: nfile_total
     integer :: nfile_local
     integer :: first_file
     integer :: last_file
     character(len=:), allocatable :: data_dir
     character(len=:), allocatable :: amr_filename
     character(len=:), allocatable :: hydro_filename
     character(len=:), allocatable :: hydro_descriptor_filename
     character(len=:), allocatable :: info_filename
     type(ramses_info_t) :: ramses_info
     type(ramses_amr_header_t) :: amr_header
     type(ramses_amr_data_t), allocatable, dimension(:) :: amr_datas
     type(ramses_hydro_header_t) :: hydro_header
     type(ramses_hydro_data_t), allocatable, dimension(:) :: hydro_datas
     type(ramses_hydro_descriptor_t) :: hydro_descriptor
   contains
     procedure :: Deallocate=> Deallocate_ramses_output
     procedure :: Init => Init_ramses_output
     procedure :: Read => Read_ramses_output
  end type ramses_output_t

contains
  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Deallocate_ramses_output(this)

    class(ramses_output_t), intent(inout) :: this

    integer :: alloc_stat
    integer :: ifile
    character(len=ERR_MSG_LEN) :: error_message

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Deallocate_ramses_output begins on process', mpi_process%rank
#endif
    call timer%Set_ref()

    if(allocated(this%amr_datas)) then
       do ifile = this%first_file, this%last_file
          call this%amr_datas(ifile)%Deallocate()
       end do
       deallocate(this%amr_datas, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%amr_datas','Deallocate_ramses_output', error_message, alloc_stat, mpi_process%rank)
       end if
    end if

    if(allocated(this%hydro_datas)) then
       do ifile = this%first_file, this%last_file
          call this%hydro_datas(ifile)%Deallocate()
       end do
       deallocate(this%hydro_datas, stat=alloc_stat, errmsg=error_message)
       if(alloc_stat /= 0) then
          call Deallocate_error('this%hydro_datas','Deallocate_ramses_output', error_message, alloc_stat, mpi_process%rank)
       end if
    end if

    call timer%Inc_comp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Deallocate_ramses_output ends on process', mpi_process%rank
#endif

  end subroutine Deallocate_ramses_output


  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Init_ramses_output(this, amr2cell_hdf5_parameters)

    use amr2cell_hdf5_parameters_m

    class(ramses_output_t), intent(out) :: this
    type(amr2cell_hdf5_parameters_t), intent(in) :: amr2cell_hdf5_parameters

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Init_ramses_output begins on process', mpi_process%rank
#endif
    call timer%Set_ref()

    this%data_dir = amr2cell_hdf5_parameters%data_dir
    this%amr_filename = amr2cell_hdf5_parameters%amr_filename
    this%hydro_filename = amr2cell_hdf5_parameters%hydro_filename
    this%info_filename = amr2cell_hdf5_parameters%info_filename
    this%hydro_descriptor_filename = amr2cell_hdf5_parameters%hydro_descriptor_filename

    call timer%Inc_comp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Init_ramses_output ends on process', mpi_process%rank
#endif

  end subroutine Init_ramses_output


  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Read_ramses_output(this)

    use char_utils_m, only : Int_to_char5
    use mpi

    class(ramses_output_t), intent(inout) :: this

    integer :: ifile
    integer :: rest
    integer :: mpierr
    character(len=ERR_MSG_LEN) :: error_message
    integer :: alloc_stat
    character(len=:), allocatable :: filename

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_ramses_output begins on process', mpi_process%rank
#endif
    call timer%Set_ref()

    filename = trim(this%data_dir)//trim(this%info_filename)
    call this%ramses_info%read(filename, mpi_process)
    if(mpi_process%rank==0) call this%ramses_info%print(OUTPUT_UNIT, mpi_process)

    if(mpi_process%rank == 0) then
       filename=trim(this%data_dir)//trim(this%amr_filename)//Int_to_char5(1)
       call this%amr_header%Read(filename)
       this%nfile_total = this%amr_header%ncpu
    end if
    call timer%Inc_inp()
    call mpi_bcast(this%nfile_total, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, mpierr)
    call timer%Inc_comm()
    filename = trim(this%data_dir)//trim(this%hydro_descriptor_filename)
    call this%hydro_descriptor%read(filename)
    if(mpi_process%rank==0) call this%hydro_descriptor%print()

    this%nfile_local = this%nfile_total / mpi_process%comm%size
    rest = mod(this%nfile_total, mpi_process%comm%size)
    this%first_file = mpi_process%rank * this%nfile_local + 1 
    if(rest /= 0) then
       if(mpi_process%rank < rest) then
          this%nfile_local = this%nfile_local + 1
          this%first_file = this%first_file + mpi_process%rank
       else
          this%first_file = this%first_file + rest
       end if
    end if
    this%last_file = this%first_file + this%nfile_local - 1

    allocate(this%amr_datas(this%first_file:this%last_file), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%amr_datas','Read_ramses_output', error_message, alloc_stat, mpi_process%rank)
    end if

    allocate(this%hydro_datas(this%first_file:this%last_file), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%hydro_datas','Read_ramses_output', error_message, alloc_stat, mpi_process%rank)
    end if

    this%hydro_datas(:)%n_non_thermal_pressures = this%hydro_descriptor%n_non_thermal_pressures
    this%hydro_datas(:)%n_passive_scalars = this%hydro_descriptor%n_passive_scalars

    do ifile = this%first_file, this%last_file ! this%first_file+2
       filename = trim(this%data_dir)//trim(this%amr_filename)//Int_to_char5(ifile)
       call this%amr_datas(ifile)%Read(this%amr_header, filename)
       filename = trim(this%data_dir)//trim(this%hydro_filename)//Int_to_char5(ifile)
       call this%hydro_datas(ifile)%Read(this%hydro_header, filename, this%amr_datas(ifile)%ncells_total)
    end do

    call timer%Inc_inp()
#ifdef DEBUG
    if(mpi_process%rank==0) then
       call this%amr_datas(1)%Test()
    end if
    write(ERROR_UNIT,'(a,i0)') 'Read_ramses_output ends on process', mpi_process%rank
#endif

  end subroutine Read_ramses_output

end module ramses_output_m

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Ramses amr header class
!! @brief
!!
!! @author Fabrice Roy

!> Ramses amr header class
!------------------------------------------------------------------------------------------------------------------------------------

module ramses_amr_header_m

  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use shared_variables_m

  implicit none

  private
  public :: ramses_amr_header_t

  type ramses_amr_header_t
     integer(kind=4) :: ncpu
     integer(kind=4) :: ndim
     integer(kind=4) :: nx
     integer(kind=4) :: ny
     integer(kind=4) :: nz
     integer(kind=4) :: nlevel_max
     integer(kind=4) :: ngrid_max
     integer(kind=4) :: nboundary
     integer(kind=4) :: ngrid_current
     real(kind=4) :: boxlen
   contains
     procedure :: Read => Read_ramses_amr_header
  end type ramses_amr_header_t

contains
  !----------------------------------------------------------------------------------------------------------------------------------
  !> Read header from RAMSES amr file
  subroutine Read_ramses_amr_header(this, filename)

    use error_handling_m, only : IO_error, ERR_MSG_LEN

    class(ramses_amr_header_t), intent(out) :: this
    character(len=*), intent(in) :: filename

    integer :: ioerr
    integer :: unit_amr
    character(len=ERR_MSG_LEN) :: error_message

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Read_ramses_amr_header begins on process', mpi_process%rank
#endif
    call timer%Set_ref()
    open(newunit=unit_amr, file=filename, status='old', action='read', form='unformatted', iostat = ioerr, iomsg = error_message)
    if( ioerr > 0 ) then
       call IO_error('open file '//filename, 'Read_ramses_amr_header', error_message, ioerr, mpi_process%rank)
    end if

    read(unit_amr, iostat=ioerr, iomsg=error_message) this%ncpu
    if(ioerr/=0) call IO_error('read this%ncpu', 'Read_ramses_amr_header', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) this%ndim
    if(ioerr/=0) call IO_error('read this%ndim', 'Read_ramses_amr_header', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) this%nx, this%ny, this%nz
    if(ioerr/=0) call IO_error('read this%nx/ny/nz', 'Read_ramses_amr_header', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) this%nlevel_max
    if(ioerr/=0) call IO_error('read this%nlevel_max', 'Read_ramses_amr_header', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) this%ngrid_max
    if(ioerr/=0) call IO_error('read this%ngrid_max', 'Read_ramses_amr_header', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) this%nboundary
    if(ioerr/=0) call IO_error('read this%nboundary', 'Read_ramses_amr_header', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) this%ngrid_current
    if(ioerr/=0) call IO_error('read this%ngrid_current', 'Read_ramses_amr_header', error_message, ioerr, mpi_process%rank)
    read(unit_amr, iostat=ioerr, iomsg=error_message) this%boxlen
    if(ioerr/=0) call IO_error('read this%boxlen', 'Read_ramses_amr_header', error_message, ioerr, mpi_process%rank)

    close(unit_amr)

    call timer%Inc_inp()
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Read_ramses_amr_header ends on process', mpi_process%rank
#endif

  end subroutine Read_ramses_amr_header

end module ramses_amr_header_m

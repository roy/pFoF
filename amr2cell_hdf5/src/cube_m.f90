!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Class for a cube output (without particles, only amr + hydro)
!! @brief
!! 
!! @author Fabrice Roy

!> Class for a cube output (without particles, only amr + hydro)
!----------------------------------------------------------------------------------------------------------------------------------

module cube_m

  use amr2cell_hdf5_parameters_m, only : amr2cell_hdf5_parameters_t
  use cells_array_m, only : cells_array_t
  use constants_m, only : DP
  use error_handling_m, only : ERR_MSG_LEN, Allocate_error
  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use ramses_info_m, only : ramses_info_t
  use shared_variables_m

  implicit none

  private

  public :: cube_t

  type cube_t
     type(ramses_info_t) :: ramses_info
     integer(4) :: level ! same meaning as in Ramses for cells, i.e. level=1 is the whole simulation cube
     character(len=:), allocatable :: filename
     real(DP) :: size
     real(DP), allocatable, dimension(:) :: center_position
     integer(4) :: number
     integer(4) :: identity
     integer(4) :: process_rank
     type(cells_array_t) :: cells_array
   contains
     procedure :: Init => Init_cube
     procedure :: Print_info => Print_info_cube
     procedure :: Read_hdf5 => Read_hdf5_cube
     procedure :: Write_hdf5 => Write_hdf5_cube
  end type cube_t

contains

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Init_cube(this, amr2cell_hdf5_parameters, identity, ramses_info)

    use constants_m, only : IDKIND 
    use char_utils_m, only : Int_to_char5
    use index_m, only : Id_to_coords

    class(cube_t), intent(out) :: this
    type(amr2cell_hdf5_parameters_t), intent(in) :: amr2cell_hdf5_parameters
    integer(kind=4), intent(in) :: identity
    type(ramses_info_t), intent(in) :: ramses_info

    integer :: idim
    character(len=ERR_MSG_LEN) :: error_message
    integer :: alloc_stat

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Init_cube begins on process', mpi_process%rank
#endif

    call timer%Set_ref()
    this%filename = trim(amr2cell_hdf5_parameters%cube_filename)//Int_to_char5(identity)//".h5"
    this%level = amr2cell_hdf5_parameters%cube_level
    this%size = 1.0_DP / (2 ** (this%level-1))
    this%number = (2 ** (this%level-1)) ** 3
    this%identity = identity
    this%process_rank = -1
    allocate(this%center_position(ramses_info%ndim), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%center_position','Init_cube', error_message, alloc_stat, mpi_process%rank)
    end if
    this%center_position(:) = Id_to_coords((/2**(this%level-1),2**(this%level-1),2**(this%level-1)/),identity)
    this%center_position = (this%center_position +1._DP/2._DP) * this%size 
    this%ramses_info = ramses_info
    ! this%cells_array is not initialized yet

    call timer%Inc_comp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Init_cube ends on process', mpi_process%rank
#endif

  end subroutine Init_cube


  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Print_info_cube(this, unit_number)

    class(cube_t), intent(in) :: this
    integer, intent(in) :: unit_number

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Print_info_cube begins on process', mpi_process%rank
#endif

    call timer%Set_ref()

    write(unit_number,'(a)') '---------------------------------------------'
    write(unit_number,'(a,a)') 'Cube information:'
    write(unit_number,'(a,i0,a,i0)') 'cube n.',this%identity, ' / ', this%number
    write(unit_number,'(a,f11.8,a,f11.8,a,f11.8,a)') 'position: (',this%center_position(1),',',this%center_position(2),',',this%center_position(3),')'

    call timer%Inc_out()

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Print_info_cube ends on process', mpi_process%rank
#endif

  end subroutine Print_info_cube

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Read_hdf5_cube(this, filename, amr2cell_hdf5_parameters)

    use amr2cell_hdf5_parameters_m, only : amr2cell_hdf5_parameters_t
    use fortran_hdf5_constants_m
    use fortran_hdf5_manage_files_m
    use fortran_hdf5_manage_groups_m
    use fortran_hdf5_read_attribute_m
    use hdf5
    use ramses_hydro_descriptor_m, only : ramses_hydro_descriptor_t
    
    class(cube_t), intent(out) :: this
    character(len=H5_FILENAME_LEN), intent(in) :: filename
    type(amr2cell_hdf5_parameters_t), intent(out) :: amr2cell_hdf5_parameters

    integer :: alloc_stat
    integer(kind=hid_t) :: cells_group_id
    integer(kind=hid_t) :: cube_file_id
    integer(kind=hid_t) :: cube_info_id    
    character(len=ERR_MSG_LEN) :: error_message
    character(len=H5_STR_LEN) :: groupname
    integer :: icell
    integer(HID_T) :: metadata_id
    character(len=H5_STR_LEN) :: metadata_name
    type(ramses_hydro_descriptor_t) :: ramses_hydro_descriptor
    character(len=H5_FILENAME_LEN) :: tmpchar    

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_hdf5_cube begins on process', mpi_process%rank
#endif
    call timer%Set_ref()
    
    call Hdf5_open_file(filename, cube_file_id)

    groupname = 'metadata'
    call Hdf5_open_group(cube_file_id, groupname, metadata_id)
    call this%ramses_info%Read_hdf5(metadata_id,mpi_process)
    call amr2cell_hdf5_parameters%Read_hdf5(metadata_id,mpi_process)
    call ramses_hydro_descriptor%Read_hdf5(metadata_id)

    allocate(this%center_position(this%ramses_info%ndim), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
       call Allocate_error('this%center_position','Init_cube', error_message, alloc_stat, mpi_process%rank)
    end if

    ! write metadata
    groupname = 'cube_info'
    call Hdf5_open_group(metadata_id, groupname, cube_info_id)

    metadata_name = 'level'
    call Hdf5_read_attr(cube_info_id, metadata_name, this%level)
    metadata_name = 'filename'
    call Hdf5_read_attr(cube_info_id, metadata_name, H5_FILENAME_LEN, tmpchar)
    this%filename = tmpchar
    metadata_name = 'size'
    call Hdf5_read_attr(cube_info_id, metadata_name, this%size)
    metadata_name = 'center_position'
    call Hdf5_read_attr(cube_info_id, metadata_name, 3, this%center_position)
    metadata_name = 'number'
    call Hdf5_read_attr(cube_info_id, metadata_name, this%number)
    metadata_name = 'identity'
    call Hdf5_read_attr(cube_info_id, metadata_name, this%identity)
    metadata_name = 'process_rank'
    call Hdf5_read_attr(cube_info_id, metadata_name, this%process_rank)

    call Hdf5_close_group(cube_info_id)
    call Hdf5_close_group(metadata_id)

    groupname = 'cells'
    call Hdf5_open_group(cube_file_id, groupname, cells_group_id)
    call this%cells_array%Read_hdf5(cells_group_id, ramses_hydro_descriptor)
    call Hdf5_close_group(cells_group_id)

    call Hdf5_close_file(cube_file_id)    

    call timer%Inc_inp()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Read_hdf5_cube ends on process', mpi_process%rank
#endif
  
  end subroutine Read_hdf5_cube

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Write_hdf5_cube(this, amr2cell_hdf5_parameters, ramses_hydro_descriptor)

    use amr2cell_hdf5_parameters_m, only : amr2cell_hdf5_parameters_t
    use fortran_hdf5_constants_m
    use fortran_hdf5_manage_files_m
    use fortran_hdf5_manage_groups_m
    use fortran_hdf5_write_attribute_m
    use hdf5
    use ramses_hydro_descriptor_m, only : ramses_hydro_descriptor_t

    type(amr2cell_hdf5_parameters_t), intent(in) :: amr2cell_hdf5_parameters
    type(ramses_hydro_descriptor_t), intent(in) :: ramses_hydro_descriptor
    class(cube_t), intent(in) :: this

    integer(kind=hid_t) :: cells_group_id
    integer(kind=hid_t) :: cube_file_id
    integer(kind=hid_t) :: cube_info_id
    character(len=H5_FILENAME_LEN) :: filename
    character(len=H5_STR_LEN) :: groupname
    integer :: icell
    integer(HID_T) :: metadata_id
    character(len=H5_STR_LEN) :: metadata_name

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Write_hdf5_cube begins on process', mpi_process%rank
#endif
    call timer%Set_ref()

    filename = this%filename
    call Hdf5_create_file(filename, cube_file_id)

    groupname = 'metadata'
    call Hdf5_open_group(cube_file_id, groupname, metadata_id)
    call this%ramses_info%Write_hdf5(metadata_id,mpi_process)
    call amr2cell_hdf5_parameters%Write_hdf5(metadata_id,mpi_process)
    call ramses_hydro_descriptor%Write_hdf5(metadata_id)

    ! write metadata
    groupname = 'cube_info'
    call Hdf5_create_group(metadata_id, groupname, cube_info_id)

    metadata_name = 'level'
    call Hdf5_write_attr(cube_info_id, metadata_name, this%level)
    metadata_name = 'filename'
    call Hdf5_write_attr(cube_info_id, metadata_name, this%filename)
    metadata_name = 'size'
    call Hdf5_write_attr(cube_info_id, metadata_name, this%size)
    metadata_name = 'center_position'
    call Hdf5_write_attr(cube_info_id, metadata_name, 3, this%center_position)
    metadata_name = 'number'
    call Hdf5_write_attr(cube_info_id, metadata_name, this%number)
    metadata_name = 'identity'
    call Hdf5_write_attr(cube_info_id, metadata_name, this%identity)
    metadata_name = 'process_rank'
    call Hdf5_write_attr(cube_info_id, metadata_name, this%process_rank)

    call Hdf5_close_group(cube_info_id)
    call Hdf5_close_group(metadata_id)

    groupname = 'cells'
    call Hdf5_create_group(cube_file_id, groupname, cells_group_id)
    call this%cells_array%Write_hdf5(cells_group_id)
    call Hdf5_close_group(cells_group_id)

    call Hdf5_close_file(cube_file_id)

    call timer%Inc_out()
#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'Write_hdf5_cube ends on process', mpi_process%rank
#endif

  end subroutine Write_hdf5_cube


end module cube_m

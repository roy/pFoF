FC=h5pfc
FHIDIR = /opt/fhi/1.0/hdf5/1.14.3/openmpi/4.1.6/gcc/13.2.0

FHIMOD = $(FHIDIR)/include
FHILIB = $(FHIDIR)/lib

# Optional precompiler options used in the code:
# -DDEBUGHDF5: print some debug info in HDF5 I/O routines
# -DLONGING: uses integer(kind=8) for ID
# -DOPTI: uses new algorithm to distribute halos and particles amongst processes (under development and buggy)

# GNU release flags
FCFLAGS= -O3 -g -I$(FHIMOD) -cpp -DNPRE=8 -ffree-line-length-none #-fconvert=big-endian # -DLONGINT -DOPTI

# GNU debug flags
DEBUGFLAGS= -O0 -g -I$(FHIMOD) -Wall -Wextra -finit-real=zero -finit-integer=0 -std=f2008 -fcheck=all -fbacktrace -fmax-errors=10 -cpp -DDEBUG -ffpe-trap=invalid,zero -DNPRE=8 #-DDEBUGHDF5 #-DLONGINT #-DOPTI -fconvert=big-endian 

LDFLAGS=-g -L$(FHILIB) -lfortran-hdf5-interface-mpi 

MKDIR_P=mkdir -p


!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Contains the initialization and the communication functions that merge the halo parts of haloes that extend across several processes
!! @brief
!! Contains the initialization and the communication functions that merge the halo parts of haloes that extend across several processes
!! @author Fabrice Roy
!! @author Vincent Bouillot
! ======================================================================
module modfofmpi

  use iso_fortran_env, only : OUTPUT_UNIT
  use mpi
  use modconstant, only : IDKIND
  use modmpicommons, only : emergencystop

  implicit none

  real   (kind=4),   dimension(:,:), allocatable :: posAva, posArr, posBas, posDro, posGau, posHau
  integer(kind=IDKIND), dimension(:),   allocatable :: strAva, strArr, strBas, strDro, strGau, strHau, strRec
  integer(kind=4)                                :: nbridgearr, nbridgeava, nbridgegau
  integer(kind=4)                                :: nbridgedro, nbridgebas, nbridgehau
  integer(kind=IDKIND), dimension(:,:), allocatable :: bridgeArr, bridgeAva, bridgeGau
  integer(kind=IDKIND), dimension(:,:), allocatable :: bridgeDro, bridgeBas, bridgeHau
  integer(kind=4),   dimension(6)                :: nflagrecv
  integer(kind=4),   dimension(6)                :: nflagloc
  integer(kind=1),   dimension(:),   allocatable :: border

  private

  public :: border, &
       nflagloc, &
       mergehaloes


contains

  ! ======================================================================
  !> This routine merges haloes that extends across several process by setting
  !> their halo ID to the same value.
  subroutine mergehaloes(perco2, info_proc)

    use modmpicommons, only :  type_info_process

    real(kind=4), intent(in) :: perco2  !< Value of the percolation length to the square in normalized units (where the box length in equal to 1.0)
    type(Type_info_process), intent(in) :: info_proc

    integer(kind=4) :: f, fi, fp
    integer(kind=4) :: fiID, fpID
    integer(kind=4) :: mpistatf(MPI_STATUS_SIZE,6)
    integer(kind=4), dimension(6) :: mpireqsf, mpireqrf
    integer(kind=4) :: mpierr

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Enter mergehaloes on process ', info_proc%global_comm%pid
#endif

    call initmerging(info_proc)

    ! Processes exchange positions of the particles close to the faces
    do f=1,3 ! loop over the faces, we deal with 2 faces at a time
       fi = 2*f-1
       fp = 2*f

       fiID = info_proc%global_comm%neighbours(fi)
       fpID = info_proc%global_comm%neighbours(fp)

       call Mpi_Irecv(nflagrecv(fi), 1, Mpi_Integer, fiID, 1, &
            info_proc%global_comm%name, mpireqrf(fi), mpierr)
       call Mpi_Irecv(nflagrecv(fp), 1, Mpi_Integer, fpID, 2, &
            info_proc%global_comm%name, mpireqrf(fp), mpierr)
       call Mpi_Isend(nflagloc(fp) , 1, Mpi_Integer, fpID, 1, &
            info_proc%global_comm%name, mpireqsf(fp), mpierr)
       call Mpi_Isend(nflagloc(fi) , 1, Mpi_Integer, fiID, 2, &
            info_proc%global_comm%name, mpireqsf(fi), mpierr)

    end do

    call Mpi_Waitall(6,mpireqrf,mpistatf,mpierr)
    call Mpi_Waitall(6,mpireqsf,mpistatf,mpierr)

    ! find the pairs of particles separated by a face that are distant by less than the percolation length from each other
    call findbridge(nflagloc, nflagrecv, posArr, posAva, nbridgearr, bridgeArr, nbridgeava,&
         bridgeAva, perco2, 1, 2, info_proc)
    if(allocated(posAva)) deallocate(posAva)
    if(allocated(posArr)) deallocate(posArr)

    call findbridge(nflagloc, nflagrecv, posGau, posDro, nbridgegau, bridgeGau, nbridgedro, &
         bridgeDro, perco2, 3, 4, info_proc)
    if(allocated(posGau)) deallocate(posGau)
    if(allocated(posDro)) deallocate(posDro)

    call findbridge(nflagloc, nflagrecv, posBas, posHau, nbridgebas, bridgeBas, nbridgehau, &
         bridgeHau, perco2, 5, 6, info_proc)
    if(allocated(posBas)) deallocate(posBas)
    if(allocated(posHau)) deallocate(posHau)


    call setcommonhaloid(info_proc)

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Exit mergehaloes on process ', info_proc%global_comm%pid
#endif

  end subroutine mergehaloes


  ! ======================================================================
  !> This routine initializes the merging of halo parts. If the flag
  !! value is not 0, the position and halo ID of the particle are saved
  !! in specific arrays (one array per face).
  subroutine initmerging(info_proc)

    use modvarcommons, only : local_npart, position, structure_id
    use modmpicommons, only : emergencystop, type_info_process

    type(Type_info_process), intent(in) :: info_proc

    integer :: allocstat
    integer(kind=4) :: debh, debb, deba, debr, debg, debd
    integer(kind=4) :: i

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Enter initmerging on process ', info_proc%global_comm%pid
#endif

    debh = 1
    debb = 1
    deba = 1
    debr = 1
    debg = 1
    debd = 1

    allocate (posArr(3,nflagloc(1)), strArr(nflagloc(1)),STAT=allocstat)
    if(allocstat > 0) call emergencyStop('Allocate failed for posarr in initmerging',allocstat)
    allocate (posAva(3,nflagloc(2)), strAva(nflagloc(2)),STAT=allocstat)
    if(allocstat > 0) call emergencyStop('Allocate failed for posava in initmerging',allocstat)
    allocate (posGau(3,nflagloc(3)), strGau(nflagloc(3)),STAT=allocstat)
    if(allocstat > 0) call emergencyStop('Allocate failed for posgau in initmerging',allocstat)
    allocate (posDro(3,nflagloc(4)), strDro(nflagloc(4)),STAT=allocstat)
    if(allocstat > 0) call emergencyStop('Allocate failed for posdro in initmerging',allocstat)
    allocate (posBas(3,nflagloc(5)), strBas(nflagloc(5)),STAT=allocstat)
    if(allocstat > 0) call emergencyStop('Allocate failed for posbas in initmerging',allocstat)
    allocate (posHau(3,nflagloc(6)), strHau(nflagloc(6)),STAT=allocstat)
    if(allocstat > 0) call emergencyStop('Allocate failed for poshau in initmerging',allocstat)

    ! loop over the particles
    ! if the particle is located near a face, its position and its halo ID are saved in specific arrays
    bufferface : do i = 1, local_npart
       bord : if(border(i) /= 0) then
          haut : if(border(i) >= 32_1) then
             posHau (:,debh) = position(:,i)
             strHau (debh) = structure_id(i)
             debh = debh+1
             border(i) = border(i) - 32_1
          end if haut
          bas : if(border(i) >= 16_1) then
             posBas (:,debb) = position(:,i)
             strBas (debb) = structure_id(i)
             debb = debb+1
             border(i) = border(i) - 16_1
          end if bas
          avant : if(border(i) >= 8_1) then
             posAva (:,deba) = position(:,i)
             strAva (deba) = structure_id(i)
             deba = deba+1
             border(i) = border(i) - 8_1
          end if avant
          arriere : if(border(i) >=4_1) then
             posArr (:,debr) = position(:,i)
             strArr (debr) = structure_id(i)
             debr = debr+1
             border(i) = border(i) - 4_1
          end if arriere
          droite : if(border(i) >=2_1) then
             posDro (:,debd) = position(:,i)
             strDro (debd) = structure_id(i)
             debd = debd+1
             border(i) = border(i) - 2_1
          end if droite
          gauche : if(border(i) >= 1_1) then
             posGau (:,debg) = position(:,i)
             strGau (debg) = structure_id(i)
             debg = debg+1
          end if gauche
       end if bord

    end do bufferface

    deallocate(border)

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Exit initmerging on process ', info_proc%global_comm%pid
#endif

  end subroutine initmerging


  ! ======================================================================
  !> Finds the pairs of particles that should be linked through the merging procedure. <br>
  !! two directions are considered simultaneously by the local process: <br>
  !! 1= back  2= front <br>
  !! 1= left  2= right <br>
  !! 1= bottom  2= top <br>
  !! meaning that the local process exchanges information with the processes located in these two directions.
  subroutine findbridge(nloc, nrecv, pos1, pos2, nbridge1, bridge1, nbridge2, bridge2, r2, v1, v2, info_proc)

    use modmpicommons, only : emergencystop, type_info_process

    ! input
    integer(kind=4), intent(in), dimension(6) :: nloc   !< local number of particles flagged for link detection in direction 1
    integer(kind=4), intent(in), dimension(6) :: nrecv  !< local number of particles flagged for link detection in direction 2
    integer(kind=4), intent(in) :: v1                   !< neighbour index in direction 1
    integer(kind=4), intent(in) :: v2                   !< neighbour index in direction 2
    real(kind=4), intent(in), dimension(3,*) :: pos1    !< positions of local particles flagged for link detection in direction 1
    real(kind=4), intent(in), dimension(3,*) :: pos2    !< positions of distant particles flagged for link detection in direction 2
    real(kind=4), intent(in) :: r2                      !< percolation length
    type(Type_info_process), intent(in) :: info_proc

    ! output
    integer(kind=4), intent(inout) :: nbridge1                                !< number of "bridges" found, e.g. pairs of particles that should be linked, in direction 1
    integer(kind=4), intent(inout) :: nbridge2                                !< number of "bridges" found, e.g. pairs of particles that should be linked, in direction 2
    integer(kind=IDKIND), dimension(:,:), allocatable, intent(inout) :: bridge1  !< array of indices of the particles forming a bridge in direction 1
    integer(kind=IDKIND), dimension(:,:), allocatable, intent(inout) :: bridge2  !< array of indices of the particles forming a bridge in direction 2

    ! local
    integer :: allocstat
    integer(kind=4) :: mpistat(MPI_STATUS_SIZE)   ! MPI comm. status
    integer(kind=4) :: li, ri, ind
    real(kind=4) :: d, dx, dy, dz
    real(kind=4), dimension(:,:), allocatable :: posRec
    integer(kind=4) :: mpierr
    integer(kind=4) :: mpireqr, mpireqs
    logical(kind=4) :: periodic

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Enter findbridge on process ', info_proc%global_comm%pid
#endif

    periodic = info_proc%global_comm%periods(v2/2)

    ! send flagged positions to direction 2 and receive from direction 1
    allocate (posRec(3,nrecv(v1)), STAT=allocstat)
    if(allocstat > 0) call emergencyStop('Allocate failed for posrec (1) in findbridge',allocstat)
    call Mpi_Irecv(posRec,3*nrecv(v1),Mpi_Real, info_proc%global_comm%neighbours(v1),&
         1,info_proc%global_comm%name,mpireqr,mpierr)
    call Mpi_ISend(pos2,3*nloc(v2), Mpi_Real,info_proc%global_comm%neighbours(v2), &
         1,info_proc%global_comm%name,mpireqs,mpierr)

    call Mpi_Wait(mpireqs,mpistat,mpierr)
    call Mpi_Wait(mpireqr,mpistat,mpierr)

    ! loop over local flagged positions for direction 1 and received positions from direction 1
    nbridge1 = 0
    do li = 1, nloc(v1)
       do ri = 1, nrecv(v1)
          dx = abs(pos1(1,li)-posRec(1,ri))
          dy = abs(pos1(2,li)-posRec(2,ri))
          dz = abs(pos1(3,li)-posRec(3,ri))
          if(periodic) then
             dx = min(dx,1.0-dx)
             dy = min(dy,1.0-dy)
             dz = min(dz,1.0-dz)
          end if
          d  = dx*dx+dy*dy+dz*dz
          ! count the number of bridges
          if(d <= r2 ) then
             nbridge1 = nbridge1 + 1
          end if
       end do
    end do

    allocate(bridge1(2,nbridge1), STAT=allocstat)
    if(allocstat > 0) call emergencyStop('Allocate failed for bridge1 in findbridge',allocstat)

    ! we have to loop again to save the local ID of the particles that constitute the bridges
    ind = 1
    do li = 1, nloc(v1)
       do ri = 1, nrecv(v1)
          dx = abs(pos1(1,li)-posRec(1,ri))
          dy = abs(pos1(2,li)-posRec(2,ri))
          dz = abs(pos1(3,li)-posRec(3,ri))
          if(periodic) then
             dx = min(dx,1.0-dx)
             dy = min(dy,1.0-dy)
             dz = min(dz,1.0-dz)
          end if
          d  = dx*dx+dy*dy+dz*dz
          if(d <= r2 ) then
             bridge1(1,ind) = li
             bridge1(2,ind) = ri
             ind = ind + 1
          end if
       end do
    end do

    deallocate(posRec)


    ! send position to direction 1 and receive from direction 2
    allocate (posRec(3,nrecv(v2)), STAT=allocstat)
    if(allocstat > 0) call emergencyStop('Allocate failed for posrec (2) in findbridge',allocstat)
    call Mpi_Irecv(posRec,3*nrecv(v2),Mpi_Real,info_proc%global_comm%neighbours(v2),&
         2,info_proc%global_comm%name,mpireqr,mpierr)
    call Mpi_ISend(pos1,3*nloc(v1), Mpi_Real,info_proc%global_comm%neighbours(v1),&
         2,info_proc%global_comm%name,mpireqs,mpierr)

    call Mpi_Wait(mpireqs,mpistat,mpierr)
    call Mpi_Wait(mpireqr,mpistat,mpierr)

    ! same loops with positions corresponding with the second direction
    nbridge2 = 0
    do li = 1, nloc(v2)
       do ri = 1, nrecv(v2)
          dx = abs(pos2(1,li)-posRec(1,ri))
          dy = abs(pos2(2,li)-posRec(2,ri))
          dz = abs(pos2(3,li)-posRec(3,ri))
          if(periodic) then
             dx = min(dx,1.0-dx)
             dy = min(dy,1.0-dy)
             dz = min(dz,1.0-dz)
          end if
          d  = dx*dx+dy*dy+dz*dz
          if(d <= r2 ) then
             nbridge2 = nbridge2+1
          end if
       end do
    end do

    allocate(bridge2(2,nbridge2), STAT=allocstat)
    if(allocstat > 0) call emergencyStop('Allocate failed for bridge2 in findbridge',allocstat)

    ind = 1
    do li = 1, nloc(v2)
       do ri = 1, nrecv(v2)
          dx = abs(pos2(1,li)-posRec(1,ri))
          dy = abs(pos2(2,li)-posRec(2,ri))
          dz = abs(pos2(3,li)-posRec(3,ri))
          if(periodic) then
             dx = min(dx,1.0-dx)
             dy = min(dy,1.0-dy)
             dz = min(dz,1.0-dz)
          end if
          d  = dx*dx+dy*dy+dz*dz
          if(d <= r2 ) then
             bridge2(1,ind) = li
             bridge2(2,ind) = ri
             ind = ind + 1
          end if
       end do
    end do

    deallocate(posRec)

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Exit findbridge on process ', info_proc%global_comm%pid
#endif

  end subroutine findbridge


  ! ======================================================================
  !> Exchanges the haloID of the particles located near the faces and edges and set the same haloID
  !! to each particles that are in the same halo.
  subroutine setcommonhaloid(info_proc)

    use modconstant, only : IDKIND, MPI_IDKIND
    use modmpicommons, only : type_info_process
    use modvarcommons, only : local_npart, structure_id

    type(Type_info_process), intent(in) :: info_proc

    integer :: allocstat
    integer(kind=4) :: nbpassage
    logical :: nopermut, finished
    integer(kind=4) :: sendID, recvID
    integer(kind=4) :: ib, k
    integer(kind=IDKIND) :: il, ir
    integer(kind=4) :: mpistat(MPI_STATUS_SIZE)   ! MPI comm. status
    integer(kind=4) :: mpierr
    integer(kind=4) :: mpireqr, mpireqs

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Enter setcommonhaloid on process ', info_proc%global_comm%pid
#endif

    nbpassage = 0

    if(info_proc%global_comm%pid==0) write(OUTPUT_UNIT,*) 'Beginning of the merging phase...'

    raccordement : do
       nopermut = .true.
       nbpassage = nbpassage + 1
       if(info_proc%global_comm%pid == 0) write(OUTPUT_UNIT,*) 'Loop number ',nbpassage

       !------------------------------------------------------!
       ! Envoi du bas vers le bas et reception venant du haut !
       !------------------------------------------------------!
#ifdef DEBUG
       write(OUTPUT_UNIT,*) 'setcommonhaloid: send to bottom, receive from top'
#endif
       sendID = info_proc%global_comm%neighbours(5)
       recvID = info_proc%global_comm%neighbours(6)

       allocate (strRec(nflagrecv(6)), STAT=allocstat)
       if(allocstat > 0) call emergencyStop('Allocate failed for strrec (6) in setcommonhaloid',allocstat)

       call Mpi_ISend(strBas,nflagloc(5), MPI_IDKIND,sendID,1,info_proc%global_comm%name,mpireqs,mpierr)
       call Mpi_IRecv(strRec,nflagrecv(6),MPI_IDKIND,recvID,1,info_proc%global_comm%name,mpireqr,mpierr)
       call Mpi_Wait(mpireqs,mpistat,mpierr)
       call Mpi_Wait(mpireqr,mpistat,mpierr)

       do ib = 1, nbridgehau
          il = bridgeHau(1,ib)
          ir = bridgeHau(2,ib)
          raccordh : if(strRec(ir)< strHau(il) ) then

             nopermut = .false.
             renumallh : do k=1,local_npart
                if(structure_id(k) == strHau(il)) structure_id(k) = strRec(ir)
             end do renumallh
             do k=1, nflagloc(4)
                if(strDro(k) == strHau(il)) strDro(k) = strRec(ir)
             end do
             do k=1, nflagloc(3)
                if(strGau(k) == strHau(il)) strGau(k) = strRec(ir)
             end do
             do k=1, nflagloc(2)
                if(strAva(k) == strHau(il)) strAva(k) = strRec(ir)
             end do
             do k=1, nflagloc(1)
                if(strArr(k) == strHau(il)) strArr(k) = strRec(ir)
             end do
             do k=1, nflagloc(6)
                if(strHau(k) == strHau(il) .and. k/= il) strHau(k) = strRec(ir)
             end do
             strHau(il) = strRec(ir)
          end if raccordh
       end do

       deallocate(strRec)


       !-------------------------------------------------------------------
       ! Envoi de ma frontiere haut vers le haut et reception venant du bas
       !-------------------------------------------------------------------

#ifdef DEBUG
       write(OUTPUT_UNIT,*) 'setcommonhaloid: send to top, receive from bottom'
#endif
       sendID = info_proc%global_comm%neighbours(6)
       recvID = info_proc%global_comm%neighbours(5)
       allocate (strRec(nflagrecv(5)), STAT=allocstat)
       if(allocstat > 0) call emergencyStop('Allocate failed for strrec (5) in setcommonhaloid',allocstat)

       call Mpi_ISend(strHau,nflagloc(6), MPI_IDKIND,sendID,2,info_proc%global_comm%name,mpireqs,mpierr)
       call Mpi_IRecv(strRec,nflagrecv(5),MPI_IDKIND,recvID,2,info_proc%global_comm%name,mpireqr,mpierr)
       call Mpi_Wait(mpireqs,mpistat,mpierr)
       call Mpi_Wait(mpireqr,mpistat,mpierr)

       do ib = 1, nbridgebas
          il = bridgeBas(1,ib)
          ir = bridgeBas(2,ib)

          raccordb : if(strRec(ir)< strBas(il) ) then

             nopermut = .false.
             renumallb : do k = 1,local_npart
                if(structure_id(k) == strBas(il)) structure_id(k) = strRec(ir)
             end do renumallb
             do k=1, nflagloc(4)
                if(strDro(k) == strBas(il)) strDro(k) = strRec(ir)
             end do
             do k=1, nflagloc(3)
                if(strGau(k) == strBas(il)) strGau(k) = strRec(ir)
             end do
             do k=1, nflagloc(2)
                if(strAva(k) == strBas(il)) strAva(k) = strRec(ir)
             end do
             do k=1, nflagloc(1)
                if(strArr(k) == strBas(il)) strArr(k) = strRec(ir)
             end do
             do k=1, nflagloc(5)
                if(strBas(k) == strBas(il) .and. k/= il) strBas(k) = strRec(ir)
             end do
             strBas(il) = strRec(ir)
          end if raccordb
       end do

       deallocate(strRec)


       !--------------------------------------------------------------------------
       ! Envoi de ma frontiere droite vers la droite et reception venant de gauche
       !--------------------------------------------------------------------------

#ifdef DEBUG
       write(OUTPUT_UNIT,*) 'setcommonhaloid: send to right, receive from left'
#endif
       sendID = info_proc%global_comm%neighbours(4)
       recvID = info_proc%global_comm%neighbours(3)

       allocate (strRec(nflagrecv(3)), STAT=allocstat)
       if(allocstat > 0) call emergencyStop('Allocate failed for strrec (3) in setcommonhaloid',allocstat)

       call Mpi_ISend(strDro,nflagloc(4), MPI_IDKIND,sendID,3,info_proc%global_comm%name,mpireqs,mpierr)
       call Mpi_IRecv(strRec,nflagrecv(3),MPI_IDKIND,recvID,3,info_proc%global_comm%name,mpireqr,mpierr)
       call Mpi_Wait(mpireqs,mpistat,mpierr)
       call Mpi_Wait(mpireqr,mpistat,mpierr)

       ! comparaison des part. proche de ma face 'gauche' avec les part. venant de mon neighbours de gauche
       do ib = 1, nbridgegau
          il = bridgeGau(1,ib)
          ir = bridgeGau(2,ib)

          raccordg : if(strRec(ir)< strGau(il) ) then

             nopermut = .false.
             renumallg : do k = 1,local_npart
                if(structure_id(k) == strGau(il)) structure_id(k) = strRec(ir)
             end do renumallg
             do k=1, nflagloc(5)
                if(strBas(k) == strGau(il)) strBas(k) = strRec(ir)
             end do
             do k=1, nflagloc(3)
                if(strGau(k) == strGau(il) .and. k/= il) strGau(k) = strRec(ir)
             end do
             do k=1, nflagloc(2)
                if(strAva(k) == strGau(il)) strAva(k) = strRec(ir)
             end do
             do k=1, nflagloc(1)
                if(strArr(k) == strGau(il)) strArr(k) = strRec(ir)
             end do
             do k=1, nflagloc(6)
                if(strHau(k) == strGau(il)) strHau(k) = strRec(ir)
             end do
             strGau(il) = strRec(ir)
          end if raccordg
       end do

       deallocate(strRec)


       !---------------------------------------------------------------------
       ! Envoi de ma face gauche vers la gauche et reception venant de droite
       !---------------------------------------------------------------------
#ifdef DEBUG
       write(OUTPUT_UNIT,*) 'setcommonhaloid: send to left, receive from right'
#endif

       sendID = info_proc%global_comm%neighbours(3)
       recvID = info_proc%global_comm%neighbours(4)

       allocate (strRec(nflagrecv(4)), STAT=allocstat)
       if(allocstat > 0) call emergencyStop('Allocate failed for strrec (4) in setcommonhaloid',allocstat)

       call Mpi_ISend(strGau,nflagloc(3), MPI_IDKIND,sendID,4,info_proc%global_comm%name,mpireqs,mpierr)
       call Mpi_IRecv(strRec,nflagrecv(4),MPI_IDKIND,recvID,4,info_proc%global_comm%name,mpireqr,mpierr)
       call Mpi_Wait(mpireqs,mpistat,mpierr)
       call Mpi_Wait(mpireqr,mpistat,mpierr)

       ! comparaison des part. proche de ma face 'droite' avec les part. venant de mon neighbours de droite
       do ib = 1, nbridgedro
          il = bridgeDro(1,ib)
          ir = bridgeDro(2,ib)

          raccordd : if(strRec(ir)< strDro(il) ) then

             nopermut = .false.
             renumalld : do k = 1,local_npart
                if(structure_id(k) == strDro(il)) structure_id(k) = strRec(ir)
             end do renumalld
             do k=1, nflagloc(4)
                if(strDro(k) == strDro(il) .and. k/= il) strDro(k) = strRec(ir)
             end do
             do k=1, nflagloc(5)
                if(strBas(k) == strDro(il)) strBas(k) = strRec(ir)
             end do
             do k=1, nflagloc(2)
                if(strAva(k) == strDro(il)) strAva(k) = strRec(ir)
             end do
             do k=1, nflagloc(1)
                if(strArr(k) == strDro(il)) strArr(k) = strRec(ir)
             end do
             do k=1, nflagloc(6)
                if(strHau(k) == strDro(il)) strHau(k) = strRec(ir)
             end do
             strDro(il) = strRec(ir)
          end if raccordd
       end do

       deallocate(strRec)


       !--------------------------------------------------------------------------
       ! Envoi de ma frontiere avant vers l'avant et reception venant de l'arriere
       !--------------------------------------------------------------------------
#ifdef DEBUG
       write(OUTPUT_UNIT,*) 'setcommonhaloid: send to front, receive from back'
#endif

       sendID = info_proc%global_comm%neighbours(2)
       recvID = info_proc%global_comm%neighbours(1)

       allocate (strRec(nflagrecv(1)), STAT=allocstat)
       if(allocstat > 0) call emergencyStop('Allocate failed for strrec (1) in setcommonhaloid',allocstat)

       call Mpi_ISend(strAva,nflagloc(2), MPI_IDKIND,sendID,5,info_proc%global_comm%name,mpireqs,mpierr)
       call Mpi_IRecv(strRec,nflagrecv(1),MPI_IDKIND,recvID,5,info_proc%global_comm%name,mpireqr,mpierr)
       call Mpi_Wait(mpireqs,mpistat,mpierr)
       call Mpi_Wait(mpireqr,mpistat,mpierr)

       ! comparaison des part. proche de ma face 'arriere' avec les part. venant de mon neighbours de l'arriere
       do ib = 1, nbridgearr
          il = bridgeArr(1,ib)
          ir = bridgeArr(2,ib)

          raccordr : if(strRec(ir)< strArr(il) ) then

             nopermut = .false.
             renumallr : do k = 1,local_npart
                if(structure_id(k) == strArr(il)) structure_id(k) = strRec(ir)
             end do renumallr
             do k=1, nflagloc(5)
                if(strBas(k) == strArr(il)) strBas(k) = strRec(ir)
             end do
             do k=1, nflagloc(3)
                if(strGau(k) == strArr(il)) strGau(k) = strRec(ir)
             end do
             do k=1, nflagloc(4)
                if(strDro(k) == strArr(il)) strDro(k) = strRec(ir)
             end do
             do k=1, nflagloc(1)
                if(strArr(k) == strArr(il) .and. k/= il) strArr(k) = strRec(ir)
             end do
             do k=1, nflagloc(6)
                if(strHau(k) == strArr(il)) strHau(k) = strRec(ir)
             end do
             strArr(il) = strRec(ir)
          end if raccordr
       end do

       deallocate(strRec)


       !---------------------------------------------------------------------
       ! Envoi de ma face arriere vers l'arriere et reception venant de l'avant
       !---------------------------------------------------------------------
#ifdef DEBUG
       write(OUTPUT_UNIT,*) 'setcommonhaloid: send to back, receive from front'
#endif

       sendID = info_proc%global_comm%neighbours(1)
       recvID = info_proc%global_comm%neighbours(2)

       allocate (strRec(nflagrecv(2)), STAT=allocstat)
       if(allocstat > 0) call emergencyStop('Allocate failed for strrec (2) in setcommonhaloid',allocstat)

       call Mpi_ISend(strArr,nflagloc(1), MPI_IDKIND,sendID,6,info_proc%global_comm%name,mpireqs,mpierr)
       call Mpi_IRecv(strRec,nflagrecv(2),MPI_IDKIND,recvID,6,info_proc%global_comm%name,mpireqr,mpierr)
       call Mpi_Wait(mpireqs,mpistat,mpierr)
       call Mpi_Wait(mpireqr,mpistat,mpierr)

       ! comparaison des part. proche de ma face 'avant' avec les part. venant de mon neighbours de l'avant
       do ib = 1, nbridgeava
          il = bridgeAva(1,ib)
          ir = bridgeAva(2,ib)

          raccorda : if(strRec(ir)< strAva(il) ) then

             nopermut = .false.
             renumalla : do k = 1,local_npart
                if(structure_id(k) == strAva(il)) structure_id(k) = strRec(ir)
             end do renumalla
             do k=1, nflagloc(4)
                if(strDro(k) == strAva(il)) strDro(k) = strRec(ir)
             end do
             do k=1, nflagloc(5)
                if(strBas(k) == strAva(il)) strBas(k) = strRec(ir)
             end do
             do k=1, nflagloc(2)
                if(strAva(k) == strAva(il) .and. k/= il) strAva(k) = strRec(ir)
             end do
             do k=1, nflagloc(3)
                if(strGau(k) == strAva(il)) strGau(k) = strRec(ir)
             end do
             do k=1, nflagloc(6)
                if(strHau(k) == strAva(il)) strHau(k) = strRec(ir)
             end do
             strAva(il) = strRec(ir)
          end if raccorda
       end do

       deallocate(strRec)

       call mpi_allreduce(nopermut,finished,1,Mpi_Logical,Mpi_Land,info_proc%global_comm%name,mpierr)
       if(finished) exit

    end do raccordement

    if(info_proc%global_comm%pid==0) then
       write(OUTPUT_UNIT,*) ''
       write(OUTPUT_UNIT,*) 'End of the permutations'
       write(OUTPUT_UNIT,*) ''
    end if

    if(allocated(strHau)) deallocate(strHau)
    if(allocated(strBas)) deallocate(strBas)
    if(allocated(strDro)) deallocate(strDro)
    if(allocated(strGau)) deallocate(strGau)
    if(allocated(strAva)) deallocate(strAva)
    if(allocated(strArr)) deallocate(strArr)

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Exit setcommonhaloid on process ', info_proc%global_comm%pid
#endif

  end subroutine setcommonhaloid

end module modfofmpi

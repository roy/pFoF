!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2016 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Contains subroutines to compute halo properties
!! @brief
!! 
!! @author Fabrice Roy

!> Contains subroutines to compute halo properties
!------------------------------------------------------------------------------------------------------------------------------------

module compute_halo_properties_mod

  use iso_fortran_env, only : OUTPUT_UNIT
  use modmpicommons, only : procid

  implicit none

  private
  public :: compute_halo_com,&
       compute_halo_mass_and_id,&
       compute_halo_radius

contains
  !> Stores id and mass (i.e. number of dm particles) of each structure in an array
  !=======================================================================
  subroutine compute_halo_mass_and_id(halo_id_vector, halo_mass_vector, final_local_halo_number, finalstructureid)

    use modconstant, only : IDKIND

    integer(kind=IDKIND), intent(inout), dimension(:) :: halo_id_vector
    integer(kind=4),   intent(inout), dimension(:) :: halo_mass_vector
    integer(kind=4),   intent(in)                  :: final_local_halo_number
    integer(kind=IDKIND), intent(in),    dimension(:) :: finalstructureid

    integer(kind=IDKIND) :: current_id
    integer(kind=4)   :: current_index
    integer(kind=4)   :: current_size
    integer(kind=IDKIND) :: ip
    integer(kind=IDKIND) :: particle_number

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6,A,I9)') 'Enter compute_halo_mass_and_id on process ', procID, &
         ' with final_local_halo_number=', final_local_halo_number
#endif

    particle_number = ubound(finalstructureid,dim=1)
    if(final_local_halo_number/=0) then
       current_id = finalstructureid(1)
       current_index = 1
       current_size = 1
       do ip = 2, particle_number
          if(current_id == finalstructureid(ip)) then
             current_size = current_size+1
          else
             halo_id_vector(current_index) = current_id
             halo_mass_vector(current_index) = current_size
             current_id = finalstructureid(ip)
             current_size = 1
             current_index = current_index + 1
          end if
       end do
       halo_id_vector(current_index) = current_id
       halo_mass_vector(current_index) = current_size
    end if

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Exit compute_halo_mass_and_id on process ', procID
#endif

  end subroutine compute_halo_mass_and_id


  ! ======================================================================
  !> Computes the position and the velocity of the center of mass for each halo
  subroutine compute_halo_com(halo_com_position_vector, halo_com_velocity_vector, &
       halo_mass_vector, particle_position_vector, particle_velocity_vector, periodic)


    real(kind=8),    intent(inout), dimension(:,:) :: halo_com_position_vector
    real(kind=8),    intent(inout), dimension(:,:) :: halo_com_velocity_vector
    integer(kind=4), intent(in),    dimension(:)   :: halo_mass_vector
    real(kind=4),    intent(in),    dimension(:,:) :: particle_position_vector
    real(kind=4),    intent(in),    dimension(:,:) :: particle_velocity_vector
    logical,         intent(in)                    :: periodic

    integer(kind=4) :: fi, li, h, i, j
    integer(kind=4) :: halom
    real(kind=8), dimension(3) :: delta
    integer(kind=4) :: halo_nb

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Enter compute_halo_com on process ', procID
#endif

    halo_nb = ubound(halo_mass_vector,dim=1)

    fi = 1
    do h = 1, halo_nb
       halom = 1
       li = fi + halo_mass_vector(h) - 1
       halo_com_position_vector(:,h) = particle_position_vector(:,fi)
       halo_com_velocity_vector(:,h) = particle_velocity_vector(:,fi)
       do i = fi+1, li
          delta(:) = halo_com_position_vector(:,h) / halom - particle_position_vector(:,i)
          if(periodic) then
             do j = 1, 3
                if(abs(delta(j)) > 0.5d0) then
                   if(delta(j) > 0d0) halo_com_position_vector(j,h) = halo_com_position_vector(j,h) + 1.0d0
                   if(delta(j) < 0d0) halo_com_position_vector(j,h) = halo_com_position_vector(j,h) - 1.0d0
                end if
             end do
          end if
          halom = halom + 1
          halo_com_position_vector(:,h) = halo_com_position_vector(:,h) + particle_position_vector(:,i)
          halo_com_velocity_vector(:,h) = halo_com_velocity_vector(:,h) + particle_velocity_vector(:,i)
       end do
       fi = li + 1

    end do

    do h = 1, halo_nb
       halo_com_position_vector(:,h) = halo_com_position_vector(:,h) / halo_mass_vector(h)
       halo_com_velocity_vector(:,h) = halo_com_velocity_vector(:,h) / halo_mass_vector(h)
       if(periodic) then
          do j = 1, 3
             if(halo_com_position_vector(j,h) > 1.0d0) halo_com_position_vector(j,h) = halo_com_position_vector(j,h) - 1.0d0
             if(halo_com_position_vector(j,h) < 0.0d0) halo_com_position_vector(j,h) = halo_com_position_vector(j,h) + 1.0d0
          end do
       end if
    end do

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Exit compute_halo_com on process ', procID
#endif

  end subroutine compute_halo_com


  ! ======================================================================
  !> Computes the radius, i.e. the max distance between a particle and the center of mass, for each halo
  subroutine compute_halo_radius(halo_radius_vector, halo_com_position_vector, halo_mass_vector, particle_position_vector, periodic)

    real(kind=8),    intent(inout), dimension(:)   :: halo_radius_vector
    real(kind=8),    intent(in),    dimension(:,:) :: halo_com_position_vector
    integer(kind=4), intent(in),    dimension(:)   :: halo_mass_vector
    real(kind=4),    intent(in),    dimension(:,:) :: particle_position_vector
    logical,         intent(in)                    :: periodic

    integer(kind=4) :: ib
    integer(kind=4) :: id
    integer(kind=4) :: ih
    integer(kind=4) :: ip
    integer(kind=4) :: halo_nb
    real(kind=8) :: rmax2
    real(kind=8) :: d2
    real(kind=8), dimension(3) :: delta

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Enter compute_halo_radius on process ', procID
#endif

    halo_nb = ubound(halo_radius_vector, dim=1)

    ib = 0
    do ih = 1, halo_nb
       rmax2 = 0.d0
       do ip = ib+1, ib+halo_mass_vector(ih)
          delta(:) = particle_position_vector(:,ip) - halo_com_position_vector(:,ih)
          if(periodic) then
             do id = 1, 3
                if(abs(delta(id)) > 0.5d0) then
                   delta(id) = 1.d0 - abs(delta(id))
                end if
             end do
          end if
          d2 = delta(1)*delta(1) + delta(2)*delta(2) + delta(3)*delta(3)
          if(d2 > rmax2) rmax2 = d2
       end do
       ib = ib + halo_mass_vector(ih)
       halo_radius_vector(ih) = sqrt(rmax2)
    end do

#ifdef DEBUG
    write(OUTPUT_UNIT,'(A,I6)') 'Exit compute_halo_radius on process ', procID
#endif

  end subroutine compute_halo_radius


end module compute_halo_properties_mod

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Subroutines to read pFoF star cube files
!! @brief
!! Subroutines to read pFoF star cube files
!! @author Fabrice Roy
!------------------------------------------------------------------------------------------------------------------------------------

module read_star_cube_mod

  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use modconstant, only : IDKIND, type_common_metadata, type_parameter_pfof_snap, type_info_ramses
  use fortran_hdf5_constants_m, only : H5_FILENAME_LEN, H5_STR_LEN
  use fortran_hdf5_manage_file_m, only : hdf5_close_file, hdf5_open_file, hdf5_open_mpi_file
  use fortran_hdf5_manage_group_m, only : hdf5_close_group, hdf5_open_group
  use fortran_hdf5_read_attribute_m
  use fortran_hdf5_read_data_m
  use fortran_hdf5_read_mpi_data_m
  use hdf5, only : HID_T
  use modmpicommons, only : Emergencystop
  use mpi
  use modreadmeta, only : Read_meta_common, Read_meta_info_ramses, Read_meta_halofinder_parameter

  implicit none

  private
  public :: type_star_cube_metadata, &
       Select_read_star_cube, &
       Read_star_cube

  type :: type_star_cube_metadata
     type(type_common_metadata) :: common 
     type(type_parameter_pfof_snap) :: pfof
     type(type_info_ramses) :: ramses
     real(kind=4), dimension(6) :: boundaries
     integer(kind=4) :: nfile
     integer(kind=4) :: ngroup = 0
     integer(kind=8) :: npart_file 
     character(len=H5_STR_LEN) :: file_type
    integer(kind=4), dimension(:), allocatable :: npart_per_group
  end type type_star_cube_metadata
  
  procedure(), pointer :: Read_star_cube


contains

  !=======================================================================
  subroutine Select_read_star_cube(filename, process_id)
    
    character(len=H5_FILENAME_LEN), intent(in) :: filename
    integer(kind=4), intent(in) :: process_id
    
    integer(kind=HID_T) :: file_id
    integer(kind=HID_T) :: gr_id
    character(len=H5_STR_LEN) :: h5name
    character(len=H5_STR_LEN) :: filetype
    integer(kind=4) :: mpierr

    if(process_id==0) then
       call hdf5_open_file(filename, file_id)
       h5name = 'metadata'
       call hdf5_open_group(file_id, h5name, gr_id)
       h5name = 'file_type'
       call hdf5_read_attr(gr_id, h5name, len(filetype), filetype)
       call hdf5_close_group(gr_id)
       call hdf5_close_file(file_id)
    end if

    call Mpi_Bcast(filetype, len(filetype), Mpi_Char, 0, Mpi_Comm_World, mpierr)

    select case (filetype)
    case ("cube")
       Read_star_cube => H5_read_star_cube
    case ("mpicube")
       Read_star_cube => H5_read_star_cube
    case ("sortedcube")
       Read_star_cube => H5_read_sorted_star_cube
    case ("mpisortedcube")
       Read_star_cube => H5_read_sorted_star_cube
    case default
       call Emergencystop("Error while reading cube type",31)
    end select

  end subroutine Select_Read_Star_Cube

  !=======================================================================
  !> This subroutine reads hdf5 cube files created by pFOF.
  subroutine H5_read_star_cube(star_cube_metadata, stars, filename, process_id)

    use type_particles_mod, only : type_stars, Allocate_stars
    
    type(type_star_cube_metadata), intent(out) :: star_cube_metadata
    type(type_stars), intent(inout) :: stars
    character(len=H5_FILENAME_LEN), intent(in) :: filename
    integer(kind=4), intent(in) :: process_id

    character(len=:), allocatable :: alloc_err_msg
    integer(HID_T) :: file_id                                           ! File identifier
    integer(HID_T) :: gr_id                                             ! Group identifier
    character(len=H5_STR_LEN) :: aname                                  ! Attribute name
    character(len=H5_STR_LEN) :: dsetname                               ! Dataset name
    character(len=H5_STR_LEN) :: groupname
    logical(kind=4) :: islast
    integer(kind=4) :: local_npart

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Enter H5_read_star_cube on process ', process_id
#endif

    ! open the file
    call Hdf5_open_file(filename, file_id)

    call Read_meta_common(file_id, star_cube_metadata%common)
    islast = .false.
    call Read_meta_info_ramses(file_id, star_cube_metadata%ramses, islast)
    call Read_meta_halofinder_parameter(file_id, star_cube_metadata%pfof)
    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id,groupname, gr_id)

    aname = 'file_type'
    call hdf5_read_attr(gr_id, aname, H5_STR_LEN, star_cube_metadata%file_type)

    ! read attribute partNB
    dsetname = 'npart_file'
    call hdf5_read_data(gr_id, dsetname, star_cube_metadata%npart_file)
    local_npart = int(star_cube_metadata%npart_file, kind=4)

    aname = 'nfile'
    call hdf5_read_attr(gr_id, aname, star_cube_metadata%nfile)
    ! read attribute boundaries
    aname = 'boundaries'
    call hdf5_read_attr(gr_id, aname, 6, star_cube_metadata%boundaries)

    call hdf5_close_group(gr_id)

    call allocate_stars(stars, local_npart, process_id)
        
    groupname = 'data'
    call hdf5_open_group(file_id, groupname, gr_id)

    ! read position of the particles
    if(stars%has_position) then
       dsetname = 'position_part'
       call hdf5_read_data(gr_id, dsetname, 3, local_npart, stars%position)
    end if

    ! read velocity of the particles
    if(stars%has_velocity) then
       dsetname = 'velocity_part'
       call hdf5_read_data(gr_id, dsetname, 3, local_npart, stars%velocity)
    end if
    
    ! read id of the particles
    if(stars%has_ramses_identity) then
       dsetname = 'ramses_identity_part'
       call hdf5_read_data(gr_id, dsetname, local_npart, stars%ramses_identity)
    end if
    
    if(stars%has_potential) then
       dsetname = 'potential_part'
       call hdf5_read_data(gr_id, dsetname, local_npart, stars%potential)
    end if

    if(stars%has_field) then
       dsetname = 'gravitational_field_part'
       call hdf5_read_data(gr_id, dsetname, 3, local_npart, stars%field)
    end if
    
    if(stars%has_mass) then
       dsetname = 'mass_part'
       call hdf5_read_data(gr_id, dsetname, local_npart, stars%mass)
    end if

    if(stars%has_birth_date) then
       dsetname = 'birth_date_part'
       call hdf5_read_data(gr_id, dsetname, local_npart, stars%birth_date)
    end if
    
    if(stars%has_metallicity) then
       dsetname = 'metallicity_part'
       call hdf5_read_data(gr_id, dsetname, local_npart, stars%metallicity)
    end if

    ! Close the root group.
    call hdf5_close_group(gr_id)

    call hdf5_close_file(file_id)

#ifdef DEBUG
    write(ERROR_UNIT,*) "Exit H5_read_star_cube on process ",process_id
#endif

  end subroutine H5_Read_Star_Cube



  !=======================================================================
  !> This subroutine reads hdf5 sorted cube files created by pFOF.
  subroutine H5_read_sorted_star_cube(star_cube_metadata, stars, filename, process_id)

    use type_particles_mod, only : type_stars, Allocate_stars
    use error_handling_mod, only : Allocate_error

    type(type_star_cube_metadata), intent(out) :: star_cube_metadata
    type(type_stars), intent(inout) :: stars
    character(len=H5_FILENAME_LEN), intent(in) :: filename
    integer(kind=4), intent(in) :: process_id

    integer :: alloc_stat
    character(len=:), allocatable :: alloc_err_msg
    integer(HID_T) :: file_id                               ! File identifier
    integer(HID_T) :: gr_data_id                            ! Group identifier
    integer(HID_T) :: gr_id                                 ! Group identifier
    character(len=8)  :: gid_char
    character(len=H5_STR_LEN) :: aname                              ! Attribute name
    character(len=H5_STR_LEN) :: dsetname                           ! Dataset name
    character(len=H5_STR_LEN) :: groupname
    integer(kind=4) :: igroup
    integer(kind=4) :: indbeg, indend
    logical(kind=4) :: islast
    integer(kind=4) :: local_npart

#ifdef DEBUG
    write(ERROR_UNIT,*) "Enter H5_read_sorted_star_cube on process ",process_id
#endif


    ! open the file
    call Hdf5_open_file(filename, file_id)

    call Read_meta_common(file_id, star_cube_metadata%common)
    islast = .false.
    call Read_meta_info_ramses(file_id, star_cube_metadata%ramses, islast)
    call Read_meta_halofinder_parameter(file_id, star_cube_metadata%pfof)
    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id,groupname, gr_id)

    aname = 'file_type'
    call hdf5_read_attr(gr_id, aname, H5_STR_LEN, star_cube_metadata%file_type)

    ! read attribute partNB
    dsetname = 'npart_file'
    call hdf5_read_data(gr_id, dsetname, star_cube_metadata%npart_file)
    local_npart = int(star_cube_metadata%npart_file, kind=4)

    aname = 'nfile'
    call hdf5_read_attr(gr_id, aname, star_cube_metadata%nfile)
    ! read attribute boundaries
    aname = 'boundaries'
    call hdf5_read_attr(gr_id, aname, 6, star_cube_metadata%boundaries)

    ! read attribute ngroup
    aname = 'ngroup'
    call hdf5_read_attr(gr_id, aname, star_cube_metadata%ngroup)

    allocate(star_cube_metadata%npart_per_group(star_cube_metadata%ngroup), stat=alloc_stat, errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       call Allocate_error('star_cube_metadata%npartpergroup','H5_read_sorted_star_cube',alloc_err_msg,alloc_stat, process_id)
    end if
    aname = 'npart_grp_array'
    call hdf5_read_data(gr_id, aname, star_cube_metadata%ngroup, star_cube_metadata%npart_per_group)

    call hdf5_close_group(gr_id)

    call allocate_stars(stars, local_npart, process_id)

    groupname = 'data'
    call hdf5_open_group(file_id, groupname, gr_data_id)

    indbeg = 1
    indend = 0
    ! loop over the groups
    do igroup=1, star_cube_metadata%ngroup
       if(star_cube_metadata%npart_per_group(igroup) /= 0) then ! there is at least one part. to read in this group
          write(gid_char(1:8),'(I8.8)') igroup
          groupname='group'//gid_char
          call hdf5_open_group(gr_data_id,groupname,gr_id)

          indend = indbeg + star_cube_metadata%npart_per_group(igroup) - 1

          ! read position of the particles
          if(stars%has_position) then
             dsetname = 'position_part'
             call hdf5_read_data(gr_id, dsetname, 3, local_npart, stars%position(:,indbeg:indend))
          end if

          ! read velocity of the particles
          if(stars%has_velocity) then
             dsetname = 'velocity_part'
             call hdf5_read_data(gr_id, dsetname, 3, local_npart, stars%velocity(:,indbeg:indend))
          end if

          ! read id of the particles
          if(stars%has_ramses_identity) then
             dsetname = 'ramses_identity_part'
             call hdf5_read_data(gr_id, dsetname, local_npart, stars%ramses_identity(indbeg:indend))
          end if

          ! read potential 
          if(stars%has_potential) then
             dsetname = 'potential_part'
             call hdf5_read_data(gr_id, dsetname, local_npart, stars%potential(indbeg:indend))
          end if

          if(stars%has_field) then
             dsetname = 'gravitational_field_part'
             call hdf5_read_data(gr_id, dsetname, 3, local_npart, stars%field(:,indbeg:indend))
          end if
          
          if(stars%has_mass) then
             dsetname = 'mass_part'
             call hdf5_read_data(gr_id, dsetname, local_npart, stars%mass(indbeg:indend))
          end if
          
          if(stars%has_birth_date) then
             dsetname = 'birth_date_part'
             call hdf5_read_data(gr_id, dsetname, local_npart, stars%birth_date(indbeg:indend))
          end if
          
          if(stars%has_metallicity) then
             dsetname = 'metallicity_part'
             call hdf5_read_data(gr_id, dsetname, local_npart, stars%metallicity(indbeg:indend))
          end if
          
          indbeg = indend + 1

          call hdf5_close_group(gr_id)
       end if
    end do

    if(indend /= local_npart) then
       write(ERROR_UNIT,*) 'Error while reading particles from file ',filename
       call EmergencyStop('Error in h5readsortedcube',32)
    end if

    ! Close the root group.
    call hdf5_close_group(gr_data_id)

    call hdf5_close_file(file_id)


#ifdef DEBUG
    write(ERROR_UNIT,*) "Exit H5_read_sorted_star_cube on process ",process_id
#endif

  end subroutine H5_read_sorted_star_cube

  
end module read_star_cube_mod

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Program that reads HDF5 'amr_cube' files
!! @brief
!! 
!! @author Fabrice Roy

!> Program that reads HDF5 'amr_cube' files
!------------------------------------------------------------------------------------------------------------------------------------
module mpi_utils_m

    implicit none

    private
    public :: Mpi_distribution

    interface Mpi_distribution
        module procedure Mpi_distribution_int4
        module procedure Mpi_distribution_int8
    end interface
    
    contains
!------------------------------------------------------------------------------------------------------------------------------------
    subroutine Mpi_distribution_int4(first_object, last_object, n_objects, mpi_process)
    
        use mpi_process_m

        integer(kind=4), intent(out) :: first_object
        integer(kind=4), intent(out) :: last_object
        integer(kind=4), intent(in) :: n_objects
        type(mpi_process_t), intent(in) :: mpi_process

        integer(kind=4) :: n_objects_per_process
        integer(kind=4) :: rest
                
        n_objects_per_process = n_objects / mpi_process%comm%size
        rest = mod(n_objects, mpi_process%comm%size)
        first_object = mpi_process%rank * n_objects_per_process + 1 
        if(rest /= 0) then
           if(mpi_process%rank < rest) then
              n_objects_per_process = n_objects_per_process + 1
              first_object = first_object + mpi_process%rank
           else
              first_object = first_object + rest
           end if
        end if
        last_object = first_object + n_objects_per_process - 1

    end subroutine Mpi_distribution_int4
!------------------------------------------------------------------------------------------------------------------------------------

    subroutine Mpi_distribution_int8(first_object, last_object, n_objects, mpi_process)
    
        use mpi_process_m

        integer(kind=8), intent(out) :: first_object
        integer(kind=8), intent(out) :: last_object
        integer(kind=8), intent(in) :: n_objects
        type(mpi_process_t), intent(in) :: mpi_process

        integer(kind=8) :: n_objects_per_process
        integer(kind=8) :: rest
                
        n_objects_per_process = n_objects / mpi_process%comm%size
        rest = mod(n_objects, mpi_process%comm%size)
        first_object = mpi_process%rank * n_objects_per_process + 1 
        if(rest /= 0) then
           if(mpi_process%rank < rest) then
              n_objects_per_process = n_objects_per_process + 1
              first_object = first_object + mpi_process%rank
           else
              first_object = first_object + rest
           end if
        end if
        last_object = first_object + n_objects_per_process - 1

    end subroutine Mpi_distribution_int8


end module mpi_utils_m
!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2015 Fabrice Roy 
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Subroutines to write HDF5 halo files created with pfof_snap and pfof_cone
!! @brief
!! Subroutines to write HDF5 halo files created with pfof_snap and pfof_cone
!! @author Fabrice Roy
! ======================================================================

module modwritehalo

  use iso_fortran_env, only : ERROR_UNIT
  use modconstant, only : ERR_MSG_LEN,&
       IDKIND,&
       MPI_IDKIND,&
       type_parameter_halofinder,&
       type_parameter_pfof,&
       type_parameter_pfof_snap,&
       type_parameter_pfof_cone,&
       type_parameter_psod_snap
  
  use mpi
  use modmpicommons, only : procID,&
       type_info_process,&
       emergencystop

  use fortran_hdf5_constants_m, only : H5_FILENAME_LEN, H5_STR_LEN
  use fortran_hdf5_manage_files_m, only : hdf5_close_file, hdf5_close_mpi_file, hdf5_create_file, hdf5_create_mpi_file
  use fortran_hdf5_manage_groups_m, only : hdf5_close_group, hdf5_create_group, hdf5_open_group
  use fortran_hdf5_write_attribute_m
  use fortran_hdf5_write_data_m
  use fortran_hdf5_write_mpi_data_m
  use hdf5, only : HID_T

  use modwritemeta, only : Write_meta_common,&
       Write_meta_halofinder_parameter,&
       Write_meta_info_ramses,&
       Write_meta_info_cone

  use type_info_ramses_mod, only : type_info_cone, &
       type_info_ramses
  
  implicit none

  private

  public :: h5writehalopart, &
       mpih5writehalopart, &
       mpih5writehalomass

contains

  !=======================================================================
  !> This subroutine writes for each halo the position, the velocity and the id of each particle in this halo
  !! in a hdf5 file.
  !! One file is written per MPI process and several halos per file.
  !! There is one group per halo, the name of the group is the ID of the halo.
  subroutine h5writehalopart(info_proc, param_pfof, global_npart, haloNB, halopartNB, haloMass, haloID, &
       halopartPos, halopartVel, halopartID, halopartmas, halopartFor, halopartPot, halopartRamsesID, &
       inforamses, infocone)

    type(Type_info_process), intent(in) :: info_proc !< MPI communicator used to gather informations
    class(Type_parameter_halofinder), intent(in) :: param_pfof
    integer(kind=IDKIND), intent(in) :: global_npart
    integer(kind=4), intent(in) :: haloNB !< Number of halos
    integer(kind=4), intent(in) :: halopartNB  !< Number of particles belonging to the halos
    integer(kind=IDKIND), dimension(haloNB), intent(in) :: haloID !< Array containing the ID of the halos
    integer(kind=IDKIND), dimension(halopartNB), intent(in), target :: halopartID !< Array containing the ID of the
    !< particles belonging to the halos
    integer(kind=4), dimension(haloNB), intent(in) :: haloMass !< Array containing the mass of the halos
    real   (kind=4), dimension(3,halopartNB), intent(in), target :: halopartPos, halopartVel !< Array containing the position
    !< and velocity of the particles belonging to the halos
    real   (kind=4), dimension(halopartNB), intent(in), target, optional :: halopartmas !< Array containing the mass of the
    !< particles belonging to the halos, this argument is optional
    real   (kind=4), dimension(halopartNB), intent(in), target, optional :: halopartPot !< Array containing the potential of the
    !< particles belonging to the halos, this argument is optional
    real   (kind=4), dimension(3,halopartNB), intent(in), target, optional :: halopartFor !< Array containing the force on the particles belonging to the halos, this argument is optional
    integer(kind=IDKIND), dimension(halopartNB), intent(in), target, optional :: halopartRamsesID !< Array containing the "Ramses" ID of the particles belonging to the halos detected in lightcones

    type(Type_info_ramses),intent(in) :: inforamses
    class(Type_info_cone), intent(in), optional :: infocone

    integer(kind=4) :: ih, hptr
    character(len=H5_FILENAME_LEN) :: filestrct
    character(len=5)  :: pid_char
    character(len=H5_STR_LEN) :: groupname
    character(len=H5_STR_LEN) :: dsetname                           ! Dataset name
    character(len=H5_STR_LEN) :: aname                              ! Attribute name
    character(len=H5_STR_LEN) :: adata
    character(len=H5_STR_LEN) :: halofinder

    integer(HID_T) :: file_id                               ! File identifier
    integer(HID_T) :: gr_id                                 ! Group identifier
    integer(HID_T) :: gr_halo_id                            ! Group identifier

    integer(kind=4), dimension(:), allocatable :: halo_number_vector, dspl
    integer(kind=4), dimension(:), allocatable :: haloMasstab
    integer(kind=IDKIND), dimension(:), allocatable :: haloIDtab
    integer(kind=IDKIND), dimension(2) :: IDminmax
    integer(kind=4) :: haloNBall, p
    integer(kind=4) :: mpierr

    integer(kind=8) :: npart8
    character(len=H5_STR_LEN) :: codename
    logical(kind=4) :: islast
    integer(kind=4) :: alloc_stat
    character(len=ERR_MSG_LEN) :: alloc_err_msg
    integer(kind=4) :: constant_mass

#ifdef DEBUG
    print *,'Enter h5writehalopart on process ', info_proc%global_comm%pid
#endif

    write(pid_char(1:5),'(I5.5)')  info_proc%global_comm%pid

    allocate(halo_number_vector(info_proc%global_comm%size),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A,I6)') 'Error in halo_number_vector allocation in h5writehalopart on process ',info_proc%global_comm%pid
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(dspl(info_proc%global_comm%size),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A,I6)') 'Error in dspl allocation in h5writehalopart on process ',info_proc%global_comm%pid
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if

    select type (param_pfof)
       class is (Type_parameter_pfof_snap)
       filestrct = 'pfof_halo_snap_dm_data_'//&
            trim(param_pfof%simulation_name)//'_'//pid_char//'.h5'
       codename='pfof_snap'
       halofinder = 'pfof'
       islast = .false.
       class is (Type_parameter_pfof_cone)
       filestrct = 'pfof_halo_cone_dm_data_'//&
            trim(param_pfof%simulation_name)//'_'//pid_char//'.h5'
       codename='pfof_cone'
       halofinder = 'pfof'
       islast = .true.
       class is (Type_parameter_psod_snap)
       filestrct = 'psod_halo_snap_dm_data_'//&
            trim(param_pfof%simulation_name)//'_'//pid_char//'.h5'
       codename='psod_snap'
       halofinder = 'psod'
       islast = .false.
    end select

    call Mpi_Gather(haloNB, 1, Mpi_Integer, halo_number_vector, 1, Mpi_Integer, 0, &
         info_proc%global_comm%name, mpierr)
    if(info_proc%global_comm%pid == 0) then
       haloNBall = halo_number_vector(1)
       dspl(1) = 0
       do p=2, info_proc%global_comm%size
          dspl(p) = haloNBall
          haloNBall= haloNBall + halo_number_vector(p)
       end do
       allocate(haloMasstab(haloNBall),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in halomasstab allocation in h5writehalopart on process ',info_proc%global_comm%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
       allocate(haloIDtab(haloNBall),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in haloidtab allocation in h5writehalopart on process ',info_proc%global_comm%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    else
       allocate(haloMasstab(0),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in halomass_vector allocation in h5writehalopart on process ',info_proc%global_comm%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
       allocate(haloIDtab(0),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in haloidtab allocation in h5writehalopart on process ',info_proc%global_comm%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    end if
    call Mpi_Gatherv(haloMass, haloNB, Mpi_Integer, haloMasstab, halo_number_vector, dspl,&
         Mpi_Integer, 0, info_proc%global_comm%name, mpierr)
    call Mpi_Gatherv(haloID, haloNB, Mpi_IDKIND, haloIDtab, halo_number_vector, dspl, &
         Mpi_IDKIND, 0, info_proc%global_comm%name, mpierr)

    ! create the hdf5 file
    call hdf5_create_file(filestrct, file_id)

    npart8 = int(global_npart,kind=8)
    if(param_pfof%do_skip_mass) then
       constant_mass=1
    else
       constant_mass=-1
    end if
    call Write_meta_common(file_id, codename, npart8, constant_mass=constant_mass,&
         process_id=info_proc%global_comm%pid)
    call Write_meta_halofinder_parameter(file_id, param_pfof)
    call Write_meta_info_ramses(file_id, inforamses, islast)

    if(present(infocone)) then
       call Write_meta_info_cone(file_id, infocone,islast)
    end if


    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id,groupname, gr_id)

    aname = 'halo_finder'
    adata = halofinder
    call hdf5_write_attr(gr_id, aname, adata)

    ! Write type as attribute
    aname = 'file_type'
    adata = 'halo'
    call hdf5_write_attr(gr_id, aname, adata)

    aname = 'nfile'
    call hdf5_write_attr(gr_id, aname, info_proc%global_comm%size)

    ! write the number of haloes as an attribute
    aname = 'nhalo_file'
    call hdf5_write_attr(gr_id, aname, haloNB)

    ! write the number of particles written in the file
    dsetname = 'npart_file'
    npart8 = int(halopartNB,kind=8)
    call hdf5_write_data(gr_id, dsetname, npart8)

    call hdf5_close_group(gr_id)

    groupname = 'data'
    call hdf5_create_group(file_id, groupname, gr_id)

    if(haloNB/=0) then
       !! write the halo ID as data and not attribute: it seems that we cannot write integer(kind=8) attribute
       aname = 'identity_halo'
       call hdf5_write_data(gr_id, aname, haloNB, haloID)

       aname = 'identity_halo_minmax'
       IDminmax(1) = haloID(1)
       IDminmax(2) = haloID(haloNB)
       call hdf5_write_data(gr_id, aname, 2, IDminmax)

       ! pointer to the current halo
       hptr = 1

       do ih = 1, haloNB
          ! create a group for each halo
          groupname = 'halo_0000000000000000000'
          write(groupname(6:24),'(I19.19)') haloID(ih)
          call hdf5_create_group(gr_id, groupname, gr_halo_id)
          ! create an attribute containing the number of particles in the halo
          aname = 'npart_halo'
          call hdf5_write_attr(gr_halo_id, aname, haloMass(ih))
          dsetname='position_part'
          call hdf5_write_data(gr_halo_id, dsetname, 3, haloMass(ih), &
               halopartPos(:,hptr:hptr+haloMass(ih)-1))
          dsetname='velocity_part'
          call hdf5_write_data(gr_halo_id, dsetname, 3, haloMass(ih), &
               halopartVel(:,hptr:hptr+haloMass(ih)-1))
          dsetname = 'identity_part'
          call hdf5_write_data(gr_halo_id, dsetname, haloMass(ih), &
               halopartID(hptr:hptr+haloMass(ih)-1))
          if(present(halopartmas)) then
             dsetname = 'mass_part'
             call hdf5_write_data(gr_halo_id, dsetname, halomass(ih), &
                  halopartmas(hptr:hptr+halomass(ih)-1))
          end if
          if(present(halopartPot)) then
             dsetname = 'potential_part'
             call hdf5_write_data(gr_halo_id, dsetname, haloMass(ih), &
                  halopartPot(hptr:hptr+haloMass(ih)-1))
          end if
          if(present(halopartFor)) then
             dsetname = 'gravitational_field_part'
             call hdf5_write_data(gr_halo_id, dsetname, 3, haloMass(ih), &
                  halopartFor(:,hptr:hptr+haloMass(ih)-1))
          end if
          if(present(halopartRamsesID)) then
             dsetname = 'ramses_identity_part'
             call hdf5_write_data(gr_halo_id, dsetname, haloMass(ih), &
                  halopartRamsesID(hptr:hptr+haloMass(ih)-1))
          end if
          ! Close the halo group.
          call hdf5_close_group(gr_halo_id)
          ! move the pointer to the next halo
          hptr = hptr + haloMass(ih)
       end do

       ! Close the root group.
       call hdf5_close_group(gr_id)
       call hdf5_close_file(file_id)
    end if

    if(allocated(halo_number_vector)) deallocate(halo_number_vector)
    if(allocated(dspl)) deallocate(dspl)

#ifdef DEBUG
    print *,'Exit h5writehalopart on process ',info_proc%global_comm%pid
#endif

  end subroutine h5writehalopart



  !=======================================================================
  !> This subroutine writes for each halo the position, the velocity and the id of each
  !! particle in this halo in a hdf5 file.
  !! One file is written per MPI communicator write_comm and several halos per file, using parallel HDF5.
  !! There is one group per halo, the name of the group is the ID of the halo.
  subroutine mpih5writehalopart(info_proc, param_pfof, global_npart, haloNB, halopartNB, haloMass, haloID, &
       halopartPos, halopartVel, halopartID, halopartmas,halopartFor, halopartPot, halopartRamsesID, &
       inforamses, infocone)

    type(Type_info_process), intent(in) :: info_proc !< MPI communicator used to gather informations
    class(Type_parameter_halofinder), intent(in) :: param_pfof
    integer(kind=IDKIND), intent(in) :: global_npart
    integer(kind=4), intent(in) :: haloNB !< Number of halos
    integer(kind=4), intent(in) :: halopartNB  !< Number of particles belonging to the halos
    integer(kind=IDKIND), dimension(haloNB), intent(in) :: haloID !< Array containing the ID of the halos
    integer(kind=IDKIND), dimension(halopartNB), intent(in), target :: halopartID !< Array containing the ID of the
    !< particles belonging to the halos
    integer(kind=4), dimension(haloNB), intent(in) :: haloMass !< Array containing the mass of the halos
    real   (kind=4), dimension(3,halopartNB), intent(in), target :: halopartPos, halopartVel !< Array containing the position
    !< and velocity of the particles belonging to the halos
    real   (kind=4), dimension(halopartNB), intent(in), target, optional :: halopartmas !< Array containing the mass of the
    !< particles belonging to the halos, this argument is optional
    real   (kind=4), dimension(halopartNB), intent(in), target, optional :: halopartPot !< Array containing the potential of the
    !< particles belonging to the halos, this argument is optional
    real   (kind=4), dimension(3,halopartNB), intent(in), target, optional :: halopartFor !< Array containing the force on the particles belonging to the halos, this argument is optional
    integer(kind=IDKIND), dimension(halopartNB), intent(in), target, optional :: halopartRamsesID !< Array containing the "Ramses" ID of the particles belonging to the halos detected in lightcones

    type(Type_info_ramses),intent(in) :: inforamses
    class(Type_info_cone), intent(in), optional :: infocone

    integer(kind=4) :: ih
    character(len=H5_FILENAME_LEN) :: filestrct
    character(len=5)  :: pid_char
    character(len=H5_STR_LEN) :: groupname
    character(len=H5_STR_LEN) :: dsetname                           ! Dataset name
    character(len=H5_STR_LEN) :: aname                              ! Attribute name
    character(len=H5_STR_LEN) :: adata
    character(len=H5_STR_LEN) :: halofinder

    integer(HID_T) :: file_id                               ! File identifier
    integer(HID_T) :: gr_halo_id                            ! Group identifier
    integer(HID_T) :: gr_data_id                            ! Group identifier
    integer(HID_T) :: gr_meta_id                            ! Group identifier

    integer(kind=4), dimension(:), allocatable :: halo_number_vector, dspl
    integer(kind=4), dimension(:), allocatable :: haloMasstab
    integer(kind=IDKIND), dimension(:), allocatable :: haloIDtab
    integer(kind=IDKIND), dimension(2) :: IDminmax
    integer(kind=4) :: haloNBall, p

    integer(kind=8) :: npart8
    character(len=H5_STR_LEN) :: codename
    logical(kind=4) :: islast

    integer(kind=4) :: fp, lp, hptr, pptr, partnb
    logical(kind=4) :: empty

    integer(kind=4) :: mpierr
    integer(kind=4) :: nfile
    integer(kind=4), dimension(:), allocatable :: colortab
    integer(kind=4) :: alloc_stat
    character(len=ERR_MSG_LEN) :: alloc_err_msg
    integer(kind=4) :: constant_mass

#ifdef DEBUG
    print *,'Enter mpih5writehalopart on process ',info_proc%global_comm%pid
#endif

    ! pid_char contains the 'index' of the file in which the process will write its data
    write(pid_char(1:5),'(I5.5)') info_proc%write_comm%color

    allocate(colortab(info_proc%global_comm%size),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A,I6)') 'Error in colortab allocation in mpih5writehalopart on process ',info_proc%global_comm%pid
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(halo_number_vector(info_proc%write_comm%size),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A,I6)') 'Error in halo_number_vector allocation in mpih5writehalopart on process ',&
            info_proc%global_comm%pid
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(dspl(info_proc%write_comm%size),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A,I6)') 'Error in dspl allocation in mpih5writehalopart on process ',info_proc%global_comm%pid
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if

    select type (param_pfof)
       class is (Type_parameter_pfof_snap)
       filestrct = 'pfof_halo_snap_dm_data_'//&
            trim(param_pfof%simulation_name)//'_'//pid_char//'.h5'
       codename='pfof_snap'
       halofinder = 'pfof'
       islast = .false.
       class is (Type_parameter_pfof_cone)
       filestrct = 'pfof_halo_cone_dm_data_'//&
            trim(param_pfof%simulation_name)//'_'//pid_char//'.h5'
       codename='pfof_cone'
       halofinder = 'pfof'
       islast = .true.
       class is (Type_parameter_psod_snap)
       filestrct = 'psod_halo_snap_dm_data_'//&
            trim(param_pfof%simulation_name)//'_'//pid_char//'.h5'
       codename='psod_snap'
       halofinder = 'psod'
       islast = .false.
    end select

    !! essai...
    call Mpi_Allgather(info_proc%write_comm%color, 1, Mpi_Integer, colortab, 1, Mpi_Integer, &
         info_proc%global_comm%name, mpierr)
    nfile = maxval(colortab) - minval(colortab) + 1

    ! Gather halo nb from every process on process 0
    call Mpi_Gather(haloNB, 1, Mpi_Integer, halo_number_vector, 1, Mpi_Integer, 0, &
         info_proc%write_comm%name, mpierr)

    ! Create arrays containing halo mass and halo ID with length haloNBall
    if(info_proc%write_comm%pid==0) then
       haloNBall = halo_number_vector(1)
       dspl(1) = 0
       do p=2, info_proc%write_comm%size
          dspl(p) = haloNBall
          haloNBall= haloNBall + halo_number_vector(p)
       end do
    end if

    call Mpi_Bcast(haloNBall, 1, Mpi_Integer, 0, info_proc%write_comm%name, mpierr)
    allocate(haloMasstab(haloNBall),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A,I6)') 'Error in halomasstab allocation in mpih5writehalopart on process ',info_proc%global_comm%pid
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(haloIDtab(haloNBall),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A,I6)') 'Error in haloidtab allocation in mpih5writehalopart on process ',info_proc%global_comm%pid
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if

    ! Process 0 gathers mass and ID of each halo
    call Mpi_Gatherv(haloMass, haloNB, Mpi_Integer, haloMasstab, halo_number_vector, dspl, &
         Mpi_Integer, 0, info_proc%write_comm%name, mpierr)

    call Mpi_Gatherv(haloID, haloNB, Mpi_IDKIND, haloIDtab, halo_number_vector, dspl, &
         Mpi_IDKIND, 0, info_proc%write_comm%name, mpierr)

    call Mpi_Bcast(haloIDtab, haloNBall, MPI_IDKIND, 0, info_proc%write_comm%name, mpierr)

    call Mpi_Bcast(haloMasstab, haloNBall, Mpi_Integer, 0, info_proc%write_comm%name, mpierr)

    ! create the hdf5 file
    call hdf5_create_mpi_file(filestrct, info_proc%write_comm%name, file_id)

    if(param_pfof%do_skip_mass) then
       constant_mass=1
    else
       constant_mass=-1
    end if    
    npart8 = int(global_npart,kind=8)
    call Write_meta_common(file_id, codename, npart8, constant_mass=constant_mass, &
         process_id=info_proc%global_comm%pid)
    call Write_meta_halofinder_parameter(file_id, param_pfof)
    call Write_meta_info_ramses(file_id, inforamses, islast)

    if(present(infocone)) then
       call Write_meta_info_cone(file_id, infocone,islast)
    end if

    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id,groupname, gr_meta_id)

    aname = 'halo_finder'
    adata = halofinder
    call hdf5_write_attr(gr_meta_id, aname, adata)

    ! Write type as attribute
    aname = 'file_type'
    adata = 'halo'
    call hdf5_write_attr(gr_meta_id, aname, adata)

    aname = 'nfile'
    call hdf5_write_attr(gr_meta_id, aname, nfile )

    ! write the number of haloes as an attribute
    aname = 'nhalo_file'
    call hdf5_write_attr(gr_meta_id, aname, haloNBall)

    ! write the number of particles written in the file
    dsetname = 'npart_file'
    npart8 = int(sum(haloMasstab) ,kind=8)
    call hdf5_write_data(gr_meta_id, dsetname, npart8)

    call hdf5_close_group(gr_meta_id)

    groupname = 'data'
    call hdf5_create_group(file_id, groupname, gr_data_id)

    if(haloNBall/=0) then

       if(haloNB==0) then
          empty=.true.
       else
          empty=.false.
       end if

       !! write the halo ID as data and not attribute:
       !! it seems that we cannot write integer(kind=8) attribute
       !! parallel write: each process write its haloID array
       aname = 'identity_halo'
       call hdf5_write_mpi_data(gr_data_id, aname, haloNB, haloID,&
            info_proc%write_comm%name,empty)

       if(info_proc%write_comm%pid == 0) then
          IDminmax(1) = haloIDtab(1)
          IDminmax(2) = haloIDtab(haloNBall)
          empty = .false.
       else
          IDminmax(1) = haloID(1)
          IDminmax(2) = haloID(haloNB)
          empty = .true.
       end if
       aname = 'identity_halo_minmax'
       call hdf5_write_mpi_data(gr_data_id, aname, 2, IDminmax, &
            info_proc%write_comm%name, empty)

       ! pointer to the current halo
       hptr = 1
       ! pointer to the current particle
       pptr = 1

       do ih = 1, haloNBall
          ! create a group for each halo
          groupname = 'halo_0000000000000000000'
          write(groupname(6:24),'(I19.19)') haloIDtab(ih)
          call hdf5_create_group(gr_data_id, groupname, gr_halo_id)

          if( haloNB == 0 .or. hptr > haloNB) then
             empty = .true.
             fp = 1
             lp = 1
          else
             if( haloID(hptr) == haloIDtab(ih) ) then
                empty = .false.
                partnb = haloMass(hptr)
                fp = pptr
                lp = pptr + partnb - 1
                hptr = hptr + 1
                pptr = pptr + partnb
             else
                empty = .true.
                partnb = 1
                fp = 1
                lp = 1
             end if
          end if

          ! create an attribute containing the number of particles in the halo
          aname = 'npart_halo'
          call hdf5_write_attr(gr_halo_id, aname, haloMasstab(ih))

          dsetname='position_part'
          call hdf5_write_mpi_data(gr_halo_id, dsetname, 3, partnb, &
               halopartPos(:,fp:lp), info_proc%write_comm%name, empty)


          dsetname='velocity_part'
          call hdf5_write_mpi_data(gr_halo_id, dsetname, 3, partnb, &
               halopartVel(:,fp:lp), info_proc%write_comm%name, empty)

          dsetname = 'identity_part'
          call hdf5_write_mpi_data(gr_halo_id, dsetname, partnb, &
               halopartID(fp:lp), info_proc%write_comm%name, empty)

          if(present(halopartmas)) then
             dsetname = 'mass_part'
             call hdf5_write_mpi_data(gr_halo_id, dsetname, partnb, &
                  halopartmas(fp:lp), info_proc%write_comm%name, empty)
          end if

          if(present(halopartPot)) then
             dsetname = 'potential_part'
             call hdf5_write_mpi_data(gr_halo_id, dsetname, partnb, &
                  halopartPot(fp:lp), info_proc%write_comm%name, empty)
          end if

          if(present(halopartFor)) then
             dsetname = 'gravitational_field_part'
             call hdf5_write_mpi_data(gr_halo_id, dsetname, 3, partnb, &
                  halopartFor(:,fp:lp), info_proc%write_comm%name, empty)
          end if

          if(present(halopartRamsesID)) then
             dsetname = 'ramses_identity_part'
             call hdf5_write_mpi_data(gr_halo_id, dsetname, partnb, &
                  halopartRamsesID(fp:lp), info_proc%write_comm%name, empty)
          end if

          ! Close the halo group.
          call hdf5_close_group(gr_halo_id)

       end do

    end if


    call hdf5_close_group(gr_data_id)
    call hdf5_close_mpi_file(file_id)

    deallocate(halo_number_vector, dspl, haloMasstab, haloIDtab)

#ifdef DEBUG
    print *,'Exit mpih5writehalopart on process ',info_proc%global_comm%pid
#endif

  end subroutine mpih5writehalopart



  !=======================================================================
  !> This subroutine writes, for each halo, its mass (number of particles),
  !! the position and the velocity of its center of mass and its ID
  !! in only one hdf5 file using parallel HDF5.
  subroutine mpih5writehalomass(mpicomm, param_pfof, global_npart, haloNB_all, haloNB, nh, &
       haloMass, halocomPos, halocomVel, haloID, haloRadius, haloSubHaloNB, &
       inforamses, infocone)

    integer(kind=IDKIND), intent(in) :: global_npart
    integer(kind=4), intent(in) :: haloNB_all !< Total number of halos (sum over all processes)
    integer(kind=4), intent(in) :: haloNB !< Number of halos
    integer(kind=4), intent(in) :: nh
    integer(kind=4), dimension(nh), intent(in), target :: haloMass !< Mass of the halos
    real(kind=8), dimension(3,nh), intent(in), target :: halocomPos !< Position of the center of mass of the halos
    real(kind=8), dimension(3,nh), intent(in), target :: halocomVel !< Velocity of the center of mass of the halos
    integer(kind=IDKIND), dimension(nh), intent(in), target :: haloID !< ID of the halos
    real(kind=8), dimension(nh), intent(in), target :: haloRadius !< Radius of the halos
    integer(kind=4), dimension(nh), intent(in), target :: haloSubHaloNB !< Number of subhalos in each halo
    integer(kind=4), intent(in) :: mpicomm !< MPI communicator used to create and write the file
    type(Type_info_ramses),intent(in) :: inforamses
    class(Type_info_cone), intent(in), optional :: infocone
    class(Type_parameter_pfof), intent(in) :: param_pfof

    character(len=H5_FILENAME_LEN) :: filename
    character(len=H5_STR_LEN) :: aname                           ! Attribute name
    character(len=H5_STR_LEN) :: dsetname                        ! Dataset name
    character(len=H5_STR_LEN) :: adata
    character(len=H5_STR_LEN) :: groupname
    character(len=H5_STR_LEN) :: codename
    character(len=H5_STR_LEN) :: halofinder
    character(len=ERR_MSG_LEN) :: errormessage

    integer(HID_T) :: file_id                            ! File identifier
    integer(HID_T) :: gr_id
    integer(kind=4) :: begh, endh
    logical(kind=4) :: empty
    integer(kind=8) :: npart8
    logical(kind=4) :: islast
    integer(kind=4) :: constant_mass

#ifdef DEBUG
    print *,'Enter mpih5writehalomass on process ',procID
#endif

    select type(param_pfof)
       class Is(Type_parameter_pfof_snap)
       filename = 'pfof_halo_snap_dm_hfprop_'//trim(param_pfof%simulation_name)//'.h5'
       codename = 'pfof_snap'
       halofinder = 'pfof'
       islast = .false.
       class Is(Type_parameter_pfof_cone)
       filename = 'pfof_halo_cone_dm_hfprop_'//trim(param_pfof%simulation_name)//'.h5'
       codename = 'pfof_cone'
       halofinder = 'pfof'
       islast = .true.
    end select

#ifdef DEBUG
    print *,'Enter mpih5writehalomass: filename is ',trim(filename),' on process ',procID
#endif

    ! Create h5 parallel file
    call hdf5_create_mpi_file(filename, mpicomm, file_id)

    if(param_pfof%do_skip_mass) then
       constant_mass=1
    else
       constant_mass=-1
    end if
    npart8 = int(global_npart,kind=8)
    call Write_meta_common(file_id, codename, npart8, constant_mass=constant_mass, process_id=procID)
    call Write_meta_halofinder_parameter(file_id, param_pfof)
    call Write_meta_info_ramses(file_id, inforamses,islast)

    if(present(infocone)) then
       call Write_meta_info_cone(file_id, infocone,islast)
    end if


    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id,groupname, gr_id)

    aname = 'halo_finder'
    adata = halofinder
    call hdf5_write_attr(gr_id, aname, adata)

    ! Write type as attribute
    aname = 'file_type'
    adata = 'halomass'
    call hdf5_write_attr(gr_id, aname, adata)

    aname = 'center_type'
    adata = 'center_of_mass'
    call hdf5_write_attr(gr_id, aname, adata)

    ! write the number of haloes as an attribute
    aname = 'nhalo_file'
    call hdf5_write_attr(gr_id, aname, haloNB_all)

    call hdf5_close_group(gr_id)

    if(haloNB == 0) then
       begh = 1
       endh = 1
       empty = .true.
    else
       begh = 1
       endh = haloNB
       empty = .false.
    end if

#ifdef DEBUG
    if(nh/=endh-begh+1) then
       write(ERROR_UNIT,'(A,I6,I12,I12)' ) procID, 'Error in sizes of arrays in mpih5writehalomass on process ',&
            procID,' : ',nh,endh-begh+1
       errormessage = 'Error in mpih5writehalomass'
       call EmergencyStop(errormessage,111)
    end if
#endif

    groupname = 'data'
    call hdf5_create_group(file_id, groupname, gr_id)
    dsetname = 'position_halo'
    call hdf5_write_mpi_data(gr_id, dsetname, 3, nh, halocomPos(:,begh:endh), mpicomm, empty)
    dsetname = 'velocity_halo'
    call hdf5_write_mpi_data(gr_id, dsetname, 3, nh, halocomVel(:,begh:endh), mpicomm, empty)
    dsetname = 'identity_halo'
    call hdf5_write_mpi_data(gr_id, dsetname, nh, haloID(begh:endh), mpicomm, empty)
    dsetname = 'npart_halo'
    call hdf5_write_mpi_data(gr_id, dsetname, nh, haloMass(begh:endh), mpicomm, empty)
    dsetname = 'rmax_halo'
    call hdf5_write_mpi_data(gr_id, dsetname, nh, haloRadius(begh:endh), mpicomm, empty)
    if(param_pfof%do_subhalo) then
       dsetname = 'nsubhalo_halo'
       call hdf5_write_mpi_data(gr_id, dsetname, nh, haloSubHaloNB(begh:endh), mpicomm, empty)
    end if

    call hdf5_close_group(gr_id)
    ! Close h5 file
    call hdf5_close_mpi_file(file_id)

#ifdef DEBUG
    print *,'Exit mpih5writehalomass on process ',procID
#endif

  end subroutine mpih5writehalomass

end module modwritehalo

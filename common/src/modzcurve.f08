module modzcurve

  implicit none
  integer(4), dimension(9), parameter :: patternI=(/0,3,6,9 ,12,15,18,21,24/)
  integer(4), dimension(9), parameter :: patternJ=(/1,4,7,10,13,16,19,22,25/)
  integer(4), dimension(9), parameter :: patternK=(/2,5,8,11,14,17,20,23,26/)


contains

  !---------------------------------------
  subroutine init_morton_table(morton_table)
    integer(kind=8), dimension (0:1023), intent(out) :: morton_table
    integer :: b, v, z
    do v=0, 1023
       z = 0
       do b=0, 9
          call mvbits(v,b,1,z,3*b)
       end do
       morton_table(v) = z
    end do
  end subroutine init_morton_table

  !---------------------------------------
  subroutine pos_to_z2(i, j, k, zval, morton_table)
    integer, intent(in) :: i, j, k
    integer(kind=8), dimension (0:1023), intent (in) :: morton_table
    integer(kind=8), intent (out) :: zval
    integer(kind=8) :: z, i8, j8, k8

    i8 = i-1
    j8 = j-1
    k8 = k-1

    z = morton_table(iand(k8, 1023))
    z = z + ishft(morton_table(iand(j8, 1023)),1)
    z = z + ishft(morton_table(iand(i8, 1023)),2)
    z = z + ishft(morton_table(iand(ishft(k8,-10), 1023)),30)
    z = z + ishft(morton_table(iand(ishft(j8,-10), 1023)),31)
    zval = z + ishft(morton_table(iand(ishft(i8,-10), 1023)),32) + 1

  end subroutine pos_to_z2

  !---------------------------------------
  subroutine z_to_pos2(zval,i,j,k,morton_table)

    implicit none
    integer(kind=8), intent(in) :: zval
    integer(kind=4), intent(out) :: i,j,k
    integer(kind=8), dimension(0:1023), intent(in) :: morton_table


  end subroutine z_to_pos2

  !---------------------------------------
  subroutine pos_to_z(i, j, k, zval)

    implicit none

    integer(kind=4), intent(IN)  :: i, j, k
    integer(kind=8), intent(OUT) :: zval
    integer(kind=8) :: i8, j8, k8
    integer(kind=4) :: b

    zval = 0
    i8 = i-1
    j8 = j-1
    k8 = k-1

    do b=0, 19
       call mvbits(i8,b,1,zval,3*b+2)
       call mvbits(j8,b,1,zval,3*b+1)
       call mvbits(k8,b,1,zval,3*b  )
    end do

    zval = zval+1

  end subroutine pos_to_z


  !---------------------------------------
  subroutine z_to_pos(zval, i, j, k)

    implicit none

    integer(kind=8), intent(IN)  :: zval
    integer(kind=4), intent(OUT) :: i, j, k
    integer(kind=8) :: i8, j8, k8, z_order
    integer(kind=4) :: b

    z_order = zval-1
    i8 = 0
    j8 = 0
    k8 = 0

    do b=0, 19
       call mvbits(z_order,3*b+2,1,i8,b)
       call mvbits(z_order,3*b+1,1,j8,b)
       call mvbits(z_order,3*b  ,1,k8,b)
    end do

    i = int(i8,kind=4) + 1
    j = int(j8,kind=4) + 1
    k = int(k8,kind=4) + 1

  end subroutine z_to_pos

  !---------------------------------------
  subroutine coord2zcurve(i, j, k, zind)

    implicit none
    integer(kind=4), intent(in) :: i, j, k
    integer(kind=8), intent(out) :: zind

    integer(kind=4) :: ib
    integer(kind=4) :: is, js, ks

    is = 0
    js = 0
    ks = 0

    do ib = 0, 9
       call mvbits(i,ib,1,is,3*ib)
       call mvbits(j,ib,1,js,3*ib+1)
       call mvbits(k,ib,1,ks,3*ib+2)
    end do

    zind = is + js + ks

  end subroutine coord2zcurve

  !---------------------------------------
  subroutine zcurve2coord(zind, i, j, k)

    implicit none
    integer(kind=8), intent(in) :: zind
    integer(kind=4), intent(out) :: i, j, k

    integer(kind=4), dimension(3,9) :: bittab
    integer(kind=4) :: pos
    integer(kind=4) :: ib

    bittab(1,:) = ibits(zind,patternI,1)
    bittab(2,:) = ibits(zind,patternJ,1)
    bittab(3,:) = ibits(zind,patternK,1)

    do ib = 1, 9
       if(bittab(1,ib)>0) then
          pos = patternI(ib) / 3
          i = ibset( i, pos )
       end if
       if(bittab(2,ib)>0) then
          pos = patternJ(ib) / 3
          j = ibset( j, pos )
       end if
       if(bittab(3,ib)>0) then
          pos = patternK(ib) / 3
          k = ibset( k, pos)
       end if
    end do

  end subroutine zcurve2coord

end module modzcurve

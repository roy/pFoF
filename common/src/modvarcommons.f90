!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Variables used by pfof_snap and pfof_cone
!! @brief
!! 
!! @author Fabrice Roy
!! @author Vincent Bouillot

!> variables used by pfof_snap and pfof_cone
!------------------------------------------------------------------------------------------------------------------------------------
module modvarcommons

  use modconstant, only : IDKIND

  implicit none

  integer(kind=4)   :: local_npart
  !< local dark matter particles number
  integer(kind=4)   :: nres
  !< 1-D resolution: number of grid points in each dimension ; nres = 2 ** lmin
  integer(kind=IDKIND) :: global_npart
  !< total dark matter particles number: npart = nres ** 3
  integer(kind=4) :: local_nstar
  !< local stars number
  integer(kind=IDKIND) :: global_nstar
  !< global stars number

  real(kind=4),    dimension(:,:), allocatable :: position
  !< position of the particles
  real(kind=4),    dimension(:,:), allocatable :: velocity
  !< velocity of the particles
  real(kind=4),    dimension(:,:), allocatable :: field
  !< force on the particles
  real(kind=4),    dimension(:),   allocatable :: potential
  !< potential at the position of the particles (optional)
  real(kind=4), dimension(:), allocatable :: mass
  !< mass of the particles in case of a hydrodynamical simulation
  real(kind=4), dimension(:), allocatable :: birth_date
  !< time of birth for stars in case of a hydrodynamical simulation
  real(kind=4), dimension(:), allocatable :: metallicity
  !< metallicity of the stars in case of a hydrodynamical simulation
  integer(kind=IDKIND), dimension(:), allocatable :: pfof_id
  !< ID of the particles used by pfof to identify particles (= ramses_id for snapshot)
  integer(kind=IDKIND), dimension(:), allocatable :: structure_id
  !< Halo ID of the partickes; integer8 if particle number exceeds 2^31-1
  integer(kind=IDKIND), dimension(:), allocatable :: ramses_id
  !< RAMSES id of the particles; integer8 if particle number exceeds 2^31-1

  real(kind=4),    dimension(:,:), allocatable :: fposition
  !< position of the particles
  real(kind=4),    dimension(:,:), allocatable :: fvelocity
  !< velocity of the particles
  real(kind=4),    dimension(:,:), allocatable :: ffield
  !< force on the particles
  real(kind=4),      dimension(:), allocatable :: fpotential
  !< potential at the position of the particles (optional)
  real(kind=4), dimension(:), allocatable :: fmass
  !< mass of the particles in case of a hydrodynamical simulation
  integer(kind=IDKIND), dimension(:), allocatable :: fpfof_id
  !< ID of the particles used by pfof to identify particles
  integer(kind=IDKIND), dimension(:), allocatable :: fstructure_id
  !< Halo ID of the particles; integer8 if particle number exceeds 2^31-1
  integer(kind=IDKIND), dimension(:), allocatable :: framses_id
  !< RAMSES id of the particles; integer8 if particle number exceeds 2^31-1

  real(kind=4),    dimension(:,:), allocatable :: star_position
  !< position of the stars
  real(kind=4),    dimension(:,:), allocatable :: star_velocity
  !< velocity of the stars
  real(kind=4),    dimension(:,:), allocatable :: star_field
  !< force on the stars
  real(kind=4),    dimension(:),   allocatable :: star_potential
  !< potential at the position of the stars (optional)
  real(kind=4), dimension(:), allocatable :: star_mass
  !< mass of the stars in case of a hydrodynamical simulation
  real(kind=4), dimension(:), allocatable :: star_birth_date
  !< time of birth of the stars in case of a hydrodynamical simulation
  real(kind=4), dimension(:), allocatable :: star_metallicity
  !< metallicity of the stars in case of a hydrodynamical simulation
  integer(kind=IDKIND), dimension(:), allocatable :: star_pfof_id
  !< ID of the stars used by pfof to identify particles (= ramses_id for snapshot)
  integer(kind=IDKIND), dimension(:), allocatable :: star_ramses_id
  !< RAMSES id of the particles; integer8 if particle number exceeds 2^31-1


end module modvarcommons

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Subroutines used to gather haloes and compute some observables.
!! @brief
!! 
!! @author Fabrice Roy
!! @author Vincent Bouillot

!> Subroutines used to gather haloes and compute some observables.
module modhalo

  use iso_fortran_env, only : ERROR_UNIT, &
       OUTPUT_UNIT
  use modconstant, only : ERR_CODE_COMPUTATION, &
       IDKIND, &
       MPI_IDKIND, &
       type_parameter_pfof, &
       type_parameter_pfof_snap, &
       type_parameter_pfof_cone
  use modmpicommons, only : Emergencystop

  implicit none

  private

  public :: Gatherhaloes, &
       Selecthaloes, &
       Computecom, &
       Computeradius, &
       halopartpos, &
       halopartvel, &
       halopartpot, &
       halopartfor, &
       halopartmas, &
       halocompos, &
       halocomvel, &
       haloradius, &
       halosubhalonb, &
       halopartid, &
       haloid, &
       halomass, &
       halonb, &
       halonb_all, &
       halopartnb, &
       halopartramsesid, &
       final_local_npart


  integer(kind=4) :: halonb        !< local number of haloes
  integer(kind=4) :: halonb_all    !< global number of haloes
  integer(kind=4) :: halopartnb    !< number of particles belonging to one of the local haloes
  integer(kind=4) :: final_local_npart  !< number of particles after the particles have been exchanged to gather them by haloes
  integer(kind=4),   dimension(:), allocatable :: halomass       !< mass of the haloes
  integer(kind=IDKIND), dimension(:), allocatable :: haloid         !< ID of the haloes
  integer(kind=IDKIND), dimension(:), allocatable :: halopartid     !< ID of the particles belonging to a halo
  integer(kind=IDKIND), dimension(:), allocatable :: halopartramsesid     !< RAMSES ID of the particles belonging to a halo, used only for lightcone halo
  integer(kind=4),   dimension(:), allocatable :: halosubhalonb  !< number of subhalo for each halo
  real(kind=4), dimension(:,:), allocatable :: halopartpos    !< position of the particles belonging to one of the local haloes
  real(kind=4), dimension(:,:), allocatable :: halopartvel    !< velocity of the particles belonging to one of the local haloes
  real(kind=4), dimension(:), allocatable :: halopartmas   !< mass of the particles belonging to one of the local haloes
  real(kind=4), dimension(:), allocatable :: halopartpot    !< potential of the particles belonging to one of the local haloes
  real(kind=4), dimension(:,:), allocatable :: halopartfor     !< force on the particles belonging to one of the local haloes
  real(kind=8), dimension(:,:), allocatable :: halocompos     !< position of the center of mass of each halo
  real(kind=8), dimension(:,:), allocatable :: halocomvel     !< velocity of the center of mass of each halo
  real(kind=8), dimension(:),   allocatable :: haloradius     !< radius of each halo

contains

  !=======================================================================
  !> Exchange the particles so that particles belonging to one halo are gathered on the same process.
  subroutine Gatherhaloes(mpicomm, param)

    use mpi
    use modvarcommons, only : ffield, fmass, fpfof_id, fposition, fpotential, framses_id, fstructure_id, fvelocity, &
         field, mass, pfof_id, position, potential, ramses_id, structure_id, velocity, global_npart, local_npart
    use modmpicommons, only : procid, procnb

    integer, intent(in) :: mpicomm   !< MPI communicator used for the communications
    class(type_parameter_pfof), intent(in) :: param

    integer(kind=IDKIND), dimension(:), allocatable :: strsend ! structure ID array, and tmp array used for comm
    integer(kind=IDKIND), dimension(:), allocatable :: idsend  ! particle ID array and tmp array used for comm
    integer(kind=IDKIND), dimension(:), allocatable :: ramsesidsend
    real(kind=4), dimension(:,:), allocatable :: possend, velsend, forsend
    real(kind=4), dimension(:), allocatable :: potsend, massend
    integer(kind=4) :: str_pid
    integer(kind=4), dimension(:),allocatable :: str_pid_vec, local_str_pid_vec  ! array of particle nb by process after
    integer(kind=4) :: send_id, recv_id   ! ID for process to send to and to receive from
    integer(kind=4) :: i, ind, iproc
    integer(kind=4) :: allocstat
    integer(kind=4) :: mpistat(MPI_STATUS_SIZE)   ! MPI comm. status
    integer(kind=4) :: nbrec, nbsend    ! nb of elements to recv and to send
    integer(kind=IDKIND) :: str_nb_beg, str_nb_end
    integer(kind=IDKIND) :: smin,smax
    integer(kind=IDKIND) :: tmpdi
    integer(kind=4) :: np_per_proc
    integer(kind=4) :: recvpoint
    integer(kind=4) :: mpierr
    integer(kind=4) :: mpireqs1, mpireqs2, mpireqs3, mpireqs4, mpireqs5, mpireqs6, mpireqs7, mpireqs8
    integer(kind=4) :: mpireqr1, mpireqr2, mpireqr3, mpireqr4, mpireqr5, mpireqr6, mpireqr7, mpireqr8

    logical(kind=4) :: do_read_ramses_part_id


    select type (param)
    type is (type_parameter_pfof_snap)
       do_read_ramses_part_id = .false.
    type is (type_parameter_pfof_cone)
       do_read_ramses_part_id = param%do_read_ramses_part_id
    end select

    tmpdi = global_npart/procnb
    np_per_proc = int(tmpdi, kind=4)
#ifdef LONGINT
    if(mod(global_npart,int(procnb,kind=8)) /= 0) then
       np_per_proc = np_per_proc + 1
    end if
#else
    if(mod(global_npart,procnb) /= 0) then
       np_per_proc = np_per_proc + 1
    end if
#endif

    str_nb_beg = int(np_per_proc,kind=IDKIND) * int(procid,kind=IDKIND) + 1    ! id min de halo sur le process courant
    str_nb_end = int(np_per_proc,kind=IDKIND) * int((procid + 1),kind=IDKIND)  ! id max de halo sur le process courant

    allocate(local_str_pid_vec(procnb), str_pid_vec(procnb))
    local_str_pid_vec = 0  ! nb de particules par process
    str_pid_vec = 0

    do i = 1,local_npart
       str_pid = int((structure_id(i)-1) / np_per_proc, kind=4) + 1  ! id du process ou se trouvera la particule
       local_str_pid_vec(str_pid) = local_str_pid_vec(str_pid) + 1         ! on incremente le nb de particules sur ce process
    end do    

    call mpi_allreduce(local_str_pid_vec, str_pid_vec, procnb, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, mpierr)

#ifdef DEBUG
    write(ERROR_UNIT,*) 'process:', procid, ' ; np_per_proc=', np_per_proc
    write(ERROR_UNIT,*) 'process:', procid, ' ; str_nb_beg=', str_nb_beg
    write(ERROR_UNIT,*) 'process:', procid, ' ; str_nb_end=', str_nb_end
    write(ERROR_UNIT,*) 'process:', procid, ' ; local_str_pid_vec=',local_str_pid_vec
    write(ERROR_UNIT,*) 'process:', procid, ' ; nb of part with structure_id=0 =',count(structure_id==0)
#endif
    
    final_local_npart = str_pid_vec(procid+1)             ! nb de particules sur le process courant apres redistribution selon les halos
    allocate(fposition(3,final_local_npart),STAT=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for fposition in gatherhaloes', allocstat)
    allocate(fvelocity(3,final_local_npart),STAT=allocstat)
    if(allocstat > 0) call EmergencyStop('Allocate failed for fvelocity in gatherhaloes',allocstat)
    allocate(fstructure_id(final_local_npart),STAT=allocstat)
    if(allocstat > 0) call EmergencyStop('Allocate failed for fstructure_id in gatherhaloes',allocstat)
    allocate(fpfof_id(final_local_npart),STAT=allocstat)
    if(allocstat > 0) call EmergencyStop('Allocate failed for fpfof_id in gatherhaloes',allocstat)
    if(.not.param%do_skip_mass) then
       allocate(fmass(final_local_npart),STAT=allocstat)
       if(allocstat > 0) call EmergencyStop('Allocate failed for fmass in gatherhaloes',allocstat)
    end if
    if(param%do_read_potential) then
       allocate(fpotential(final_local_npart),STAT=allocstat)
       if(allocstat > 0) call EmergencyStop('Allocate failed for fpotential in gatherhaloes',allocstat)
    end if
    if(param%do_read_gravitational_field) then
       allocate(ffield(3,final_local_npart),STAT=allocstat)
       if(allocstat > 0) call EmergencyStop('Allocate failed for ffield in gatherhaloes',allocstat)
    end if
    if(do_read_ramses_part_id) then
       allocate(framses_id(final_local_npart),STAT=allocstat)
       if(allocstat > 0) call EmergencyStop('Allocate failed for framses_id in gatherhaloes',allocstat)
    end if

    recvpoint = 1
    procMasse : do iproc = 1,procnb-1
       if(procid==0) write(*,'(A,I5,A)') 'Gatherhalo permutation ',iproc,'...'

       send_id = mod(procid + iproc,procnb)
       recv_id = mod(procid + procnb - iproc,procnb)
       nbsend = local_str_pid_vec(send_id+1)

       call mpi_isend(nbsend, 1, MPI_INTEGER, send_id, send_id, mpicomm, mpireqs1, mpierr)
       call mpi_irecv(nbrec, 1, MPI_INTEGER, recv_id, procid, mpicomm, mpireqr1, mpierr)

       call mpi_wait(mpireqs1, mpistat, mpierr)
       call mpi_wait(mpireqr1, mpistat, mpierr)

       if(nbsend /= 0) then
          allocate(strsend(nbsend), STAT=allocstat)
          if(allocstat > 0) call Emergencystop('Allocate failed for strsend in gatherhaloes',allocstat)
          allocate(possend(3,nbsend), STAT=allocstat)
          if(allocstat > 0) call Emergencystop('Allocate failed for possend in gatherhaloes',allocstat)
          allocate(velsend(3,nbsend),idsend(nbsend), STAT=allocstat)
          if(allocstat > 0) call Emergencystop('Allocate failed for velsend in gatherhaloes',allocstat)
          if(.not.param%do_skip_mass) then
             allocate(massend(nbsend), STAT=allocstat)
             if(allocstat > 0) call Emergencystop('Allocate failed for massend in gatherhaloes',allocstat)
          end if          
          if(param%do_read_potential) then
             allocate(potsend(nbsend), STAT=allocstat)
             if(allocstat > 0) call Emergencystop('Allocate failed for potsend in gatherhaloes',allocstat)
          end if
          if(param%do_read_gravitational_field) then
             allocate(forsend(3,nbsend), STAT=allocstat)
             if(allocstat > 0) call Emergencystop('Allocate failed for forsend in gatherhaloes',allocstat)
          end if
          if(do_read_ramses_part_id) then
             allocate(ramsesidsend(nbsend), STAT=allocstat)
             if(allocstat > 0) call Emergencystop('Allocate failed for ramsesidsend in gatherhaloes',allocstat)
          end if

          smin = int(np_per_proc,kind=IDKIND) * int(send_id,kind=IDKIND) + 1
          smax = int(np_per_proc,kind=IDKIND) * int((send_id+1),kind=IDKIND)

          ind=1
          do i=1, local_npart
             if(structure_id(i)>= smin .and. structure_id(i)<= smax) then
                strsend(ind) = structure_id(i)
                possend(:,ind) = position(:,i)
                velsend(:,ind) = velocity(:,i)
                if(.not.param%do_skip_mass) massend(ind) = mass(i)
                if(param%do_read_potential) potsend(ind) = potential(i)
                if(param%do_read_gravitational_field) forsend(:,ind) = field(:,i)
                if(do_read_ramses_part_id) ramsesidsend(ind) = ramses_id(i)
                idsend(ind) = pfof_id(i)
                ind = ind+1
             end if
          end do

          if(ind /= nbsend +1 ) then
             write(ERROR_UNIT,*) ind, nbsend, send_id, smin, smax, str_pid_vec
             call EmergencyStop('Error  1 while sharing structures for output.',ERR_CODE_COMPUTATION)
          end if

          call mpi_isend(strsend, nbsend,   MPI_IDKIND, send_id, 1, mpicomm, mpireqs1, mpierr)
          call mpi_isend(possend, 3*nbsend, MPI_REAL,   send_id, 2, mpicomm, mpireqs2, mpierr)
          call mpi_isend(velsend, 3*nbsend, MPI_REAL,   send_id, 3, mpicomm, mpireqs3, mpierr)
          call mpi_isend(idsend, nbsend,  MPI_IDKIND,   send_id, 4, mpicomm, mpireqs4, mpierr)
          if(param%do_read_potential) call mpi_isend(potsend, nbsend, MPI_REAL, send_id, 5, mpicomm, mpireqs5, mpierr)
          if(param%do_read_gravitational_field) call mpi_isend(forsend, 3*nbsend, MPI_REAL, send_id, 6, mpicomm, mpireqs6, mpierr)
          if(do_read_ramses_part_id) call mpi_isend(ramsesidsend, nbsend, MPI_IDKIND,&
               send_id, 7, mpicomm, mpireqs7, mpierr)
          if(.not.param%do_skip_mass) call mpi_isend(massend, nbsend, MPI_REAL, send_id, 8, mpicomm, mpireqs8, mpierr)
       end if

       if(nbrec /= 0) then
          call mpi_irecv(fstructure_id(recvpoint), nbrec, MPI_IDKIND, recv_id, 1, mpicomm, mpireqr1, mpierr)
          call mpi_irecv(fposition(1,recvpoint), 3*nbrec, MPI_REAL, recv_id, 2, mpicomm, mpireqr2, mpierr)
          call mpi_irecv(fvelocity(1,recvpoint), 3*nbrec, MPI_REAL, recv_id, 3, mpicomm, mpireqr3, mpierr)
          call mpi_irecv(fpfof_id(recvpoint), nbrec,  MPI_IDKIND, recv_id,4,mpicomm,mpireqr4,mpierr)
          if(param%do_read_potential) call mpi_irecv(fpotential(recvpoint), nbrec,  &
               MPI_REAL, recv_id, 5, mpicomm, mpireqr5, mpierr)
          if(param%do_read_gravitational_field) call mpi_irecv(ffield(1,recvpoint), 3*nbrec, &
               MPI_REAL, recv_id, 6, mpicomm, mpireqr6, mpierr)
          if(do_read_ramses_part_id) call mpi_irecv(framses_id(recvpoint), nbrec, MPI_IDKIND, &
               recv_id, 7, mpicomm, mpireqr7, mpierr)
          if(.not.param%do_skip_mass) call mpi_irecv(fmass(recvpoint), nbrec, MPI_REAL, &
               recv_id, 8, mpicomm, mpireqr8, mpierr)
          recvpoint=recvpoint+nbrec
       end if


       if(nbsend/=0) then
          call mpi_wait(mpireqs1, mpistat, mpierr)
          call mpi_wait(mpireqs2, mpistat, mpierr)
          call mpi_wait(mpireqs3, mpistat, mpierr)
          call mpi_wait(mpireqs4, mpistat, mpierr)
          deallocate(strsend, possend, idsend, velsend)
          if(param%do_read_potential) then
             call mpi_wait(mpireqs5, mpistat, mpierr)
             deallocate(potsend)
          end if
          if(param%do_read_gravitational_field) then
             call mpi_wait(mpireqs6, mpistat, mpierr)
             deallocate(forsend)
          end if
          if(do_read_ramses_part_id) then
             call mpi_wait(mpireqs7, mpistat, mpierr)
             deallocate(ramsesidsend)
          end if
          if(.not.param%do_skip_mass) then
             call mpi_wait(mpireqs8, mpistat, mpierr)
             deallocate(massend)
          end if

       end if
       if(nbrec/=0) then
          call mpi_wait(mpireqr1, mpistat, mpierr)
          call mpi_wait(mpireqr2, mpistat, mpierr)
          call mpi_wait(mpireqr3, mpistat, mpierr)
          call mpi_wait(mpireqr4, mpistat, mpierr)
          if(param%do_read_potential) call mpi_wait(mpireqr5, mpistat, mpierr)
          if(param%do_read_gravitational_field) call mpi_wait(mpireqr6, mpistat, mpierr)
          if(do_read_ramses_part_id) call mpi_wait(mpireqr7, mpistat, mpierr)
          if(.not.param%do_skip_mass) call mpi_wait(mpireqr8, mpistat, mpierr)
       end if

    end do procMasse

    ind= recvpoint
    do i=1, local_npart
       if(structure_id(i)>= str_nb_beg .and. structure_id(i)<= str_nb_end) then
          fstructure_id(recvpoint)  = structure_id(i)
          fposition(:,recvpoint) = position(:,i)
          fvelocity(:,recvpoint) = velocity(:,i)
          fpfof_id(recvpoint)  = pfof_id(i)
          if(.not.param%do_skip_mass) fmass(recvpoint) = mass(i)
          if(param%do_read_potential) fpotential(recvpoint) = potential(i)
          if(param%do_read_gravitational_field) ffield(:,recvpoint) = field(:,i)
          if(do_read_ramses_part_id) framses_id(recvpoint) = ramses_id(i)
          recvpoint = recvpoint+1
       end if
    end do

    deallocate(structure_id)
    deallocate(local_str_pid_vec,str_pid_vec)
    deallocate(position, velocity, pfof_id)
    if(.not.param%do_skip_mass) deallocate(mass)
    if(param%do_read_potential) deallocate(potential)
    if(param%do_read_gravitational_field) deallocate(field)
    if(do_read_ramses_part_id) deallocate(ramses_id)

    if( recvpoint/= final_local_npart +1 ) then
       write(ERROR_UNIT, *) 'Process ', procid,' : recvpoint = ', recvpoint
       write(ERROR_UNIT, *) 'Process ', procid,' : final_local_npart = ', final_local_npart
       call EmergencyStop('Error while gathering haloes in gatherhaloes.', ERR_CODE_COMPUTATION)
    end if


  end subroutine Gatherhaloes


  ! ======================================================================
  ! Select haloes whose mass is >= Mmin
  subroutine Selecthaloes(param)

    use modconstant, only : ERR_MSG_LEN
    use modvarcommons, only : ffield, fmass, fpfof_id, fposition, fpotential, framses_id, fstructure_id, fvelocity
    use modmpicommons, only : procid


    class(Type_parameter_pfof), intent(in) :: param

    integer :: allocstat
    character(len=ERR_MSG_LEN) :: errormsg
    character(len=5) :: idchar
    integer(kind=8) :: hidmin, hidmax
    integer(kind=4) :: nbhid
    integer(kind=4) :: hindex
    integer(kind=4) :: li, lp, fi, fp, h, i
    integer(kind=4), dimension(:), allocatable :: halomasstmp

    logical(kind=4) :: do_read_ramses_part_id


    select type (param)
    type is (Type_parameter_pfof_snap)
       do_read_ramses_part_id = .false.
    type is (Type_parameter_pfof_cone)
       do_read_ramses_part_id = param%do_read_ramses_part_id
    end select

#ifdef DEBUG
    write(OUTPUT_UNIT, *) 'Process ',procid, ' enters selecthaloes'
#endif

    if(final_local_npart /= 0) then
       hidmin = fstructure_id(1)
       hidmax = fstructure_id(final_local_npart)
    else
       hidmin=0
       hidmax=-1
    end if
    nbhid = int(hidmax - hidmin + 1, kind=4)

#ifdef DEBUG
    write(OUTPUT_UNIT, *) 'hidmax=',hidmax,' ; hidmin=',hidmin,' ; nbhid=',nbhid, ' ; final_local_npart=',final_local_npart
#endif

    allocate(halomasstmp(nbhid), STAT=allocstat)

    halomasstmp = 0

    do i=1, final_local_npart
       hindex = int(fstructure_id(i) - hidmin + 1,kind=4)
       halomasstmp(hindex) = halomasstmp(hindex) + 1
    end do

    haloNB = 0
    halopartNB = 0

    ! Compute total nb of particles in halo with M >= Mmin and nb of halos with M >= Mmin
    do i = 1, nbhid
       if(halomasstmp(i) >= param%mmin) then
          haloNB = haloNB + 1
          halopartNB = halopartNB + halomasstmp(i)
       end if
    end do


    ! Keep positions, velocities and id for particles in halo with M >= Mmin, and potential if requested
    allocate(halopartPos(3,halopartNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for halopartpos in selecthaloes', allocstat)
    allocate(halopartVel(3,halopartNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for halopartvel in selecthaloes', allocstat)
    allocate(halopartID(halopartNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for halopartid in selecthaloes', allocstat)
    if(.not.param%do_skip_mass) then
       allocate(halopartmas(halopartnb), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halopartmas in selecthaloes', allocstat)
    end if       
    if(param%do_read_potential) then
       allocate(halopartPot(halopartNB), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halopartpot in selecthaloes', allocstat)
    end if
    if(param%do_read_gravitational_field) then
       allocate(halopartFor(3,halopartNB), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halopartfor in selecthaloes', allocstat)
    end if
    if(do_read_ramses_part_id) then
       allocate(halopartramsesid(halopartNB), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halopartramsesid in selecthaloes', allocstat)
    end if
    ! Keep mass and id for halos with M >= Mmin
    if(haloNB==0) then
       allocate(haloMass(1), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halomass in selecthaloes', allocstat)
       allocate(haloID(1), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for haloid in selecthaloes', allocstat)
    else
       allocate(haloMass(haloNB), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halomass in selecthaloes', allocstat)
       allocate(haloID(haloNB), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for haloid in selecthaloes', allocstat)
    end if
    ! Sub-halo detection is not implemented yet
    ! We allocate halosubhaloNB with a size=1
    allocate(halosubhaloNB(1), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for halosubhalonb in selecthaloes', allocstat)

    fp = 1
    lp = 0
    fi = 1
    li = 0
    h = 1
    do i = 1, nbhid
       if(halomasstmp(i) >= param%mmin) then
          lp = fp + halomasstmp(i) - 1
          li = fi + halomasstmp(i) - 1
          halopartPos(:,fp:lp) = fposition(:,fi:li)
          halopartVel(:,fp:lp) = fvelocity(:,fi:li)
          if(.not.param%do_skip_mass) halopartmas(fp:lp) = fmass(fi:li)
          if(param%do_read_potential) halopartPot(fp:lp) = fpotential(fi:li)
          if(param%do_read_gravitational_field) halopartFor(:,fp:lp) = ffield(:,fi:li)
          if(do_read_ramses_part_id) halopartramsesid(fp:lp) = framses_id(fi:li)
          halopartID(fp:lp) = fpfof_id(fi:li)
          haloMass(h) = halomasstmp(i)
          haloID(h) = fstructure_id(fi)
          fp = lp + 1
          fi = li + 1
          h = h + 1
       else
          li = fi + halomasstmp(i) - 1
          fi = li + 1
       end if
    end do
    if(li /= final_local_npart) then
       write(idchar(1:5), '(I5.5)') procid
       errormsg = 'Error when keeping particles positions in halo with M >= Mmin'// &
            '(li!=final_local_npart) in selecthaloes on process '//idchar
       call emergencystop(errormsg, ERR_CODE_COMPUTATION)
    end if
    if(lp /= halopartNB) then
       write(idchar(1:5), '(I5.5)') procid
       errormsg = 'Error when keeping particles positions in halo with M >= Mmin' // &
            '(lp!=halopartnb) in selecthaloes on process '//idchar
       call emergencystop(errormsg, ERR_CODE_COMPUTATION)
    end if


    if(allocated(fposition)) deallocate(fposition)
    if(allocated(fvelocity)) deallocate(fvelocity)
    if(allocated(fpfof_id)) deallocate(fpfof_id)
    if(allocated(fmass)) deallocate(fmass)
    if(allocated(halomasstmp)) deallocate(halomasstmp)
    if(allocated(fpotential)) deallocate(fpotential)
    if(allocated(ffield)) deallocate(ffield)
    if(allocated(framses_id)) deallocate(framses_id)
  end subroutine selecthaloes


  ! ======================================================================
  ! Computes the position and the velocity of the center of mass for each halo
  subroutine computecom(periodic)

    use mpi

    logical, intent(in) :: periodic

    integer :: allocstat
    integer(kind=4) :: fi, li, h, i, j
    integer(kind=4) :: halom
    real(kind=8), dimension(3) :: delta

    if(haloNB==0) then
       allocate(halocomPos(3,1), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halocompos in computecom', allocstat)
       allocate(halocomVel(3,1), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halocomvel in computecom', allocstat)
    else
       allocate(halocomPos(3,haloNB), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halocomvel in computecom', allocstat)
       allocate(halocomVel(3,haloNB), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halocomvel in computecom', allocstat)
    end if

    halocomPos = 0.d0
    halocomVel = 0.d0

    fi = 1
    do h = 1, haloNB
       halom = 1
       li = fi + haloMass(h) - 1
       halocomPos(:,h) = halopartPos(:,fi)
       halocomVel(:,h) = halopartVel(:,fi)
       do i = fi+1, li
          delta(:) = halocomPos(:,h) / halom - halopartPos(:,i)
          if(periodic) then
             do j = 1, 3
                if(abs(delta(j)) > 0.5d0) then
                   if(delta(j) > 0d0) halocomPos(j,h) = halocomPos(j,h) + 1.0d0
                   if(delta(j) < 0d0) halocomPos(j,h) = halocomPos(j,h) - 1.0d0
                end if
             end do
          end if
          halom = halom + 1
          halocomPos(:,h) = halocomPos(:,h) + halopartPos(:,i)
          halocomVel(:,h) = halocomVel(:,h) + halopartVel(:,i)
       end do
       fi = li + 1

    end do

    do h = 1, haloNB
       halocomPos(:,h) = halocomPos(:,h) / haloMass(h)
       halocomVel(:,h) = halocomVel(:,h) / haloMass(h)
       if(periodic) then
          do j = 1, 3
             if(halocomPos(j,h) > 1.0d0) halocomPos(j,h) = halocomPos(j,h) - 1.0d0
             if(halocomPos(j,h) < 0.0d0) halocomPos(j,h) = halocomPos(j,h) + 1.0d0
          end do
       end if
    end do
    
  end subroutine computecom


  ! ======================================================================
  subroutine computeradius(periodic)


    logical, intent(in) :: periodic

    integer :: allocstat
    integer(kind=4) :: ib
    integer(kind=4) :: id
    integer(kind=4) :: ih
    integer(kind=4) :: ip

    real(kind=8) :: rmax2
    real(kind=8) :: d2
    real(kind=8), dimension(3) :: delta

    if(haloNB==0) then
       allocate(haloRadius(1), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for haloradius in computeradius', allocstat)
    else
       allocate(haloRadius(haloNB), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for haloradius in computeradius', allocstat)
    end if

    ib = 0
    do ih = 1, haloNB
       rmax2 = 0.d0
       do ip = ib+1, ib+haloMass(ih)
          delta(:) = halopartPos(:,ip) - haloComPos(:,ih)
          if(periodic) then
             do id = 1, 3
                if(abs(delta(id)) > 0.5d0) then
                   delta(id) = 1.d0 - abs(delta(id))
                end if
             end do
          end if
          d2 = delta(1)*delta(1) + delta(2)*delta(2) + delta(3)*delta(3)
          if(d2 > rmax2) rmax2 = d2
       end do
       ib = ib + haloMass(ih)
       haloRadius(ih) = sqrt(rmax2)
    end do

    deallocate(halopartPos, halopartVel)


  end subroutine computeradius


end module modhalo

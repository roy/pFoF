!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Contains types for a collection of particles and their allocator
!! @brief
!! 
!! @author Fabrice Roy

!> Contains types for a collection of particles and their allocator
!------------------------------------------------------------------------------------------------------------------------------------
module type_particles_mod

  use iso_fortran_env, only : ERROR_UNIT
  use error_handling_mod, only : Allocate_error
  use modconstant, only : IDKIND
  
  implicit none
  private

  public :: type_particles, type_stars, type_dms, &
       Allocate_stars, Allocate_dms

  !> generic type for a collection of particles
  type :: type_particles
     integer(kind=4) :: size
     logical :: has_position = .true.
     logical :: has_velocity = .true.
     logical :: has_mass = .false. 
     logical :: has_potential = .false.
     logical :: has_field = .false.
     logical :: has_ramses_identity = .false.
     logical :: has_sort_key = .false.
     real(kind=4), allocatable, dimension(:,:) :: position
     real(kind=4), allocatable, dimension(:,:) :: velocity
     real(kind=4), allocatable, dimension(:) :: mass
     real(kind=4), allocatable, dimension(:) :: potential
     real(kind=4), allocatable, dimension(:,:) :: field
     integer(kind=IDKIND), allocatable, dimension(:) :: ramses_identity
     integer(kind=IDKIND), allocatable, dimension(:) :: sort_key
  end type type_particles

  !> type for a collection of stars
  type, extends(type_particles) :: type_stars
     logical :: has_birth_date = .true.
     logical :: has_metallicity = .true.
     real(kind=4), allocatable, dimension(:) :: birth_date
     real(kind=4), allocatable, dimension(:) :: metallicity     
  end type type_stars

  !> type for a collection of dark matter particles
  type, extends(type_particles) :: type_dms
     logical :: has_identity = .true.
     logical :: has_structure = .false.
     integer(kind=IDKIND), allocatable, dimension(:) :: identity
     integer(kind=IDKIND), allocatable, dimension(:) :: structure
  end type type_dms

contains
  !----------------------------------------------------------------------------------------------------------------------------------
  !> allocates arrays for a collection of particles
  subroutine Allocate_particles(particles, npart, process_id)

    class(type_particles), intent(inout) :: particles
    integer(kind=4), intent(in) :: npart
    integer(kind=4), intent(in) :: process_id

    character(len=:), allocatable :: alloc_err_msg
    integer(kind=4) :: alloc_stat

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Enter Allocate_particles on process', process_id
#endif

    particles%size = npart
    
    if(particles%has_position) then
       allocate(particles%position(3,npart), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          call Allocate_error('particles%position','Allocate_particles',alloc_err_msg,alloc_stat, process_id)
       end if
    end if
    
    if(particles%has_velocity) then
       allocate(particles%velocity(3,npart), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          call Allocate_error('particles%velocity','Allocate_particles',alloc_err_msg,alloc_stat, process_id)
       end if
    end if
    
    if(particles%has_mass) then
       allocate(particles%mass(npart), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          call Allocate_error('particles%mass','Allocate_particles',alloc_err_msg,alloc_stat, process_id)
       end if
    end if
    
    if(particles%has_potential) then
       allocate(particles%potential(npart), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          call Allocate_error('particles%potential','Allocate_particles',alloc_err_msg,alloc_stat, process_id)
       end if
    end if

    if(particles%has_field) then
       allocate(particles%field(3,npart), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          call Allocate_error('particles%field','Allocate_particles',alloc_err_msg,alloc_stat, process_id)
       end if
    end if

    if(particles%has_ramses_identity) then
       allocate(particles%ramses_identity(npart), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          call Allocate_error('particles%ramses_identity','Allocate_particles',alloc_err_msg,alloc_stat, process_id)
       end if
    end if
    
    if(particles%has_sort_key) then
       allocate(particles%sort_key(npart), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          call Allocate_error('particles%sort_key','Allocate_particles',alloc_err_msg,alloc_stat, process_id)
       end if
    end if

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Exit Allocate_particles on process', process_id
#endif

  end subroutine Allocate_particles
  
  !----------------------------------------------------------------------------------------------------------------------------------
  !> allocates arrays for a collection of stars
  subroutine Allocate_stars(stars, npart, process_id)

    type(type_stars), intent(inout) :: stars
    integer(kind=4), intent(in) :: npart
    integer(kind=4), intent(in) :: process_id

    character(len=:), allocatable :: alloc_err_msg
    integer(kind=4) :: alloc_stat

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Enter Allocate_stars on process', process_id
#endif

    call Allocate_particles(stars, npart, process_id)
    
    if(stars%has_birth_date) then
       allocate(stars%birth_date(npart), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) call Allocate_error('stars%birth_date','Allocate_stars',alloc_err_msg,alloc_stat, process_id)
    end if
    
    if(stars%has_metallicity) then
       allocate(stars%metallicity(npart), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) call Allocate_error('stars%metallicity','Allocate_stars',alloc_err_msg,alloc_stat, process_id)
    end if
    
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Exit Allocate_stars on process', process_id
#endif

  end subroutine Allocate_stars

  !----------------------------------------------------------------------------------------------------------------------------------
  !> allocates arrays for a collection of dark matter particles
  subroutine Allocate_dms(dms, npart, process_id)

    type(type_dms), intent(inout) :: dms
    integer(kind=4), intent(in) :: npart
    integer(kind=4), intent(in) :: process_id

    character(len=:), allocatable :: alloc_err_msg
    integer(kind=4) :: alloc_stat
    
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Enter Allocate_dms on process', process_id
#endif

    call Allocate_particles(dms, npart, process_id)

    if(dms%has_identity) then
       allocate(dms%identity(npart), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          call Allocate_error('dms%identity','Allocate_dms',alloc_err_msg,alloc_stat, process_id)
       end if
    end if
    
    if(dms%has_structure) then
       allocate(dms%structure(npart), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) call Allocate_error('dms%structure','Allocate_dms',alloc_err_msg,alloc_stat, process_id)
    end if

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Exit Allocate_dms on process', process_id
#endif
    
  end subroutine Allocate_dms

end module type_particles_mod

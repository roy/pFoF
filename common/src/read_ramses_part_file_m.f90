!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Subroutine to read one RAMSES part.out file
!! @brief
!! 
!! @author Fabrice Roy

!> Subroutine to read one RAMSES part.out file
module read_ramses_part_file_m

  implicit none

  private
  public :: Read_ramses_part_file

contains

  !> Read a part. file produced by Ramses
  subroutine Read_ramses_part_file(local_nparts, deltasd, nsd, filename, parameters, positions, velocities, identities, cube_ids, &
       masses, birth_dates, metallicities, fields, potentials)

    use iso_fortran_env
    use modconstant, only : ERR_MSG_LEN, IDKIND, type_parameter_pfof_snap
    use modindex, only : coord_to_id, id_to_coord
    use modmpicommons, only : Emergencystop, procnb

    integer(kind=4), dimension(procnb), intent(inout) :: local_nparts
    real(kind=4), intent(in) :: deltasd
    integer(kind=4), intent(in) :: nsd
    character(len=*), intent(in) :: filename
    type(type_parameter_pfof_snap), intent(in) :: parameters
    real(kind=4), dimension(:,:), intent(out) :: positions
    real(kind=4), dimension(:,:), intent(out) :: velocities
    integer(kind=IDKIND), dimension(:), intent(out) :: identities
    integer(kind=IDKIND), dimension(:), intent(out) :: cube_ids
    real(kind=4), dimension(:), intent(out), optional :: masses
    real(kind=4), dimension(:), intent(out), optional :: birth_dates
    real(kind=4), dimension(:), intent(out), optional :: metallicities
    real(kind=4), dimension(:,:), intent(out), optional :: fields
    real(kind=4), dimension(:), intent(out), optional :: potentials

    character(ERR_MSG_LEN) :: error_msg
    integer(kind=4) :: ioerr
    integer(kind=4) :: part_unit
    integer(kind=4) :: ndim
    integer(kind=4) :: npart
    integer(kind=4) :: allocstat
    integer(kind=4) :: idim
    integer(kind=4) :: ipart
    integer(kind=4) :: n_i, n_j, n_k
    integer(kind=IDKIND) :: ind
    integer(kind=4) :: int4_tmp
    real(kind=4) :: real4_tmp
    real(kind=8) :: real8_tmp
    real(kind=8), allocatable, dimension(:) :: real8_tmps

#ifdef DEBUG
   write(ERROR_UNIT,*) 'number of particles expected in file ',trim(filename),' : ',&
   ubound(positions,dim=2)-lbound(positions, dim=2)+1
#endif

    open(newunit=part_unit, file=filename, status='old', action='read', form='unformatted', iostat=ioerr, iomsg=error_msg)
#ifdef DEBUG
    if(ioerr/=0) then
       call Emergencystop('Error opening file '//trim(filename)//' in Read_ramses_part.',ioerr)
    end if
#endif
    read(part_unit, iostat=ioerr, iomsg=error_msg) int4_tmp
#ifdef DEBUG
    if(ioerr/=0) then
       write(ERROR_UNIT,*) trim(error_msg)
       call Emergencystop('Error reading int4_tmp in file '//trim(filename)//' in Read_ramses_part.',ioerr)
    end if
#endif
    read(part_unit, iostat=ioerr, iomsg=error_msg) ndim
#ifdef DEBUG
    if(ioerr/=0) then
      write(ERROR_UNIT,*) trim(error_msg)
       call Emergencystop('Error reading ndim in file '//trim(filename)//' in Read_ramses_part.',ioerr)
    end if
#endif
    read(part_unit, iostat=ioerr, iomsg=error_msg) npart
#ifdef DEBUG
    if(ioerr/=0) then
      write(ERROR_UNIT,*) trim(error_msg)
       call Emergencystop('Error reading npart in file '//trim(filename)//' in Read_ramses_part.',ioerr)
    end if
#endif

    ramsesV2 : if(parameters%code_index.eq.'RA2') then
       ! Read positions
       do idim = 1, ndim
          read(part_unit, iostat=ioerr) positions(idim,1:npart)
#ifdef DEBUG
          if(ioerr/=0) then
             call Emergencystop('Error reading positions in file '//trim(filename)//' in Read_ramses_part.',ioerr)
          end if
#endif
       end do

       ! Read velocities
       do idim = 1, ndim
          read(part_unit, iostat=ioerr) velocities(idim,1:npart)
#ifdef DEBUG
          if(ioerr/=0) then
             call Emergencystop('Error reading velocities in file '//trim(filename)//' in Read_ramses_part.',ioerr)
          end if
#endif
       end do

       if(parameters%do_skip_mass) then
          read(part_unit, iostat=ioerr) real4_tmp
#ifdef DEBUG
          if(ioerr/=0) then
             call Emergencystop('Error reading masses in dummy variable in file '//trim(filename)//' in Read_ramses_part.',ioerr)
          end if
#endif
       else
          ! Read masses if needed
          read(part_unit, iostat=ioerr) masses(:)
#ifdef DEBUG
          if(ioerr/=0) then
             call Emergencystop('Error reading masses in file '//trim(filename)//' in Read_ramses_part.',ioerr)
          end if
#endif
       end if
    end if ramsesV2

    ramsesV3 : if(parameters%code_index == 'RA3') then
       allocate(real8_tmps(npart), stat=allocstat)
       if(allocstat > 0) call Emergencystop('Allocate failed for real8_tmps in Read_ramses_part', allocstat)

       read(part_unit, iostat=ioerr)
       read(part_unit, iostat=ioerr)
       read(part_unit, iostat=ioerr)
       read(part_unit, iostat=ioerr)
       read(part_unit, iostat=ioerr)
       ! Read positions
       do idim = 1, ndim
          read(part_unit, iostat=ioerr) real8_tmps
#ifdef DEBUG
          if(ioerr/=0) then
             call Emergencystop('Error reading positions in file '//trim(filename)//' in Read_ramses_part.',ioerr)
          end if
#endif
          positions(idim,1:npart) = real(real8_tmps, kind=4)
       end do

       ! Read velocities
       do idim = 1, ndim
          read(part_unit, iostat=ioerr) real8_tmps
#ifdef DEBUG
          if(ioerr/=0) then
             call Emergencystop('Error reading velocities in file '//trim(filename)//' in Read_ramses_part.',ioerr)
          end if
#endif
          velocities(idim,1:npart) = real(real8_tmps(:),kind=4)
       end do

       if(parameters%do_skip_mass) then
          read(part_unit, iostat=ioerr) real8_tmp
#ifdef DEBUG
          if(ioerr/=0) then
             call Emergencystop('Error reading masses in dummy variable in file '//trim(filename)//' in Read_ramses_part.',ioerr)
          end if
#endif
       else
          ! Read masses if needed
          read(part_unit, iostat=ioerr) real8_tmps
#ifdef DEBUG
          if(ioerr/=0) then
             call Emergencystop('Error reading masses in file '//trim(filename)//' in Read_ramses_part.',ioerr)
          end if
#endif
          masses(:) = real(real8_tmps, kind=4)
       end if
    end if ramsesV3

    ! Read particle id
    read(part_unit, iostat=ioerr) identities(1:npart)
#ifdef DEBUG
    if(ioerr/=0) then
       call Emergencystop('Error reading identities in file '//trim(filename)//' in Read_ramses_part.',ioerr)
    end if
#endif

    ! First: skip the level (integer 4)
    read(part_unit, iostat=ioerr) int4_tmp
#ifdef DEBUG
    if(ioerr/=0) then
       call Emergencystop('Error reading level in dummy variable in file '//trim(filename)//' in Read_ramses_part.',ioerr)
    end if
#endif
    
    ! Read potential if potential parameters is .true.
    if(parameters%do_read_potential) then
       !  same kind as position and velocity
       read(part_unit, iostat=ioerr) potentials(:)
#ifdef DEBUG
       if(ioerr/=0) then
          call Emergencystop('Error reading potentials in file '//trim(filename)//' in Read_ramses_part.',ioerr)
       end if
#endif
    end if

    ! Read force if force parameters is .true.
    if(parameters%do_read_gravitational_field) then
       !  same kind as position and velocity
       do idim = 1,ndim
          read(part_unit, iostat=ioerr) fields(idim,:)
#ifdef DEBUG
          if(ioerr/=0) then
             call Emergencystop('Error reading fields in file '//trim(filename)//' in Read_ramses_part.',ioerr)
          end if
#endif
       end do
    end if


    if(parameters%star) then
       if(parameters%do_skip_star) then
          read(part_unit, iostat=ioerr) real8_tmp
#ifdef DEBUG
          if(ioerr/=0) then
             call Emergencystop('Error reading birth_dates in dummy variable in file '&
                  //trim(filename)//' in Read_ramses_part.',ioerr)
          end if
#endif
       else
          read(part_unit, iostat=ioerr) real8_tmps
#ifdef DEBUG
          if(ioerr/=0) then
             call Emergencystop('Error reading birth_dates in file '//trim(filename)//' in Read_ramses_part.',ioerr)
          end if
#endif
          birth_dates(:) = real(real8_tmps(:),kind=4)
       end if
       if(parameters%metal) then
          if(parameters%do_skip_metal) then
             read(part_unit, iostat=ioerr) real8_tmp
#ifdef DEBUG
             if(ioerr/=0) then
                call Emergencystop('Error reading metallicities in dummy variable in file '&
                     //trim(filename)//' in Read_ramses_part.',ioerr)
             end if
#endif
          else
             read(part_unit, iostat=ioerr) real8_tmps
#ifdef DEBUG
             if(ioerr/=0) then
                call Emergencystop('Error reading metallicities in file '//trim(filename)//' in Read_ramses_part.',ioerr)
             end if
#endif
             metallicities(:) = real(real8_tmps(:),kind=4)
          end if
       end if
    end if

    deallocate(real8_tmps)
    

    
    ! count the particles in each cube
    do ipart = 1, npart
       do idim = 1, ndim
          if(abs(positions(idim,ipart)-1.0e0) < 1.e-8) positions(idim,ipart) = 0.e0
       end do
       n_i = int(positions(1,ipart)/deltasd)
       n_j = int(positions(2,ipart)/deltasd)
       n_k = int(positions(3,ipart)/deltasd)
       call coord_to_id((/n_i,n_j,n_k/),ind,(/nsd,nsd,nsd/))
       local_nparts(ind) = local_nparts(ind)+1
       cube_ids(ipart) = ind
    end do

    close(part_unit)

  end subroutine Read_ramses_part_file

end module read_ramses_part_file_m

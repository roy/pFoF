!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Class for the timing of the applications of the software package
!! @brief
!! 
!! @author Fabrice Roy

!> Class for the timing of the applications of the software package
!----------------------------------------------------------------------------------------------------------------------------------

module timer_m

  use mpi 

  implicit none

  private
  public :: timer_t

  type :: timer_t
     real(kind=8), private :: begin = 0.d0                  !< beginning of the execution
     real(kind=8), private :: communication = 0.d0          !< time spend in mpi communications
     real(kind=8), private :: computation = 0.d0            !< time spend in computations
     real(kind=8), private :: end = 0.d0                     !< end of the execution 
     real(kind=8), private :: input = 0.d0                  !< time for input
     real(kind=8), private :: output = 0.d0                 !< time used to distribute the particles if read from Ramses files
     real(kind=8), private :: reference = 0.d0
   contains
     procedure :: Finalize => Finalize_timer
     procedure :: Inc_comm => Increment_communication
     procedure :: Inc_comp => Increment_computation
     procedure :: Inc_inp => Increment_input
     procedure :: Inc_out => Increment_output
     procedure :: Init => Init_timer
     procedure :: Print => Print_timer
     procedure :: Set_ref => Set_reference
  end type timer_t

contains
  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Finalize_timer(this)

    class(timer_t), intent(inout) :: this

    this%end = mpi_wtime()

  end subroutine Finalize_timer


  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Increment_communication(this)

    class(timer_t), intent(inout) :: this

    real(kind=8) :: comm_time
    real(kind=8) :: ref

    ref = mpi_wtime()
    comm_time = ref - this%reference
    this%communication = this%communication + comm_time
    this%reference = ref

  end subroutine Increment_communication

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Increment_computation(this)

    class(timer_t), intent(inout) :: this

    real(kind=8) :: comp_time
    real(kind=8) :: ref

    ref = mpi_wtime()
    comp_time = ref - this%reference
    this%computation = this%computation + comp_time
    this%reference = ref

  end subroutine Increment_computation

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Increment_input(this)

    class(timer_t), intent(inout) :: this

    real(kind=8) :: input_time
    real(kind=8) :: ref

    ref = mpi_wtime()
    input_time = ref - this%reference
    this%input = this%input + input_time
    this%reference = ref

  end subroutine Increment_input

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Increment_output(this)

    class(timer_t), intent(inout) :: this

    real(kind=8) :: output_time
    real(kind=8) :: ref

    ref = mpi_wtime()
    output_time = ref - this%reference
    this%output = this%output + output_time
    this%reference = ref

  end subroutine Increment_output

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Init_timer(this)

    class(timer_t),intent(inout) :: this

    this%begin = mpi_wtime()
    this%reference = this%begin

  end subroutine Init_timer

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Print_timer(this, unit)

    class(timer_t), intent(in) :: this
    integer, intent(in) :: unit

    write(unit, '(a)')        'Timing (in seconds):'
    write(unit, '(a,es10.3)') '  input: ', this%input
    write(unit, '(a,es10.3)') '  computation: ', this%computation
    write(unit, '(a,es10.3)') '  communication: ', this%communication
    write(unit, '(a,es10.3)') '  output: ', this%output
    write(unit, '(a,es10.3)') '= total: ', this%end - this%begin 

  end subroutine Print_timer

  !----------------------------------------------------------------------------------------------------------------------------------
  subroutine Set_reference(this)

    class(timer_t),intent(inout) :: this

    this%reference = mpi_wtime()

  end subroutine Set_reference

end module timer_m

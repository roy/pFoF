!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2015 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Type definitions for Ramses informations read from info_* files and info_cone_* files
!! @brief
!! 
!! @author Fabrice Roy

!> Type definitions for Ramses informations read from info_* files and info_cone_* files
module type_info_ramses_mod

  implicit none

  private
  public :: type_info_cone, type_info_cone_grav, type_info_cone_part, type_info_ramses
  
  ! type declaration for information read from cone info file
  type :: type_info_cone !< type for information read from cone info file
     integer(kind=4) :: ncpu
     integer(kind=4) :: nstride
     integer(kind=4) :: nstep_coarse
     integer(kind=4) :: cone_id
     integer(kind=4) :: nglobalfile
     integer(kind=4) :: isfullsky
     integer(kind=4) :: future
     real(kind=8) :: aexp
     real(kind=8) :: observer_x
     real(kind=8) :: observer_y
     real(kind=8) :: observer_z
     real(kind=8) :: observer_rds
     real(kind=8) :: cone_zlim
     real(kind=8) :: amax
     real(kind=8) :: amin
     real(kind=8) :: zmax
     real(kind=8) :: zmin
     real(kind=8) :: dmax
     real(kind=8) :: dmin
     real(kind=8) :: dtol
     real(kind=8) :: thetay
     real(kind=8) :: thetaz
     real(kind=8) :: theta
     real(kind=8) :: phi
     real(kind=8) :: aendconem2
     real(kind=8) :: aendconem1
     real(kind=8) :: aendcone
     real(kind=8) :: zendconem2
     real(kind=8) :: zendconem1
     real(kind=8) :: zendcone
     real(kind=8) :: dendconem2
     real(kind=8) :: dendconem1
     real(kind=8) :: dendcone
  end type type_info_cone

  type, extends(type_info_cone) :: type_info_cone_part !< extension of type_info_cone for particles cone
     integer(kind=8) :: npart          !! nglobalcell in the file
     real(kind=8) :: aexpold
     real(kind=8) :: zexpold
     real(kind=8) :: zexp
     real(kind=8) :: dexpold
     real(kind=8) :: dexp
  end type type_info_cone_part
  
  type, extends(type_info_cone) :: type_info_cone_grav !< extension of type_info_cone for cells cone
     integer(kind=4) :: nlevel
     integer(kind=4) :: levelmin
     integer(kind=4) :: levelmax
     integer(kind=8) :: nglobalcell
  end type type_info_cone_grav
  
  ! type declaration for information read from ramses info file
  type :: type_info_ramses !< type for information read from ramses info file
     integer(kind=4) :: ncpu
     integer(kind=4) :: ndim          !< number of dimensions
     integer(kind=4) :: levelmin          !< minimum mesh refinement level
     integer(kind=4) :: levelmax
     integer(kind=4) :: ngridmax
     integer(kind=4) :: nstep_coarse
     real(kind=8) :: boxlen
     real(kind=8) :: time
     real(kind=8) :: aexp
     real(kind=8) :: h0
     real(kind=8) :: omega_m
     real(kind=8) :: omega_l
     real(kind=8) :: omega_k
     real(kind=8) :: omega_b
     real(kind=8) :: unit_l
     real(kind=8) :: unit_d
     real(kind=8) :: unit_t
     character(len=80) :: ordering
     real(kind=8), dimension(:), allocatable :: hilbert_bound_key
!!$   contains
!!$     procedure :: print_info_ramses
  end type type_info_ramses

contains

!!$  !> Print process
!!$  subroutine print_info_ramses(info_ramses, unit_number)
!!$    
!!$    class(type_info_ramses), intent(in) :: info_ramses
!!$    integer, intent(in) :: unit_number
!!$    
!!$    write(unit_number,'(a)') 'RAMSES info: '
!!$    write(unit_number,'(a,i9)') 'number of cpu: ', info_ramses%ncpu
!!$    write(unit_number,'(a,i2)') 'number of dimensions: ', info_ramses%ndim
!!$    write(unit_number,'(a,i3)') 'AMR min level: ', info_ramses%levelmin
!!$    write(unit_number,'(a,i3)') 'AMR max level: ', info_ramses%levelmax
!!$    write(unit_number,'(a,i15)') 'max ngrid: ', info_ramses%ngridmax
!!$    write(unit_number,'(a,i10)') 'coarse nstep: ', info_ramses%nstep_coarse
!!$    write(unit_number,'(a,e23.16)') 'length of simulation box: ', info_ramses%boxlen
!!$    write(unit_number,'(a,e23.16)') 'time: ', info_ramses%time
!!$    write(unit_number,'(a,e23.16)') 'aexp: ', info_ramses%aexp
!!$    write(unit_number,'(a,e23.16)') 'h0: ', info_ramses%h0
!!$    write(unit_number,'(a,e23.16)') 'omega_m: ', info_ramses%omega_m
!!$    write(unit_number,'(a,e23.16)') 'omega_b: ', info_ramses%omega_l
!!$    write(unit_number,'(a,e23.16)') 'omega_k: ', info_ramses%omega_k
!!$    write(unit_number,'(a,e23.16)') 'omega_b: ', info_ramses%omega_b
!!$    write(unit_number,'(a,e23.16)') 'unit_l: ', info_ramses%unit_l
!!$    write(unit_number,'(a,e23.16)') 'unit_d: ', info_ramses%unit_d
!!$    write(unit_number,'(a,e23.16)') 'unit_t: ', info_ramses%unit_t
!!$    write(unit_number,'(a,a)') 'ordering of AMR cells: ', info_ramses%ordering
!!$    if(trim(info_ramses%ordering) == 'hilbert') then
!!$       if(allocated(info_ramses%hilbert_bound_key)) then
!!$          write(unit_number,'(a,e23.16)') 'max value of hilbert bound key : ', maxval(info_ramses%hilbert_bound_key)
!!$          write(unit_number,'(a,e23.16)') 'min value of hilbert bound key : ', minval(info_ramses%hilbert_bound_key)
!!$       end if
!!$    end if
!!$  end subroutine print_info_ramses
  
end module type_info_ramses_mod

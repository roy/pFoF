Program zcurve

  Use modzcurve
  Implicit None

  Integer(4) :: ib
  Integer(4) :: i, j, k
  Integer(8) :: z1, z2
  Integer(4) :: ishifted, jshifted, kshifted
  Integer(4), dimension(9) :: pattern, bittab
  Integer(4) :: is, js, ks, pos
  Integer(4) :: nmax
  Integer(8), dimension(0:1023) :: table
  
  i = 57
  j = 61
  k = 56
  
  ishifted = 0
  jshifted = 0
  kshifted = 0

  Do ib = 0, 9
     Call mvbits(i,ib,1,ishifted,3*ib)
     Call mvbits(j,ib,1,jshifted,3*ib+1)
     Call mvbits(k,ib,1,kshifted,3*ib+2)
  End Do
  
  z1 = ishifted + jshifted + kshifted

  z2 = ior(ishifted,jshifted)
  z2 = ior(z2, kshifted)

!  pattern = (/0,3,6,9,12,15,18,21,24/)
!  pattern = (/1,4,7,10,13,16,19,22,25/)
  pattern = (/2,5,8,11,14,17,20,23,26/)
  bittab = ibits(z1,pattern,1)

  

  Do ib = 1, 9
     If(bittab(ib)>0) then
        pos = pattern(ib) / 3
        is = ibset( is, pos )
!        pos = pattern(ib) / 3
!        js = ibset( js, pos )
     End If
  End Do

  Print *,'is :',is

  Print *, i, ishifted
  Print *, j, jshifted
  Print *, k, kshifted

  Print *,'z1=',z1 
  Print *,'z2=',z2 

  Call coord2zcurve(i,j,k,z1)
  
  Print *,'z module =',z1

  Call zcurve2coord(z1,is,js,ks) 
  Print *,'coord module=',is,js,ks

  nmax=  1000
  
!!$  Do i = 1, 10
!!$     Do j = 1, nmax
!!$        Do k = 1, nmax
!!$  
           Print *,' '
           Print *,'i,j,k=',i,j,k
!!$           
!!$           Call pos_to_z(i,j,k,z1)
!!$!           Print *,'z=',z1
!!$           
!!$           Call z_to_pos(z1,is,js,ks)
!!$!           Print *,'i,j,k=',is,js,ks
!!$
!!$        End Do
!!$     End Do
!!$  End Do


  Call init_morton_table(table)


  nmax = 2
  Do i = 1, nmax
     Do j = 1, nmax
        Do k = 1, nmax
           
           Print *,' '
           Print *,'i,j,k=',i,j,k
           
           Call pos_to_z2(i,j,k,z1,table)
           Print *,'z=',z1
           
           Call z_to_pos(z1,is,js,ks)
           Print *,'i,j,k=',is,js,ks

        End Do
     End Do
  End Do

  i = 16384
  j = 16384
  k = 16384
  Call pos_to_z2(i,j,k,z1,table)
  Print *,'z=',z1
  Call z_to_pos(z1,is,js,ks)
  Print *,'i,j,k=',is,js,ks
  
End Program zcurve


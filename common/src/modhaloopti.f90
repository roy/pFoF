module modhaloopti

  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT

  use modconstant, only : ERR_CODE_COMPUTATION, &
       IDKIND, &
       MPI_IDKIND, &
       Type_parameter_pfof, &
       Type_parameter_pfof_snap, &
       Type_parameter_pfof_cone

  use modmpicommons, only : emergencystop, procid, procnb

  implicit none

  private

  public :: gatherhalolb, &
       halopartpos, &
       halopartvel, &
       halopartpot, &
       halopartfor, &
       halocompos, &
       halocomvel, &
       haloradius, &
       halosubhalonb, &
       halopartid, &
       haloid, &
       halomass, &
       halonb, &
       halonb_all, &
       halopartnb, &
       halopartramsesid, &
       final_local_npart


  integer(kind=4) :: haloNB        !< local number of haloes
  integer(kind=4) :: haloNB_all    !< global number of haloes
  integer(kind=4) :: halopartNB    !< number of particles belonging to one of the local haloes
  integer(kind=4) :: final_local_npart  !< number of particles after the particles have been exchanged to gather them by haloes
  integer(kind=4),   dimension(:),   allocatable :: haloMass       !< mass of the haloes
  integer(kind=IDKIND), dimension(:),   allocatable :: haloID         !< ID of the haloes
  real(kind=8),      dimension(:),   allocatable :: haloRadius     !< radius of each halo
  integer(kind=4),   dimension(:),   allocatable :: haloSubHaloNB  !< number of subhalo for each halo
  integer(kind=IDKIND), dimension(:),   allocatable :: halopartID     !< ID of the particles belonging to a halo
  integer(kind=IDKIND), dimension(:),   allocatable :: halopartramsesID     !< RAMSES ID of the particles belonging to a halo, used only for lightcone halo
  real(kind=4),      dimension(:,:), allocatable :: halopartPos    !< position of the particles belonging to one of the local haloes
  real(kind=4),      dimension(:,:), allocatable :: halopartVel    !< velocity of the particles belonging to one of the local haloes
  real(kind=4),      dimension(:),   allocatable :: halopartPot    !< potential of the particles belonging to one of the local haloes
  real(kind=4),      dimension(:,:), allocatable :: halopartFor     !< force on the particles belonging to one of the local haloes
  real(kind=8),      dimension(:,:), allocatable :: halocomPos     !< position of the center of mass of each halo
  real(kind=8),      dimension(:,:), allocatable :: halocomVel     !< velocity of the center of mass of each halo
  integer(kind=4),   dimension(:,:), allocatable :: need_to_communicate


contains

  !======================================================================
  subroutine gatherhaloLB(param, mpicomm)

    use mpi
    use modsort, only : heapsort
    use modvarcommons, only : local_npart,&
         field, &
         pfof_id,&
         position,&
         potential,&
         structure_id,&
         velocity
    use compute_halo_properties_mod, only : compute_halo_mass_and_id

    integer(kind=4),            intent(in) :: mpicomm
    class(Type_parameter_pfof), intent(in) :: param

    integer :: allocstat
    integer(kind=4)                                :: local_structure_number
    integer(kind=IDKIND), dimension(:,:), allocatable :: local_part_number_per_structure
    integer(kind=IDKIND), dimension(:,:), allocatable :: global_particles_count
    integer(kind=IDKIND), dimension(:,:), allocatable :: distributed_halo_size
    integer(kind=4)                                :: local_selected_halo_number
    integer(kind=4),   dimension(:),   allocatable :: global_selected_halo_pid
    integer(kind=4),   dimension(:),   allocatable :: local_selected_halo_pid
    integer(kind=IDKIND), dimension(:,:), allocatable :: particle_disp
    integer(kind=IDKIND), dimension(:),   allocatable :: displacement_particles_exchange
    integer(kind=IDKIND)                              :: local_selected_particle_number
    integer(kind=IDKIND), dimension(:),   allocatable :: number_of_particles_to_send_to
    integer(kind=4)                                :: final_local_halo_number
    integer(kind=IDKIND), dimension(:),   allocatable :: finalstructureid
    integer(kind=4)                                :: mpierr

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter gatherhaloLB on process ',procID
#endif

    ! sort the particles according structure_id
    if(param%do_read_gravitational_field) then
       if(param%do_read_potential) then
          call heapsort(local_npart, structure_id, position, velocity, field, potential, pfof_id) !, ramses_id)
       else
          call heapsort(local_npart, structure_id, position, velocity, field, pfof_id)
       end if
    else
       if(param%do_read_potential) then
          call heapsort(local_npart, structure_id, position, velocity, potential, pfof_id) !, ramses_id)
       else
          call heapsort(local_npart, structure_id, position, velocity, pfof_id)
       end if
    end if

    ! count particles in each halo on the current process
    call count_particles_number(local_npart, structure_id, local_part_number_per_structure, local_structure_number)

    ! initialize global_particles_count with local_part_number_per_structure
    allocate(global_particles_count(2,local_structure_number), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for global_particles_count in gatherhalolb', allocstat)
    global_particles_count = local_part_number_per_structure

    ! merge particles numbers in each halo across the processes
    call merge_particles_number(local_structure_number,global_particles_count)

    ! select halos with mass > param%mmin in global count
    call select_halos(param, global_particles_count, distributed_halo_size)
    deallocate(global_particles_count)

    ! select "local" halos (or halo parts) that belong to final halo list
    call select_local_halos(distributed_halo_size, local_part_number_per_structure, &! local_halo_size, &
         global_selected_halo_pid, local_selected_halo_pid, local_selected_halo_number, mpicomm)

    ! subhalo detection not implemented yet: allocation for compatibility purpose
    allocate(halosubhaloNB(1), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for halosubhalonb in gatherhalolb', allocstat)

    ! for each selected halo: assign halo to a process
    call dispatch_halos(local_part_number_per_structure, distributed_halo_size, &
         local_selected_halo_pid, global_selected_halo_pid, mpicomm, displacement_particles_exchange, &
         local_selected_particle_number, number_of_particles_to_send_to, final_local_halo_number)

    ! compute displacement to the first particle of each selected halo in pos/vel/id/etc... arrays
    call compute_particle_disp(local_selected_halo_number, local_selected_halo_pid, local_part_number_per_structure, particle_disp)

    call dispatch_particles(finalstructureid, displacement_particles_exchange, local_part_number_per_structure, &
         local_selected_halo_pid, local_selected_particle_number,&
         mpicomm, number_of_particles_to_send_to, param, particle_disp)

    if(param%do_read_gravitational_field) then
       if(param%do_read_potential) then
          call heapsort(int(local_selected_particle_number, kind=4),finalstructureid,halopartpos,&
               halopartvel,halopartfor,halopartpot,halopartid)
       else
          call heapsort(int(local_selected_particle_number, kind=4),finalstructureid,halopartpos,&
               halopartvel,halopartfor,halopartid)
       end if
    else
       if(param%do_read_potential) then
          call heapsort(int(local_selected_particle_number, kind=4),finalstructureid,halopartpos,&
               halopartvel,halopartpot,halopartid)
       else
          call heapsort(int(local_selected_particle_number, kind=4),finalstructureid,halopartpos,&
               halopartvel,halopartid)
       end if
    end if


    if(final_local_halo_number==0) then
       allocate(haloMass(1), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halomass in gatherhalolb', allocstat)
       allocate(haloID(1), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for haloid in gatherhalolb', allocstat)
    else
       allocate(haloMass(final_local_halo_number), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halomass in gatherhalolb', allocstat)
       allocate(haloID(final_local_halo_number), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for haloid in gatherhalolb', allocstat)
    end if

    call compute_halo_mass_and_id(haloid, halomass, final_local_halo_number, finalstructureid)

    halopartNB = ubound(finalstructureid,dim=1)
    haloNB = final_local_halo_number
    call Mpi_AllReduce(haloNB,haloNB_all,1,Mpi_Integer,Mpi_Sum,Mpi_Comm_World,mpierr)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Exit gatherhaloLB on process ',procID
#endif


  end subroutine gatherhaloLB


  !======================================================================
  ! count the number of particles with same halo id for each halo id on the local process
  ! structure_id is ordered
  subroutine count_particles_number(local_npart, structure_id, local_part_number_per_structure, structure_number)

    integer(kind=4), intent(in) :: local_npart
    integer(kind=IDKIND), intent(in), dimension(:) :: structure_id
    integer(kind=IDKIND), intent(out), allocatable, dimension(:,:) :: local_part_number_per_structure
    integer(kind=4), intent(out) :: structure_number

    integer :: allocstat
    integer(kind=4) :: ip
    integer(kind=IDKIND) :: current_id
    integer(kind=4) :: current_value

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Exit count_particles_number on process ',procID
#endif

    current_id = structure_id(1)
    structure_number = 1
    do ip = 2, local_npart
       if(structure_id(ip) /= current_id) then
          structure_number = structure_number + 1
          current_id = structure_id(ip)
       end if
    end do

    allocate(local_part_number_per_structure(2,structure_number), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for local_part_number_per_structure in count_particles_number',&
         allocstat)
    local_part_number_per_structure = 0

    current_value = 1
    local_part_number_per_structure(1,1) = structure_id(1)
    local_part_number_per_structure(2,1) = 1
    do ip = 2, local_npart
       if(structure_id(ip) == local_part_number_per_structure(1,current_value)) then
          local_part_number_per_structure(2,current_value) = local_part_number_per_structure(2,current_value) + 1
       else
          current_value = current_value + 1
          local_part_number_per_structure(1,current_value) = structure_id(ip)
          local_part_number_per_structure(2,current_value) = 1
       end if
    end do

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Exit count_particles_number on process ',procID
#endif

  end subroutine count_particles_number

  !======================================================================
  subroutine merge_particles_number(local_structure_number, global_count)

    use modmpicommons, only : procID, &
         procNB
    use mpi
    
    integer(kind=4), intent(inout) :: local_structure_number
    integer(kind=IDKIND), allocatable, dimension(:,:), intent(inout) :: global_count
    integer(kind=IDKIND), allocatable, dimension(:,:) :: merged_particles_count
    integer(kind=IDKIND), allocatable, dimension(:,:) :: dest_particles_count

    integer :: allocstat
    integer(kind=4) :: dest, right, left, dest_nvalues, merged_nvalues
    integer(kind=4) :: loop, loop_number
    logical(kind=4) :: even_pid
    integer(kind=4) :: dest_ip, local_ip, merged_ip, first_ip, last_ip

    integer(kind=4) :: mpierr
    integer(kind=4), dimension(Mpi_Status_Size) :: mpistat

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter merge_partiles_number on process ',procID
#endif

    ! pid of the process right and left to current one
    right = mod(procID+1,procNB)
    left = mod(procID-1+procNB,procNB)

    ! first process has no left neighboor
    ! and last process has no right neighboor
    if(procID == 0) left = Mpi_Proc_Null
    if(procID == procNB-1) right = Mpi_Proc_Null

    ! is current process even?
    even_pid = (mod(procID,2)==0)

    ! there will be procNB/2 merging and 2 merging per loop => procNB/4 loops +1 for safety
    loop_number = procNB / 2 + 1


    do loop = 1, loop_number

       ! merge 0 with 1, 2 with 3, 4 with 5, etc...
       if(even_pid) then
          dest = right ! even process communicates with right neighboor
       else
          dest = left  ! odd process communicates with left neighboor
       end if

       dest_nvalues = 0
       ! exchange of number of values in particles number per halo ID
       call Mpi_Sendrecv(local_structure_number, 1, Mpi_Integer, dest, 0, &
            dest_nvalues, 1, Mpi_Integer, dest, 0, Mpi_Comm_World, mpistat, mpierr)

       ! allocate array for particles number per halo ID from neighbour
       if(dest_nvalues /= 0) then
          allocate(dest_particles_count(2,dest_nvalues), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for dest_particles_count in merge_particles_number', &
               allocstat)
          ! allocate array for merged particles number per halo ID
          merged_nvalues = (local_structure_number + dest_nvalues)
          allocate(merged_particles_count(2,merged_nvalues), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for merged_particles_count in merge_particles_number', &
               allocstat)

          ! exchange particles number per halo ID
          call Mpi_Sendrecv(global_count, 2*local_structure_number, MPI_IDKIND, dest, 0, &
               dest_particles_count, 2*dest_nvalues, MPI_IDKIND, dest, 0, Mpi_Comm_World, mpistat, mpierr)

          ! loop until we reach the end of both our and neighboor's array of particles number per halo ID
          local_ip = 1
          dest_ip = 1
          merged_ip = 1
          do
             ! we hit the end of both array: end of the loop
             if(local_ip == local_structure_number .and. dest_ip == dest_nvalues) exit
             ! we hit the end of local array: we copy the end of neighboor's array
             if(local_ip == local_structure_number) then
                !                Print *,procID,' LOCAL TERMINE A MERGED=',merged_ip
                do
                   merged_particles_count(1,merged_ip) = dest_particles_count(1,dest_ip)
                   merged_particles_count(2,merged_ip) = dest_particles_count(2,dest_ip)
                   merged_ip = merged_ip + 1
                   dest_ip = dest_ip + 1
                   if(dest_ip > dest_nvalues) exit
                end do
                exit
             end if
             ! we hit the end of neighboor's array: we copy the end of local array
             if(dest_ip == dest_nvalues) then
                !                Print *,procID,' DEST TERMINE A MERGED=',merged_ip
                do
                   merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                   merged_particles_count(2,merged_ip) = global_count(2,local_ip)
                   merged_ip = merged_ip + 1
                   local_ip = local_ip + 1
                   if(local_ip > local_structure_number) exit
                end do
                exit
             end if
             ! we add next element from local array because the haloID is lower than neighboor's one
             if(global_count(1,local_ip) < dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                merged_particles_count(2,merged_ip) = global_count(2,local_ip)
                merged_ip = merged_ip + 1
                local_ip = local_ip + 1
                cycle
                ! local and neighboor's halo ID are the same: same halo, we add particles numbers
             else if(global_count(1,local_ip) == dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                merged_particles_count(2,merged_ip) = global_count(2,local_ip) + &
                     dest_particles_count(2,dest_ip)
                merged_ip = merged_ip + 1
                local_ip = local_ip + 1
                dest_ip = dest_ip + 1
                cycle
                ! we add next element from neighboor's array because the haloID is lower than local one
             else if(global_count(1,local_ip) > dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = dest_particles_count(1,dest_ip)
                merged_particles_count(2,merged_ip) = dest_particles_count(2,dest_ip)
                merged_ip = merged_ip + 1
                dest_ip = dest_ip + 1
                cycle
             end if
          end do

          ! decread merge_ip by one (correction from the loop)
          merged_ip = merged_ip - 1

          ! even: keep left part of the merged count array
          if(even_pid) then
             local_structure_number = merged_ip/2
             if(mod(merged_ip,2)==1) local_structure_number = local_structure_number+1
             first_ip = 1
             last_ip = local_structure_number
          else ! odd: keep right part of the merged count array
             local_structure_number = merged_ip/2
             first_ip = local_structure_number+1
             if(mod(merged_ip,2)==1) first_ip = first_ip+1
             last_ip = first_ip + local_structure_number - 1
          end if

          deallocate(global_count)
          allocate(global_count(2,local_structure_number), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for global_count in merge_particles_number',&
               allocstat)
          global_count(:,:) = merged_particles_count(:,first_ip:last_ip)
          deallocate(merged_particles_count)
          deallocate(dest_particles_count)

       end if


       ! 1 with 2, 3 with 4, etc...
       if(even_pid) then
          dest = left
       else
          dest = right
       end if

       dest_nvalues = 0
       ! exchange of number of values in particles number per halo ID
       call Mpi_Sendrecv(local_structure_number, 1, Mpi_Integer, dest, 0, &
            dest_nvalues, 1, Mpi_Integer, dest, 0, Mpi_Comm_World, mpistat, mpierr)

       if(dest_nvalues /= 0) then
          ! allocate array for particles number per halo ID from neighbour
          allocate(dest_particles_count(2,dest_nvalues), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for dest_particles_count in merge_particles_number', &
               allocstat)
          ! allocate array for merged particles number per halo ID
          merged_nvalues = (local_structure_number + dest_nvalues)
          allocate(merged_particles_count(2,merged_nvalues), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for merged_particles_count in merge_particles_number',&
               allocstat)

          ! exchange particles number per halo ID
          call Mpi_Sendrecv(global_count, 2*local_structure_number, MPI_IDKIND, dest, 0, &
               dest_particles_count, 2*dest_nvalues, MPI_IDKIND, dest, 0, Mpi_Comm_World, mpistat, mpierr)

          ! loop until we reach the end of both our and neighboor's array of particles number per halo ID
          local_ip = 1
          dest_ip = 1
          merged_ip = 1
          do
             ! we hit the end of both array: end of the loop
             if(local_ip == local_structure_number .and. dest_ip == dest_nvalues) exit
             ! we hit the end of local array: we copy the end of neighboor's array
             if(local_ip == local_structure_number) then
                do
                   merged_particles_count(1,merged_ip) = dest_particles_count(1,dest_ip)
                   merged_particles_count(2,merged_ip) = dest_particles_count(2,dest_ip)
                   merged_ip = merged_ip + 1
                   dest_ip = dest_ip + 1
                   if(dest_ip > dest_nvalues) exit
                end do
                exit
             end if
             ! we hit the end of neighboor's array: we copy the end of local array
             if(dest_ip == dest_nvalues) then
                do
                   merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                   merged_particles_count(2,merged_ip) = global_count(2,local_ip)
                   merged_ip = merged_ip + 1
                   local_ip = local_ip + 1
                   if(local_ip > local_structure_number) exit
                end do
                exit
             end if
             ! we add next element from local array because the haloID is lower than neighboor's one
             if(global_count(1,local_ip) < dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                merged_particles_count(2,merged_ip) = global_count(2,local_ip)
                merged_ip = merged_ip + 1
                local_ip = local_ip + 1
                cycle
                ! local and neighboor's halo ID are the same: same halo, we add particles numbers
             else if(global_count(1,local_ip) == dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                merged_particles_count(2,merged_ip) = global_count(2,local_ip) + &
                     dest_particles_count(2,dest_ip)
                merged_ip = merged_ip + 1
                local_ip = local_ip + 1
                dest_ip = dest_ip + 1
                cycle
                ! we add next element from neighboor's array because the haloID is lower than local one
             else if(global_count(1,local_ip) > dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = dest_particles_count(1,dest_ip)
                merged_particles_count(2,merged_ip) = dest_particles_count(2,dest_ip)
                merged_ip = merged_ip + 1
                dest_ip = dest_ip + 1
                cycle
             end if
          end do

          ! decread merge_ip by one (correction from the loop)
          merged_ip = merged_ip - 1

          ! even: keep right part of the merged count array
          if(even_pid) then
             local_structure_number = merged_ip/2
             first_ip = local_structure_number+1
             if(mod(merged_ip,2)==1) first_ip = first_ip+1
             last_ip = first_ip + local_structure_number - 1
          else ! odd: keep left part of the merged count array
             local_structure_number = merged_ip/2
             if(mod(merged_ip,2)==1) local_structure_number = local_structure_number+1
             first_ip = 1
             last_ip = local_structure_number
          end if

          deallocate(global_count)
          allocate(global_count(2,local_structure_number), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for global_count in merge_particles_number', allocstat)
          global_count(:,:) = merged_particles_count(:,first_ip:last_ip)
          deallocate(merged_particles_count)
          deallocate(dest_particles_count)
       end if

    end do

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter merged_particles_number on process ',procID
#endif

  end subroutine merge_particles_number


  !======================================================================
  subroutine select_halos(param, global_particles_count, distributed_halo_size)

    type(Type_parameter_pfof), intent(in) :: param
    integer(kind=IDKIND), allocatable, dimension(:,:), intent(in) :: global_particles_count
    integer(kind=IDKIND), allocatable, dimension(:,:), intent(out) :: distributed_halo_size

    integer :: allocstat
    integer(kind=4) :: local_structure_number
    integer(kind=4) :: local_halo_number
    integer(kind=4) :: local_ip, ih

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter select_halos on process ',procID
#endif

    local_structure_number = ubound(global_particles_count,dim=2)

    ! count the number of halo with number of particles > Mmin
    local_halo_number = 0
    do local_ip = 1, local_structure_number
       if(global_particles_count(2,local_ip) >= param%mmin) then
          local_halo_number = local_halo_number + 1
       end if
    end do

    ! allocate array to store the number of particles per halo and the corresponding haloID
    allocate(distributed_halo_size(2,local_halo_number), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for distributed_halo_size in select_halos', allocstat)

    ! we only keep halo ID and particles number for halo with particles number > Mmin
    ih = 1
    do local_ip = 1, local_structure_number
       if(global_particles_count(2,local_ip) >= param%mmin) then
          distributed_halo_size(1,ih) = global_particles_count(1,local_ip)
          distributed_halo_size(2,ih) = global_particles_count(2,local_ip)
          ih = ih + 1
       end if
    end do

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter select_halos on process ',procID
#endif

  end subroutine select_halos

  !======================================================================
  subroutine select_local_halos(distributed_halo_size, local_part_number_per_structure, &
       global_selected_halo_pid, local_selected_halo_pid, local_selected_halo_number, mpicomm)

    use modmpicommons, only : procID, &
         procNB
    use mpi

    integer(kind=IDKIND), allocatable, dimension(:,:), intent(in) :: distributed_halo_size
    integer(kind=IDKIND), dimension(:,:), intent(in) :: local_part_number_per_structure
    integer(kind=4), dimension(:), allocatable, intent(out) :: global_selected_halo_pid
    integer(kind=4), dimension(:), allocatable, intent(out) :: local_selected_halo_pid
    integer(kind=4), intent(out) :: local_selected_halo_number

    integer(kind=4), intent(in) :: mpicomm

    integer :: allocstat
    integer(kind=IDKIND), allocatable, dimension(:,:) :: first_last_number_haloid
    integer(kind=IDKIND), dimension(3) :: tmpintpri
    integer(kind=4) :: mpierr
    integer(kind=4) :: distributed_halo_number, local_halo_number, tmp_halo_number

    integer(kind=IDKIND), dimension(:,:), allocatable :: tmp_halo_list
    integer(kind=4) :: my_ih, dist_process, dist_ih, ip
    integer(kind=IDKIND) :: my_hid

    integer(kind=MPI_ADDRESS_KIND) :: size_of_pri, size_of_integer, lower_bound
    integer(kind=4) :: distrib_window, global_pid_window

    integer(kind=4), dimension(:), allocatable :: tmp_global_pid


#ifdef DEBUG
    write(OUTPUT_UNIT, *) 'Enter select_local_halos on process ',procID
#endif

    ! for each process, first (1,:) and last halo (2,:) id in distributed_halo_size
    allocate(first_last_number_haloid(3,procNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for first_last_number_haloid in select_local_halos', allocstat)

    distributed_halo_number = ubound(distributed_halo_size, dim=2)
    local_halo_number = ubound(local_part_number_per_structure, dim=2)

    allocate(global_selected_halo_pid(distributed_halo_number), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for global_selected_halo_pid in select_local_halos', allocstat)
    allocate(local_selected_halo_pid(local_halo_number), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for local_selected_halo_pid in select_local_halos', allocstat)
    allocate(need_to_communicate(2,procNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for need_to_communicate in select_local_halos', allocstat)

    global_selected_halo_pid = 0
    local_selected_halo_pid = 0
    need_to_communicate = 0

    tmpintpri(1) = distributed_halo_size(1,1)
    tmpintpri(2) = distributed_halo_size(1,distributed_halo_number)
    tmpintpri(3) = distributed_halo_number
    ! Communicate first and last haloid to everyone (all to all, bad communication...)
    call Mpi_Allgather(tmpintpri, 3, MPI_IDKIND, first_last_number_haloid, 3, MPI_IDKIND, Mpi_Comm_World, mpierr)

    ! Create the windows
    call Mpi_Type_Get_Extent(MPI_IDKIND, lower_bound, size_of_pri, mpierr)
    call Mpi_Type_Get_Extent(Mpi_Integer, lower_bound, size_of_integer, mpierr)


    call Mpi_Win_Create(distributed_halo_size, 2*distributed_halo_number*size_of_pri,&
         int(size_of_pri,kind=4), MPI_INFO_NULL, mpicomm, distrib_window,mpierr)

    call Mpi_Win_Create(global_selected_halo_pid, distributed_halo_number*size_of_integer,&
         int(size_of_integer, kind=4), MPI_INFO_NULL, mpicomm, global_pid_window, mpierr)

    ! now we have to loop over our halos and find the ones that are selected final halos
    ! and loop on the distant processes to read their halo sizes
    ! we place a pointer to our first halo
    my_ih = 1
    my_hid = local_part_number_per_structure(1,1)
    ! we search on which process should be this halo if it is a final selected halo
!!$    Do ip = 1, procNB
!!$       If(my_hid >= first_last_number_haloid(1, ip) .and. my_hid <= first_last_number_haloid(2, ip)) Then
!!$          dist_process = ip
!!$          Exit
!!$       End If
!!$    End Do

    if(my_hid < first_last_number_haloid(1,1)) dist_process = 1
    do ip= 1, procNB-1
       if(my_hid >= first_last_number_haloid(1, ip) .and. my_hid <= first_last_number_haloid(2, ip)) then
          dist_process = ip
          exit
       end if
       if(my_hid >= first_last_number_haloid(2, ip) .and. my_hid <= first_last_number_haloid(1, ip+1)) then
          dist_process = ip+1
          exit
       end if
    end do
    if(my_hid >= first_last_number_haloid(1, procNB)) dist_process = procNB

    local_selected_halo_number = 0

!!$    If(procID==11 .or. procID==21) print *,'FLNH:',first_last_number_haloid
!!$    Print *, 'DISTPROCESS=',procID, dist_process, my_hid, first_last_number_haloid(1,1)


    ! loop over the processes
    processloop: do while (dist_process <= procNB)
       ! if our first halo is on our process we proceed
       if(dist_process == procID + 1) then
          dist_ih = 1
          halolooploc: do
             if(my_ih > local_halo_number) then
                exit processloop
             end if
             if(dist_ih > first_last_number_haloid(3,dist_process)) then
                dist_process = dist_process + 1
                exit halolooploc
             end if
             my_hid = local_part_number_per_structure(1,my_ih)
             if(my_hid > distributed_halo_size(1,dist_ih)) then
                dist_ih = dist_ih + 1
                cycle
             else if(my_hid < distributed_halo_size(1,dist_ih)) then
                local_selected_halo_pid(my_ih) = -1 !! pid go from 1 to procNB here !!
                my_ih = my_ih + 1
                cycle
             else if(my_hid == distributed_halo_size(1,dist_ih)) then !
                ! keep this halo: will I get it completely?
                if(local_part_number_per_structure(2,my_ih) > distributed_halo_size(2,dist_ih) / 2) then
                   ! I have more than 50% of the particles => I will get the rest
                   global_selected_halo_pid(dist_ih) = procID + 1 !! pid go from 1 to procNB here !!
                   local_selected_halo_pid(my_ih) = procID + 1 !! pid go from 1 to procNB here !!
                end if
#ifdef DEBUG
                write(10,*) local_selected_halo_number + 1, local_part_number_per_structure(1,my_ih),&
                     local_part_number_per_structure(2,my_ih)
#endif
                my_ih = my_ih + 1   ! keep and move to next local halo
                local_selected_halo_number = local_selected_halo_number + 1
                cycle
             end if

          end do halolooploc

       else ! if not we get the list of final selected halo from the distant process and we proceed
          ! remember, for later comm, that I need to communicate with dist_process
          need_to_communicate(1,dist_process) = 1
          tmp_halo_number = int(first_last_number_haloid(3,dist_process),kind=4)
          need_to_communicate(2,dist_process) = tmp_halo_number
          allocate(tmp_global_pid(tmp_halo_number), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for tmp_global_pid in select_local_halos', allocstat)
          allocate(tmp_halo_list(2,tmp_halo_number), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for tmp_halo_list in select_local_halos', allocstat)
          tmp_global_pid = 0 !! pid go from 1 to procNB here !!
          tmp_halo_list = 0

          ! open the windows on the halo size
          call Mpi_Win_Lock(MPI_LOCK_SHARED, dist_process-1, 0, distrib_window, mpierr)
          call Mpi_Get(tmp_halo_list(1,1), 2*tmp_halo_number, MPI_IDKIND,&
               dist_process-1, int(0,kind=MPI_ADDRESS_KIND), &
               2*tmp_halo_number, MPI_IDKIND, distrib_window, mpierr)
          call Mpi_Win_Unlock(dist_process-1, distrib_window, mpierr)

          ! we begin the loop
          dist_ih = 1
          haloloopdist: do
             if(my_ih > local_halo_number) then
                call Mpi_Win_Lock(MPI_LOCK_EXCLUSIVE, dist_process-1, 0, global_pid_window,mpierr)
                call Mpi_Accumulate(tmp_global_pid,tmp_halo_number, Mpi_Integer,&
                     dist_process-1, int(0,kind=MPI_ADDRESS_KIND),&
                     tmp_halo_number, Mpi_Integer, Mpi_Sum, global_pid_window, mpierr)
                call Mpi_Win_Unlock(dist_process-1, global_pid_window, mpierr)
                deallocate(tmp_global_pid)
                deallocate(tmp_halo_list)
                exit processloop
             end if
             if(dist_ih > first_last_number_haloid(3,dist_process)) then
                call Mpi_Win_Lock(MPI_LOCK_EXCLUSIVE, dist_process-1, 0, global_pid_window,mpierr)
                call Mpi_Accumulate(tmp_global_pid,tmp_halo_number, Mpi_Integer,&
                     dist_process-1, int(0,kind=MPI_ADDRESS_KIND),&
                     tmp_halo_number, Mpi_Integer, Mpi_Sum, global_pid_window, mpierr)
                call Mpi_Win_Unlock(dist_process-1, global_pid_window, mpierr)
                dist_process = dist_process + 1
                deallocate(tmp_global_pid)
                deallocate(tmp_halo_list)
                exit haloloopdist
             end if
             my_hid = local_part_number_per_structure(1,my_ih)
             if(my_hid > tmp_halo_list(1,dist_ih)) then
                dist_ih = dist_ih + 1
                cycle
             else if(my_hid < tmp_halo_list(1,dist_ih)) then
                local_selected_halo_pid(my_ih) = -1 !! pid go from 1 to procNB here !!
                my_ih = my_ih + 1
                cycle
             else if(my_hid == tmp_halo_list(1,dist_ih)) then !
                ! keep this halo: will I get it completely?
                if(local_part_number_per_structure(2,my_ih) > tmp_halo_list(2,dist_ih) / 2) then
                   ! I have more than 50% of the particles => I will get the rest
                   tmp_global_pid(dist_ih) = procID+1 !! pid go from 1 to procNB here !!
                   local_selected_halo_pid(my_ih) = procID+1  !! pid go from 1 to procNB here !!
                end if
                my_ih = my_ih + 1
                local_selected_halo_number = local_selected_halo_number + 1
                cycle
             end if

          end do haloloopdist
       end if

    end do processloop

    call Mpi_Win_Fence(0,global_pid_window,mpierr)
    call Mpi_Win_Fence(0,distrib_window,mpierr)

    ! If there are halos left (my_ih < local_halo_number)
    ! then we have to mark them as not selected
    do dist_ih = my_ih, local_halo_number
       local_selected_halo_pid(dist_ih) = -1
    end do


    ! last loops (1): if a dist process has given a global_selected_halo_pid to
    ! one of my local_selected_halo : set the right local_selected_halo_pid
    dist_ih = 1
    do my_ih = 1, local_halo_number
       if(local_selected_halo_pid(my_ih) == 0) then
          my_hid = local_part_number_per_structure(1,my_ih)
          distributedloop : do dist_ih = 1, distributed_halo_number
             if(my_hid == distributed_halo_size(1,dist_ih)) then
                local_selected_halo_pid(my_ih) = global_selected_halo_pid(dist_ih)
                exit distributedloop
             end if
          end do distributedloop
       end if
    end do

    call Mpi_Win_Fence(0,global_pid_window,mpierr)
    call Mpi_Win_Fence(0,distrib_window,mpierr)

    ! last loops (2): if a dist process has received a global_selected_halo_pid for
    ! one of my local_selected_halo: set the right local_selected_halo_pid
    do ip = 1, procNB-1
       dist_process = mod(procID+ip,procNB) + 1 ! dist_process goes from 1 to procNB => we add 1
       dist_ih = 1

       if(need_to_communicate(1,dist_process)==1) then
          tmp_halo_number = int(first_last_number_haloid(3,dist_process),kind=4)
          allocate(tmp_global_pid(tmp_halo_number), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for tmp_global_pid in select_local_halos', allocstat)
          allocate(tmp_halo_list(2,tmp_halo_number), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for tmp_halo_list in select_local_halos', allocstat)
          tmp_global_pid = 0 !! pid go from 1 to procNB here !!
          tmp_halo_list = 0

          call Mpi_Win_Lock(MPI_LOCK_SHARED, dist_process-1, 0, distrib_window, mpierr)
          call Mpi_Get(tmp_halo_list(1,1), 2*tmp_halo_number, MPI_IDKIND,&
               dist_process-1, int(0,kind=MPI_ADDRESS_KIND), &
               2*tmp_halo_number, MPI_IDKIND, distrib_window, mpierr)
          call Mpi_Win_Unlock(dist_process-1, distrib_window, mpierr)

          call Mpi_Win_Lock(MPI_LOCK_SHARED, dist_process-1, 0, global_pid_window,mpierr)
          call Mpi_Get(tmp_global_pid,tmp_halo_number, Mpi_Integer,&
               dist_process-1, int(0,kind=MPI_ADDRESS_KIND),&
               tmp_halo_number, Mpi_Integer, global_pid_window, mpierr)
          call Mpi_Win_Unlock(dist_process-1, global_pid_window, mpierr)

          do my_ih = 1, local_halo_number
             if(local_selected_halo_pid(my_ih) == 0) then
                my_hid = local_part_number_per_structure(1,my_ih)
                distantloop : do dist_ih = 1, tmp_halo_number
                   if(my_hid == tmp_halo_list(1,dist_ih)) then
                      local_selected_halo_pid(my_ih) = tmp_global_pid(dist_ih)
                      exit distantloop
                   end if
                end do distantloop
             end if
          end do

          deallocate(tmp_global_pid)
          deallocate(tmp_halo_list)

       end if
    end do

    deallocate(first_last_number_haloid)
    deallocate(need_to_communicate)
    call Mpi_Win_Free(distrib_window, mpierr)
    call Mpi_Win_Free(global_pid_window, mpierr)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Exit select_local_halos on process ',procID, local_selected_halo_number, my_ih, my_hid
#endif

  end subroutine select_local_halos


  !======================================================================
  ! compute the displacement in the position (and all other particle properties arrays)
  ! from the beginning of the array
  ! to the i-th selected halo
  ! particle_disp(2,local_selected_halo_number) = (halo_id, displacement)
  subroutine compute_particle_disp(local_selected_halo_number, local_selected_halo_pid, &
       local_part_number_per_structure, particle_disp)

    integer(kind=4), intent(in) :: local_selected_halo_number
    integer(kind=IDKIND), dimension(:,:), intent(in) :: local_part_number_per_structure
    integer(kind=IDKIND), allocatable, dimension(:,:), intent(out) :: particle_disp
    integer(kind=4), dimension(:), intent(in) :: local_selected_halo_pid

    integer :: allocstat
    integer(kind=4) :: halo_ptr, ih, local_halo_number
    integer(kind=IDKIND) :: particle_ptr

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter compute_particle_disp on process ',procID
#endif

    local_halo_number = ubound(local_part_number_per_structure,dim=2)

    allocate(particle_disp(2,local_selected_halo_number), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for particle_disp in compute_particle_disp', allocstat)

    particle_disp = 0
    particle_ptr =  0
    halo_ptr = 0
    do ih = 1, local_halo_number
       if(local_selected_halo_pid(ih) > 0) then
          halo_ptr = halo_ptr + 1
          particle_disp(1,halo_ptr) = local_part_number_per_structure(1,ih)
          particle_disp(2,halo_ptr) = particle_ptr
       end if
       particle_ptr = particle_ptr + local_part_number_per_structure(2,ih)
    end do

    if(halo_ptr /= local_selected_halo_number) then
       write(ERROR_UNIT,*) ' Error in displacement computation: ', procID, halo_ptr, local_selected_halo_number
       call emergencystop('Error in displacement computation in compute_particle_disp', ERR_CODE_COMPUTATION)
    end if

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Exit compute_particle_disp on process ',procID
#endif

  end subroutine compute_particle_disp

  !======================================================================
  subroutine dispatch_halos(local_part_number_per_structure, distributed_halo_size, &
       local_selected_halo_pid, global_selected_halo_pid, mpicomm, displacement_particles_exchange, &
       local_selected_particle_number, number_of_particles_to_send_to, final_local_halo_number)

    use modsort, only : heapsort
    use mpi

    implicit none

    integer(kind=IDKIND), intent(inout), dimension(:,:), allocatable :: distributed_halo_size
    integer(kind=4),   intent(inout), dimension(:)                :: local_selected_halo_pid
    integer(kind=4),   intent(inout), dimension(:),   allocatable :: global_selected_halo_pid
    integer(kind=IDKIND), intent(in),    dimension(:,:)              :: local_part_number_per_structure
    integer(kind=4),   intent(in)                                 :: mpicomm
    integer(kind=IDKIND), intent(out),   dimension(:),   allocatable :: number_of_particles_to_send_to
    integer(kind=4),   intent(out)                                :: final_local_halo_number
    integer(kind=IDKIND), intent(out)                                :: local_selected_particle_number
    integer(kind=IDKIND), intent(out),   dimension(:),   allocatable :: displacement_particles_exchange

    integer :: allocstat
    integer(kind=4),   dimension(:),   allocatable :: halo_number_per_process
    integer(kind=IDKIND), dimension(:),   allocatable :: particle_number_per_process
    integer(kind=4)                                :: distributed_halo_number
    integer(kind=4)                                :: local_halo_number
    integer(kind=MPI_ADDRESS_KIND)                 :: size_of_pri, lower_bound
    integer(kind=4)                                :: mpierr
    integer(kind=4)                                :: global_unassigned_halo_number, local_unassigned_halo_number
    integer(kind=IDKIND), dimension(:),   allocatable :: global_unassigned_halo, local_unassigned_halo
    integer(kind=4),   dimension(:),   allocatable :: unassigned_halo_number_array, disp
    integer(kind=4),   dimension(:),   allocatable :: contributors
    integer(kind=IDKIND), dimension(:),   allocatable :: local_contribution
    integer(kind=IDKIND), dimension(:,:), allocatable :: contribution_array
    integer(kind=4)                                :: ip, ih, ind, gih, lih, gpid, lpid, pid
    integer(kind=IDKIND)                              :: hid
    integer(kind=IDKIND)                              :: n_to_put
    integer(kind=IDKIND), dimension(:),   allocatable :: number_of_particles_to_receive_from
    integer(kind=4)                                :: win_number_part_recv

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter dispatch_halos on process ',procID
#endif

    allocate(halo_number_per_process(procNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for halo_number_per_process in dispatch_halos', allocstat)
    allocate(particle_number_per_process(procNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for particle_number_per_process in dispatch_halos', allocstat)

    distributed_halo_number = ubound(distributed_halo_size, dim=2)
    local_halo_number = ubound(local_part_number_per_structure, dim=2)

    ! search for the halos that have not been assigned to a process
    local_unassigned_halo_number = 0
    do ih = 1, distributed_halo_number
       if(global_selected_halo_pid(ih) == 0) then
          local_unassigned_halo_number = local_unassigned_halo_number + 1
       end if
    end do

    ! make a list of unassigned halos
    allocate(local_unassigned_halo(local_unassigned_halo_number))
    ind = 1
    do ih = 1, distributed_halo_number
       if(global_selected_halo_pid(ih) == 0) then
          local_unassigned_halo(ind) = distributed_halo_size(1,ih)
          ind = ind + 1
       end if
    end do

    allocate(unassigned_halo_number_array(procNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for unassigned_halo_number_array in dispatch_halos', allocstat)
    allocate(disp(procNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for disp in dispatch_halos', allocstat)

    ! gather unassigned halo numbers from all processes
    call Mpi_Allgather(local_unassigned_halo_number, 1, Mpi_Integer, &
         unassigned_halo_number_array, 1, Mpi_Integer,&
         mpicomm, mpierr)

    global_unassigned_halo_number = sum(unassigned_halo_number_array)

    ! if there are unassigned halos
    if(global_unassigned_halo_number /= 0) then
       ! compute displacements to gather unassigned halo ids
       disp(1) = 0
       do ip = 2, procNB
          disp(ip) = disp(ip-1) + unassigned_halo_number_array(ip-1)
       end do

       allocate(global_unassigned_halo(global_unassigned_halo_number), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for global_unassigned_halo in dispatch_halos', allocstat)
       ! gather unassigned halos
       call Mpi_Allgatherv(local_unassigned_halo, local_unassigned_halo_number, MPI_IDKIND, &
            global_unassigned_halo, unassigned_halo_number_array, disp, MPI_IDKIND,&
            mpicomm, mpierr)
       deallocate(disp)
       deallocate(local_unassigned_halo)
       deallocate(unassigned_halo_number_array)

       ! assign them
       ! 1- each process give its number of particles for the unassigned halos
       ! 2- gather by 0 of number
       ! 3- 0 assign and let know to everyone else
       allocate(local_contribution(global_unassigned_halo_number), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for local_contribution in dispatch_halos', allocstat)
       gloop1: do gih = 1, global_unassigned_halo_number
          lloop1: do lih = 1, local_halo_number
             if(local_part_number_per_structure(1,lih) == global_unassigned_halo(gih)) then
                local_contribution(gih) = local_part_number_per_structure(2,lih)
                exit lloop1
             end if
          end do lloop1
       end do gloop1
       !If(procID==0)
       allocate(contribution_array(global_unassigned_halo_number, procNB), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for contribution_array in dispatch_halos', allocstat)

       call Mpi_Gather(local_contribution, global_unassigned_halo_number, MPI_IDKIND,&
            contribution_array, global_unassigned_halo_number, MPI_IDKIND,&
            0, mpicomm, mpierr)

       deallocate(local_contribution)
       allocate(contributors(global_unassigned_halo_number))
       if(allocstat > 0) call emergencystop('Allocate failed for contributors in dispatch_halos', allocstat)

       if(procID==0) then
          contributors=maxloc(contribution_array, dim=2)
#ifdef DEBUG
          write(OUTPUT_UNIT,*) 'CONTRIBUTORS=',contributors
#endif
       end if

       deallocate(contribution_array)
       call Mpi_Bcast(contributors,global_unassigned_halo_number, Mpi_Integer,&
            0, mpicomm, mpierr)

       call heapsort(global_unassigned_halo_number, global_unassigned_halo, contributors)


       ! loop over the unassigned halos
       gpid = 1
       lpid = 1
       do gih = 1, global_unassigned_halo_number
          hid = global_unassigned_halo(gih)
          ! if this unassigned halo is in my part of the global list
          !          gpidloop: Do while(gpid<=distributed_halo_number)
          gpidloop: do gpid=1, distributed_halo_number
             if(distributed_halo_size(1,gpid) == hid) then
#ifdef DEBUG
                write(OUTPUT_UNIT,*) 'ASSIGN UNASSIGN GLOBAL:',gpid, gih
#endif
                global_selected_halo_pid(gpid) = contributors(gih)
                exit gpidloop
             end if
             !             gpid = gpid + 1
          end do gpidloop
          !          lpidloop:Do while(lpid<=local_halo_number)
          lpidloop:do lpid = 1, local_halo_number
             if(local_part_number_per_structure(1,lpid) == hid) then
#ifdef DEBUG
                write(OUTPUT_UNIT,*) 'ASSIGN UNASSIGN LOCAL:',lpid, gih
#endif
                local_selected_halo_pid(lpid) = contributors(gih)
                exit lpidloop
             end if
             !             lpid = lpid + 1
          end do lpidloop
       end do

       deallocate(contributors)
       deallocate(global_unassigned_halo)

    end if

    ! all halos have been assigned
    ! count halos per process and particles per process
    halo_number_per_process = 0
    particle_number_per_process = 0
    do ih = 1, distributed_halo_number
       pid = global_selected_halo_pid(ih)
#ifdef DEBUG
       if(pid < 1 .or. pid > procNB) then
          write(ERROR_UNIT,*) 'Invalid pid on process ',procID,' : halo ',ih,' is associated with process ',pid-1
          write(ERROR_UNIT,*) 'Invalid pid on process ',procID,' : distributed_halo_number = ',distributed_halo_number
          write(ERROR_UNIT,*) 'Invalid pid on process ',procID,' : distributed_halo_size = ',distributed_halo_size(:,ih)
       end if
#endif
       halo_number_per_process(pid) = halo_number_per_process(pid) + 1
       particle_number_per_process(pid) = particle_number_per_process(pid) + distributed_halo_size(2,ih)
    end do

    deallocate(distributed_halo_size)
    deallocate(global_selected_halo_pid)
    call Mpi_Allreduce(MPI_IN_PLACE, halo_number_per_process, procNB, MPI_INTEGER, MPI_SUM, mpicomm, mpierr)
    call Mpi_Allreduce(MPI_IN_PLACE, particle_number_per_process, procNB, MPI_IDKIND, MPI_SUM, mpicomm, mpierr)

    final_local_halo_number = halo_number_per_process(procID + 1)
    local_selected_particle_number = particle_number_per_process(procID + 1)

    deallocate(halo_number_per_process)
    deallocate(particle_number_per_process)

    ! compute the number of particles I will send to each other process
    allocate(number_of_particles_to_send_to(procNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for number_of_particles_to_send_to in dispatch_halos', &
         allocstat)
    number_of_particles_to_send_to = 0
    do ih = 1, local_halo_number
       pid = local_selected_halo_pid(ih)
       if(pid > 0) then
          number_of_particles_to_send_to(pid) = number_of_particles_to_send_to(pid) + local_part_number_per_structure(2,ih)
       end if
    end do

    ! write the 'number of particles to send to' to each other process
    ! so that each process can compute a displacement for particles exchanges
    allocate(number_of_particles_to_receive_from(procNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for number_of_particles_to_receive_from in dispatch_halos', &
         allocstat)
    number_of_particles_to_receive_from = 0
    ! open window
    call Mpi_Type_Get_Extent(MPI_IDKIND, lower_bound, size_of_pri, mpierr)
    call Mpi_Win_Create(number_of_particles_to_receive_from, procNB*size_of_pri,&
         int(size_of_pri,kind=4), MPI_INFO_NULL, mpicomm, win_number_part_recv, mpierr)

    call Mpi_Win_Fence(0,win_number_part_recv,mpierr)
    do ip = 1, procNB
       n_to_put = number_of_particles_to_send_to(ip)
       if(ip /= procID+1 .and. n_to_put /= 0) then
          call Mpi_Put(n_to_put, 1, MPI_IDKIND, &
               ip-1, int(procID,MPI_ADDRESS_KIND), 1, MPI_IDKIND, &
               win_number_part_recv, mpierr)
       end if
    end do
    call Mpi_Win_Fence(0,win_number_part_recv,mpierr)
    call Mpi_Win_Free(win_number_part_recv, mpierr)

    ! compute the displacement for particles exchange
    allocate(displacement_particles_exchange(procNB), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for displacement_particles_exchange in dispatch_halos', &
         allocstat)
    ! classic displacement with local number of particles as basic displacement
    displacement_particles_exchange(1) = number_of_particles_to_send_to(procID+1)
    do ip = 2, procNB
       displacement_particles_exchange(ip) = displacement_particles_exchange(ip-1) + number_of_particles_to_receive_from(ip-1)
    end do
    ! set displacement for myself to 0
    displacement_particles_exchange(procID+1) = 0
    deallocate(number_of_particles_to_receive_from)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Exit dispatch_halos on process ',procID
#endif

  end subroutine dispatch_halos

  !======================================================================
  ! Returns the displacement from 0 to the target halo ID
  function haloid_to_disp(haloid, particle_disp) result (displacement)

    integer(kind=IDKIND), intent(in) :: haloid
    integer(kind=IDKIND), dimension(:,:), intent(in) ::  particle_disp
    integer(kind=4) :: displacement

    integer(kind=4) :: ih

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter gatherhaloLB on process ',procID
#endif

    do ih = 1, ubound(particle_disp,dim=2)
       if(haloid == particle_disp(1,ih)) then
          displacement = int(particle_disp(2,ih),kind=4)
          exit
       end if
    end do

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter gatherhaloLB on process ',procID
#endif

  end function haloid_to_disp


  !======================================================================
  subroutine dispatch_particles(finalstructureid,displacement_particles_exchange, local_part_number_per_structure, &
       local_selected_halo_pid, local_selected_particle_number, mpicomm, &
       number_of_particles_to_send_to, param, particle_disp)

    use mpi
    use modvarcommons, only : field, pfof_id, position, potential, ramses_id, structure_id, velocity

    integer(kind=IDKIND), dimension(:), allocatable, intent(out) :: finalstructureid
    integer(kind=IDKIND), dimension(:), allocatable, intent(inout) :: displacement_particles_exchange
    integer(kind=IDKIND), dimension(:,:), allocatable, intent(inout) :: local_part_number_per_structure
    integer(kind=4),   dimension(:), allocatable, intent(inout) :: local_selected_halo_pid
    integer(kind=IDKIND),                 intent(in) :: local_selected_particle_number
    integer(kind=4),                   intent(in) :: mpicomm
    integer(kind=IDKIND), dimension(:), allocatable, intent(inout) :: number_of_particles_to_send_to
    class(Type_parameter_pfof),        intent(in) :: param
    integer(kind=IDKIND), dimension(:,:), allocatable, intent(inout) :: particle_disp

    integer :: allocstat
    integer(kind=4) :: disp
    integer(kind=IDKIND) :: distant_disp
    logical(kind=4) :: do_read_ramses_part_id
    integer(kind=4) :: firstpart, lastpart
    integer(kind=4) :: ih
    integer(kind=4) :: iprocess
    integer(kind=4) :: local_partial_halo_number
    integer(kind=MPI_ADDRESS_KIND) :: lower_bound
    integer(kind=4) :: mpierr
    integer(kind=4) :: npart_to_send
    integer(kind=4) :: pid
    integer(kind=4) :: size
    integer(kind=MPI_ADDRESS_KIND) :: size_of_pri
    integer(kind=MPI_ADDRESS_KIND) :: size_of_real
    real(kind=4), dimension(:,:), allocatable :: tmpfield
    integer(kind=IDKIND), dimension(:), allocatable :: tmppfofid
    real(kind=4), dimension(:,:), allocatable :: tmppos
    real(kind=4), dimension(:), allocatable :: tmppot
    integer(kind=IDKIND), dimension(:), allocatable :: tmpramsesid
    integer(kind=IDKIND), dimension(:), allocatable :: tmpstrid
    real(kind=4), dimension(:,:), allocatable :: tmpvel
    integer(kind=4) :: win_displacement
    integer(kind=4) :: win_field
    integer(kind=4) :: win_pfof_id
    integer(kind=4) :: win_position
    integer(kind=4) :: win_potential
    integer(kind=4) :: win_ramses_id
    integer(kind=4) :: win_structure_id
    integer(kind=4) :: win_velocity

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter dispatch_particles on process ',procID
#endif

    local_partial_halo_number = ubound(local_selected_halo_pid,dim=1)

    select type (param)
    type is (Type_parameter_pfof_snap)
       do_read_ramses_part_id = .false.
    type is (Type_parameter_pfof_cone)
       do_read_ramses_part_id = param%do_read_ramses_part_id
    end select

    call Mpi_Type_Get_Extent(MPI_IDKIND, lower_bound, size_of_pri, mpierr)
    call Mpi_Type_Get_Extent(Mpi_Real, lower_bound, size_of_real, mpierr)

    ! allocate final arrays
    if(.not.allocated(halopartpos)) then
       allocate(halopartpos(3,local_selected_particle_number), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halopartpos in dispatch_particles', allocstat)
    end if
    if(.not.allocated(halopartvel)) then
       allocate(halopartvel(3,local_selected_particle_number), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halopartvel in dispatch_particles', allocstat)
    end if
    if(.not.allocated(halopartid)) then
       allocate(halopartid(local_selected_particle_number), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halopartid in dispatch_particles', allocstat)
    end if
    if(.not.allocated(finalstructureid)) then
       allocate(finalstructureid(local_selected_particle_number), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for finalstructureid in dispatch_particles', allocstat)
    end if
#ifdef DEBUG
    halopartpos = 0.d0
    halopartvel = 0.d0
    halopartid = 0
    finalstructureid = 0
#endif
    ! put the particles to their right place!
    ! create the windows
    call Mpi_Win_Create(displacement_particles_exchange, procNB*size_of_pri, &
         int(size_of_pri,kind=4), MPI_INFO_NULL, mpicomm, win_displacement, mpierr)
    call Mpi_Win_Create(halopartpos, 3*local_selected_particle_number*size_of_real, &
         int(size_of_real,kind=4), MPI_INFO_NULL, mpicomm, win_position, mpierr)
    call Mpi_Win_Create(halopartvel, 3*local_selected_particle_number*size_of_real, &
         int(size_of_real,kind=4), MPI_INFO_NULL, mpicomm, win_velocity, mpierr)
    call Mpi_Win_Create(halopartid, local_selected_particle_number*size_of_pri, &
         int(size_of_pri,kind=4), MPI_INFO_NULL, mpicomm, win_pfof_id, mpierr)
    call Mpi_Win_Create(finalstructureid, local_selected_particle_number*size_of_pri, &
         int(size_of_pri,kind=4), MPI_INFO_NULL, mpicomm, win_structure_id, mpierr)

    ! same for the optional properties
    if(param%do_read_gravitational_field) then
       allocate(halopartfor(3,local_selected_particle_number), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halopartfor in dispatch_particles', allocstat)
       call Mpi_Win_Create(halopartfor, 3*local_selected_particle_number*size_of_real, &
            int(size_of_real,kind=4), MPI_INFO_NULL, mpicomm, win_field, mpierr)
    end if
    if(param%do_read_potential) then
       allocate(halopartpot(local_selected_particle_number), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halopartpot in dispatch_particles', allocstat)
       call Mpi_Win_Create(halopartpot, local_selected_particle_number*size_of_real, &
            int(size_of_real,kind=4), MPI_INFO_NULL, mpicomm, win_potential, mpierr)
    end if
    if(do_read_ramses_part_id) then
       allocate(halopartramsesid(local_selected_particle_number), STAT=allocstat)
       if(allocstat > 0) call emergencystop('Allocate failed for halopartramsesid in dispatch_particles', allocstat)
       call Mpi_Win_Create(halopartramsesid, local_selected_particle_number*size_of_pri, &
            int(size_of_pri,kind=4), MPI_INFO_NULL, mpicomm, win_ramses_id, mpierr)
    end if

    ! loop over the processes
    do iprocess = 1, procNB-1
       ! first is procID+1, last is procID-1
       pid = mod(procID+iprocess,procNB) ! goes from 0 to procNB-1
       if(number_of_particles_to_send_to(pid+1) /= 0) then ! if i have particles to send to this one
          npart_to_send = int(number_of_particles_to_send_to(pid+1),kind=4)
          allocate(tmppos(3,npart_to_send), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for tmppos in dispatch_particles', allocstat)
          allocate(tmpvel(3,npart_to_send), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for tmpvel in dispatch_particles', allocstat)
          allocate(tmppfofid(npart_to_send), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for tmppfofid in dispatch_particles', allocstat)
          allocate(tmpstrid(npart_to_send), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for tmpstrid in dispatch_particles', allocstat)
          if(param%do_read_potential) then
             allocate(tmppot(npart_to_send), STAT=allocstat)
             if(allocstat > 0) call emergencystop('Allocate failed for tmppot in dispatch_particles', allocstat)
          end if
          if(param%do_read_gravitational_field) then
             allocate(tmpfield(3,npart_to_send), STAT=allocstat)
             if(allocstat > 0) call emergencystop('Allocate failed for tmpfield in dispatch_particles', allocstat)
          end if
          if(do_read_ramses_part_id) then
             allocate(tmpramsesid(npart_to_send), STAT=allocstat)
             if(allocstat > 0) call emergencystop('Allocate failed for tmpramsesid in dispatch_particles', allocstat)
          end if

          firstpart = 1
          ! loop over the selected halos
          do ih = 1, local_partial_halo_number
             if(local_selected_halo_pid(ih) == pid+1) then ! if it goes to dist. process
                ! use tmp arrays to store particle propererties we will write on distant process
                disp = haloid_to_disp(local_part_number_per_structure(1,ih), particle_disp) !particle_disp(2,ih),kind=4)
                size = int(local_part_number_per_structure(2,ih),kind=4)
                lastpart = firstpart+size-1
                tmppos(:,firstpart:lastpart) = position(:,disp+1:disp+size)
                tmpvel(:,firstpart:lastpart) = velocity(:,disp+1:disp+size)
                tmppfofid(firstpart:lastpart) = pfof_id(disp+1:disp+size)
                tmpstrid(firstpart:lastpart) = structure_id(disp+1:disp+size)
                if(param%do_read_potential) tmppot(firstpart:lastpart) = potential(disp+1:disp+size)
                if(param%do_read_gravitational_field) tmpfield(:,firstpart:lastpart) = field(:,disp+1:disp+size)
                if(do_read_ramses_part_id) tmpramsesid(firstpart:lastpart) = ramses_id(disp+1:disp+size)
                firstpart = lastpart+1
             end if
          end do

          call Mpi_Win_Lock(MPI_LOCK_SHARED, pid, 0, win_displacement,mpierr)
          ! get distant particle displacement
          call Mpi_Get(distant_disp, 1, MPI_IDKIND,&
               pid, int(procID,kind=MPI_ADDRESS_KIND), &
               1, MPI_IDKIND, win_displacement, mpierr)
          call Mpi_Win_Unlock(pid, win_displacement, mpierr)

          ! write the properties there
          call Mpi_Win_Lock(MPI_LOCK_SHARED, pid, 0, win_position,mpierr)
          call Mpi_Put(tmppos,3*npart_to_send,Mpi_Real,pid,int(3*distant_disp,kind=MPI_ADDRESS_KIND),&
               3*npart_to_send,Mpi_Real,win_position,mpierr)
          call Mpi_Win_Unlock(pid, win_position, mpierr)
          call Mpi_Win_Lock(MPI_LOCK_SHARED, pid, 0, win_velocity,mpierr)
          call Mpi_Put(tmpvel,3*npart_to_send,Mpi_Real,pid,int(3*distant_disp,kind=MPI_ADDRESS_KIND),&
               3*npart_to_send,Mpi_Real,win_velocity,mpierr)
          call Mpi_Win_Unlock(pid, win_velocity, mpierr)
          call Mpi_Win_Lock(MPI_LOCK_SHARED, pid, 0, win_pfof_id,mpierr)
          call Mpi_Put(tmppfofid,npart_to_send,MPI_IDKIND,pid,int(distant_disp,kind=MPI_ADDRESS_KIND),&
               npart_to_send,MPI_IDKIND,win_pfof_id,mpierr)
          call Mpi_Win_Unlock(pid, win_pfof_id, mpierr)
          call Mpi_Win_Lock(MPI_LOCK_SHARED, pid, 0, win_structure_id,mpierr)
          call Mpi_Put(tmpstrid,npart_to_send,MPI_IDKIND,pid,int(distant_disp,kind=MPI_ADDRESS_KIND),&
               npart_to_send,MPI_IDKIND,win_structure_id,mpierr)
          call Mpi_Win_Unlock(pid, win_structure_id, mpierr)
          if(param%do_read_potential) then
             call Mpi_Win_Lock(MPI_LOCK_SHARED, pid, 0, win_potential,mpierr)
             call Mpi_Put(tmppot,npart_to_send,Mpi_Real,pid,int(distant_disp,kind=MPI_ADDRESS_KIND),&
                  npart_to_send,Mpi_Real,win_potential,mpierr)
             call Mpi_Win_Unlock(pid, win_potential, mpierr)
          end if
          if(param%do_read_gravitational_field) then
             call Mpi_Win_Lock(MPI_LOCK_SHARED, pid, 0, win_field,mpierr)
             call Mpi_Put(tmpfield,3*npart_to_send,Mpi_Real,pid,int(3*distant_disp,kind=MPI_ADDRESS_KIND),&
                  3*npart_to_send,Mpi_Real,win_field,mpierr)
             call Mpi_Win_Unlock(pid, win_field, mpierr)
          end if
          if(do_read_ramses_part_id) then
             call Mpi_Win_Lock(MPI_LOCK_SHARED, pid, 0, win_ramses_id,mpierr)
             call Mpi_Put(tmpramsesid,npart_to_send,MPI_IDKIND,pid,int(distant_disp,kind=MPI_ADDRESS_KIND),&
                  npart_to_send,MPI_IDKIND,win_ramses_id,mpierr)
             call Mpi_Win_Unlock(pid, win_ramses_id, mpierr)
          end if
          deallocate(tmppos)
          deallocate(tmpvel)
          deallocate(tmppfofid)
          deallocate(tmpstrid)
          if(param%do_read_potential) deallocate(tmppot)
          if(param%do_read_gravitational_field) deallocate(tmpfield)
          if(do_read_ramses_part_id) deallocate(tmpramsesid)

       end if
    end do

    firstpart = 1
    do ih = 1, local_partial_halo_number
       if(local_selected_halo_pid(ih) == procID+1) then
          disp = haloid_to_disp(local_part_number_per_structure(1,ih), particle_disp)
          size = int(local_part_number_per_structure(2,ih),kind=4)
          lastpart = firstpart+size-1
          halopartpos(:,firstpart:lastpart) = position(:,disp+1:disp+size)
          halopartvel(:,firstpart:lastpart) = velocity(:,disp+1:disp+size)
          halopartid(firstpart:lastpart) = pfof_id(disp+1:disp+size)
          finalstructureid(firstpart:lastpart) = structure_id(disp+1:disp+size)
          if(param%do_read_potential) halopartpot(firstpart:lastpart) = potential(disp+1:disp+size)
          if(param%do_read_gravitational_field) halopartfor(:,firstpart:lastpart) = field(:,disp+1:disp+size)
          if(do_read_ramses_part_id) halopartramsesid(firstpart:lastpart) = ramses_id(disp+1:disp+size)
          firstpart = lastpart+1
       end if
    end do

    call Mpi_Win_Free(win_displacement, mpierr)
    call Mpi_Win_Free(win_position, mpierr)
    call Mpi_Win_Free(win_velocity, mpierr)
    call Mpi_Win_Free(win_pfof_id, mpierr)
    call Mpi_Win_Free(win_structure_id, mpierr)

    deallocate(local_part_number_per_structure)
    deallocate(local_selected_halo_pid)
    deallocate(number_of_particles_to_send_to)
    deallocate(particle_disp)
    deallocate(displacement_particles_exchange)
    deallocate(position)
    deallocate(velocity)
    deallocate(pfof_id)
    deallocate(structure_id)
    if(param%do_read_potential) deallocate(potential)
    if(param%do_read_gravitational_field) deallocate(field)
    if(do_read_ramses_part_id) deallocate(ramses_id)

    ! same for the optional properties
    if(param%do_read_potential) then
       call Mpi_Win_Free(win_potential, mpierr)
    end if
    if(param%do_read_gravitational_field) then
       call Mpi_Win_Free(win_field, mpierr)
    end if
    if(do_read_ramses_part_id) then
       call Mpi_Win_Free(win_ramses_id, mpierr)
    end if

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Exit dispatch_particles on process ',procID
#endif

  end subroutine dispatch_particles


end module modhaloopti

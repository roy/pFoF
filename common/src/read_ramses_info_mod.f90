!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2015 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Subroutines to read info files written by RAMSES for snapshot and light cone
!! @brief
!! Subroutines to read info files written by RAMSES for snapshot and light cone
!! @author Fabrice Roy
! ======================================================================
module read_ramses_info_mod

  use error_handling_mod, only : Allocate_error, IO_error
  use iso_fortran_env, only : ERROR_UNIT
  
  implicit none

  private
  public :: Read_ramses_info, &
       Read_ramses_conepart_info, &
       Read_ramses_conegrav_info

  character(*), parameter :: INT_FMT='(a13,i11)'
  character(*), parameter :: LONGINT_FMT='(a13,i20)'
  character(*), parameter :: DOUBLE_FMT='(a13,e24.15)'
  character(*), parameter :: CHAR_FMT='(a14,a80)'
contains

  !=======================================================================
  !> Read RAMSES snapshot info file
  subroutine Read_ramses_info(ramses_info, filename, process_info)

    use modconstant, only : ERR_MSG_LEN, FILENAME_LEN
    use type_info_ramses_mod, only : type_info_ramses
    use mpi_process_m, only : process_t

    type(process_t), intent(in) :: process_info
    character(len=FILENAME_LEN), intent(in)  :: filename     !< info file name
    type(type_info_ramses), intent(out) :: ramses_info   !< info structure

    ! Local variable
    character(len=14) :: dumchar
    integer :: unit_info
    character(len=ERR_MSG_LEN) :: error_message, alloc_err_msg
    integer :: ioerr, alloc_stat
    integer :: dumint
    integer :: icpu
    
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Read_ramses_info begins on process', process_info%rank
#endif
    
    open(newunit=unit_info, file=filename, status='old', action='read', form='formatted', iostat = ioerr, iomsg = error_message)
    if( ioerr /= 0 ) then
       call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    end if

    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, ramses_info%ncpu
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, ramses_info%ndim
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, ramses_info%levelmin
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, ramses_info%levelmax
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, ramses_info%ngridmax
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, ramses_info%nstep_coarse
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, *,              iostat=ioerr, iomsg=error_message) ! skip empty line
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, ramses_info%boxlen
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, ramses_info%time
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, ramses_info%aexp
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, ramses_info%h0
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, ramses_info%omega_m
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, ramses_info%omega_l
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, ramses_info%omega_k
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, ramses_info%omega_b
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, ramses_info%unit_l
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, ramses_info%unit_d
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, ramses_info%unit_t
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, *,              iostat=ioerr, iomsg=error_message) ! skip empty line
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=CHAR_FMT,   iostat=ioerr, iomsg=error_message) dumchar, ramses_info%ordering
    if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    if(trim(ramses_info%ordering).eq.'hilbert') then
       read(unit_info, *,              iostat=ioerr, iomsg=error_message) ! skip empty line
       if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)       
       allocate(ramses_info%hilbert_bound_key(0:ramses_info%ncpu), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          call Allocate_error('ramses_info%hilbert_bound_key','Read_ramses_info', &
               alloc_err_msg, alloc_stat, process_info%rank)
       end if
       do icpu = 1, ramses_info%ncpu
          read(unit_info,fmt='(i8,1x,e23.15,1x,e23.15)', iostat=ioerr, iomsg=error_message) dumint, &
               ramses_info%hilbert_bound_key(icpu-1), ramses_info%hilbert_bound_key(icpu)
          if(ioerr/=0) call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
       end do
    end if

    close(unit_info)

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Read_ramses_info ends on process', process_info%rank
#endif

  end subroutine Read_ramses_info


  !=======================================================================
  !> Read RAMSES particles light cone info file
  subroutine Read_ramses_conepart_info(filename, infocone, process_info)

    use modconstant, only : ERR_MSG_LEN, FILENAME_LEN
    use type_info_ramses_mod, only : type_info_cone_part
    use mpi_process_m, only : process_t

    type(process_t), intent(in) :: process_info
    character(len=FILENAME_LEN), intent(in) :: filename         !< info file name
    type(type_info_cone_part), intent(out) :: infocone !< info structure

    ! Local variable
    character(len=13) :: dumchar
    integer :: unit_info
    character(len=ERR_MSG_LEN) :: error_message
    integer :: ioerr

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Read_ramses_conepart_info begins on process', process_info%rank
#endif

    open(newunit=unit_info, file=filename, status='old', action='read', form='formatted', iostat = ioerr, iomsg = error_message)
    if( ioerr /= 0 ) then
       call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    end if

    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%ncpu
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%nstride
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%nstep_coarse
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%aexp
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%observer_x
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%observer_y
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%observer_z
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%observer_rds
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%cone_id
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%cone_zlim
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%amax
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%amin
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zmax
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zmin
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dmax
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dmin
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dtol
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=LONGINT_FMT, iostat=ioerr, iomsg=error_message) dumchar, infocone%nglobalfile
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=LONGINT_FMT, iostat=ioerr, iomsg=error_message) dumchar, infocone%npart
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%isfullsky
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%thetay
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%thetaz
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%theta
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%phi
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%aendconem2
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%aendconem1
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%aendcone
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%aexpold
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%aexp
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zendconem2
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zendconem1
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zendcone
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zexpold
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zexp
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dendconem2
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dendconem1
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dendcone
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dexpold
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dexp
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%future
    if(ioerr/=0) call IO_error('Read_ramses_conepart_info', error_message, ioerr, process_info%rank)
    close(unit_info)


#ifdef DEBUG
    write(ERROR_UNIT,*) 'Read_ramses_conepart_info ends on process', process_info%rank
#endif
    
  end subroutine Read_ramses_conepart_info

  !=======================================================================
  !> Read RAMSES cells light cone info file
  subroutine Read_ramses_conegrav_info(filename, infocone, process_info)

    use modconstant, only : ERR_MSG_LEN, FILENAME_LEN
    use type_info_ramses_mod, only : type_info_cone_grav
    use mpi_process_m, only : process_t

    type(process_t), intent(in) :: process_info
    character(len=FILENAME_LEN), intent(in) :: filename         !< info file name
    type(type_info_cone_grav), intent(out) :: infocone !< info structure

    ! Local variable
    character(len=13) :: dumchar
    integer :: unit_info
    character(len=ERR_MSG_LEN) :: error_message
    integer :: ioerr

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Read_ramses_conegrav_info begins on process', process_info%rank
#endif

    open(newunit=unit_info, file=filename, status='old', action='read', form='formatted', iostat = ioerr, iomsg = error_message)
    if( ioerr /= 0 ) then
       call IO_error('Read_ramses_info', error_message, ioerr, process_info%rank)
    end if

    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%ncpu
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%nstride
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%nstep_coarse
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%aexp
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%nlevel
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%levelmin
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%levelmax
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%observer_x
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%observer_y
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%observer_z
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%observer_rds
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%cone_id
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%cone_zlim
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%amax
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%amin
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zmax
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zmin
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dmax
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dmin
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dtol
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=LONGINT_FMT, iostat=ioerr, iomsg=error_message) dumchar, infocone%nglobalfile
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=LONGINT_FMT, iostat=ioerr, iomsg=error_message) dumchar, infocone%nglobalcell
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%isfullsky
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%thetay
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%thetaz
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%theta
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%phi
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%aendconem2
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%aendconem1
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%aendcone
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zendconem2
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zendconem1
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%zendcone
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dendconem2
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dendconem1
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ioerr, iomsg=error_message) dumchar, infocone%dendcone
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    read(unit_info, fmt=INT_FMT,     iostat=ioerr, iomsg=error_message) dumchar, infocone%future
    if(ioerr/=0) call IO_error('Read_ramses_conegrav_info', error_message, ioerr, process_info%rank)
    close(unit_info)

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Read_ramses_conegrav_info ends on process', process_info%rank
#endif

  end subroutine Read_ramses_conegrav_info

end module read_ramses_info_mod

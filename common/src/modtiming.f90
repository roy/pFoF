!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Variables used for timings
!! @brief
!! Variables used for timings
!! @author Fabrice Roy
!! @author Vincent Bouillot
! ======================================================================

module modtiming

  implicit none
  real(kind=8) :: time0 = 0.0d0                  !< beginning of the execution
  real(kind=8) :: timeInt = 0.0d0                !< intermediate time
  real(kind=8) :: tReadFile = 0.0d0              !< time used to read particles files (cubes or Ramses files)
  real(kind=8) :: tTailPart = 0.0d0              !< time used to distribute the particles if read from Ramses files
  real(kind=8) :: tInitRead = 0.0d0              !< time used to initialize reading from Ramses files
  real(kind=8) :: tRead = 0.0d0                  !< global reading time
  real(kind=8) :: tObs = 0.0d0                   !< time used to compute observables for haloes
  real(kind=8) :: tOut = 0.0d0                   !< time used to write output files
  real(kind=8) :: tFoF = 0.0d0                   !< global time for friends-of-friends halo detection
  real(kind=8) :: tFoFloc = 0.0d0                !< time for local fof algorithm
  real(kind=8) :: tFoFinit = 0.0d0               !< time for fof initialization (construction of the tree)
  real(kind=8) :: tRaccord = 0.0d0               !< time for halo merging procedure
  real(kind=8) :: tGatherhalo = 0.0d0            !< time for gathering particles following their haloID
  real(kind=8) :: tOuthalopart = 0.0d0           !< time for writing halo part files
  real(kind=8) :: tOutmass = 0.0d0               !< time for writing halo mass files
  real(kind=8) :: tSelectHalo = 0.0d0            !< time for selecting halo with mass > Mmin
  real(kind=8) :: tSort = 0.0d0                  !< time for sorting particles following their haloID

end module modtiming

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2015 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Subroutines to read info files written by RAMSES for snapshot and light cone
!! @brief
!! Subroutines to read info files written by RAMSES for snapshot and light cone
!! @author Fabrice Roy
! ======================================================================
module modreadinfo

  implicit none

  private

  public :: readinforamses, &
       readinfoconepart, &
       readinfoconegrav

  character(*), parameter :: INT_FMT='(A13,I11)'
  character(*), parameter :: LONGINT_FMT='(A13,I20)'
  character(*), parameter :: DOUBLE_FMT='(A13,E24.15)'

contains

  !=======================================================================
  !> Read RAMSES snapshot info file
  subroutine Readinforamses(filename, inforamses, ierr, errormessage)

    use modconstant, only : ERR_MSG_LEN, FILENAME_LEN
    use type_info_ramses_mod, only : type_info_ramses

    character(len=FILENAME_LEN), intent(in)  :: filename     !< info file name
    type(type_info_ramses), intent(out) :: inforamses   !< info structure
    integer(kind=4), intent(out) :: ierr         !< error code
    character(len=ERR_MSG_LEN), intent(out) :: errormessage !< error message

    ! Local variable
    character(len=13) :: dumchar
    integer :: unit_info

    open(newunit=unit_info, file=filename, status='old', iostat=ierr)
    if( ierr > 0 ) then
       errormessage='Error opening ramses info file '//trim(filename)
       return
    end if

    read(unit_info, fmt=INT_FMT,    iostat=ierr) dumchar, inforamses%ncpu
    read(unit_info, fmt=INT_FMT,    iostat=ierr) dumchar, inforamses%ndim
    read(unit_info, fmt=INT_FMT,    iostat=ierr) dumchar, inforamses%levelmin
    read(unit_info, fmt=INT_FMT,    iostat=ierr) dumchar, inforamses%levelmax
    read(unit_info, fmt=INT_FMT,    iostat=ierr) dumchar, inforamses%ngridmax
    read(unit_info, fmt=INT_FMT,    iostat=ierr) dumchar, inforamses%nstep_coarse
    read(unit_info, *,              iostat=ierr)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ierr) dumchar, inforamses%boxlen
    read(unit_info, fmt=DOUBLE_FMT, iostat=ierr) dumchar, inforamses%time
    read(unit_info, fmt=DOUBLE_FMT, iostat=ierr) dumchar, inforamses%aexp
    read(unit_info, fmt=DOUBLE_FMT, iostat=ierr) dumchar, inforamses%h0
    read(unit_info, fmt=DOUBLE_FMT, iostat=ierr) dumchar, inforamses%omega_m
    read(unit_info, fmt=DOUBLE_FMT, iostat=ierr) dumchar, inforamses%omega_l
    read(unit_info, fmt=DOUBLE_FMT, iostat=ierr) dumchar, inforamses%omega_k
    read(unit_info, fmt=DOUBLE_FMT, iostat=ierr) dumchar, inforamses%omega_b
    read(unit_info, fmt=DOUBLE_FMT, iostat=ierr) dumchar, inforamses%unit_l
    read(unit_info, fmt=DOUBLE_FMT, iostat=ierr) dumchar, inforamses%unit_d
    read(unit_info, fmt=DOUBLE_FMT, iostat=ierr) dumchar, inforamses%unit_t
    close(unit_info)

    if( ierr > 0 ) then
       errormessage='Error reading ramses info file '//trim(filename)
       return
    end if

  end subroutine Readinforamses


  !=======================================================================
  !> Read RAMSES particles light cone info file
  subroutine Readinfoconepart(filename, infocone, ierr, errormessage)

    use modconstant, only : ERR_MSG_LEN, FILENAME_LEN
    use type_info_ramses_mod, only : type_info_cone_part

    character(len=FILENAME_LEN), intent(in) :: filename         !< info file name
    type(type_info_cone_part), intent(out) :: infocone !< info structure
    integer(kind=4), intent(out) :: ierr               !< error code
    character(len=ERR_MSG_LEN), intent(out) :: errormessage    !< error message

    ! Local variable
    character(len=13) :: dumchar
    integer :: unit_info

    open(newunit=unit_info, file=filename, status='old', iostat=ierr)
    if(ierr > 0) then
       errormessage = 'Error opening conepart info file '//trim(filename)
       return
    end if
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%ncpu
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%nstride
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%nstep_coarse
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%aexp
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%observer_x
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%observer_y
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%observer_z
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%observer_rds
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%cone_id
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%cone_zlim
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%amax
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%amin
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zmax
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zmin
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dmax
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dmin
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dtol
    read(unit_info, fmt=LONGINT_FMT, iostat=ierr) dumchar, infocone%nglobalfile
    read(unit_info, fmt=LONGINT_FMT, iostat=ierr) dumchar, infocone%npart
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%isfullsky
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%thetay
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%thetaz
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%theta
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%phi
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%aendconem2
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%aendconem1
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%aendcone
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%aexpold
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%aexp
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zendconem2
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zendconem1
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zendcone
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zexpold
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zexp
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dendconem2
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dendconem1
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dendcone
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dexpold
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dexp
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%future
    close(unit_info)

    if(ierr > 0) then
       errormessage = 'Error reading in conepart info file '//trim(filename)
       return
    end if

  end subroutine Readinfoconepart

  !=======================================================================
  !> Read RAMSES cells light cone info file
  subroutine Readinfoconegrav(filename, infocone, ierr, errormessage)

    use modconstant, only : ERR_MSG_LEN, FILENAME_LEN
    use type_info_ramses_mod, only : type_info_cone_grav

    character(len=FILENAME_LEN), intent(in) :: filename         !< info file name
    type(type_info_cone_grav), intent(out) :: infocone !< info structure
    integer(kind=4), intent(out) :: ierr               !< error code
    character(len=ERR_MSG_LEN), intent(out) :: errormessage    !< error message

    ! Local variable
    character(len=13) :: dumchar
    integer :: unit_info

    open(newunit=unit_info, file=filename, status='old', iostat=ierr)
    if(ierr > 0) then
       errormessage = 'Error opening conegrav info file '//trim(filename)
       return
    end if
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%ncpu
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%nstride
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%nstep_coarse
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%aexp
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%nlevel
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%levelmin
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%levelmax
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%observer_x
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%observer_y
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%observer_z
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%observer_rds
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%cone_id
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%cone_zlim
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%amax
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%amin
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zmax
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zmin
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dmax
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dmin
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dtol
    read(unit_info, fmt=LONGINT_FMT, iostat=ierr) dumchar, infocone%nglobalfile
    read(unit_info, fmt=LONGINT_FMT, iostat=ierr) dumchar, infocone%nglobalcell
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%isfullsky
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%thetay
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%thetaz
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%theta
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%phi
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%aendconem2
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%aendconem1
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%aendcone
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zendconem2
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zendconem1
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%zendcone
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dendconem2
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dendconem1
    read(unit_info, fmt=DOUBLE_FMT,  iostat=ierr) dumchar, infocone%dendcone
    read(unit_info, fmt=INT_FMT,     iostat=ierr) dumchar, infocone%future
    close(unit_info)

    if(ierr > 0) then
       errormessage = 'Error reading in conegrav info file '//trim(filename)
       return
    end if

  end subroutine Readinfoconegrav

end module modreadinfo

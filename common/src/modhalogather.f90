!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2007-2014 Fabrice Roy, Vincent Bouillot, Yann Rasera
! Copyright 2017 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Contains subroutines used to gather haloes
!! @brief
!> Contains subroutines used to gather haloes 
!! @author Fabrice Roy
!! @author Vincent Bouillot
!! @author Yann Rasera
! ======================================================================

module modhalogather

  use iso_fortran_env, only : OUTPUT_UNIT

  use modconstant, only : PRI, &
       MPI_PRI, &
       Type_parameter_pfof, &
       Type_parameter_pfof_snap, &
       Type_parameter_pfof_cone

  implicit none

  private
  public :: gatherhalo,&
       count_particles_number, &
       merge_particles_number

contains

  !======================================================================
  subroutine gatherhalo(param, mpicomm)

    use modmpicommons, only : procID
    use modsort, only : heapsort
    use modvarcommons, only : local_npart,&
         field, &
         pfof_id,&
         position,&
         potential,&
         structure_id,&
         velocity

    integer(kind=4), intent(in) :: mpicomm
    class(Type_parameter_pfof), intent(in) :: param

    integer(kind=4) :: local_structure_number
    integer(kind=PRI), allocatable, dimension(:,:) :: local_part_number_per_structure
    integer(kind=PRI), allocatable, dimension(:,:) :: global_particles_count
    integer(kind=PRI), allocatable, dimension(:,:) :: distributed_halo_size
    integer(kind=4) :: local_selected_halo_number
    integer(kind=4), dimension(:), allocatable :: global_selected_halo_pid
    integer(kind=4), dimension(:), allocatable :: local_selected_halo_pid
    integer(kind=PRI), dimension(:,:), allocatable :: particle_disp
    integer(kind=PRI), dimension(:), allocatable   :: displacement_particles_exchange
    integer(kind=PRI)                  :: local_selected_particle_number
    integer(kind=PRI), dimension(:), allocatable :: number_of_particles_to_send_to
    integer(kind=4) :: final_local_halo_number
    integer(kind=PRI), dimension(:), allocatable :: finalstructureid

    integer :: ip

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter gatherhalo on process ',procID
#endif

    ! sort the particles according structure_id
    if(param%do_read_gravitational_field) then
       if(param%do_read_potential) then
          call heapsort(local_npart, structure_id, position, velocity, field, potential, pfof_id) !, ramses_id)
       else
          call heapsort(local_npart, structure_id, position, velocity, field, pfof_id)
       end if
    else
       if(param%do_read_potential) then
          call heapsort(local_npart, structure_id, position, velocity, potential, pfof_id) !, ramses_id)
       else
          call heapsort(local_npart, structure_id, position, velocity, pfof_id)
       end if
    end if

    ! count particles in each halo on the current process
    call count_particles_number(local_npart, structure_id, local_part_number_per_structure, local_structure_number)

    ! initialize global_particles_count with local_part_number_per_structure
    allocate(global_particles_count(2,local_structure_number), stat=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for global_particles_count in gatherhalo', ERR_CODE_MEM_ALLOC)
    global_particles_count = local_part_number_per_structure

    ! merge particles numbers in each halo across the processes
    call merge_particles_number(local_structure_number,global_particles_count)

    ! select halos with mass > param%mmin in global count
    call select_halos(param, global_particles_count, distributed_halo_size)
    deallocate(global_particles_count)

    ! select "local" halos (or halo parts) that belong to final halo list
    call select_local_halos(distributed_halo_size, local_part_number_per_structure, &! local_halo_size, &
         global_selected_halo_pid, local_selected_halo_pid, local_selected_halo_number, mpicomm)

    ! subhalo detection not implemented yet: allocation for compatibility purpose
    allocate(halosubhaloNB(1), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for halosubhalonb in gatherhalo', ERR_CODE_MEM_ALLOC)

    ! for each selected halo: assign halo to a process
    call dispatch_halos(local_part_number_per_structure, distributed_halo_size, &
         local_selected_halo_pid, global_selected_halo_pid, mpicomm, displacement_particles_exchange, &
         local_selected_particle_number, number_of_particles_to_send_to, final_local_halo_number)

    ! compute displacement to the first particle of each selected halo in pos/vel/id/etc... arrays
    call compute_particle_disp(local_selected_halo_number, local_selected_halo_pid, local_part_number_per_structure, particle_disp)

    call dispatch_particles(finalstructureid, displacement_particles_exchange, local_part_number_per_structure, &
         local_selected_halo_pid, local_selected_particle_number,&
         mpicomm, number_of_particles_to_send_to, param, particle_disp)

    call heapsort(int(local_selected_particle_number, kind=4),finalstructureid,halopartpos,&
         halopartvel,halopartfor,halopartpot,halopartid)

    call computemassandid(final_local_halo_number,finalstructureid)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Exit gatherhalo on process ',procID
#endif


  end subroutine gatherhalo


  !======================================================================
  ! count the number of particles with same halo id for each halo id on the local process
  ! structure_id is ordered
  subroutine count_particles_number(local_npart, structure_id, part_number_per_structure, structure_number)

    integer(kind=4), intent(in) :: local_npart
    integer(kind=PRI), intent(in), dimension(:) :: structure_id
    integer(kind=PRI), intent(out), allocatable, dimension(:,:) :: part_number_per_structure
    integer(kind=4), intent(out) :: structure_number

    integer(kind=4) :: ip
    integer(kind=PRI) :: current_id
    integer(kind=4) :: current_value

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter count_particles_number on process ', procid
#endif

    current_id = structure_id(1)
    structure_number = 1
    do ip = 2, local_npart
       if(structure_id(ip) /= current_id) then
          structure_number = structure_number + 1
          current_id = structure_id(ip)
       end if
    end do

    allocate(part_number_per_structure(2,structure_number), STAT=allocstat)
    if(allocstat > 0) call emergencystop('Allocate failed for part_number_per_structure in count_particles_number', ERR_CODE_MEM_ALLOC)
    part_number_per_structure = 0

    current_value = 1
    part_number_per_structure(1,1) = structure_id(1)
    part_number_per_structure(2,1) = 1
    do ip = 2, local_npart
       if(structure_id(ip) == part_number_per_structure(1,current_value)) then
          part_number_per_structure(2,current_value) = part_number_per_structure(2,current_value) + 1
       else
          current_value = current_value + 1
          part_number_per_structure(1,current_value) = structure_id(ip)
          part_number_per_structure(2,current_value) = 1
       end if
    end do

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Exit count_particles_number on process ', procid
#endif


  end subroutine count_particles_number

  !======================================================================
  subroutine merge_particles_number(local_structure_number, global_count)

    use modmpicommons, only : procID, &
         procNB

    integer(kind=4), intent(inout) :: local_structure_number
    integer(kind=PRI), allocatable, dimension(:,:), intent(inout) :: global_count
    integer(kind=PRI), allocatable, dimension(:,:) :: merged_particles_count
    integer(kind=PRI), allocatable, dimension(:,:) :: dest_particles_count

    integer(kind=4) :: dest, right, left, dest_nvalues, merged_nvalues
    integer(kind=4) :: loop, loop_number
    logical(kind=4) :: even_pid
    integer(kind=4) :: dest_ip, local_ip, merged_ip, first_ip, last_ip

    integer(kind=4) :: mpierr
    integer(kind=4), dimension(Mpi_Status_Size) :: mpistat

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter merge_particles_number on process ',procid
#endif

    ! pid of the process right and left to current one
    right = mod(procID+1,procNB)
    left = mod(procID-1+procNB,procNB)

    ! first process has no left neighboor
    ! and last process has no right neighboor
    if(procID == 0) left = Mpi_Proc_Null
    if(procID == procNB-1) right = Mpi_Proc_Null

    ! is current process even?
    even_pid = (mod(procID,2)==0)

    ! there will be procNB/2 merging and 2 merging per loop => procNB/4 loops +1 for safety
    loop_number = procNB / 2 + 1


    do loop = 1, loop_number

       ! merge 0 with 1, 2 with 3, 4 with 5, etc...
       if(even_pid) then
          dest = right ! even process communicates with right neighboor
       else
          dest = left  ! odd process communicates with left neighboor
       end if

       dest_nvalues = 0
       ! exchange of number of values in particles number per halo ID
       call Mpi_Sendrecv(local_structure_number, 1, Mpi_Integer, dest, 0, &
            dest_nvalues, 1, Mpi_Integer, dest, 0, Mpi_Comm_World, mpistat, mpierr)

       ! allocate array for particles number per halo ID from neighbour
       if(dest_nvalues /= 0) then
          allocate(dest_particles_count(2,dest_nvalues), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for dest_particules_count in merge_particles_number', ERR_CODE_MEM_ALLOC)
          ! allocate array for merged particles number per halo ID
          merged_nvalues = (local_structure_number + dest_nvalues)
          allocate(merged_particles_count(2,merged_nvalues), STAT=allocstat)
          if(allocstat > 0) call emergencystop('Allocate failed for merge_particles_count in merge_particles_count', ERR_CODE_MEM_ALLOC)

          ! exchange particles number per halo ID
          call Mpi_Sendrecv(global_count, 2*local_structure_number, Mpi_Pri, dest, 0, &
               dest_particles_count, 2*dest_nvalues, Mpi_Pri, dest, 0, Mpi_Comm_World, mpistat, mpierr)

          ! loop until we reach the end of both our and neighboor's array of particles number per halo ID
          local_ip = 1
          dest_ip = 1
          merged_ip = 1
          do
             ! we hit the end of both array: end of the loop
             if(local_ip == local_structure_number .and. dest_ip == dest_nvalues) exit
             ! we hit the end of local array: we copy the end of neighboor's array
             if(local_ip == local_structure_number) then
                !                write(OUTPUT_UNIT,*) procID,' LOCAL TERMINE A MERGED=',merged_ip
                do
                   merged_particles_count(1,merged_ip) = dest_particles_count(1,dest_ip)
                   merged_particles_count(2,merged_ip) = dest_particles_count(2,dest_ip)
                   merged_ip = merged_ip + 1
                   dest_ip = dest_ip + 1
                   if(dest_ip > dest_nvalues) exit
                end do
                exit
             end if
             ! we hit the end of neighboor's array: we copy the end of local array
             if(dest_ip == dest_nvalues) then
                !                write(OUTPUT_UNIT,*) procID,' DEST TERMINE A MERGED=',merged_ip
                do
                   merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                   merged_particles_count(2,merged_ip) = global_count(2,local_ip)
                   merged_ip = merged_ip + 1
                   local_ip = local_ip + 1
                   if(local_ip > local_structure_number) exit
                end do
                exit
             end if
             ! we add next element from local array because the haloID is lower than neighboor's one
             if(global_count(1,local_ip) < dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                merged_particles_count(2,merged_ip) = global_count(2,local_ip)
                merged_ip = merged_ip + 1
                local_ip = local_ip + 1
                cycle
                ! local and neighboor's halo ID are the same: same halo, we add particles numbers
             else if(global_count(1,local_ip) == dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                merged_particles_count(2,merged_ip) = global_count(2,local_ip) + &
                     dest_particles_count(2,dest_ip)
                merged_ip = merged_ip + 1
                local_ip = local_ip + 1
                dest_ip = dest_ip + 1
                cycle
                ! we add next element from neighboor's array because the haloID is lower than local one
             else if(global_count(1,local_ip) > dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = dest_particles_count(1,dest_ip)
                merged_particles_count(2,merged_ip) = dest_particles_count(2,dest_ip)
                merged_ip = merged_ip + 1
                dest_ip = dest_ip + 1
                cycle
             end if
          end do

          ! decread merge_ip by one (correction from the loop)
          merged_ip = merged_ip - 1

          ! even: keep left part of the merged count array
          if(even_pid) then
             local_structure_number = merged_ip/2
             if(mod(merged_ip,2)==1) local_structure_number = local_structure_number+1
             first_ip = 1
             last_ip = local_structure_number
          else ! odd: keep right part of the merged count array
             local_structure_number = merged_ip/2
             first_ip = local_structure_number+1
             if(mod(merged_ip,2)==1) first_ip = first_ip+1
             last_ip = first_ip + local_structure_number - 1
          end if

          deallocate(global_count)
          allocate(global_count(2,local_structure_number))
          global_count(:,:) = merged_particles_count(:,first_ip:last_ip)
          deallocate(merged_particles_count)
          deallocate(dest_particles_count)

       end if


       ! 1 with 2, 3 with 4, etc...
       if(even_pid) then
          dest = left
       else
          dest = right
       end if

       dest_nvalues = 0
       ! exchange of number of values in particles number per halo ID
       call Mpi_Sendrecv(local_structure_number, 1, Mpi_Integer, dest, 0, &
            dest_nvalues, 1, Mpi_Integer, dest, 0, Mpi_Comm_World, mpistat, mpierr)

       if(dest_nvalues /= 0) then
          ! allocate array for particles number per halo ID from neighbour
          allocate(dest_particles_count(2,dest_nvalues))
          ! allocate array for merged particles number per halo ID
          merged_nvalues = (local_structure_number + dest_nvalues)
          allocate(merged_particles_count(2,merged_nvalues))

          ! exchange particles number per halo ID
          call Mpi_Sendrecv(global_count, 2*local_structure_number, Mpi_Pri, dest, 0, &
               dest_particles_count, 2*dest_nvalues, Mpi_Pri, dest, 0, Mpi_Comm_World, mpistat, mpierr)

          ! loop until we reach the end of both our and neighboor's array of particles number per halo ID
          local_ip = 1
          dest_ip = 1
          merged_ip = 1
          do
             ! we hit the end of both array: end of the loop
             if(local_ip == local_structure_number .and. dest_ip == dest_nvalues) exit
             ! we hit the end of local array: we copy the end of neighboor's array
             if(local_ip == local_structure_number) then
                do
                   merged_particles_count(1,merged_ip) = dest_particles_count(1,dest_ip)
                   merged_particles_count(2,merged_ip) = dest_particles_count(2,dest_ip)
                   merged_ip = merged_ip + 1
                   dest_ip = dest_ip + 1
                   if(dest_ip > dest_nvalues) exit
                end do
                exit
             end if
             ! we hit the end of neighboor's array: we copy the end of local array
             if(dest_ip == dest_nvalues) then
                do
                   merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                   merged_particles_count(2,merged_ip) = global_count(2,local_ip)
                   merged_ip = merged_ip + 1
                   local_ip = local_ip + 1
                   if(local_ip > local_structure_number) exit
                end do
                exit
             end if
             ! we add next element from local array because the haloID is lower than neighboor's one
             if(global_count(1,local_ip) < dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                merged_particles_count(2,merged_ip) = global_count(2,local_ip)
                merged_ip = merged_ip + 1
                local_ip = local_ip + 1
                cycle
                ! local and neighboor's halo ID are the same: same halo, we add particles numbers
             else if(global_count(1,local_ip) == dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = global_count(1,local_ip)
                merged_particles_count(2,merged_ip) = global_count(2,local_ip) + &
                     dest_particles_count(2,dest_ip)
                merged_ip = merged_ip + 1
                local_ip = local_ip + 1
                dest_ip = dest_ip + 1
                cycle
                ! we add next element from neighboor's array because the haloID is lower than local one
             else if(global_count(1,local_ip) > dest_particles_count(1,dest_ip)) then
                merged_particles_count(1,merged_ip) = dest_particles_count(1,dest_ip)
                merged_particles_count(2,merged_ip) = dest_particles_count(2,dest_ip)
                merged_ip = merged_ip + 1
                dest_ip = dest_ip + 1
                cycle
             end if
          end do

          ! decread merge_ip by one (correction from the loop)
          merged_ip = merged_ip - 1

          ! even: keep right part of the merged count array
          if(even_pid) then
             local_structure_number = merged_ip/2
             first_ip = local_structure_number+1
             if(mod(merged_ip,2)==1) first_ip = first_ip+1
             last_ip = first_ip + local_structure_number - 1
          else ! odd: keep left part of the merged count array
             local_structure_number = merged_ip/2
             if(mod(merged_ip,2)==1) local_structure_number = local_structure_number+1
             first_ip = 1
             last_ip = local_structure_number
          end if

          deallocate(global_count)
          allocate(global_count(2,local_structure_number))
          global_count(:,:) = merged_particles_count(:,first_ip:last_ip)
          deallocate(merged_particles_count)
          deallocate(dest_particles_count)
       end if

    end do

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'Enter merge_particles_number on process ',procid
#endif

  end subroutine merge_particles_number



end module modhalogather

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Constant parameters definition
!! @brief
!! 
!! @author Fabrice Roy
!! @author Vincent Bouillot

!> Constant parameters definition
module modconstant

  use iso_c_binding
  use mpi

  implicit none

  private
  public :: ERR_CODE_COMPUTATION, &
       ERR_CODE_FILE_NOT_FOUND, &
       ERR_CODE_READ_ERROR, &
       ERR_CODE_WRONG_PARAMETER, &
       ERR_MSG_LEN, &
       FILENAME_LEN, &
       IDKIND, &
       LOG_UNIT, &
       MPI_IDKIND, &
       NAME_CONECREATOR_GRAV, &
       NAME_CONECREATOR_PART, &
       NAME_CONEMAPPER, &
       NAME_PFOF_CONE, &
       NAME_PFOF_SNAP, &
       NAME_PSOD_SNAP, &
       PR, &
       DP, &
       QDP, &
       GIT_VERSION, &
       INT_FMT, LONGINT_FMT, DOUBLE_FMT, CHAR_FMT, &
       type_common_metadata, &
       type_parameter_conecreator, &
       type_parameter_conecreator_grav, &
       type_parameter_conecreator_part, &
       type_parameter_halofinder, &
       type_parameter_pfof, &
       type_parameter_pfof_cone, &
       type_parameter_pfof_snap, &
       type_parameter_psod_snap

#include "gitversion.h"

  ! error handling
  integer, parameter :: ERR_CODE_COMPUTATION = 30
  integer, parameter :: ERR_CODE_FILE_NOT_FOUND = 1
  integer, parameter :: ERR_CODE_MEM_ALLOC = 20
  integer, parameter :: ERR_CODE_READ_ERROR = 2
  integer, parameter :: ERR_CODE_WRONG_PARAMETER = 3
  integer, parameter :: ERR_MSG_LEN = 256

  ! filename length
  integer, parameter :: FILENAME_LEN = 512

#ifdef LONGINT
  integer, parameter :: IDKIND = 8
  integer, parameter :: MPI_IDKIND = MPI_INTEGER8
#else
  integer, parameter :: IDKIND = 4
  integer, parameter :: MPI_IDKIND = MPI_INTEGER
#endif

  ! Output Units
  integer, parameter :: LOG_UNIT = 50   !< I/O unit for text log file

  ! Name of the codes to write as metadata in HDF5 files
  character(len=16), parameter :: NAME_CONECREATOR_GRAV = 'conecreator_grav'
  character(len=16), parameter :: NAME_CONECREATOR_PART = 'conecreator_part'
  character(len=16), parameter :: NAME_CONEMAPPER = 'conemapper'
  character(len=16), parameter :: NAME_PFOF_CONE = 'pfof_cone'
  character(len=16), parameter :: NAME_PFOF_SNAP = 'pfof_snap'
  character(len=16), parameter :: NAME_PSOD_SNAP = 'psod_snap'

#ifdef LONGREAL
  integer, parameter :: PR=8   !< Precision for real arrays read from Ramses simulations (position/velocities)
#else
  integer, parameter :: PR=4   !< Precision for real arrays read from Ramses simulations (position/velocities)
#endif

  ! constant definitions from RAMSES
#ifndef NPRE
  integer, parameter :: DP=kind(1.0_4)
#else
#if NPRE==4
  integer, parameter :: DP=kind(1.0_4)
#else  
  integer, parameter :: DP=kind(1.0_8)
#endif
#endif

#ifdef QUADHILBERT  
  integer, parameter :: QDP=kind(1.0_16)
#else
  integer, parameter :: QDP=kind(1.0_8)
#endif
  
  ! GIT Revision
  character(len=64), parameter :: GIT_VERSION = GITVERSION

  ! FORMAT constants
  character(*), parameter :: INT_FMT='(a13,i11)'
  character(*), parameter :: LONGINT_FMT='(a13,i20)'
  character(*), parameter :: DOUBLE_FMT='(a13,e24.15)'
  character(*), parameter :: CHAR_FMT='(a14,a80)'

  
  type :: type_parameter_halofinder
     character(len=200) :: input_path      !< path to the directory containing the ramses files
     character(len=200) :: part_input_file  !< name of the files containing the particles data
     character(len=200) :: simulation_name  !< simulation name to be written in output files
     integer(kind=4)    :: mmin            !< minimum mass of the haloes
     integer(kind=4)    :: mmax            !< maximum mass of the haloes
     logical(kind=4)    :: star                 !< do the ramses files contain stars?
     logical(kind=4)    :: metal                !< do the ramses files contain metalicities?
     logical(kind=4)    :: do_skip_mass         !< does the user want to skip masses (i.e. they will not be written in output files)
     logical(kind=4)    :: do_skip_star         !< does the user want to skip stars (i.e. they will not be written in output files)?
     logical(kind=4)    :: do_skip_metal        !< does the user want to skip metallicities (i.e. they will not be written in output files)?
     logical(kind=4)    :: do_read_potential    !< do the ramses files contain potential?
     logical(kind=4)    :: do_read_gravitational_field  !< do the ramses files contain force?
     logical(kind=4)    :: do_unbinding    !< unbinding process? (not implemented yet)
     logical(kind=4)    :: do_subhalo      !< subhalo detection? (not implemented yet)
     logical(kind=4)    :: do_timings  !< should pfof perform timings (imply extra synchronizations)
  end type type_parameter_halofinder
  
  type, extends(type_parameter_halofinder) :: type_parameter_psod_snap
     logical(kind=4) :: do_sod
  end type type_parameter_psod_snap
  
  type, extends(type_parameter_halofinder) :: type_parameter_pfof !< type for pfof input parameters common to snapshot and cone versions
     real(kind=4)       :: percolation_length  !< fof percolation length
  end type type_parameter_pfof
  
  type, extends(type_parameter_pfof)  :: type_parameter_pfof_snap
     integer(kind=4)    :: grpsize     !< size of the i/o group used for the ramses simulation
     integer(kind=4)    :: gatherread_factor    !< gather parameter for parallel hdf5 input
     integer(kind=4)    :: snapshot             !< ramses output number to be written in output files
     integer(kind=4)    :: gatherwrite_factor   !< gather parameter for parallel hdf5 output
     logical(kind=4)    :: do_read_from_cube    !< should pfof read particles from cube files?
     logical(kind=4)    :: do_fof   !< should pfof perform fof halo finding?
     logical(kind=4)    :: do_write_cube        !< should pfof write cube files?
     logical(kind=4)    :: do_sort_cube         !< sort the particles and write a 'sorted cube'
     character(len=3)   :: code_index  !< version of ramses used to produce the ramses files
     character(len=200) :: info_input_file  !< name of the ramses info file
  end type type_parameter_pfof_snap
  
  type, extends(type_parameter_pfof) :: type_parameter_pfof_cone !< extension of type_parameter_pfof for the cone version
     logical(kind=4) :: do_read_ramses_part_id
     integer(kind=4) :: shell_first_id
     integer(kind=4) :: shell_last_id
     logical(kind=4) :: do_gather_halo
  end type type_parameter_pfof_cone
  
  type :: type_parameter_conecreator !< type for conecreator input parameters common to particles and cells versions
     character(len=200) :: input_path
     character(len=200) :: simulation_name
     character(len=200) :: cone_input_file
     character(len=200) :: info_cone_input_file
     character(len=200) :: info_ramses_input_file
     integer(kind=4) :: nfile
     integer(kind=4) :: first_file
     integer(kind=4) :: last_file
     real(kind=8) :: cone_max_radius
     real(kind=8) :: cube_size
  end type type_parameter_conecreator
  
  type, extends(type_parameter_conecreator) :: type_parameter_conecreator_part !< extension of type_parameter_conecreator for the particles version
     logical(kind=4) :: star                 !< do the ramses files contain stars?
     logical(kind=4) :: metal                !< do the ramses files contain metalicities?
     logical(kind=4) :: do_skip_mass         !< does the user want to skip masses (i.e. they will not be written in output files)
     logical(kind=4) :: do_skip_star         !< does the user want to skip stars (i.e. they will not be written in output files)?
     logical(kind=4) :: do_skip_metal        !< does the user want to skip metallicities (i.e. they will not be written in output files)?
     logical(kind=4) :: do_read_ramses_part_id
     logical(kind=4) :: do_read_potential
     logical(kind=4) :: do_read_gravitational_field
  end type type_parameter_conecreator_part
  
  type, extends(type_parameter_conecreator) :: type_parameter_conecreator_grav !< extension of type_parameter_conecreator for the cells version
     integer(kind=4) :: nlevel
     integer(kind=4) :: levelmin
  end type type_parameter_conecreator_grav
  
  
  type :: type_common_metadata !< type for metadata common to every kind of hdf5 output file
     character(len=16) :: created_by
     integer(kind=4) :: version
     character(len=16) :: simulation_code
     character(len=16) :: particle_type
     integer(kind=4) :: constant_mass
     character(len=16) :: units
     integer(kind=8) :: npart_simulation
  end type type_common_metadata
  
end module modconstant

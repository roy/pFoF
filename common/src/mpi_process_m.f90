!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Class definitions for process and communicator
!! @brief
!! 
!! @author Fabrice Roy

!> Class definitions for process and communicator
module mpi_process_m

  use iso_fortran_env, only : OUTPUT_UNIT
  use mpi

  implicit none

  private
  public :: mpi_process_t

  type :: mpi_communicator_t
     integer(kind=4) :: name
     integer(kind=4) :: size
     integer(kind=4), dimension(3) :: dims
     logical(kind=4), dimension(3) :: periods
  end type mpi_communicator_t

  type :: mpi_process_t
     integer(kind=4) :: rank
     integer(kind=4), dimension(3) :: coords
     integer(kind=4), dimension(6) :: neighbours
     type(mpi_communicator_t) :: comm
     class(mpi_process_t), pointer :: reading_process => null()
     class(mpi_process_t), pointer :: writing_process => null()
   contains
     procedure :: Init => Init_mpi_process
     procedure :: Print => Print_mpi_process
  end type mpi_process_t
     
contains
!----------------------------------------------------------------------------------------------------------------------------------
  !> Initialize process
  subroutine Init_mpi_process(process)

    class(mpi_process_t), intent(out) :: process
    integer :: mpierr

    call mpi_comm_rank(MPI_COMM_WORLD, process%rank, mpierr)
    call mpi_comm_size(MPI_COMM_WORLD, process%comm%size, mpierr)
    process%comm%name=MPI_COMM_WORLD
    process%coords=0
    process%neighbours=MPI_COMM_NULL

  end subroutine Init_mpi_process

!----------------------------------------------------------------------------------------------------------------------------------
  !> Print process
  subroutine Print_mpi_process(process)

    class(mpi_process_t), intent(in) :: process

    write(OUTPUT_UNIT,'(a,i0)') 'process rank = ', process%rank
    write(OUTPUT_UNIT,'(a,i0,a,i0,a,i0,a)') 'process coordinates = (', process%coords(1), ';', &
         process%coords(2),';', process%coords(1), ')' 

  end subroutine Print_mpi_process

end module mpi_process_m

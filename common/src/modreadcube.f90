!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2015 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Subroutines to read HDF5 dark matter cube files
!! @brief
!! Subroutines to read HDF5 dark matter cube files
!! @author Fabrice Roy
! ======================================================================

module modreadcube

  use fortran_hdf5_constants_m, only : H5_FILENAME_LEN, H5_STR_LEN
  use fortran_hdf5_manage_groups_m, only : hdf5_close_group, hdf5_open_group
  use fortran_hdf5_manage_files_m, only : hdf5_close_file, hdf5_open_file, Hdf5_open_mpi_file
  use fortran_hdf5_read_attribute_m
  use fortran_hdf5_read_data_m
  use fortran_hdf5_read_mpi_data_m
  use hdf5, only : HID_T
  use iso_fortran_env, only : OUTPUT_UNIT
  use modconstant, only : IDKIND
  use mpi_communicator_m, only : info_proc
  use variables_m, only : common_meta, inforamses, ngrid, param, xmax, xmin, ymax, ymin, zmax, zmin
  use modmpicommons, only : Emergencystop, procid
  use modvarcommons, only : field, global_npart, local_npart, nres, pfof_id, position, potential, velocity
  use mpi
  use modreadmeta, only : Read_meta_common, Read_meta_info_ramses


  implicit none

  procedure(), pointer :: Readcube

  private
  public :: Selectreadcubetype, &
       Readcube

contains

  !=======================================================================
  subroutine Selectreadcubetype()

    integer(kind=HID_T) :: file_id
    integer(kind=HID_T) :: gr_id
    character(len=H5_FILENAME_LEN) :: filename
    character(len=H5_STR_LEN) :: h5name
    character(len=H5_STR_LEN) :: filetype
    integer(kind=4) :: mpierr

    if(procID==0) then
       filename = trim(param%input_path)//trim(param%part_input_file)
       call hdf5_open_file(filename, file_id)
       h5name = 'metadata'
       call hdf5_open_group(file_id, h5name, gr_id)
       h5name = 'file_type'
       call hdf5_read_attr(gr_id, h5name, len(filetype), filetype)
       call hdf5_close_group(gr_id)
       call hdf5_close_file(file_id)
    end if

    call Mpi_Bcast(filetype, len(filetype), Mpi_Char, 0, Mpi_Comm_World, mpierr)

    select case (filetype)
    case ("cube")
       Readcube => H5readcube
    case ("mpicube")
       Readcube => Mpih5readcube
    case ("sortedcube")
       Readcube => H5readsortedcube
    case ("mpisortedcube")
       Readcube => Mpih5readsortedcube
    case default
       call Emergencystop("Error while reading cube type",31)
    end select

  end subroutine Selectreadcubetype

  !=======================================================================
  !> This subroutine reads hdf5 cube files created by pFOF.
  subroutine H5readcube()

    integer :: allocstat
    character(len=H5_FILENAME_LEN) :: filename                          ! File name
    integer(HID_T) :: file_id                               ! File identifier
    integer(HID_T) :: gr_id                                 ! Group identifier
    character(len=H5_STR_LEN) :: aname                              ! Attribute name
    character(len=H5_STR_LEN) :: dsetname                           ! Dataset name
    character(len=H5_STR_LEN) :: groupname
    real(kind=4), dimension(6) :: boundaries
    integer(kind=8) :: npart8
    integer(kind=4) :: length
    logical(kind=4) :: islast

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "Enter H5readcube on process ",procID
#endif

    filename = trim(param%input_path)//trim(param%part_input_file)
    length=len_trim(filename)
    write(filename(length-7:length-3),'(I5.5)') procID

    ! open the file
    call Hdf5_open_file(filename, file_id)

    call Read_meta_common(file_id, common_meta)
    islast = .false.
    call Read_meta_info_ramses(file_id, inforamses, islast)

    nres = 2**inforamses%levelmin
    ngrid = int(nres,kind=IDKIND)**3
    global_npart = int(common_meta%npart_simulation,kind=IDKIND)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'levelmin=',inforamses%levelmin, ' on process ',procID
    write(OUTPUT_UNIT,*) 'nres=',nres, ' on process ',procID
    write(OUTPUT_UNIT,*) 'ngrid=',ngrid, ' on process ',procID
#endif

    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id,groupname, gr_id)

    ! read attribute partNB
    dsetname = 'npart_file'
    call hdf5_read_data(gr_id, dsetname, npart8)
    local_npart = int(npart8, kind=4)

    ! read attribute boundaries
    aname = 'boundaries'
    call hdf5_read_attr(gr_id, aname, 6, boundaries)

    xmin=boundaries(1)
    xmax=boundaries(2)
    ymin=boundaries(3)
    ymax=boundaries(4)
    zmin=boundaries(5)
    zmax=boundaries(6)

    call hdf5_close_group(gr_id)

    if(.not.allocated(pfof_id)) allocate(pfof_id(local_npart),stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for pfof_id in H5readcube', allocstat)
    if(.not.allocated(position))  allocate(position(3,local_npart),stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for position in H5readcube', allocstat)
    if(.not.allocated(velocity))  allocate(velocity(3,local_npart),stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for velocity in H5readcube', allocstat)

    groupname = 'data'
    call hdf5_open_group(file_id, groupname, gr_id)

    ! read position of the particles
    dsetname = 'position_part'
    call hdf5_read_data(gr_id, dsetname, 3, local_npart, position)

    ! read velocity of the particles
    dsetname = 'velocity_part'
    call hdf5_read_data(gr_id, dsetname, 3, local_npart, velocity)

    ! read id of the particles
    dsetname = 'identity_part'
    call hdf5_read_data(gr_id, dsetname, local_npart, pfof_id)

    if(param%do_read_potential) then
       if(.not.allocated(potential)) allocate(potential(local_npart), stat=allocstat)
       if(allocstat > 0) call Emergencystop('Allocate failed for potential in H5readcube', allocstat)
       dsetname = 'potential_part'
       call hdf5_read_data(gr_id, dsetname, local_npart, potential)
    end if

    if(param%do_read_gravitational_field) then
       if(.not.allocated(field)) allocate(field(3,local_npart), stat=allocstat)
       if(allocstat > 0) call Emergencystop('Allocate failed for field in H5readcube', allocstat)
       dsetname = 'gravitational_field_part'
       call hdf5_read_data(gr_id, dsetname, 3, local_npart, field)
    end if

    ! Close the root group.
    call hdf5_close_group(gr_id)

    call hdf5_close_file(file_id)


#ifdef DEBUG
    write(OUTPUT_UNIT,*) "Exit h5readcube on process ",procID
#endif

  end subroutine H5readcube



  !=======================================================================
  !> This subroutine reads hdf5 sorted cube files created by pFOF.
  subroutine H5readsortedcube()

    integer :: allocstat
    character(len=H5_FILENAME_LEN) :: filename                          ! File name
    integer(HID_T) :: file_id                               ! File identifier
    integer(HID_T) :: gr_data_id                            ! Group identifier
    integer(HID_T) :: gr_id                                 ! Group identifier
    character(len=8)  :: gid_char
    character(len=H5_STR_LEN) :: aname                              ! Attribute name
    character(len=H5_STR_LEN) :: dsetname                           ! Dataset name
    character(len=H5_STR_LEN) :: groupname
    real(kind=4), dimension(6) :: boundaries
    integer(kind=4) :: ngroup
    integer(kind=4), dimension(:), allocatable :: npartpergroup
    integer(kind=4) :: igroup
    integer(kind=4) :: indbeg, indend
    integer(kind=4) :: length
    integer(kind=8) :: npart8
    logical(kind=4) :: islast

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "Enter H5readsortedcube on process ",procID
#endif

    filename = trim(param%input_path)//trim(param%part_input_file)
    length=len_trim(filename)
    write(filename(length-7:length-3),'(I5.5)') procID

    ! open the file
    call hdf5_open_file(filename, file_id)

    call Read_meta_common(file_id, common_meta)
    islast = .false.
    call Read_meta_info_ramses(file_id, inforamses, islast)

    nres = 2**inforamses%levelmin
    ngrid = int(nres,kind=IDKIND)**3
    global_npart = int(common_meta%npart_simulation,kind=IDKIND)

    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id,groupname, gr_id)

    ! read attribute partNB
    dsetname = 'npart_file'
    call hdf5_read_data(gr_id, dsetname, npart8)
    local_npart = int(npart8,kind=4)

    ! read attribute boundaries
    aname = 'boundaries'
    call hdf5_read_attr(gr_id, aname, 6, boundaries)

    xmin=boundaries(1)
    xmax=boundaries(2)
    ymin=boundaries(3)
    ymax=boundaries(4)
    zmin=boundaries(5)
    zmax=boundaries(6)

    ! read attribute ngroup
    aname = 'ngroup'
    call hdf5_read_attr(gr_id, aname, ngroup)

    allocate(npartpergroup(ngroup), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for npartpergroup in H5readsortedcube', allocstat)
    aname = 'npart_grp_array'
    call hdf5_read_data(gr_id, aname, ngroup, npartpergroup)

    call hdf5_close_group(gr_id)

    groupname='data'
    call hdf5_open_group(file_id, groupname, gr_data_id)

    if(.not.allocated(pfof_id)) allocate(pfof_id(local_npart))
    if(allocstat > 0) call Emergencystop('Allocate failed for pfof_id in H5readsortedcube', allocstat)
    if(.not.allocated(position))  allocate(position(3,local_npart))
    if(allocstat > 0) call Emergencystop('Allocate failed for position in H5readsortedcube', allocstat)
    if(.not.allocated(velocity))  allocate(velocity(3,local_npart))
    if(allocstat > 0) call Emergencystop('Allocate failed for velocity in H5readsortedcube', allocstat)

    indbeg = 1
    indend = 0
    ! loop over the groups
    do igroup=1, ngroup
       if(npartpergroup(igroup) /= 0) then ! there is at least one part. to read in this group
          write(gid_char(1:8),'(I8.8)') igroup
          groupname='group'//gid_char
          call hdf5_open_group(gr_data_id,groupname,gr_id)

          indend = indbeg + npartpergroup(igroup) - 1
          ! read position of the particles
          dsetname = 'position_part'
          call hdf5_read_data(gr_id, dsetname, 3, local_npart, position(:,indbeg:indend))

          ! read velocity of the particles
          dsetname = 'velocity_part'
          call hdf5_read_data(gr_id, dsetname, 3, local_npart, velocity(:,indbeg:indend))

          ! read id of the particles
          dsetname = 'identity_part'
          call hdf5_read_data(gr_id, dsetname, local_npart, pfof_id(indbeg:indend))

          ! read potential if requested
          if(param%do_read_potential) then
             if(.not.allocated(potential)) allocate(potential(local_npart))
             if(allocstat > 0) call Emergencystop('Allocate failed for potential in H5readsortedcube', allocstat)
             dsetname = 'potential_part'
             call hdf5_read_data(gr_id, dsetname, local_npart, potential(indbeg:indend))
          end if

          ! read force if requested
          if(param%do_read_gravitational_field) then
             if(.not.allocated(field)) allocate(field(3,local_npart))
             if(allocstat > 0) call Emergencystop('Allocate failed for field in H5readsortedcube', allocstat)
             dsetname = 'gravitational_field_part'
             call hdf5_read_data(gr_id, dsetname, 3, local_npart, field(:,indbeg:indend))
          end if

          indbeg = indend + 1

          call hdf5_close_group(gr_id)
       end if
    end do

    if(indend /= local_npart) then
       write(OUTPUT_UNIT,*) 'Error while reading particles from file ',filename
       call EmergencyStop('Error in h5readsortedcube',32)
    end if

    ! Close the root group.
    call hdf5_close_group(gr_data_id)

    call hdf5_close_file(file_id)

    deallocate(npartpergroup)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "Exit h5readsortedcube on process ",procID
#endif

  end subroutine h5readsortedcube


  !=======================================================================

  subroutine Mpih5readcube()

#ifdef DEBUG
    use modmpicommons, only : procID
#endif

    integer :: allocstat
    character(len=H5_FILENAME_LEN) :: filename

    character(len=H5_STR_LEN) :: dsetname                           ! Dataset name
    character(len=H5_STR_LEN) :: aname                              ! Attribute name
    character(len=H5_STR_LEN) :: groupname

    integer(HID_T) :: file_id                               ! File identifier
    integer(HID_T) :: gr_id                                 ! Group identifier

    integer(kind=4) :: procperfile
    integer(kind=4), dimension(:), allocatable :: partnb_tab

    real(kind=4), dimension(:,:), allocatable :: bound_tab
    integer(kind=8) :: npart8
    integer(kind=4) :: length
    logical(kind=4) :: islast

#ifdef DEBUG
    integer(kind=4) :: mpierr

    write(OUTPUT_UNIT,*) "Enter mpih5readcube on process ",procID
#endif

    ! number of processes writing in the same file
    procperfile = info_proc%read_comm%size !param%gatherread_factor**3

    allocate(partnb_tab(procperfile), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for partnb_tab in Mpih5readcube', allocstat)
    allocate(bound_tab(6,procperfile), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for bound_tab in Mpih5readcube', allocstat)

    filename = trim(param%input_path)//trim(param%part_input_file)
    length=len_trim(filename)
    write(filename(length-7:length-3),'(I5.5)') info_proc%read_comm%color

    call hdf5_open_mpi_file(filename, info_proc%read_comm%name, file_id)

    call Read_meta_common(file_id, common_meta)
    islast = .false.
    call Read_meta_info_ramses(file_id, inforamses, islast)

    nres = 2**inforamses%levelmin
    ngrid = int(nres,kind=IDKIND)**3
    global_npart = int(common_meta%npart_simulation,kind=IDKIND)

    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id,groupname, gr_id)

    ! read attribute partNB
    dsetname = 'npart_file'
    call hdf5_read_data(gr_id, dsetname, npart8)
    local_npart = int(npart8,kind=4)

    ! read attribute partNB
    aname = 'npart_cube_array'
    call hdf5_read_attr(gr_id, aname, 6, partnb_tab)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "partNB_tab read on process ",procID
    call Mpi_Barrier(Mpi_Comm_World, mpierr)
#endif

    local_npart = partnb_tab(info_proc%read_comm%pid+1)

    ! read attribute boundaries
    aname = 'boundaries_array'
    call hdf5_read_attr(gr_id, aname, 6, procperfile, bound_tab)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "boundaries_tab read on process ",procID
    call Mpi_Barrier(Mpi_Comm_World, mpierr)
#endif

    xmin=bound_tab(1,info_proc%read_comm%pid+1)
    xmax=bound_tab(2,info_proc%read_comm%pid+1)
    ymin=bound_tab(3,info_proc%read_comm%pid+1)
    ymax=bound_tab(4,info_proc%read_comm%pid+1)
    zmin=bound_tab(5,info_proc%read_comm%pid+1)
    zmax=bound_tab(6,info_proc%read_comm%pid+1)

    call hdf5_close_group(gr_id)

    if(.not.allocated(pfof_id)) allocate(pfof_id(local_npart), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for pfof_id in Mpih5readcube', allocstat)
    if(.not.allocated(position))  allocate(position(3,local_npart), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for position in Mpih5readcube', allocstat)
    if(.not.allocated(velocity))  allocate(velocity(3,local_npart), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for velocity in Mpih5readcube', allocstat)

    groupname = 'data'
    call hdf5_open_group(file_id, groupname, gr_id)

    dsetname = 'position_part'
    call hdf5_read_mpi_data(gr_id, dsetname, 3, local_npart, position, info_proc%read_comm%name)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "position read on process ",procID
    call Mpi_Barrier(Mpi_Comm_World, mpierr)
#endif

    dsetname = 'velocity_part'
    call hdf5_read_mpi_data(gr_id, dsetname, 3, local_npart, velocity, info_proc%read_comm%name)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "velocity read on process ",procID
    call Mpi_Barrier(Mpi_Comm_World, mpierr)
#endif

    dsetname = 'identity_part'
    call hdf5_read_mpi_data(gr_id, dsetname, local_npart, pfof_id, info_proc%read_comm%name)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "ID read on process ",procID
    call Mpi_Barrier(Mpi_Comm_World, mpierr)
#endif

    if(param%do_read_potential) then
       if(.not.allocated(potential)) allocate(potential(local_npart), stat=allocstat)
       if(allocstat > 0) call Emergencystop('Allocate failed for potential in Mpih5readcube', allocstat)
       dsetname = 'potential_part'
       call hdf5_read_mpi_data(gr_id, dsetname, local_npart, potential, info_proc%read_comm%name)
    end if

    ! if requested, try to read the force attribute
    if(param%do_read_gravitational_field) then
       if(.not.allocated(field)) allocate(field(3,local_npart), stat=allocstat)
       if(allocstat > 0) call Emergencystop('Allocate failed for field in Mpih5readcube', allocstat)
       dsetname = 'gravitational_field_part'
       call hdf5_read_mpi_data(gr_id, dsetname, 3, local_npart, field, info_proc%read_comm%name)
    end if

    ! Close the root group.
    call hdf5_close_group(gr_id)

    call hdf5_close_file(file_id)

    deallocate(partnb_tab)
    deallocate(bound_tab)


#ifdef DEBUG
    write(OUTPUT_UNIT,*) "Exit mpih5readcube on process ",procID
#endif


  end subroutine Mpih5readcube


  !=======================================================================
  subroutine Mpih5readsortedcube()

    integer :: allocstat
    character(len=H5_FILENAME_LEN) :: filename
    character(len=8)  :: gid_char

    character(len=H5_STR_LEN) :: dsetname                           ! Dataset name
    character(len=H5_STR_LEN) :: aname                              ! Attribute name
    character(len=H5_STR_LEN) :: groupname

    integer(HID_T) :: file_id                               ! File identifier
    integer(HID_T) :: gr_data_id                            ! Group identifier
    integer(HID_T) :: gr_id                                 ! Group identifier

    integer(kind=4) :: procperfile
    integer(kind=4), dimension(:), allocatable :: partnb_tab
    integer(kind=4) :: igroup, firstgroup, lastgroup
    integer(kind=4) :: indbeg, indend
    integer(kind=4) :: nc, ngroup

    integer(kind=4), dimension(:), allocatable :: npartpergroup

    real(kind=4), dimension(:,:), allocatable :: bound_tab
    integer(kind=8) :: npart8
    integer(kind=4) :: length
    logical(kind=4) :: islast

#ifdef DEBUG
    integer(kind=4) :: mpierr

    write(OUTPUT_UNIT,*) "Enter Mpih5readsortedcube on process ",procID
#endif

    ! number of processes reading in the same file
    procperfile = param%gatherread_factor**3

    allocate(partnb_tab(procperfile), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for partnb_tab in Mpih5readsortedcube', allocstat)
    allocate(bound_tab(6,procperfile), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for bound_tab in Mpih5readsortedcube', allocstat)

    filename = trim(param%input_path)//trim(param%part_input_file)
    length=len_trim(filename)
    write(filename(length-7:length-3),'(I5.5)') info_proc%read_comm%color

    ! we open the file for a serial read
    call hdf5_open_file(filename, file_id)

    call Read_meta_common(file_id, common_meta)
    islast = .false.
    call Read_meta_info_ramses(file_id, inforamses, islast)

    nres = 2**inforamses%levelmin
    ngrid = int(nres,kind=IDKIND)**3
    global_npart = int(common_meta%npart_simulation,kind=IDKIND)

    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id,groupname, gr_id)

    ! read attribute partNB
    dsetname = 'npart_file'
    call hdf5_read_data(gr_id, dsetname, npart8)
    local_npart = int(npart8,kind=4)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "Value of nres read = ",nres, " on process ",procID
    call Mpi_Barrier(Mpi_Comm_World, mpierr)
#endif

    ! read attribute partNB
    aname = 'npart_cube_array'
    call hdf5_read_attr(gr_id, aname, procperfile, partnb_tab)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "partNB_tab read on process ",procID
    call Mpi_Barrier(Mpi_Comm_World, mpierr)
#endif

    local_npart = partnb_tab(info_proc%read_comm%pid+1)

    ! read attribute boundaries
    aname = 'boundaries_array'
    call hdf5_read_attr(gr_id, aname, 6, procperfile, bound_tab)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "boundaries_tab read on process ",procID
    call Mpi_Barrier(Mpi_Comm_World, mpierr)
#endif

    xmin=bound_tab(1,info_proc%read_comm%pid+1)
    xmax=bound_tab(2,info_proc%read_comm%pid+1)
    ymin=bound_tab(3,info_proc%read_comm%pid+1)
    ymax=bound_tab(4,info_proc%read_comm%pid+1)
    zmin=bound_tab(5,info_proc%read_comm%pid+1)
    zmax=bound_tab(6,info_proc%read_comm%pid+1)

    ! read attribute ngroup
    aname = 'ngroup'
    call hdf5_read_attr(gr_id, aname, ngroup)

    allocate(npartpergroup(ngroup), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for npartpergroup in Mpih5readsortedcube', allocstat)
    aname = 'npart_grp_array'
    call hdf5_read_data(gr_id, aname, ngroup, npartpergroup)

    nc = ngroup / procperfile
    firstgroup = nc * info_proc%read_comm%pid + 1
    lastgroup  = nc *(info_proc%read_comm%pid+1)

    call hdf5_close_group(gr_id)

    if(.not.allocated(pfof_id)) allocate(pfof_id(local_npart),stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for pfof_id in Mpih5readsortedcube', allocstat)
    if(.not.allocated(position))  allocate(position(3,local_npart), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for position in Mpih5readsortedcube', allocstat)
    if(.not.allocated(velocity))  allocate(velocity(3,local_npart), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for velocity in Mpih5readsortedcube', allocstat)

    groupname ='data'
    call hdf5_open_group(file_id, groupname, gr_data_id)

    indbeg = 1
    indend = 0
    ! loop over the groups
    do igroup=firstgroup, lastgroup
       if(npartpergroup(igroup) /= 0) then ! there is at least one part. to read in this group
          write(gid_char(1:8),'(I8.8)') igroup
          groupname='group'//gid_char
          call hdf5_open_group(gr_data_id,groupname,gr_id)

          indend = indbeg + npartpergroup(igroup) - 1
          ! read position of the particles
          dsetname = 'position_part'
          call hdf5_read_data(gr_id, dsetname, 3, local_npart, position(:,indbeg:indend))

          ! read velocity of the particles
          dsetname = 'velocity_part'
          call hdf5_read_data(gr_id, dsetname, 3, local_npart, velocity(:,indbeg:indend))

          ! read id of the particles
          dsetname = 'identity_part'
          call hdf5_read_data(gr_id, dsetname, local_npart, pfof_id(indbeg:indend))

          ! read potential if requested
          if(param%do_read_potential) then
             if(.not.allocated(potential)) allocate(potential(local_npart), stat=allocstat)
             if(allocstat > 0) call Emergencystop('Allocate failed for potential in Mpih5readsortedcube', allocstat)
             dsetname = 'potential_part'
             call hdf5_read_data(gr_id, dsetname, local_npart, potential(indbeg:indend))
          end if

          ! read force if requested
          if(param%do_read_gravitational_field) then
             if(.not.allocated(field)) allocate(field(3,local_npart), stat=allocstat)
             if(allocstat > 0) call Emergencystop('Allocate failed for field in Mpih5readsortedcube', allocstat)
             dsetname = 'gravitational_field_part'
             call hdf5_read_data(gr_id, dsetname, 3, local_npart, field(:,indbeg:indend))
          end if

          indbeg = indend + 1

          call hdf5_close_group(gr_id)
       end if
    end do


    ! Close the root group.
    call hdf5_close_group(gr_data_id)

    call hdf5_close_file(file_id)

    deallocate(partnb_tab)
    deallocate(bound_tab)

#ifdef DEBUG
    write(OUTPUT_UNIT,*) "Exit mpih5readsortedcube on process ",procID
#endif

  end subroutine Mpih5readsortedcube

end module modreadcube

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Sorting subroutine for a collection of particles
!! @brief
!! 
!! @author Fabrice Roy

!> Sorting subroutine for a collection of particles
!------------------------------------------------------------------------------------------------------------------------------------
module sort_particles_mod

  use error_handling_mod, only : Algorithmic_error
  use iso_fortran_env, only : ERROR_UNIT
  use modconstant, only : IDKIND
  use type_particles_mod

  implicit none

  private
  public :: Heapsort_particles
  
contains
  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine Heapsort_particles(n, particles, key, process_id)

    ! Input/Output variables
    class(type_particles), intent(inout) :: particles    
    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays
    character(*), intent(in) :: key
    integer(kind=4), intent(in) :: process_id

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Heapsort_particles begins'
#endif

    call Heapify_particles(size, n, particles, key, process_id)

    do i = n, 2, -1
       call Swap_particles(1, i, particles, process_id)
       size = size - 1
       call Sink_particles(size, 1, particles, key, process_id)
    end do

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Heapsort_particles ends'
#endif

  end subroutine Heapsort_particles


  !=======================================================================
  !> Heapify routine for Heapsort
  subroutine Heapify_particles(size, n, particles, key, process_id)

    ! Input/Output variables
    class(type_particles), intent(inout) :: particles
    !Output variable
    integer(kind=4),intent(out)                :: size
    ! Input variable
    integer(kind=4),intent(in) :: n
    character(*), intent(in) :: key
    integer(kind=4), intent(in) :: process_id

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Heapify_particles begins'
#endif

    size = n
    do i = n/2, 1, -1
       call Sink_particles(size, i, particles, key, process_id)
    end do

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Heapify_particles ends'
#endif

  end subroutine Heapify_particles


  !=======================================================================
  !> Sink routine for Heapsort
  recursive subroutine Sink_particles(size, i, particles, key, process_id)

    class(type_particles), intent(inout) :: particles
    integer(kind=4),intent(in) :: size,i
    character(*), intent(in) :: key
    integer(kind=4), intent(in) :: process_id

    ! Local variables
    integer(kind=4) :: l, r, max
    integer(kind=IDKIND), dimension(:), pointer :: sort_key
    
#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Sink_particles begins'
#endif
 
    
    l = 2*i
    r = 2*i+1
    select type(particles)
    type is(type_dms)
       select case (key)
       case('ramses_identity')
          sort_key => particles%ramses_identity
       case('identity')
          sort_key => particles%identity
       case('structure')
          sort_key => particles%structure
       case('sort_key')
          sort_key => particles%sort_key
       case default
          sort_key => null()
          call Algorithmic_error('sink_particles','wrong key value for sorting', 1000, process_id)
       end select
    type is(type_stars)
       select case(key)
       case('ramses_identity')
          sort_key => particles%ramses_identity
       case('sort_key')
          sort_key => particles%sort_key
       case default
          sort_key => null()
          call Algorithmic_error('sink_particles','wrong key value for sorting', 1000, process_id)
       end select
    end select

    if( (l <= size) .and. sort_key(l) > sort_key(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. sort_key(r) > sort_key(max) ) then
       max = r
    end if

    if( max /= i ) then
       call Swap_particles(i, max, particles, process_id)
       call Sink_particles(size, max, particles, key, process_id)
    end if

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Sink_particles ends'
#endif

  end subroutine Sink_particles


  !=======================================================================
  !> Swap routine for Heapsort
  subroutine Swap_particles(i, j, particles, process_id)

    ! Input/Output variables
    class(type_particles), intent(inout) :: particles
    ! Input variable
    integer(kind=4),intent(in) :: i,j
    integer(kind=4), intent(in) :: process_id

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Swap_particles begins'
#endif

    if(particles%has_position) then
       tmpr(:) = particles%position(:,i)
       particles%position(:,i) = particles%position(:,j)
       particles%position(:,j) = tmpr(:)
    end if

    if(particles%has_velocity) then
       tmpr(:) = particles%velocity(:,i)
       particles%velocity(:,i) = particles%velocity(:,j)
       particles%velocity(:,j) = tmpr(:)
    end if

    if(particles%has_field) then
       tmpr(:) = particles%field(:,i)
       particles%field(:,i) = particles%field(:,j)
       particles%field(:,j) = tmpr(:)
    end if

    if(particles%has_mass) then
       tmps = particles%mass(i)
       particles%mass(i) = particles%mass(j)
       particles%mass(j) = tmps
    end if

    if(particles%has_potential) then
       tmps = particles%potential(i)
       particles%potential(i) = particles%potential(j)
       particles%potential(j) = tmps
    end if

    if(particles%has_ramses_identity) then
       tmpi = particles%ramses_identity(i)
       particles%ramses_identity(i) = particles%ramses_identity(j)
       particles%ramses_identity(j) = tmpi
    end if

    if(particles%has_sort_key) then
       tmpi = particles%sort_key(i)
       particles%sort_key(i) = particles%sort_key(j)
       particles%sort_key(j) = tmpi
    end if

    select type(particles)
    type is (type_stars)
       if(particles%has_birth_date) then
          tmps = particles%birth_date(i)
          particles%birth_date(i) = particles%birth_date(j)
          particles%birth_date(j) = tmps
       end if
       if(particles%has_metallicity) then
          tmps = particles%metallicity(i)
          particles%metallicity(i) = particles%metallicity(j)
          particles%metallicity(j) = tmps
       end if       
    type is (type_dms)
       if(particles%has_identity) then
          tmpi = particles%identity(i)
          particles%identity(i) = particles%identity(j)
          particles%identity(j) = tmpi
       end if
       if(particles%has_structure) then
          tmpi = particles%structure(i)
          particles%structure(i) = particles%structure(j)
          particles%structure(j) = tmpi
       end if
    end select

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Swap_particles ends'
#endif

  end subroutine Swap_particles

end module sort_particles_mod





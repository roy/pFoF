!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2015 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Subroutines to read HDF5 halo files created with pfof and pfof_cone.
!! @brief
!! 
!! @author Fabrice Roy

!> Subroutines to read HDF5 halo files created with pfof and pfof_cone.
!------------------------------------------------------------------------------------------------------------------------------------
module modreadhalo


  implicit none

  private
  public :: H5read_halo_hfprop,&
       H5read_halo_dm_data
  

contains
  !> Reads a HDF5 halo hfprop file created with pfof
  subroutine H5read_halo_hfprop(filename, common_metadata, parameter_pfof, info_ramses, info_cone, &
       position_halo, velocity_halo, rmax_halo, identity_halo, npart_halo)

    use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
    use modconstant, only : IDKIND, type_common_metadata, type_parameter_pfof
    use type_info_ramses_mod, only : type_info_ramses, type_info_cone_part
    use fortran_hdf5_constants_m, only : H5_FILENAME_LEN, H5_STR_LEN
    use fortran_hdf5_manage_files_m, only : hdf5_close_file, hdf5_open_file 
    use fortran_hdf5_manage_groups_m, only : hdf5_close_group, hdf5_open_group
    use fortran_hdf5_read_attribute_m
    use fortran_hdf5_read_data_m
    use hdf5, only : HID_T
    use modmpicommons, only : type_info_process
    use modreadmeta, only : Read_meta_common, &
       Read_meta_halofinder_parameter, &
       Read_meta_info_cone, &
       Read_meta_info_ramses

    !    use modiocommons

    ! input/output variables
    character(len=H5_FILENAME_LEN), intent(in) :: filename !< Name of the file
    type(type_common_metadata), intent(out) :: common_metadata
    real(kind=8), dimension(:,:), allocatable, intent(out), optional :: position_halo, velocity_halo
    real(kind=8), dimension(:), allocatable, intent(out), optional :: rmax_halo
    integer(kind=IDKIND), dimension(:), allocatable, intent(out), optional :: identity_halo
    integer(kind=4), dimension(:), allocatable, intent(out), optional :: npart_halo
    class(type_parameter_pfof), intent(out), optional :: parameter_pfof
    type(type_info_ramses), intent(out), optional :: info_ramses
    type(type_info_cone_part), intent(out), optional :: info_cone

    ! local variables
    integer(HID_T) :: file_id, data_id, meta_id
    character(len=H5_STR_LEN) :: dname, groupname
    integer(kind=4) :: nhalo


    ! Open the file
    call Hdf5_open_file(filename, file_id)

    ! open metadata group
    groupname='metadata'
    call Hdf5_open_group(file_id, groupname, meta_id)

    ! read metadata
    ! Read the number of halos
    dname = 'nhalo_file'
    call Hdf5_read_attr(meta_id, dname, nhalo)
#ifdef DEBUG
    write(OUTPUT_UNIT, *) 'DEBUG: nhalo_file=',nhalo
#endif

    ! close group 'metadata'
    call Hdf5_close_group(meta_id)

    call Read_meta_common(file_id, common_metadata)

    if(present(info_ramses)) then
       call Read_meta_info_ramses(file_id, info_ramses,.false.)
    end if

    if(present(info_cone)) then
       call Read_meta_info_cone(file_id, info_cone, .false.)
    end if

    if(present(parameter_pfof)) then
       call Read_meta_halofinder_parameter(file_id, parameter_pfof)
    end if


    ! open group 'data'
    groupname='data'
    call Hdf5_open_group(file_id, groupname, data_id)

    ! read data
    if(present(position_halo)) then
       if(.not.allocated(position_halo)) allocate(position_halo(3,nhalo))
       ! Read position of the halos
       dname = 'position_halo'
       call Hdf5_read_data(data_id, dname, 3, nhalo, position_halo)
    end if

    if(present(velocity_halo)) then
       if(.not.allocated(velocity_halo)) allocate(velocity_halo(3,nhalo))
       ! Read velocity of the halos
       dname = 'velocity_halo'
       call Hdf5_read_data(data_id, dname, 3, nhalo, velocity_halo)
    end if

    if(present(identity_halo)) then
       if(.not.allocated(identity_halo)) allocate(identity_halo(nhalo))
       ! Read identity of the halos
       dname = 'identity_halo'
       call Hdf5_read_data(data_id, dname, nhalo, identity_halo)
    end if

    if(present(npart_halo)) then
       if(.not.allocated(npart_halo)) allocate(npart_halo(nhalo))
       ! Read npart in each halo
       dname = 'npart_halo'
       call Hdf5_read_data(data_id, dname, nhalo, npart_halo)
    end if

    if(present(rmax_halo)) then
       if(.not.allocated(rmax_halo)) allocate(rmax_halo(nhalo))
       ! Read max. radius of the halos
       dname = 'rmax_halo'
       call Hdf5_read_data(data_id, dname, nhalo, rmax_halo)
    end if

    ! close group 'data'
    call Hdf5_close_group(data_id)

    ! Close file
    call Hdf5_close_file(file_id)

  end subroutine H5read_halo_hfprop

  !=======================================================================

  subroutine H5read_halo_dm_data(info_proc, filename, position_part, velocity_part, identity_part,&
       mass_part, potential_part, field_part, parameter_pfof, info_ramses, info_cone)

    use fortran_hdf5_constants_m, only : H5_FILENAME_LEN, H5_STR_LEN
    use fortran_hdf5_manage_files_m, only : hdf5_close_file, hdf5_open_file 
    use fortran_hdf5_manage_groups_m, only : hdf5_close_group, hdf5_open_group
    use fortran_hdf5_read_attribute_m
    use fortran_hdf5_read_data_m
    use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
    use modconstant, only : ERR_MSG_LEN, IDKIND, &
         type_common_metadata, type_parameter_pfof
    use type_info_ramses_mod, only : type_info_ramses, type_info_cone_part
    use modmpicommons, only : type_info_process
    use modreadmeta, only : Read_meta_common, &
       Read_meta_halofinder_parameter, &
       Read_meta_info_cone, &
       Read_meta_info_ramses
    use hdf5, only : HID_T
    
    type(type_info_process), intent(in) :: info_proc
    character(len=H5_FILENAME_LEN), intent(in) :: filename
    real(kind=4), dimension(:,:), allocatable, intent(out), optional :: position_part, velocity_part, field_part
    real(kind=4), dimension(:), allocatable, intent(out), optional :: mass_part, potential_part
    integer(kind=IDKIND), dimension(:), allocatable, intent(out), optional :: identity_part

    class(type_parameter_pfof), intent(out), optional :: parameter_pfof
    type(type_info_ramses), intent(out), optional :: info_ramses
    type(type_info_cone_part), intent(out), optional :: info_cone
    integer(kind=HID_T) :: file_id, data_id, meta_id
    type(type_common_metadata) :: common_metadata
    integer(kind=4) :: npart_file
    integer(kind=8) :: npart8
    character(len=H5_STR_LEN) :: dname, groupname
    character(len=ERR_MSG_LEN) :: alloc_err_msg
    integer(kind=4) :: alloc_stat
    
#ifdef DEBUG
    write(ERROR_UNIT,'(A,I6)') 'Enter H5read_halo_dm_data on process ', info_proc%global_comm%pid
#endif
    
    ! open the file
    call Hdf5_open_file(filename, file_id)

    call Read_meta_common(file_id, common_metadata)

    ! read number of particles in the file
    groupname = 'metadata'
    call Hdf5_open_group(file_id, groupname, meta_id)
    dname = 'npart_file'
    call Hdf5_read_data(meta_id, dname, npart8)
    npart_file = int(npart8,kind=4)
    call Hdf5_close_group(meta_id)

    if(present(info_ramses)) then
       call Read_meta_info_ramses(file_id, info_ramses,.false.)
    end if

    if(present(info_cone)) then
       call Read_meta_info_cone(file_id, info_cone,.false.)
    end if

    if(present(parameter_pfof)) then
       call Read_meta_halofinder_parameter(file_id, parameter_pfof)
    end if

    ! open group 'data'
    groupname='data'
    call Hdf5_open_group(file_id, groupname, data_id)

    if(present(position_part)) then
       if(.not.allocated(position_part)) allocate(position_part(3,npart_file),stat=alloc_stat,errmsg=alloc_err_msg)
#ifdef DEBUG
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A,I6)') 'Error in position_part allocation in H5read_halo_dm_data on process ',&
            info_proc%global_comm%pid
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if       
#endif
       dname = 'position_part'
       call Hdf5_read_data(data_id, dname, 3, npart_file, position_part)
    end if

    if(present(velocity_part)) then
       if(.not.allocated(velocity_part)) allocate(velocity_part(3,npart_file),stat=alloc_stat,errmsg=alloc_err_msg)
#ifdef DEBUG
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in velocity_part allocation in H5read_halo_dm_data on process ',&
               info_proc%global_comm%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
#endif
       dname = 'velocity_part'
       call Hdf5_read_data(data_id, dname, 3, npart_file, velocity_part)
    end if

    if(present(identity_part)) then
       if(.not.allocated(identity_part)) allocate(identity_part(npart_file),stat=alloc_stat,errmsg=alloc_err_msg)
#ifdef DEBUG
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in identity_part allocation in H5read_halo_dm_data on process ',&
               info_proc%global_comm%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
#endif
       dname = 'identity_part'
       call Hdf5_read_data(data_id, dname, npart_file, identity_part)
    end if

    if(present(mass_part)) then
       if(.not.allocated(mass_part)) allocate(mass_part(npart_file),stat=alloc_stat,errmsg=alloc_err_msg)
#ifdef DEBUG
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in mass_part allocation in H5read_halo_dm_data on process ',&
               info_proc%global_comm%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
#endif
       dname = 'mass_part'
       call Hdf5_read_data(data_id, dname, npart_file, mass_part)
    end if

    if(present(potential_part)) then
       if(.not.allocated(potential_part)) allocate(potential_part(npart_file),stat=alloc_stat,errmsg=alloc_err_msg)
#ifdef DEBUG
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in potential_part allocation in H5read_halo_dm_data on process ',&
               info_proc%global_comm%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
#endif
       dname = 'potential_part'
       call Hdf5_read_data(data_id, dname, npart_file, potential_part)
    end if

    if(present(field_part)) then
       if(.not.allocated(field_part)) allocate(field_part(3, npart_file),stat=alloc_stat,errmsg=alloc_err_msg)
#ifdef DEBUG
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in field_part allocation in H5read_halo_dm_data on process ',&
               info_proc%global_comm%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
#endif
       dname = 'field_part'
       call Hdf5_read_data(data_id, dname, 3, npart_file, field_part)
    end if
    
    call Hdf5_close_group(data_id)

    call Hdf5_close_file(file_id)

#ifdef DEBUG
    write(ERROR_UNIT,'(A,I6)') 'Exit H5read_halo_dm_data on process ', info_proc%global_comm%pid
#endif
    
  end subroutine H5read_halo_dm_data


end module modreadhalo

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2015 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Subroutines to read  input parameters for snapshot and light cone versions of pfof
!! @brief
!! Subroutines to read input parameters for snapshot and light cone versions of pfof
!! @author Fabrice Roy
! ======================================================================

module modreadparameters

  implicit none

  private
  public :: Read_pfof_cone_parameters, &
       Read_pfof_snap_parameters
contains

  !=======================================================================
  !> read pfof cone input parameters from pfof_cone.nml and save them if requested.
  subroutine Read_pfof_cone_parameters(filename, param, ioerr, error_msg, do_save)

    use iso_fortran_env
    use modconstant, only : type_parameter_pfof_cone,&
         ERR_MSG_LEN, FILENAME_LEN

    ! input/output variables
    character(len=FILENAME_LEN),             intent(in)           :: filename
    type(type_parameter_pfof_cone), intent(out)          :: param
    integer(kind=4),                intent(out)          :: ioerr
    character(len=ERR_MSG_LEN),     intent(out)          :: error_msg
    logical(kind=4),                intent(in), optional :: do_save

    ! local variable
    character(len=230) :: filesave
    integer :: param_unit
    integer :: save_unit

    !! pfof parameters
    ! input parameters
    character(len=200) :: input_path      !< path to the directory containing the ramses files
    character(len=200) :: part_input_file  !< name of the files containing the particles data
    logical(kind=4)    :: do_read_ramses_part_id
    logical(kind=4)    :: star = .false.
    logical(kind=4)    :: metal = .false.
    logical(kind=4)    :: do_skip_mass = .true.
    logical(kind=4)    :: do_skip_star = .true.
    logical(kind=4)    :: do_skip_metal = .true.
    logical(kind=4)    :: do_read_potential    !< do the ramses files contain potential?
    logical(kind=4)    :: do_read_gravitational_field  !< do the ramses files contain force?
    integer(kind=4)    :: shell_first_id
    integer(kind=4)    :: shell_last_id

    ! friend of friend parameters
    real(kind=4)       :: percolation_length  !< fof percolation length
    integer(kind=4)    :: mmin            !< minimum mass of the haloes
    integer(kind=4)    :: mmax            !< maximum mass of the haloes
    logical(kind=4)    :: do_unbinding    !< unbinding process? (not implemented yet)
    logical(kind=4)    :: do_subhalo      !< subhalo detection? (not implemented yet)

    ! output parameters
    character(len=200) :: simulation_name  !< simulation name to be written in output files
    logical(kind=4)    :: do_gather_halo   !< should pfof gather halos in output files
    logical(kind=4)    :: do_timings  !< should pfof perform timings (imply extra synchronizations)

    namelist / input_parameters / input_path, part_input_file, do_read_ramses_part_id, &
         star, metal, do_skip_mass, do_skip_star, do_skip_metal, &
         do_read_potential, do_read_gravitational_field, shell_first_id, shell_last_id
    namelist / fof_parameters / percolation_length, mmin, mmax, do_unbinding, do_subhalo
    namelist / output_parameters / simulation_name, do_gather_halo, do_timings

    open(newunit=param_unit, file=filename, status='old', action='read', iostat=ioerr, iomsg=error_msg)

    if(ioerr>0) then
       error_msg='** Error opening input file '//trim(filename)//'. Please check this file. **'
       return
    end if

    read(param_unit, nml=input_parameters, iostat=ioerr)
    if(ioerr>0) then
       error_msg='** Error reading input parameters in '//trim(filename)//'. Please check this file. **'
       return
    end if

    read(param_unit, nml=fof_parameters, iostat=ioerr)
    if(ioerr>0) then
       error_msg='** Error reading fof parameters in '//trim(filename)//'. Please check this file. **'
       return
    end if

    read(param_unit, nml=output_parameters, iostat=ioerr)
    if(ioerr>0) then
       error_msg='** Error reading output parameters in '//trim(filename)//'. Please check this file. **'
       return
    end if

    close(param_unit)

    param%input_path = input_path
    param%part_input_file = part_input_file
    param%simulation_name = simulation_name
    param%mmin = mmin
    param%mmax = mmax
    param%percolation_length = percolation_length
    param%do_read_ramses_part_id = do_read_ramses_part_id
    param%star = star
    param%metal = metal
    param%do_skip_mass = do_skip_mass
    param%do_skip_star = do_skip_star
    param%do_skip_metal = do_skip_metal
    param%do_read_potential = do_read_potential
    param%do_read_gravitational_field = do_read_gravitational_field
    param%do_unbinding = do_unbinding
    param%do_subhalo = do_subhalo
    param%do_gather_halo = do_gather_halo
    param%do_timings = do_timings
    param%shell_first_id = shell_first_id
    param%shell_last_id = shell_last_id

    if(present(do_save)) then
       if(do_save) then
          filesave = 'pfof_cone_parameters_'//trim(simulation_name)//'.nml'
          open(newunit=save_unit, file=filesave, status='unknown', action='write',iostat=ioerr,iomsg=error_msg)
          if(ioerr/=0) then
             error_msg='** Error opening file '//trim(filesave)//'. Please check disk usage.'
             return
          end if
          write(save_unit, nml=input_parameters,iostat=ioerr)
          if(ioerr/=0) then
             error_msg='** Error writing input parameters in '//trim(filesave)
             return
          end if
          write(save_unit, nml=fof_parameters,iostat=ioerr)
          if(ioerr/=0) then
             error_msg='** Error writing fof parameters in '//trim(filesave)
             return
          end if
          write(save_unit, nml=output_parameters,iostat=ioerr)
          if(ioerr/=0) then
             error_msg='** Error writing output parameters in '//trim(filesave)
             return
          end if
          close(save_unit)
       end if
    end if

end subroutine Read_pfof_cone_parameters


  !=======================================================================
  !> read pfof snap input parameters from pfof_snap.nml and save them if requested.
  subroutine Read_pfof_snap_parameters(filename, param, ioerr, error_msg, do_save)

    use iso_fortran_env
    use modconstant, only : type_parameter_pfof_snap, &
         ERR_MSG_LEN, FILENAME_LEN

    ! i/o variables
    character(len=FILENAME_LEN), intent(in) :: filename
    type(type_parameter_pfof_snap), intent(out) :: param
    integer(kind=4), intent(out)    :: ioerr
    character(len=ERR_MSG_LEN), intent(out) :: error_msg
    logical(kind=4), intent(in), optional :: do_save

    ! local variable
    character(len=230) :: filesave
    integer :: param_unit
    integer :: save_unit

    ! input parameters
    character(len=3)   :: code_index
    character(len=200) :: input_path
    character(len=200) :: part_input_file
    character(len=200) :: info_input_file
    integer(kind=4)    :: grpsize
    logical(kind=4)    :: star = .false.
    logical(kind=4)    :: metal = .false.
    logical(kind=4)    :: do_skip_mass = .true.
    logical(kind=4)    :: do_skip_star = .true.
    logical(kind=4)    :: do_skip_metal = .true.
    logical(kind=4)    :: do_read_potential
    logical(kind=4)    :: do_read_gravitational_field
    logical(kind=4)    :: do_read_from_cube
    integer(kind=4)    :: gatherread_factor

    ! friend of friend parameters
    real(kind=4)       :: percolation_length
    integer(kind=4)    :: mmin
    integer(kind=4)    :: mmax
    logical(kind=4)    :: do_fof
    logical(kind=4)    :: do_unbinding
    logical(kind=4)    :: do_subhalo

    ! output parameters
    character(len=200) :: simulation_name
    integer(kind=4)    :: snapshot
    logical(kind=4)    :: do_write_cube
    integer(kind=4)    :: gatherwrite_factor
    logical(kind=4)    :: do_sort_cube
    logical(kind=4)    :: do_timings


    ! namelist for input file
    namelist / input_parameters  / code_index, input_path, part_input_file, info_input_file, &
         grpsize, star, metal, do_skip_mass, do_skip_star, do_skip_metal, do_read_potential, do_read_gravitational_field, &
         do_read_from_cube, gatherread_factor
    namelist / fof_parameters    / percolation_length, mmin, mmax, do_fof, do_unbinding, do_subhalo
    namelist / output_parameters / simulation_name, snapshot, do_write_cube,&
         gatherwrite_factor,  do_sort_cube, do_timings


    ! read input parameters'
    open(newunit=param_unit, file=filename, status='old', action='read', iostat=ioerr, iomsg=error_msg)

    if(ioerr /= 0) then
       error_msg='** error opening input file '//trim(filename)//'. please check this file. **'
       return
    end if

    read(param_unit, nml=input_parameters, iostat=ioerr)
    if(ioerr /= 0) then
       error_msg='** error reading input parameters in '//trim(filename)//'. please check this file. **'
       return
    end if

    read(param_unit, nml=fof_parameters, iostat=ioerr)
    if(ioerr /= 0) then
       error_msg='** error reading fof parameters in '//trim(filename)//'. please check this file. **'
       return
    end if

    read(param_unit, nml=output_parameters, iostat=ioerr)
    if(ioerr /= 0) then
       error_msg='** error reading output parameters in '//trim(filename)//'. please check this file. **'
       return
    end if

    close(param_unit)

    param%code_index = code_index
    param%input_path = input_path
    param%part_input_file = part_input_file
    param%info_input_file = info_input_file
    param%grpsize = grpsize
    param%star = star
    param%metal = metal
    param%do_skip_mass = do_skip_mass
    param%do_skip_star = do_skip_star
    param%do_skip_metal = do_skip_metal
    param%do_read_potential = do_read_potential
    param%do_read_gravitational_field = do_read_gravitational_field
    param%do_read_from_cube = do_read_from_cube
    param%gatherread_factor = gatherread_factor
    param%percolation_length = percolation_length
    param%mmin = mmin
    param%mmax = mmax
    param%do_fof = do_fof
    param%do_unbinding = do_unbinding
    param%do_subhalo = do_subhalo
    param%simulation_name = simulation_name
    param%snapshot = snapshot
    param%do_write_cube = do_write_cube
    param%gatherwrite_factor = gatherwrite_factor
    param%do_sort_cube = do_sort_cube
    param%do_timings = do_timings

    if(present(do_save)) then
       if(do_save) then
          filesave = 'pfof_snap_parameters_'//trim(simulation_name)//'.nml'
          open(newunit=save_unit, file=filesave, status='unknown', action='write',iostat=ioerr,iomsg=error_msg)
          if(ioerr/=0) then
             error_msg='** Error opening file '//trim(filesave)//'. Please check disk usage.'
             return
          end if
          write(save_unit, nml=input_parameters,iostat=ioerr)
          if(ioerr/=0) then
             error_msg='** Error writing input parameters in '//trim(filesave)
             return
          end if
          write(save_unit, nml=fof_parameters,iostat=ioerr)
          if(ioerr/=0) then
             error_msg='** Error writing fof parameters in '//trim(filesave)
             return
          end if
          write(save_unit, nml=output_parameters,iostat=ioerr)
          if(ioerr/=0) then
             error_msg='** Error writing output parameters in '//trim(filesave)
             return
          end if
          close(save_unit)
       end if
    end if

end subroutine Read_pfof_snap_parameters

end module modreadparameters

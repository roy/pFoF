!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Sorting subroutines
!! @brief
!! 
!! @author Fabrice Roy

!> Sorting subroutines
!------------------------------------------------------------------------------------------------------------------------------------
module modsort

  use modconstant, only : IDKIND

  implicit none

  private

  public :: quicksort, heapsort

  !> Generic inteface used to sort arrays with quicksort algorithm.
  !> @param[in] p First index of the arrays to sort
  !> @param[in] r Last index of the arrays to sort
  !> @param[in] tref Reference array: this array will be sorted in increasing order, the other arrays will be sorted following this one
  !> @param[in] [tx,tv,tid] Arrays to sort
  !> @param[in] [tp,tf] Optional arrays to sort
  interface quicksort
     module procedure quicksort_refI_2x3R_1xI
     module procedure quicksort_refI_2x3R_1xR_1xI
     module procedure quicksort_refI_3x3R_1xR_1xI
     module procedure quicksort_refI_3x3R_1xI
  end interface quicksort


  !> Generic inteface used to sort arrays with heapsort algorithm.
  !> @param[in] n Lenght of the arrays
  !> @param[in] tref Reference array: this array will be sorted in increasing order, the other arrays will be sorted following this one
  !> @param[in] [tx,tv,tid] Arrays to sort
  !> @param[in] [tp,tf,trid] Optional arrays to sort
  interface heapsort
     module procedure heapsort_refI_1xI4
     module procedure heapsort_refI_2x3R_1xI !!
     module procedure heapsort_refI_2x3R_2xI !!
     module procedure heapsort_refI_2x3R_1xR_1xI  !!
     module procedure heapsort_refI_2x3R_1xR_2xI
     module procedure heapsort_refI_2x3R_2xR_1xI
     module procedure heapsort_refI_2x3R_3xR_1xI
     module procedure heapsort_refI_2x3R_4xR_1xI
     module procedure heapsort_refI_3x3R_1xI
     module procedure heapsort_refI_3x3R_2xI
     module procedure heapsort_refI_3x3R_1xR_1xI
     module procedure heapsort_refI_3x3R_2xR_1xI
     module procedure heapsort_refI_3x3R_3xR_1xI
     module procedure heapsort_refI_3x3R_4xR_1xI
     module procedure heapsort_refI_3x3R_1xR_2xI
     module procedure heapsort_refI_3x3R_4xR_2xI
  end interface heapsort


contains
  !=======================================================================
  !> Quicksort algorith as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  recursive subroutine quicksort_refI_2x3R_1xI(p,r,tref,tx,tv,tid)

    ! Input parameters
    integer(kind=4), intent(in) :: p     !< index of the first element of the array to be sorted
    integer(kind=4), intent(in) :: r     !< index of the last element of the array to be sorted

    ! Input parameters modified in subroutine
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref  !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tid   !< particle id array

    ! Local parameters
    integer(kind=4) :: q

#ifdef DEBUG2
    print *,'quicksort_refI_2x3R_1xI begins between ',p,' and ',r
#endif

    if(p<r) then

       call partition_refI_2x3R_1xI(p,r,q,tref,tx,tv,tid)
       call quicksort_refI_2x3R_1xI(p,q-1,tref,tx,tv,tid)
       call quicksort_refI_2x3R_1xI(q+1,r,tref,tx,tv,tid)

    end if

#ifdef DEBUG2
    print *,'quicksort ends'
#endif

  end subroutine quicksort_refI_2x3R_1xI

  !=======================================================================
  !> Quicksort algorith as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  recursive subroutine quicksort_refI_2x3R_1xR_1xI(p,r,tref,tx,tv,tp,tid)

    ! Input parameters
    integer(kind=4), intent(in) :: p     !< index of the first element of the array to be sorted
    integer(kind=4), intent(in) :: r     !< index of the last element of the array to be sorted

    ! Input parameters modified in subroutine
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    real   (kind=4), intent(inout),dimension(*)  :: tp     !< potential array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref  !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tid   !< particle id array

    ! Local parameters
    integer(kind=4) :: q

#ifdef DEBUG2
    print *,'quicksort_refI_2x3R_1xR_1xI begins'
#endif

    if(p<r) then

       call partition_refI_2x3R_1xR_1xI(p,r,q,tref,tx,tv,tp,tid)
       call quicksort_refI_2x3R_1xR_1xI(p,q-1,tref,tx,tv,tp,tid)
       call quicksort_refI_2x3R_1xR_1xI(q+1,r,tref,tx,tv,tp,tid)

    end if

#ifdef DEBUG2
    print *,'trirapide ends'
#endif


  end subroutine quicksort_refI_2x3R_1xR_1xI


  !=======================================================================
  !> Quicksort algorith as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  recursive subroutine quicksort_refI_3x3R_1xR_1xI(p,r,tref,tx,tv,tf,tp,tid)

    ! Input parameters
    integer(kind=4), intent(in) :: p     !< index of the first element of the array to be sorted
    integer(kind=4), intent(in) :: r     !< index of the last element of the array to be sorted

    ! Input parameters modified in subroutine
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    real   (kind=4), intent(inout),dimension(3,*) :: tf     !< gravitational field array
    real   (kind=4), intent(inout),dimension(*)   :: tp     !< potential array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref  !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tid   !< particle id array

    ! Local parameters
    integer(kind=4) :: q

#ifdef DEBUG2
    print *,'quicksort_refI_3x3R_1xR_1xI begins'
#endif

    if(p<r) then

       call partition_refI_3x3R_1xR_1xI(p,r,q,tref,tx,tv,tf,tp,tid)
       call quicksort_refI_3x3R_1xR_1xI(p,q-1,tref,tx,tv,tf,tp,tid)
       call quicksort_refI_3x3R_1xR_1xI(q+1,r,tref,tx,tv,tf,tp,tid)

    end if

#ifdef DEBUG2
    print *,'quicksort_refI_3x3R_1xR_1xI ends'
#endif


  end subroutine quicksort_refI_3x3R_1xR_1xI

  !=======================================================================
  !> Quicksort algorith as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  recursive subroutine quicksort_refI_3x3R_1xI(p,r,tref,tx,tv,tf,tid)

    ! Input parameters
    integer(kind=4), intent(in) :: p     !< index of the first element of the array to be sorted
    integer(kind=4), intent(in) :: r     !< index of the last element of the array to be sorted

    ! Input parameters modified in subroutine
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    real   (kind=4), intent(inout),dimension(3,*) :: tf     !< potential array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref  !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tid   !< particle id array

    ! Local parameters
    integer(kind=4) :: q

#ifdef DEBUG2
    print *,'quicksort_refI_3x3R_1xI begins'
#endif

    if(p<r) then

       call partition_refI_3x3R_1xI(p,r,q,tref,tx,tv,tf,tid)
       call quicksort_refI_3x3R_1xI(p,q-1,tref,tx,tv,tf,tid)
       call quicksort_refI_3x3R_1xI(q+1,r,tref,tx,tv,tf,tid)

    end if

#ifdef DEBUG2
    print *,'quicksort_refI_3x3R_1xI ends'
#endif

  end subroutine quicksort_refI_3x3R_1xI

  !=======================================================================
  !> Partition function for quicksort
  subroutine partition_refI_2x3R_1xI(p,r,q,tref,tx,tv,tid)

    ! Input parameters
    integer(kind=4),intent(in)  :: p, r  ! First and last index of the tables to sort
    ! Input parameters modified in subroutine
    real   (kind=4), intent(inout),dimension(3,*) :: tx,  tv  ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref,tid ! structure id and particle id
    ! Output parameters
    integer(kind=4), intent(out) :: q
    ! Local parameters
    integer(kind=IDKIND)     :: tmpi,Sref
    real(kind=4),dimension(3) :: tmpr
    integer(kind=4) :: i, j

#ifdef DEBUG2
    print *,'partition_refI_2x3R_1xI begins'
#endif

    Sref  = tref(r)

    i = p-1
    do j = p, r-1
       if(tref(j) <= Sref) then
          i = i+1
          tmpi = tref(i)
          tref(i) = tref(j)
          tref(j) = tmpi
          tmpi = tid(i)
          tid(i) = tid(j)
          tid(j) = tmpi
          tmpr(:) = tx(:,i)
          tx(:,i) = tx(:,j)
          tx(:,j) = tmpr(:)
          tmpr(:) = tv(:,i)
          tv(:,i) = tv(:,j)
          tv(:,j) = tmpr(:)
       end if
    end do

    tmpi = tref(i+1)
    tref(i+1) = tref(r)
    tref(r) = tmpi
    tmpi = tid(i+1)
    tid(i+1) = tid(r)
    tid(r) = tmpi
    tmpr(:) = tx(:,i+1)
    tx(:,i+1) = tx(:,r)
    tx(:,r) = tmpr(:)
    tmpr(:) = tv(:,i+1)
    tv(:,i+1) = tv(:,r)
    tv(:,r) = tmpr(:)

    q = i+1

#ifdef DEBUG2
    print *,'partition_refI_2x3R_1xI ends'
#endif

  end subroutine partition_refI_2x3R_1xI

  !=======================================================================
  !> Partition function for quicksort
  subroutine partition_refI_2x3R_1xR_1xI(p,r,q,tref,tx,tv,tp,tid)

    ! Input parameters
    integer(kind=4),intent(in)  :: p, r  ! First and last index of the tables to sort
    ! Input parameters modified in subroutine
    real   (kind=4), intent(inout),dimension(3,*) :: tx,  tv  ! positions and velocities
    real   (kind=4), intent(inout), dimension(*) :: tp
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref,tid ! structure id and particle id
    ! Output parameters
    integer(kind=4), intent(out) :: q
    ! Local parameters
    integer(kind=IDKIND)     :: tmpi,Sref
    real(kind=4) :: tmpf
    real(kind=4),dimension(3) :: tmpr
    integer(kind=4) :: i, j

#ifdef DEBUG2
    print *,'partition_refI_2x3R_1xR_1xI begins'
#endif

    Sref  = tref(r)

    i = p-1
    do j = p, r-1
       if(tref(j) <= Sref) then
          i = i+1
          tmpi = tref(i)
          tref(i) = tref(j)
          tref(j) = tmpi
          tmpi = tid(i)
          tid(i) = tid(j)
          tid(j) = tmpi
          tmpr(:) = tx(:,i)
          tx(:,i) = tx(:,j)
          tx(:,j) = tmpr(:)
          tmpr(:) = tv(:,i)
          tv(:,i) = tv(:,j)
          tv(:,j) = tmpr(:)
          tmpf = tp(i)
          tp(i) = tp(j)
          tp(j) = tmpf
       end if
    end do

    tmpi = tref(i+1)
    tref(i+1) = tref(r)
    tref(r) = tmpi
    tmpi = tid(i+1)
    tid(i+1) = tid(r)
    tid(r) = tmpi
    tmpr(:) = tx(:,i+1)
    tx(:,i+1) = tx(:,r)
    tx(:,r) = tmpr(:)
    tmpr(:) = tv(:,i+1)
    tv(:,i+1) = tv(:,r)
    tv(:,r) = tmpr(:)
    tmpf = tp(i+1)
    tp(i+1) = tp(r)
    tp(r) = tmpf

    q = i+1

#ifdef DEBUG2
    print *,'partition_refI_2x3R_1xR_1xI ends'
#endif

  end subroutine partition_refI_2x3R_1xR_1xI

  !=======================================================================
  !> Partition function for quicksort
  subroutine partition_refI_3x3R_1xR_1xI(p,r,q,tref,tx,tv,tf,tp,tid)

    ! Input parameters
    integer(kind=4),intent(in)  :: p, r  ! First and last index of the tables to sort
    ! Input parameters modified in subroutine
    real   (kind=4), intent(inout),dimension(3,*) :: tx,  tv  ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    real   (kind=4), intent(inout), dimension(*) :: tp    ! potential
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref,tid ! structure id and particle id
    ! Output parameters
    integer(kind=4), intent(out) :: q
    ! Local parameters
    integer(kind=IDKIND)     :: tmpi,Sref
    real(kind=4) :: tmpf
    real(kind=4),dimension(3) :: tmpr
    integer(kind=4) :: i, j

#ifdef DEBUG2
    print *,'partition_refI_3x3R_1xR_1xI begins'
#endif

    Sref  = tref(r)

    i = p-1
    do j = p, r-1
       if(tref(j) <= Sref) then
          i = i+1
          tmpi = tref(i)
          tref(i) = tref(j)
          tref(j) = tmpi
          tmpi = tid(i)
          tid(i) = tid(j)
          tid(j) = tmpi
          tmpr(:) = tx(:,i)
          tx(:,i) = tx(:,j)
          tx(:,j) = tmpr(:)
          tmpr(:) = tv(:,i)
          tv(:,i) = tv(:,j)
          tv(:,j) = tmpr(:)
          tmpr(:) = tf(:,i)
          tf(:,i) = tf(:,j)
          tf(:,j) = tmpr(:)
          tmpf = tp(i)
          tp(i) = tp(j)
          tp(j) = tmpf
       end if
    end do

    tmpi = tref(i+1)
    tref(i+1) = tref(r)
    tref(r) = tmpi
    tmpi = tid(i+1)
    tid(i+1) = tid(r)
    tid(r) = tmpi
    tmpr(:) = tx(:,i+1)
    tx(:,i+1) = tx(:,r)
    tx(:,r) = tmpr(:)
    tmpr(:) = tv(:,i+1)
    tv(:,i+1) = tv(:,r)
    tv(:,r) = tmpr(:)
    tmpr(:) = tf(:,i+1)
    tf(:,i+1) = tf(:,r)
    tf(:,r) = tmpr(:)
    tmpf = tp(i+1)
    tp(i+1) = tp(r)
    tp(r) = tmpf

    q = i+1

#ifdef DEBUG2
    print *,'partition_refI_3x3R_1xR_1xI ends'
#endif

  end subroutine partition_refI_3x3R_1xR_1xI

  !=======================================================================
  !> Partition function for quicksort
  subroutine partition_refI_3x3R_1xI(p,r,q,tref,tx,tv,tf,tid)

    ! Input parameters
    integer(kind=4),intent(in)  :: p, r  ! First and last index of the tables to sort
    ! Input parameters modified in subroutine
    real   (kind=4), intent(inout),dimension(3,*) :: tx,  tv  ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref,tid ! structure id and particle id
    ! Output parameters
    integer(kind=4), intent(out) :: q
    ! Local parameters
    integer(kind=IDKIND)     :: tmpi,Sref
    real(kind=4),dimension(3) :: tmpr
    integer(kind=4) :: i, j

#ifdef DEBUG2
    print *,'partition_refI_3x3R_1xI begins'
#endif

    Sref  = tref(r)

    i = p-1
    do j = p, r-1
       if(tref(j) <= Sref) then
          i = i+1
          tmpi = tref(i)
          tref(i) = tref(j)
          tref(j) = tmpi
          tmpi = tid(i)
          tid(i) = tid(j)
          tid(j) = tmpi
          tmpr(:) = tx(:,i)
          tx(:,i) = tx(:,j)
          tx(:,j) = tmpr(:)
          tmpr(:) = tv(:,i)
          tv(:,i) = tv(:,j)
          tv(:,j) = tmpr(:)
          tmpr(:) = tf(:,i)
          tf(:,i) = tf(:,j)
          tf(:,j) = tmpr(:)
       end if
    end do

    tmpi = tref(i+1)
    tref(i+1) = tref(r)
    tref(r) = tmpi
    tmpi = tid(i+1)
    tid(i+1) = tid(r)
    tid(r) = tmpi
    tmpr(:) = tx(:,i+1)
    tx(:,i+1) = tx(:,r)
    tx(:,r) = tmpr(:)
    tmpr(:) = tv(:,i+1)
    tv(:,i+1) = tv(:,r)
    tv(:,r) = tmpr(:)
    tmpr(:) = tf(:,i+1)
    tf(:,i+1) = tf(:,r)
    tf(:,r) = tmpr(:)

    q = i+1

#ifdef DEBUG2
    print *,'partition_refI_3x3R_1xI ends'
#endif

  end subroutine partition_refI_3x3R_1xI
  ! End of quicksort
  !=======================================================================


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_1xI4(n,tref,ti)

    ! Input/Output variables
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=4),intent(inout),dimension(*)  :: ti     !< process id array

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_1xI4 begins'
#endif

    call heapify_refI_1xI4(size,n,tref,ti)

    do i = n, 2, -1
       call swap_refI_1xI4(1,i,tref,ti)
       size = size - 1
       call sink_refI_1xI4(size,1,tref,ti)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_1xI4 ends'
#endif

  end subroutine heapsort_refI_1xI4


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_1xI4(size,n,tref,ti)

    ! Input/Output variables
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref
    integer(kind=4),intent(inout),dimension(*)   :: ti

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_1xI4 begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_1xI4(size,i,tref,ti)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_1xI4 ends'
#endif

  end subroutine heapify_refI_1xI4


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_1xI4(size,i,tref,ti)

    ! Input/Output variables
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref
    integer(kind=4),intent(inout),dimension(*)   :: ti

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_1xI4 begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_1xI4(i,max,tref,ti)
       call sink_refI_1xI4(size,max,tref,ti)
    end if

#ifdef DEBUG2
    print *,'sink_refI_1xI4 ends'
#endif

  end subroutine sink_refI_1xI4


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_1xI4(i,j,tref,ti)

    ! Input/Output variables
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref
    integer(kind=4),intent(inout),dimension(*)   :: ti

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    integer(kind=4) :: tmpi4

#ifdef DEBUG2
    print *,'swap_refI_1xI4 begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi4 = ti(i)
    ti(i) = ti(j)
    ti(j) = tmpi4

#ifdef DEBUG2
    print *,'swap_refI_1xI4 end'
#endif

  end subroutine swap_refI_1xI4


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_2x3R_1xI(n,tref,tx,tv,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref  !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tid   !< particle id array

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_1xI begins'
#endif

    call heapify_refI_2x3R_1xI(size,n,tref,tx,tv,tid)

    do i = n, 2, -1
       call swap_refI_2x3R_1xI(1,i,tref,tx,tv,tid)
       size = size - 1
       call sink_refI_2x3R_1xI(size,1,tref,tx,tv,tid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_1xI ends'
#endif

  end subroutine heapsort_refI_2x3R_1xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_2x3R_1xI(size,n,tref,tx,tv,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id

    !Output variable
    integer(kind=4),intent(out) :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_1xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_2x3R_1xI(size,i,tref,tx,tv,tid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_1xI ends'
#endif

  end subroutine heapify_refI_2x3R_1xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_2x3R_1xI(size,i,tref,tx,tv,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_2x3R_1xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_2x3R_1xI(i,max,tref,tx,tv,tid)
       call sink_refI_2x3R_1xI(size,max,tref,tx,tv,tid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_2x3R_1xI ends'
#endif

  end subroutine sink_refI_2x3R_1xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_2x3R_1xI(i,j,tref,tx,tv,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr

#ifdef DEBUG2
    print *,'swap_refI_2x3R_1xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

#ifdef DEBUG2
    print *,'swap_refI_2x3R_1xI ends'
#endif

  end subroutine swap_refI_2x3R_1xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_2x3R_2xI(n,tref,tx,tv,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: trid   !< ramses particle id array (for cone)

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_2xI begins'
#endif

    call heapify_refI_2x3R_2xI(size,n,tref,tx,tv,tid,trid)

    do i = n, 2, -1
       call swap_refI_2x3R_2xI(1,i,tref,tx,tv,tid,trid)
       size = size - 1
       call sink_refI_2x3R_2xI(size,1,tref,tx,tv,tid,trid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_2xI ends'
#endif

  end subroutine heapsort_refI_2x3R_2xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_2x3R_2xI(size,n,tref,tx,tv,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid,trid ! structure id and particle id

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_2xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_2x3R_2xI(size,i,tref,tx,tv,tid,trid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_2xI ends'
#endif

  end subroutine heapify_refI_2x3R_2xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_2x3R_2xI(size,i,tref,tx,tv,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid,trid ! structure id and particle id

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_2x3R_2xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_2x3R_2xI(i,max,tref,tx,tv,tid,trid)
       call sink_refI_2x3R_2xI(size,max,tref,tx,tv,tid,trid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_2x3R_2xI ends'
#endif

  end subroutine sink_refI_2x3R_2xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_2x3R_2xI(i,j,tref,tx,tv,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid,trid ! structure id and particle id

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr

#ifdef DEBUG2
    print *,'swap_refI_2x3R_2xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmpi = trid(i)
    trid(i) = trid(j)
    trid(j) = tmpi

#ifdef DEBUG2
    print *,'swap_refI_2x3R_2xI ends'
#endif

  end subroutine swap_refI_2x3R_2xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_2x3R_1xR_1xI(n,tref,tx,tv,tp,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    real   (kind=4), intent(inout),dimension(*)   :: tp     !< potential array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref  !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tid   !< particle id array

    ! Input variable
    integer(kind=4),intent(in) :: n !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_1xR_1xI begins'
#endif

    call heapify_refI_2x3R_1xR_1xI(size,n,tref,tx,tv,tp,tid)

    do i = n, 2, -1
       call swap_refI_2x3R_1xR_1xI(1,i,tref,tx,tv,tp,tid)
       size = size - 1
       call sink_refI_2x3R_1xR_1xI(size,1,tref,tx,tv,tp,tid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_1xR_1xI ends'
#endif

  end subroutine heapsort_refI_2x3R_1xR_1xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_2x3R_1xR_1xI(size,n,tref,tx,tv,tp,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_1xR_1xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_2x3R_1xR_1xI(size,i,tref,tx,tv,tp,tid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_1xR_1xI ends'
#endif

  end subroutine heapify_refI_2x3R_1xR_1xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_2x3R_1xR_1xI(size,i,tref,tx,tv,tp, tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_2x3R_1xR_1xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_2x3R_1xR_1xI(i,max,tref,tx,tv,tp,tid)
       call sink_refI_2x3R_1xR_1xI(size,max,tref,tx,tv,tp,tid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_2x3R_1xR_1xI ends'
#endif

  end subroutine sink_refI_2x3R_1xR_1xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_2x3R_1xR_1xI(i,j,tref,tx,tv,tp,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    print *,'swap_refI_2x3R_1xR_1xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmps = tp(i)
    tp(i) = tp(j)
    tp(j) = tmps

#ifdef DEBUG2
    print *,'swap_refI_2x3R_1xR_1xI ends'
#endif

  end subroutine swap_refI_2x3R_1xR_1xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_2x3R_1xR_2xI(n,tref,tx,tv,tp,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: trid   !< ramses particle id array (for cone)
    real   (kind=4), intent(inout), dimension(*)  :: tp     !< potential array

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_1xR_2xI begins'
#endif

    call heapify_refI_2x3R_1xR_2xI(size,n,tref,tx,tv,tp,tid,trid)

    do i = n, 2, -1
       call swap_refI_2x3R_1xR_2xI(1,i,tref,tx,tv,tp,tid,trid)
       size = size - 1
       call sink_refI_2x3R_1xR_2xI(size,1,tref,tx,tv,tp,tid,trid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_1xR_2xI ends'
#endif

  end subroutine heapsort_refI_2x3R_1xR_2xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_2x3R_1xR_2xI(size,n,tref,tx,tv,tp,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid,trid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_1xR_2xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_2x3R_1xR_2xI(size,i,tref,tx,tv,tp,tid,trid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_1xR_2xI ends'
#endif

  end subroutine heapify_refI_2x3R_1xR_2xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_2x3R_1xR_2xI(size,i,tref,tx,tv,tp,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid,trid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_2x3R_1xR_2xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_2x3R_1xR_2xI(i,max,tref,tx,tv,tp,tid,trid)
       call sink_refI_2x3R_1xR_2xI(size,max,tref,tx,tv,tp,tid,trid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_2x3R_1xR_2xI ends'
#endif

  end subroutine sink_refI_2x3R_1xR_2xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_2x3R_1xR_2xI(i,j,tref,tx,tv,tp,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid,trid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    print *,'swap_refI_2x3R_1xR_2xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmps = tp(i)
    tp(i) = tp(j)
    tp(j) = tmps

    tmpi = trid(i)
    trid(i) = trid(j)
    trid(j) = tmpi

#ifdef DEBUG2
    print *,'swap_refI_2x3R_1xR_2xI ends'
#endif

  end subroutine swap_refI_2x3R_1xR_2xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_2x3R_2xR_1xI(n,tref,tx,tv,tp,tma,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array
    real   (kind=4), intent(inout), dimension(*)  :: tp     !< potential array
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_2xR_1xI begins'
#endif

    call heapify_refI_2x3R_2xR_1xI(size,n,tref,tx,tv,tp,tma,tid)

    do i = n, 2, -1
       call swap_refI_2x3R_2xR_1xI(1,i,tref,tx,tv,tp,tma,tid)
       size = size - 1
       call sink_refI_2x3R_2xR_1xI(size,1,tref,tx,tv,tp,tma,tid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_2xR_1xI ends'
#endif

  end subroutine heapsort_refI_2x3R_2xR_1xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_2x3R_2xR_1xI(size,n,tref,tx,tv,tp,tma,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_2xR_1xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_2x3R_2xR_1xI(size,i,tref,tx,tv,tp,tma,tid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_2xR_1xI ends'
#endif

  end subroutine heapify_refI_2x3R_2xR_1xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_2x3R_2xR_1xI(size,i,tref,tx,tv,tp,tma,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_2x3R_2xR_1xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_2x3R_2xR_1xI(i,max,tref,tx,tv,tp,tma,tid)
       call sink_refI_2x3R_2xR_1xI(size,max,tref,tx,tv,tp,tma,tid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_2x3R_2xR_1xI ends'
#endif

  end subroutine sink_refI_2x3R_2xR_1xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_2x3R_2xR_1xI(i,j,tref,tx,tv,tp,tma,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    print *,'swap_refI_2x3R_2xR_1xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmps = tp(i)
    tp(i) = tp(j)
    tp(j) = tmps

    tmps = tma(i)
    tma(i) = tma(j)
    tma(j) = tmps

#ifdef DEBUG2
    print *,'swap_refI_2x3R_2xR_1xI ends'
#endif

  end subroutine swap_refI_2x3R_2xR_1xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_2x3R_3xR_1xI(n,tref,tx,tv,tp,tma,tbd,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array
    real   (kind=4), intent(inout), dimension(*)  :: tp     !< potential array
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_3xR_1xI begins'
#endif

    call heapify_refI_2x3R_3xR_1xI(size,n,tref,tx,tv,tp,tma,tbd,tid)

    do i = n, 2, -1
       call swap_refI_2x3R_3xR_1xI(1,i,tref,tx,tv,tp,tma,tbd,tid)
       size = size - 1
       call sink_refI_2x3R_3xR_1xI(size,1,tref,tx,tv,tp,tma,tbd,tid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_3xR_1xI ends'
#endif

  end subroutine heapsort_refI_2x3R_3xR_1xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_2x3R_3xR_1xI(size,n,tref,tx,tv,tp,tma,tbd,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_3xR_1xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_2x3R_3xR_1xI(size,i,tref,tx,tv,tp,tma,tbd,tid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_3xR_1xI ends'
#endif

  end subroutine heapify_refI_2x3R_3xR_1xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_2x3R_3xR_1xI(size,i,tref,tx,tv,tp,tma,tbd,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_2x3R_3xR_1xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_2x3R_3xR_1xI(i,max,tref,tx,tv,tp,tma,tbd,tid)
       call sink_refI_2x3R_3xR_1xI(size,max,tref,tx,tv,tp,tma,tbd,tid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_2x3R_3xR_1xI ends'
#endif

  end subroutine sink_refI_2x3R_3xR_1xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_2x3R_3xR_1xI(i,j,tref,tx,tv,tp,tma,tbd,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    print *,'swap_refI_2x3R_3xR_1xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmps = tp(i)
    tp(i) = tp(j)
    tp(j) = tmps

    tmps = tma(i)
    tma(i) = tma(j)
    tma(j) = tmps

    tmps = tbd(i)
    tbd(i) = tbd(j)
    tbd(j) = tmps

#ifdef DEBUG2
    print *,'swap_refI_2x3R_3xR_1xI ends'
#endif

  end subroutine swap_refI_2x3R_3xR_1xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_2x3R_4xR_1xI(n,tref,tx,tv,tp,tma,tbd,tme,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array
    real   (kind=4), intent(inout), dimension(*)  :: tp     !< potential array
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_4xR_1xI begins'
#endif

    call heapify_refI_2x3R_4xR_1xI(size,n,tref,tx,tv,tp,tma,tbd,tme,tid)

    do i = n, 2, -1
       call swap_refI_2x3R_4xR_1xI(1,i,tref,tx,tv,tp,tma,tbd,tme,tid)
       size = size - 1
       call sink_refI_2x3R_4xR_1xI(size,1,tref,tx,tv,tp,tma,tbd,tme,tid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_2x3R_4xR_1xI ends'
#endif

  end subroutine heapsort_refI_2x3R_4xR_1xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_2x3R_4xR_1xI(size,n,tref,tx,tv,tp,tma,tbd,tme,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_4xR_1xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_2x3R_4xR_1xI(size,i,tref,tx,tv,tp,tma,tbd,tme,tid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_2x3R_4xR_1xI ends'
#endif

  end subroutine heapify_refI_2x3R_4xR_1xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_2x3R_4xR_1xI(size,i,tref,tx,tv,tp,tma,tbd,tme,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_2x3R_4xR_1xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_2x3R_4xR_1xI(i,max,tref,tx,tv,tp,tma,tbd,tme,tid)
       call sink_refI_2x3R_4xR_1xI(size,max,tref,tx,tv,tp,tma,tbd,tme,tid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_2x3R_4xR_1xI ends'
#endif

  end subroutine sink_refI_2x3R_4xR_1xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_2x3R_4xR_1xI(i,j,tref,tx,tv,tp,tma,tbd,tme,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    print *,'swap_refI_2x3R_4xR_1xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmps = tp(i)
    tp(i) = tp(j)
    tp(j) = tmps

    tmps = tma(i)
    tma(i) = tma(j)
    tma(j) = tmps

    tmps = tbd(i)
    tbd(i) = tbd(j)
    tbd(j) = tmps

    tmps = tme(i)
    tme(i) = tme(j)
    tme(j) = tmps

#ifdef DEBUG2
    print *,'swap_refI_2x3R_4xR_1xI ends'
#endif

  end subroutine swap_refI_2x3R_4xR_1xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_3x3R_1xI(n,tref,tx,tv,tf,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    real   (kind=4), intent(inout), dimension(3,*) :: tf    !< gravitational field array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_1xI begins'
#endif

    call heapify_refI_3x3R_1xI(size,n,tref,tx,tv,tf,tid)

    do i = n, 2, -1
       call swap_refI_3x3R_1xI(1,i,tref,tx,tv,tf,tid)
       size = size - 1
       call sink_refI_3x3R_1xI(size,1,tref,tx,tv,tf,tid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_1xI ends'
#endif

  end subroutine heapsort_refI_3x3R_1xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_3x3R_1xI(size,n,tref,tx,tv,tf,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) ::n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_1xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_3x3R_1xI(size,i,tref,tx,tv,tf,tid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_1xI ends'
#endif

  end subroutine heapify_refI_3x3R_1xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_3x3R_1xI(size,i,tref,tx,tv,tf,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_3x3R_1xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_3x3R_1xI(i,max,tref,tx,tv,tf,tid)
       call sink_refI_3x3R_1xI(size,max,tref,tx,tv,tf,tid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_3x3R_1xI ends'
#endif

  end subroutine sink_refI_3x3R_1xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_3x3R_1xI(i,j,tref,tx,tv,tf,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr

#ifdef DEBUG2
    print *,'swap_refI_3x3R_1xI ends'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmpr(:) = tf(:,i)
    tf(:,i) = tf(:,j)
    tf(:,j) = tmpr(:)

#ifdef DEBUG2
    print *,'swap_refI_3x3R_1xI ends'
#endif

  end subroutine swap_refI_3x3R_1xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_3x3R_2xI(n,tref,tx,tv,tf,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: trid   !< ramses particle id array (for cone)
    real   (kind=4), intent(inout), dimension(3,*) :: tf    !< gravitational field array

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_2xI begins'
#endif

    call heapify_refI_3x3R_2xI(size,n,tref,tx,tv,tf,tid,trid)

    do i = n, 2, -1
       call swap_refI_3x3R_2xI(1,i,tref,tx,tv,tf,tid,trid)
       size = size - 1
       call sink_refI_3x3R_2xI(size,1,tref,tx,tv,tf,tid,trid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_2xI ends'
#endif

  end subroutine heapsort_refI_3x3R_2xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_3x3R_2xI(size,n,tref,tx,tv,tf,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid, trid ! structure id and particle id

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_2xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_3x3R_2xI(size,i,tref,tx,tv,tf,tid,trid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_2xI ends'
#endif

  end subroutine heapify_refI_3x3R_2xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_3x3R_2xI(size,i,tref,tx,tv,tf,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid, trid ! structure id and particle id

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_3x3R_2xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_3x3R_2xI(i,max,tref,tx,tv,tf,tid,trid)
       call sink_refI_3x3R_2xI(size,max,tref,tx,tv,tf,tid,trid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_3x3R_2xI ends'
#endif

  end subroutine sink_refI_3x3R_2xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_3x3R_2xI(i,j,tref,tx,tv,tf,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid, trid ! structure id and particle id

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr

#ifdef DEBUG2
    print *,'swap_refI_3x3R_2xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmpr(:) = tf(:,i)
    tf(:,i) = tf(:,j)
    tf(:,j) = tmpr(:)

    tmpi = trid(i)
    trid(i) = trid(j)
    trid(j) = tmpi

#ifdef DEBUG2
    print *,'swap_refI_3x3R_2xI ends'
#endif

  end subroutine swap_refI_3x3R_2xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_3x3R_1xR_1xI(n,tref,tx,tv,tf,tp,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*)  :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*)  :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tid    !< particle id array
    real   (kind=4), intent(inout), dimension(3,*) :: tf     !< gravitational field array
    real   (kind=4), intent(inout), dimension(*)   :: tp     !< potential array

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_1xR_1xI begins'
#endif

    call heapify_refI_3x3R_1xR_1xI(size,n,tref,tx,tv,tf,tp,tid)

    do i = n, 2, -1
       call swap_refI_3x3R_1xR_1xI(1,i,tref,tx,tv,tf,tp,tid)
       size = size - 1
       call sink_refI_3x3R_1xR_1xI(size,1,tref,tx,tv,tf,tp,tid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_1xR_1xI ends'
#endif

  end subroutine heapsort_refI_3x3R_1xR_1xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_3x3R_1xR_1xI(size,n,tref,tx,tv,tf,tp,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_1xR_1xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_3x3R_1xR_1xI(size,i,tref,tx,tv,tf,tp,tid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_1xR_1xI ends'
#endif

  end subroutine heapify_refI_3x3R_1xR_1xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_3x3R_1xR_1xI(size,i,tref,tx,tv,tf,tp,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_3x3R_1xR_1xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_3x3R_1xR_1xI(i,max,tref,tx,tv,tf,tp,tid)
       call sink_refI_3x3R_1xR_1xI(size,max,tref,tx,tv,tf,tp,tid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_3x3R_1xR_1xI ends'
#endif

  end subroutine sink_refI_3x3R_1xR_1xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_3x3R_1xR_1xI(i,j,tref,tx,tv,tf,tp,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    print *,'swap_refI_3x3R_1xR_1xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmpr(:) = tf(:,i)
    tf(:,i) = tf(:,j)
    tf(:,j) = tmpr(:)

    tmps = tp(i)
    tp(i) = tp(j)
    tp(j) = tmps

#ifdef DEBUG2
    print *,'swap_refI_3x3R_1xR_1xI ends'
#endif

  end subroutine swap_refI_3x3R_1xR_1xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_3x3R_2xR_1xI(n,tref,tx,tv,tf,tp,tma,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array
    real   (kind=4), intent(inout), dimension(*)  :: tp     !< potential array
    real   (kind=4), intent(inout), dimension(3,*) :: tf    !< gravitational field array
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_2xR_1xI begins'
#endif

    call heapify_refI_3x3R_2xR_1xI(size,n,tref,tx,tv,tf,tp,tma,tid)

    do i = n, 2, -1
       call swap_refI_3x3R_2xR_1xI(1,i,tref,tx,tv,tf,tp,tma,tid)
       size = size - 1
       call sink_refI_3x3R_2xR_1xI(size,1,tref,tx,tv,tf,tp,tma,tid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_2xR_1xI ends'
#endif

  end subroutine heapsort_refI_3x3R_2xR_1xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_3x3R_2xR_1xI(size,n,tref,tx,tv,tf,tp,tma,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_2xR_1xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_3x3R_2xR_1xI(size,i,tref,tx,tv,tf,tp,tma,tid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_2xR_1xI ends'
#endif

  end subroutine heapify_refI_3x3R_2xR_1xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_3x3R_2xR_1xI(size,i,tref,tx,tv,tf,tp,tma,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_3x3R_2xR_1xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_3x3R_2xR_1xI(i,max,tref,tx,tv,tf,tp,tma,tid)
       call sink_refI_3x3R_2xR_1xI(size,max,tref,tx,tv,tf,tp,tma,tid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_3x3R_2xR_1xI ends'
#endif

  end subroutine sink_refI_3x3R_2xR_1xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_3x3R_2xR_1xI(i,j,tref,tx,tv,tf,tp,tma,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    print *,'swap_refI_3x3R_2xR_1xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmpr(:) = tf(:,i)
    tf(:,i) = tf(:,j)
    tf(:,j) = tmpr(:)

    tmps = tp(i)
    tp(i) = tp(j)
    tp(j) = tmps

    tmps = tma(i)
    tma(i) = tma(j)
    tma(j) = tmps

#ifdef DEBUG2
    print *,'swap_refI_3x3R_2xR_1xI ends'
#endif

  end subroutine swap_refI_3x3R_2xR_1xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_3x3R_3xR_1xI(n,tref,tx,tv,tf,tp,tma,tbd,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array
    real   (kind=4), intent(inout), dimension(*)  :: tp     !< potential array
    real   (kind=4), intent(inout), dimension(3,*) :: tf    !< gravitational field array
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_3xR_1xI begins'
#endif

    call heapify_refI_3x3R_3xR_1xI(size,n,tref,tx,tv,tf,tp,tma,tbd,tid)

    do i = n, 2, -1
       call swap_refI_3x3R_3xR_1xI(1,i,tref,tx,tv,tf,tp,tma,tbd,tid)
       size = size - 1
       call sink_refI_3x3R_3xR_1xI(size,1,tref,tx,tv,tf,tp,tma,tbd,tid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_3xR_1xI ends'
#endif

  end subroutine heapsort_refI_3x3R_3xR_1xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_3x3R_3xR_1xI(size,n,tref,tx,tv,tf,tp,tma,tbd,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_3xR_1xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_3x3R_3xR_1xI(size,i,tref,tx,tv,tf,tp,tma,tbd,tid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_3xR_1xI ends'
#endif

  end subroutine heapify_refI_3x3R_3xR_1xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_3x3R_3xR_1xI(size,i,tref,tx,tv,tf,tp,tma,tbd,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_3x3R_3xR_1xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_3x3R_3xR_1xI(i,max,tref,tx,tv,tf,tp,tma,tbd,tid)
       call sink_refI_3x3R_3xR_1xI(size,max,tref,tx,tv,tf,tp,tma,tbd,tid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_3x3R_3xR_1xI ends'
#endif

  end subroutine sink_refI_3x3R_3xR_1xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_3x3R_3xR_1xI(i,j,tref,tx,tv,tf,tp,tma,tbd,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    print *,'swap_refI_3x3R_3xR_1xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmpr(:) = tf(:,i)
    tf(:,i) = tf(:,j)
    tf(:,j) = tmpr(:)

    tmps = tp(i)
    tp(i) = tp(j)
    tp(j) = tmps

    tmps = tma(i)
    tma(i) = tma(j)
    tma(j) = tmps

    tmps = tbd(i)
    tbd(i) = tbd(j)
    tbd(j) = tmps

#ifdef DEBUG2
    print *,'swap_refI_3x3R_3xR_1xI ends'
#endif

  end subroutine swap_refI_3x3R_3xR_1xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_3x3R_4xR_1xI(n,tref,tx,tv,tf,tp,tma,tbd,tme,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array
    real   (kind=4), intent(inout), dimension(*)  :: tp     !< potential array
    real   (kind=4), intent(inout), dimension(3,*) :: tf    !< gravitational field array
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_4xR_1xI begins'
#endif

    call heapify_refI_3x3R_4xR_1xI(size,n,tref,tx,tv,tf,tp,tma,tbd,tme,tid)

    do i = n, 2, -1
       call swap_refI_3x3R_4xR_1xI(1,i,tref,tx,tv,tf,tp,tma,tbd,tme,tid)
       size = size - 1
       call sink_refI_3x3R_4xR_1xI(size,1,tref,tx,tv,tf,tp,tma,tbd,tme,tid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_4xR_1xI ends'
#endif

  end subroutine heapsort_refI_3x3R_4xR_1xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_3x3R_4xR_1xI(size,n,tref,tx,tv,tf,tp,tma,tbd,tme,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_4xR_1xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_3x3R_4xR_1xI(size,i,tref,tx,tv,tf,tp,tma,tbd,tme,tid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_4xR_1xI ends'
#endif

  end subroutine heapify_refI_3x3R_4xR_1xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_3x3R_4xR_1xI(size,i,tref,tx,tv,tf,tp,tma,tbd,tme,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_3x3R_4xR_1xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_3x3R_4xR_1xI(i,max,tref,tx,tv,tf,tp,tma,tbd,tme,tid)
       call sink_refI_3x3R_4xR_1xI(size,max,tref,tx,tv,tf,tp,tma,tbd,tme,tid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_3x3R_4xR_1xI ends'
#endif

  end subroutine sink_refI_3x3R_4xR_1xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_3x3R_4xR_1xI(i,j,tref,tx,tv,tf,tp,tma,tbd,tme,tid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    print *,'swap_refI_3x3R_4xR_1xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmpr(:) = tf(:,i)
    tf(:,i) = tf(:,j)
    tf(:,j) = tmpr(:)

    tmps = tp(i)
    tp(i) = tp(j)
    tp(j) = tmps

    tmps = tma(i)
    tma(i) = tma(j)
    tma(j) = tmps

    tmps = tbd(i)
    tbd(i) = tbd(j)
    tbd(j) = tmps

    tmps = tme(i)
    tme(i) = tme(j)
    tme(j) = tmps

#ifdef DEBUG2
    print *,'swap_refI_3x3R_4xR_1xI ends'
#endif

  end subroutine swap_refI_3x3R_4xR_1xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_3x3R_1xR_2xI(n,tref,tx,tv,tf,tp,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: trid   !< ramses particle id array (for cone)
    real   (kind=4), intent(inout), dimension(*)  :: tp     !< potential array
    real   (kind=4), intent(inout), dimension(3,*) :: tf    !< gravitational field array

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_1xR_2xI begins'
#endif

    call heapify_refI_3x3R_1xR_2xI(size,n,tref,tx,tv,tf,tp,tid,trid)

    do i = n, 2, -1
       call swap_refI_3x3R_1xR_2xI(1,i,tref,tx,tv,tf,tp,tid,trid)
       size = size - 1
       call sink_refI_3x3R_1xR_2xI(size,1,tref,tx,tv,tf,tp,tid,trid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_1xR_2xI ends'
#endif

  end subroutine heapsort_refI_3x3R_1xR_2xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_3x3R_1xR_2xI(size,n,tref,tx,tv,tf,tp,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid, trid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_1xR_2xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_3x3R_1xR_2xI(size,i,tref,tx,tv,tf,tp,tid,trid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_1xR_2xI ends'
#endif

  end subroutine heapify_refI_3x3R_1xR_2xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_3x3R_1xR_2xI(size,i,tref,tx,tv,tf,tp,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid, trid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_3x3R_1xR_2xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_3x3R_1xR_2xI(i,max,tref,tx,tv,tf,tp,tid,trid)
       call sink_refI_3x3R_1xR_2xI(size,max,tref,tx,tv,tf,tp,tid,trid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_3x3R_1xR_2xI ends'
#endif

  end subroutine sink_refI_3x3R_1xR_2xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_3x3R_1xR_2xI(i,j,tref,tx,tv,tf,tp,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid, trid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    print *,'swap_refI_3x3R_1xR_2xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmpr(:) = tf(:,i)
    tf(:,i) = tf(:,j)
    tf(:,j) = tmpr(:)

    tmps = tp(i)
    tp(i) = tp(j)
    tp(j) = tmps

    tmpi = trid(i)
    trid(i) = trid(j)
    trid(j) = tmpi

#ifdef DEBUG2
    print *,'swap_refI_3x3R_1xR_2xI ends'
#endif

  end subroutine swap_refI_3x3R_1xR_2xI


  !=======================================================================
  !> Heap sort as described in Introduction to algorithms (Cormen, Leiserson, Rivest, Stein)
  subroutine heapsort_refI_3x3R_4xR_2xI(n,tref,tx,tv,tf,tp,tma,tbd,tme,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx     !< positions array
    real   (kind=4), intent(inout),dimension(3,*) :: tv     !< velocities array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tref   !< halo id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: tid    !< particle id array
    integer(kind=IDKIND),intent(inout),dimension(*)  :: trid   !< ramses particle id array (for cone)
    real   (kind=4), intent(inout), dimension(*)  :: tp     !< potential array
    real   (kind=4), intent(inout), dimension(3,*) :: tf    !< gravitational field array
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: n  !< length of the arrays

    ! Local variables
    integer(kind=4) :: i,size

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_4xR_2xI begins'
#endif

    call heapify_refI_3x3R_4xR_2xI(size,n,tref,tx,tv,tf,tp,tma,tbd,tme,tid,trid)

    do i = n, 2, -1
       call swap_refI_3x3R_4xR_2xI(1,i,tref,tx,tv,tf,tp,tma,tbd,tme,tid,trid)
       size = size - 1
       call sink_refI_3x3R_4xR_2xI(size,1,tref,tx,tv,tf,tp,tma,tbd,tme,tid,trid)
    end do

#ifdef DEBUG2
    print *,'heapsort_refI_3x3R_4xR_2xI ends'
#endif

  end subroutine heapsort_refI_3x3R_4xR_2xI


  !=======================================================================
  !> Heapify routine for heapsort
  subroutine heapify_refI_3x3R_4xR_2xI(size,n,tref,tx,tv,tf,tp,tma,tbd,tme,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid, trid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    !Output variable
    integer(kind=4),intent(out)                :: size

    ! Input variable
    integer(kind=4),intent(in) :: n

    ! Local variables
    integer(kind=4) :: i

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_4xR_2xI begins'
#endif

    size = n
    do i = n/2, 1, -1
       call sink_refI_3x3R_4xR_2xI(size,i,tref,tx,tv,tf,tp,tma,tbd,tme,tid,trid)
    end do

#ifdef DEBUG2
    print *,'heapify_refI_3x3R_4xR_2xI ends'
#endif

  end subroutine heapify_refI_3x3R_4xR_2xI


  !=======================================================================
  !> Sink routine for heapsort
  recursive subroutine sink_refI_3x3R_4xR_2xI(size,i,tref,tx,tv,tf,tp,tma,tbd,tme,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid, trid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: size,i

    ! Local variables
    integer(kind=4) :: l, r, max

#ifdef DEBUG2
    print *,'sink_refI_3x3R_4xR_2xI begins'
#endif

    l = 2*i
    r = 2*i+1

    if( (l <= size) .and. tref(l) > tref(i) ) then
       max = l
    else
       max = i
    end if

    if( (r <= size) .and. tref(r) > tref(max) ) then
       max = r
    end if

    if( max /= i ) then
       call swap_refI_3x3R_4xR_2xI(i,max,tref,tx,tv,tf,tp,tma,tbd,tme,tid,trid)
       call sink_refI_3x3R_4xR_2xI(size,max,tref,tx,tv,tf,tp,tma,tbd,tme,tid,trid)
    end if

#ifdef DEBUG2
    print *,'sink_refI_3x3R_4xR_2xI ends'
#endif

  end subroutine sink_refI_3x3R_4xR_2xI


  !=======================================================================
  !> Swap routine for heapsort
  subroutine swap_refI_3x3R_4xR_2xI(i,j,tref,tx,tv,tf,tp,tma,tbd,tme,tid,trid)

    ! Input/Output variables
    real   (kind=4), intent(inout),dimension(3,*) :: tx,tv     ! positions and velocities
    real   (kind=4), intent(inout), dimension(3,*) :: tf  ! force
    integer(kind=IDKIND),intent(inout),dimension(*)   :: tref, tid, trid ! structure id and particle id
    real   (kind=4), intent(inout), dimension(*) :: tp
    real   (kind=4), intent(inout), dimension(*) :: tma  !< mass of the particle
    real   (kind=4), intent(inout), dimension(*) :: tme  !< metallicity of the particle
    real   (kind=4), intent(inout), dimension(*) :: tbd  !< birt date of the particle

    ! Input variable
    integer(kind=4),intent(in) :: i,j

    ! Local variables
    integer(kind=IDKIND) :: tmpi
    real(kind=4), dimension(3) :: tmpr
    real(kind=4) :: tmps

#ifdef DEBUG2
    print *,'swap_refI_3x3R_4xR_2xI begins'
#endif

    tmpi = tref(i)
    tref(i) = tref(j)
    tref(j) = tmpi

    tmpi = tid(i)
    tid(i) = tid(j)
    tid(j) = tmpi

    tmpr(:) = tx(:,i)
    tx(:,i) = tx(:,j)
    tx(:,j) = tmpr(:)

    tmpr(:) = tv(:,i)
    tv(:,i) = tv(:,j)
    tv(:,j) = tmpr(:)

    tmpr(:) = tf(:,i)
    tf(:,i) = tf(:,j)
    tf(:,j) = tmpr(:)

    tmps = tp(i)
    tp(i) = tp(j)
    tp(j) = tmps

    tmps = tma(i)
    tma(i) = tma(j)
    tma(j) = tmps

    tmps = tbd(i)
    tbd(i) = tbd(j)
    tbd(j) = tmps

    tmps = tme(i)
    tme(i) = tme(j)
    tme(j) = tmps

    tmpi = trid(i)
    trid(i) = trid(j)
    trid(j) = tmpi

#ifdef DEBUG2
    print *,'swap_refI_3x3R_4xR_2xI ends'
#endif

  end subroutine swap_refI_3x3R_4xR_2xI

  
end module modsort

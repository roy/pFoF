!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2017 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Index order utils for pFoF
!! @brief
!! Index order utils for pFoF
!! @author Fabrice Roy
! ======================================================================
module modindex

  use iso_fortran_env, only : OUTPUT_UNIT
  use modmpicommons, only : procid
  use modconstant, only : IDKIND

  implicit none

  private
  public :: id_to_coord,&
       coord_to_id

#ifdef zcurve
  interface id_to_coord
     module procedure id_to_coord_zcurve
  end interface id_to_coord
  interface coord_to_id
     module procedure coord_to_id_zcurve
  end interface coord_to_id
#else
  interface id_to_coord
     module procedure id_to_coord_grid
  end interface id_to_coord
  interface coord_to_id
     module procedure coord_to_id_grid
  end interface coord_to_id
#endif

  integer(kind=IDKIND), dimension (0:1023) :: morton_table

contains

  ! 0 <= coords(i) <= dims(i)-1
  ! 1 <=    id     <= dims(1)*dims(2)*dims(3)
  !=======================================================================
  subroutine id_to_coord_grid(id, coords, dims)

    integer(kind=4), dimension(3), intent(inout) :: coords
    integer(kind=IDKIND), intent(in) :: id
    integer(kind=4), dimension(3), intent(in) :: dims

#ifdef DEBUG2
    write(OUTPUT_UNIT, *) 'Enter id_to_coord_grid on process ', procid
#endif

    coords(1) = int((id-1) / (dims(3)*dims(2)),kind=4)
    coords(2) = int((id-1 - coords(1)*dims(2)*dims(3)) / dims(3),kind=4)
    coords(3) = int((id-1 - coords(1)*dims(2)*dims(3) - coords(2)*dims(3)),kind=4)

#ifdef DEBUG2
    write(OUTPUT_UNIT, *) 'Exit id_to_coord_grid on process ', procid
#endif

  end subroutine id_to_coord_grid

  !=======================================================================
  subroutine coord_to_id_grid(coords,id,dims)

    integer(kind=IDKIND), intent(inout) :: id
    integer(kind=4), dimension(3), intent(in) :: coords
    integer(kind=4), dimension(3), intent(in) :: dims

#ifdef DEBUG2
    write(OUTPUT_UNIT, *) 'Enter coord_to_id_grid on process ', procid
#endif
    id = 1 + coords(3) + coords(2)*dims(3) + coords(1)*dims(3)*dims(2)

#ifdef DEBUG2
    write(OUTPUT_UNIT, *) 'Exit coord_to_id_grid on process ', procid
#endif

  end subroutine coord_to_id_grid

  !=======================================================================
  subroutine id_to_coord_zcurve(id,coords,dims)

    integer(kind=4), dimension(3), intent(inout) :: coords
    integer(kind=IDKIND), intent(in) :: id
    integer(kind=4), dimension(3), intent(in) :: dims

    integer(kind=IDKIND) :: i8, j8, k8, z_order
    integer(kind=4) :: b

#ifdef DEBUG2
    write(OUTPUT_UNIT, *) 'Enter id_to_coord_zcurve on process ', procid
#endif

    z_order = id-1
    i8 = 0
    j8 = 0
    k8 = 0

    do b=0, 19
       call mvbits(z_order,3*b+2,1,i8,b)
       call mvbits(z_order,3*b+1,1,j8,b)
       call mvbits(z_order,3*b  ,1,k8,b)
    end do

    coords(1) = int(i8,kind=4) + 1
    coords(2) = int(j8,kind=4) + 1
    coords(3) = int(k8,kind=4) + 1

#ifdef DEBUG2
    write(OUTPUT_UNIT, *) 'Exit id_to_coord_zcurve on process ', procid
#endif

  end subroutine id_to_coord_zcurve

  !=======================================================================
  subroutine coord_to_id_zcurve(coords,id,dims)

    integer(kind=IDKIND), intent(inout) :: id
    integer(kind=4), dimension(3), intent(in) :: coords
    integer(kind=4), dimension(3), intent(in) :: dims

    integer(kind=IDKIND) :: z, i8, j8, k8

#ifdef DEBUG2
    write(OUTPUT_UNIT, *) 'Enter coord_to_id_zcurve on process ', procid
#endif

    i8 = coords(1)-1
    j8 = coords(2)-1
    k8 = coords(3)-1

    z = morton_table(iand(k8, int(1023,kind=IDKIND)))
    z = z + ishft(morton_table(iand(j8, int(1023,kind=IDKIND))),1)
    z = z + ishft(morton_table(iand(i8, int(1023,kind=IDKIND))),2)
    z = z + ishft(morton_table(iand(ishft(k8,-int(10,kind=IDKIND)), int(1023,kind=IDKIND))),int(30,kind=IDKIND))
    z = z + ishft(morton_table(iand(ishft(j8,-int(10,kind=IDKIND)), int(1023,kind=IDKIND))),int(31,kind=IDKIND))
    id = z + ishft(morton_table(iand(ishft(i8,-int(10,kind=IDKIND)), int(1023,kind=IDKIND))),int(32,kind=IDKIND)) + 1

#ifdef DEBUG2
    write(OUTPUT_UNIT, *) 'Exit coord_to_id_zcurve on process ', procid
#endif

  end subroutine coord_to_id_zcurve

  !=======================================================================
  subroutine init_morton_table()

    integer(kind=4) :: b, v, z

#ifdef DEBUG2
    write(OUTPUT_UNIT, *) 'Enter init_morton_table on process ', procid
#endif

    do v=0, 1023
       z = 0
       do b=0, 9
          call mvbits(v,b,1,z,3*b)
       end do
       morton_table(v) = z
    end do

#ifdef DEBUG2
    write(OUTPUT_UNIT, *) 'Exit init_morton_table on process ', procid
#endif

  end subroutine init_morton_table


end module modindex

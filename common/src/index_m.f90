!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2017 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Index order utils for pFoF
!! @brief
!! Index order utils for pFoF
!! @author Fabrice Roy
!----------------------------------------------------------------------------------------------------------------------------------
module index_m

  use iso_fortran_env, only : OUTPUT_UNIT
  use constants_m, only : DP, IDKIND

  implicit none

  private
  public :: Coords_to_id, &
       Id_to_coords, &
       Position_to_id

  interface Coords_to_id
     module procedure Coords_to_id_grid_3d
  end interface Coords_to_id

  interface Id_to_coords
     module procedure Id4_to_coords_grid_3d
     module procedure Id8_to_coords_grid_3d
  end interface Id_to_coords

  interface Position_to_id
     module procedure Position_to_id_grid_3d
  end interface Position_to_id

contains

  ! 0 <= coords(i) <= dims(i)-1
  ! 1 <=    id     <= dims(1)*dims(2)*dims(3)
  !----------------------------------------------------------------------------------------------------------------------------------
  pure function Coords_to_id_grid_3d(coords, dims) result (id)

    integer(kind=4), dimension(3), intent(in) :: coords
    integer(kind=4), dimension(3), intent(in) :: dims
    integer(kind=IDKIND) :: id

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Coords_to_id_grid begins on process', mpi_process%rank
#endif
    id = 1 + coords(3) + coords(2)*dims(3) + coords(1)*dims(3)*dims(2)

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Coords_to_id_grid ends on process', mpi_process%rank
#endif

  end function Coords_to_id_grid_3d


  !----------------------------------------------------------------------------------------------------------------------------------
  pure function Id4_to_coords_grid_3d(dims, id) result (coords)

    integer(kind=4), dimension(3) :: coords
    integer(kind=4), intent(in) :: id
    integer(kind=4), dimension(3), intent(in) :: dims

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Id_to_coords_grid begins on process', mpi_process%rank
#endif

    coords(1) = int((id-1) / (dims(3)*dims(2)),kind=4)
    coords(2) = int((id-1 - coords(1)*dims(2)*dims(3)) / dims(3),kind=4)
    coords(3) = int((id-1 - coords(1)*dims(2)*dims(3) - coords(2)*dims(3)),kind=4)

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Id_to_coords_grid ends on process', mpi_process%rank
#endif

  end function Id4_to_coords_grid_3d

  !----------------------------------------------------------------------------------------------------------------------------------
  pure function Id8_to_coords_grid_3d(dims, id) result (coords)

    integer(kind=4), dimension(3) :: coords
    integer(kind=8), intent(in) :: id
    integer(kind=4), dimension(3), intent(in) :: dims

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Id_to_coords_grid begins on process', mpi_process%rank
#endif

    coords(1) = int((id-1) / (dims(3)*dims(2)),kind=4)
    coords(2) = int((id-1 - coords(1)*dims(2)*dims(3)) / dims(3),kind=4)
    coords(3) = int((id-1 - coords(1)*dims(2)*dims(3) - coords(2)*dims(3)),kind=4)

#ifdef DEBUG2
    write(ERROR_UNIT,*) 'Id_to_coords_grid ends on process', mpi_process%rank
#endif

  end function Id8_to_coords_grid_3d

  !----------------------------------------------------------------------------------------------------------------------------------
  pure function Position_to_id_grid_3d(dims,lengths,position) result(id)

    use constants_m, only : DP

    real(DP), dimension(3), intent(in) :: position
    integer, dimension(3), intent(in) :: dims
    real(DP), dimension(3), intent(in) :: lengths
    integer :: id
    integer, dimension(3) :: coords
    integer :: idim

    do idim = 1, 3
       coords(idim) = int(position(idim) / lengths(idim), kind=4)
    end do

    id = 1 + coords(3) + coords(2)*dims(3) + coords(1)*dims(3)*dims(2)

  end function Position_to_id_grid_3d


end module index_m

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2015 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Common MPI related variables, type declarations and initialization procedures.
!! @brief
!!
!! @author Fabrice Roy
! ======================================================================

!> Common MPI related variables, type declarations and initialization procedures.
module modmpicommons

  use modconstant, only : err_msg_len,&
       type_parameter_conecreator_grav, &
       type_parameter_conecreator_part,&
       type_parameter_pfof_cone,&
       type_parameter_pfof_snap

  use type_info_ramses_mod, only : type_info_cone_part, &
       type_info_cone_grav,&
       type_info_ramses

  
  use mpi

  implicit none

  private

  public :: procid, &
       procnb, &
       type_info_communicator, &
       type_info_process, &
       mpi_type_info_ramses, &
       mpi_type_info_cone_part, &
       mpi_type_info_cone_grav, &
       mpi_type_parameter_pfof_snap, &
       mpi_type_parameter_pfof_cone, &
       mpi_type_parameter_cone_part, &
       mpi_type_parameter_cone_grav, &
       Create_mpi_type_info_cone_part, &
       Create_mpi_type_info_cone_grav, &
       Create_mpi_type_info_ramses, &
       Create_mpi_type_param_pfof_snap, &
       Create_mpi_type_param_pfof_cone, &
       Create_mpi_type_param_cone_part, &
       Create_mpi_type_param_cone_grav, &
       Emergencystop

  type :: type_info_communicator
     integer(kind=4) :: name
     integer(kind=4) :: pid
     integer(kind=4) :: size
     integer(kind=4) :: color
     integer(kind=4), dimension(3) :: dims
     logical(kind=4), dimension(3) :: periods
     integer(kind=4), dimension(3) :: coords
     integer(kind=4), dimension(6) :: neighbours
  end type type_info_communicator

  type :: type_info_process
     type(type_info_communicator) :: global_comm
     type(type_info_communicator) :: read_comm
     type(type_info_communicator) :: write_comm
  end type type_info_process

  integer(kind=4) :: procid !< process id in the global communicator
  integer(kind=4) :: procnb !< process number in the global communicator
  !  Integer(kind=4) :: neighbours(6)  !< array containing the id of the neighbours of the local process in the MpiCube communicator

  integer(kind=4) :: mpi_type_info_ramses !< MPI type corresponding to Type_info_ramses
  integer(kind=4) :: mpi_type_info_cone_part !< MPI type corresponding to Type_info_cone_part
  integer(kind=4) :: mpi_type_info_cone_grav !< MPI type corresponding to Type_info_cone_grav
  integer(kind=4) :: mpi_type_parameter_pfof_snap !< MPI type corresponding to Type_parameter_pfof_snap
  integer(kind=4) :: mpi_type_parameter_pfof_cone !< MPI type corresponding to Type_parameter_pfof_cone
  integer(kind=4) :: mpi_type_parameter_cone_part !< MPI type corresponding to Type_parameter_conecreator_part
  integer(kind=4) :: mpi_type_parameter_cone_grav !< MPI type corresponding to Type_parameter_conecreator_grav

contains

  !=======================================================================
  !< Create MPI type corresponding to Type_info_cone_part
  subroutine Create_mpi_type_info_cone_part

    use iso_fortran_env, only : ERROR_UNIT

    type(type_info_cone_part) :: fooconepart
    integer(kind=4), allocatable, dimension(:) :: blocklen, type
    integer(kind=MPI_ADDRESS_KIND), allocatable, dimension(:) :: disp
    integer(kind=MPI_ADDRESS_KIND) :: base
    integer(kind=4) :: mpierr
    integer(kind=4), parameter :: typesize=39
    integer(kind=4) :: alloc_stat
    character(len=ERR_MSG_LEN) :: alloc_err_msg

    allocate(disp(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in disp allocation in create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(type(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in type allocation in create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(blocklen(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in blocklen allocation in create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if

    call Mpi_Get_Address(fooconepart%ncpu, disp(1), mpierr)
    call Mpi_Get_Address(fooconepart%nstride, disp(2), mpierr)
    call Mpi_Get_Address(fooconepart%nstep_coarse, disp(3), mpierr)
    call Mpi_Get_Address(fooconepart%cone_id, disp(4), mpierr)
    call Mpi_Get_Address(fooconepart%nglobalfile, disp(5), mpierr)
    call Mpi_Get_Address(fooconepart%isfullsky, disp(6), mpierr)
    call Mpi_Get_Address(fooconepart%future, disp(7), mpierr)
    call Mpi_Get_Address(fooconepart%npart, disp(8), mpierr)
    call Mpi_Get_Address(fooconepart%aexp, disp(9), mpierr)
    call Mpi_Get_Address(fooconepart%observer_x, disp(10), mpierr)
    call Mpi_Get_Address(fooconepart%observer_y, disp(11), mpierr)
    call Mpi_Get_Address(fooconepart%observer_z, disp(12), mpierr)
    call Mpi_Get_Address(fooconepart%observer_rds, disp(13), mpierr)
    call Mpi_Get_Address(fooconepart%cone_zlim, disp(14), mpierr)
    call Mpi_Get_Address(fooconepart%amax, disp(15), mpierr)
    call Mpi_Get_Address(fooconepart%amin, disp(16), mpierr)
    call Mpi_Get_Address(fooconepart%zmax, disp(17), mpierr)
    call Mpi_Get_Address(fooconepart%zmin, disp(18), mpierr)
    call Mpi_Get_Address(fooconepart%dmax, disp(19), mpierr)
    call Mpi_Get_Address(fooconepart%dmin, disp(20), mpierr)
    call Mpi_Get_Address(fooconepart%dtol, disp(21), mpierr)
    call Mpi_Get_Address(fooconepart%thetay, disp(22), mpierr)
    call Mpi_Get_Address(fooconepart%thetaz, disp(23), mpierr)
    call Mpi_Get_Address(fooconepart%theta, disp(24), mpierr)
    call Mpi_Get_Address(fooconepart%phi, disp(25), mpierr)
    call Mpi_Get_Address(fooconepart%aendconem2, disp(26), mpierr)
    call Mpi_Get_Address(fooconepart%aendconem1, disp(27), mpierr)
    call Mpi_Get_Address(fooconepart%aendcone, disp(28), mpierr)
    call Mpi_Get_Address(fooconepart%aexpold, disp(29), mpierr)
    call Mpi_Get_Address(fooconepart%zendconem2, disp(30), mpierr)
    call Mpi_Get_Address(fooconepart%zendconem1, disp(31), mpierr)
    call Mpi_Get_Address(fooconepart%zendcone, disp(32), mpierr)
    call Mpi_Get_Address(fooconepart%zexpold, disp(33), mpierr)
    call Mpi_Get_Address(fooconepart%zexp, disp(34), mpierr)
    call Mpi_Get_Address(fooconepart%dendconem2, disp(35), mpierr)
    call Mpi_Get_Address(fooconepart%dendconem1, disp(36), mpierr)
    call Mpi_Get_Address(fooconepart%dendcone, disp(37), mpierr)
    call Mpi_Get_Address(fooconepart%dexpold, disp(38), mpierr)
    call Mpi_Get_Address(fooconepart%dexp, disp(39), mpierr)

    base = disp(1)
    disp(:) = disp(:) - base
    blocklen(:) = 1
    type(1:7) = MPI_INTEGER
    type(8:8) = MPI_INTEGER8
    type(9:39) = MPI_DOUBLE_PRECISION

    call Mpi_Type_Create_Struct(typesize, blocklen, disp, type, Mpi_Type_info_cone_part, mpierr)
    call Mpi_Type_Commit(Mpi_Type_info_cone_part, mpierr)

    deallocate(disp, type, blocklen)

end subroutine Create_mpi_type_info_cone_part


  !=======================================================================
  !< Create MPI type corresponding to Type_info_cone_grav
  subroutine Create_mpi_type_info_cone_grav

    use iso_fortran_env, only : ERROR_UNIT

    type(Type_info_cone_grav) :: fooconegrav
    integer(kind=4), allocatable, dimension(:) :: blocklen, type
    integer(kind=MPI_ADDRESS_KIND), allocatable, dimension(:) :: disp
    integer(kind=MPI_ADDRESS_KIND) :: base
    integer(kind=4) :: mpierr
    integer(kind=4), parameter :: typesize=37
    integer(kind=4) :: alloc_stat
    character(len=ERR_MSG_LEN) :: alloc_err_msg

    allocate(disp(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in disp allocation in create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(type(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in type allocation in create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(blocklen(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in blocklen allocation in create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if

    call Mpi_Get_Address(fooconegrav%ncpu, disp(1), mpierr)
    call Mpi_Get_Address(fooconegrav%nstride, disp(2), mpierr)
    call Mpi_Get_Address(fooconegrav%nstep_coarse, disp(3), mpierr)
    call Mpi_Get_Address(fooconegrav%cone_id, disp(4), mpierr)
    call Mpi_Get_Address(fooconegrav%nglobalfile, disp(5), mpierr)
    call Mpi_Get_Address(fooconegrav%isfullsky, disp(6), mpierr)
    call Mpi_Get_Address(fooconegrav%future, disp(7), mpierr)
    call Mpi_Get_Address(fooconegrav%nlevel, disp(8), mpierr)
    call Mpi_Get_Address(fooconegrav%levelmin, disp(9), mpierr)
    call Mpi_Get_Address(fooconegrav%levelmax, disp(10), mpierr)
    call Mpi_Get_Address(fooconegrav%nglobalcell, disp(11), mpierr)
    call Mpi_Get_Address(fooconegrav%aexp, disp(12), mpierr)
    call Mpi_Get_Address(fooconegrav%observer_x, disp(13), mpierr)
    call Mpi_Get_Address(fooconegrav%observer_y, disp(14), mpierr)
    call Mpi_Get_Address(fooconegrav%observer_z, disp(15), mpierr)
    call Mpi_Get_Address(fooconegrav%observer_rds, disp(16), mpierr)
    call Mpi_Get_Address(fooconegrav%cone_zlim, disp(17), mpierr)
    call Mpi_Get_Address(fooconegrav%amax, disp(18), mpierr)
    call Mpi_Get_Address(fooconegrav%amin, disp(19), mpierr)
    call Mpi_Get_Address(fooconegrav%zmax, disp(20), mpierr)
    call Mpi_Get_Address(fooconegrav%zmin, disp(21), mpierr)
    call Mpi_Get_Address(fooconegrav%dmax, disp(22), mpierr)
    call Mpi_Get_Address(fooconegrav%dmin, disp(23), mpierr)
    call Mpi_Get_Address(fooconegrav%dtol, disp(24), mpierr)
    call Mpi_Get_Address(fooconegrav%thetay, disp(25), mpierr)
    call Mpi_Get_Address(fooconegrav%thetaz, disp(26), mpierr)
    call Mpi_Get_Address(fooconegrav%theta, disp(27), mpierr)
    call Mpi_Get_Address(fooconegrav%phi, disp(28), mpierr)
    call Mpi_Get_Address(fooconegrav%aendconem2, disp(29), mpierr)
    call Mpi_Get_Address(fooconegrav%aendconem1, disp(30), mpierr)
    call Mpi_Get_Address(fooconegrav%aendcone, disp(31), mpierr)
    call Mpi_Get_Address(fooconegrav%zendconem2, disp(32), mpierr)
    call Mpi_Get_Address(fooconegrav%zendconem1, disp(33), mpierr)
    call Mpi_Get_Address(fooconegrav%zendcone, disp(34), mpierr)
    call Mpi_Get_Address(fooconegrav%dendconem2, disp(35), mpierr)
    call Mpi_Get_Address(fooconegrav%dendconem1, disp(36), mpierr)
    call Mpi_Get_Address(fooconegrav%dendcone, disp(37), mpierr)

    base = disp(1)
    disp(:) = disp(:) - base
    blocklen(:) = 1
    type(1:10) = MPI_INTEGER
    type(11:11) = MPI_INTEGER8
    type(12:37) = MPI_DOUBLE_PRECISION

    call Mpi_Type_Create_Struct(typesize, blocklen, disp, type, Mpi_Type_info_cone_grav, mpierr)
    call Mpi_Type_Commit(Mpi_Type_info_cone_grav, mpierr)

    deallocate(disp, type, blocklen)

end subroutine Create_mpi_type_info_cone_grav


  !=======================================================================
  !< Create MPI type corresponding to Type_info_ramses
  subroutine Create_mpi_type_info_ramses

    use iso_fortran_env, only : ERROR_UNIT

    type(Type_info_ramses) :: fooramses
    integer(kind=4), allocatable, dimension(:) :: blocklen, type
    integer(kind=MPI_ADDRESS_KIND), allocatable, dimension(:) :: disp
    integer(kind=MPI_ADDRESS_KIND) :: base
    integer(kind=4) :: mpierr
    integer(kind=4), parameter :: typesize=17
    integer(kind=4) :: alloc_stat
    character(len=ERR_MSG_LEN) :: alloc_err_msg

    allocate(disp(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in disp allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(type(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in type allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(blocklen(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in blocklen allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if

    call Mpi_Get_Address(fooramses%ncpu, disp(1), mpierr)
    call Mpi_Get_Address(fooramses%ndim, disp(2), mpierr)
    call Mpi_Get_Address(fooramses%levelmin, disp(3), mpierr)
    call Mpi_Get_Address(fooramses%levelmax, disp(4), mpierr)
    call Mpi_Get_Address(fooramses%ngridmax, disp(5), mpierr)
    call Mpi_Get_Address(fooramses%nstep_coarse, disp(6), mpierr)
    call Mpi_Get_Address(fooramses%boxlen, disp(7), mpierr)
    call Mpi_Get_Address(fooramses%time, disp(8), mpierr)
    call Mpi_Get_Address(fooramses%aexp, disp(9), mpierr)
    call Mpi_Get_Address(fooramses%h0, disp(10), mpierr)
    call Mpi_Get_Address(fooramses%omega_m, disp(11), mpierr)
    call Mpi_Get_Address(fooramses%omega_l, disp(12), mpierr)
    call Mpi_Get_Address(fooramses%omega_k, disp(13), mpierr)
    call Mpi_Get_Address(fooramses%omega_b, disp(14), mpierr)
    call Mpi_Get_Address(fooramses%unit_l, disp(15), mpierr)
    call Mpi_Get_Address(fooramses%unit_d, disp(16), mpierr)
    call Mpi_Get_Address(fooramses%unit_t, disp(17), mpierr)

    base = disp(1)
    disp(:) = disp(:) - base
    blocklen(:) = 1
    type(1:6) = MPI_INTEGER
    type(7:17) = MPI_DOUBLE_PRECISION

    call Mpi_Type_Create_Struct(typesize, blocklen, disp, type, Mpi_Type_info_ramses, mpierr)
    call Mpi_Type_Commit(Mpi_Type_info_ramses, mpierr)

    deallocate(disp, type, blocklen)

  end subroutine Create_mpi_type_info_ramses


  !=======================================================================
  !< Create MPI type corresponding to Type_parameter_pfof_snap
  subroutine Create_mpi_type_param_pfof_snap

    use iso_fortran_env, only : ERROR_UNIT

    type(Type_parameter_pfof_snap) :: foopfofsnap
    integer(kind=4), allocatable, dimension(:) :: blocklen, type
    integer(kind=MPI_ADDRESS_KIND), allocatable, dimension(:) :: disp
    integer(kind=MPI_ADDRESS_KIND) :: base
    integer(kind=4) :: mpierr
    integer(kind=4), parameter :: typesize=26
    integer(kind=4) :: alloc_stat
    character(len=ERR_MSG_LEN) :: alloc_err_msg

    allocate(disp(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in disp allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(type(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in type allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(blocklen(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in blocklen allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if

    call Mpi_Get_Address(foopfofsnap%input_path, disp(1), mpierr)
    call Mpi_Get_Address(foopfofsnap%part_input_file, disp(2), mpierr)
    call Mpi_Get_Address(foopfofsnap%info_input_file, disp(3), mpierr)
    call Mpi_Get_Address(foopfofsnap%simulation_name, disp(4), mpierr)
    call Mpi_Get_Address(foopfofsnap%mmin, disp(5), mpierr)
    call Mpi_Get_Address(foopfofsnap%mmax, disp(6), mpierr)
    call mpi_get_address(foopfofsnap%star, disp(7), mpierr)
    call mpi_get_address(foopfofsnap%metal, disp(8), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_skip_mass, disp(9), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_skip_star, disp(10), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_skip_metal, disp(11), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_read_potential, disp(12), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_read_gravitational_field, disp(13), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_unbinding, disp(14), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_subHalo, disp(15), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_timings, disp(16), mpierr)
    call Mpi_Get_Address(foopfofsnap%percolation_length, disp(17), mpierr)

    call Mpi_Get_Address(foopfofsnap%grpsize, disp(18), mpierr)
    call Mpi_Get_Address(foopfofsnap%gatherread_factor, disp(19), mpierr)
    call Mpi_Get_Address(foopfofsnap%snapshot, disp(20), mpierr)
    call Mpi_Get_Address(foopfofsnap%gatherwrite_factor, disp(21), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_read_from_cube, disp(22), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_fof, disp(23), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_write_cube, disp(24), mpierr)
    call Mpi_Get_Address(foopfofsnap%do_sort_cube, disp(25), mpierr)
    call Mpi_Get_Address(foopfofsnap%code_index, disp(26), mpierr)

    base = disp(1)
    disp(:) = disp(:) - base
    blocklen(1:4) = 200
    blocklen(5:25) = 1
    blocklen(26:26) = 3
    type(1:4) = MPI_CHARACTER
    type(5:6) = MPI_INTEGER
    type(7:16) = MPI_LOGICAL
    type(17:17) = MPI_REAL
    type(18:21) = MPI_INTEGER
    type(22:25) = MPI_LOGICAL
    type(26) = MPI_CHARACTER

    call Mpi_Type_Create_Struct(typesize, blocklen, disp, type, Mpi_Type_parameter_pfof_snap, mpierr)
    call Mpi_Type_Commit(Mpi_Type_parameter_pfof_snap, mpierr)

    deallocate(disp, type, blocklen)

  end subroutine Create_mpi_type_param_pfof_snap


  !=======================================================================
  !< Create MPI type corresponding to Type_parameter_pfof_cone
  subroutine Create_mpi_type_param_pfof_cone

    use iso_fortran_env, only : ERROR_UNIT

    type(Type_parameter_pfof_cone) :: foopfofcone
    integer(kind=4), allocatable, dimension(:) :: blocklen, type
    integer(kind=MPI_ADDRESS_KIND), allocatable, dimension(:) :: disp
    integer(kind=MPI_ADDRESS_KIND) :: base
    integer(kind=4) :: mpierr
    integer(kind=4), parameter :: typesize=20
    integer(kind=4) :: alloc_stat
    character(len=ERR_MSG_LEN) :: alloc_err_msg

    allocate(disp(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in disp allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(type(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in type allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(blocklen(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in blocklen allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if

    call Mpi_Get_Address(foopfofcone%input_path, disp(1), mpierr)
    call Mpi_Get_Address(foopfofcone%part_input_file, disp(2), mpierr)
    call Mpi_Get_Address(foopfofcone%simulation_name, disp(3), mpierr)

    call Mpi_Get_Address(foopfofcone%mmin, disp(4), mpierr)
    call Mpi_Get_Address(foopfofcone%mmax, disp(5), mpierr)

    call Mpi_Get_Address(foopfofcone%do_read_ramses_part_id, disp(6), mpierr)
    call mpi_get_address(foopfofcone%star, disp(7), mpierr)
    call mpi_get_address(foopfofcone%metal, disp(8), mpierr)
    call Mpi_Get_Address(foopfofcone%do_skip_mass, disp(9), mpierr)
    call Mpi_Get_Address(foopfofcone%do_skip_star, disp(10), mpierr)
    call Mpi_Get_Address(foopfofcone%do_skip_metal, disp(11), mpierr)
    call Mpi_Get_Address(foopfofcone%do_read_potential, disp(12), mpierr)
    call Mpi_Get_Address(foopfofcone%do_read_gravitational_field, disp(13), mpierr)
    call Mpi_Get_Address(foopfofcone%do_unbinding, disp(14), mpierr)
    call Mpi_Get_Address(foopfofcone%do_subHalo, disp(15), mpierr)
    call Mpi_Get_Address(foopfofcone%do_timings, disp(16), mpierr)
    call Mpi_Get_Address(foopfofcone%do_gather_halo, disp(17), mpierr)

    call Mpi_Get_Address(foopfofcone%percolation_length, disp(18), mpierr)

    call Mpi_Get_Address(foopfofcone%shell_first_id, disp(19), mpierr)
    call Mpi_Get_Address(foopfofcone%shell_last_id, disp(20), mpierr)

    base = disp(1)
    disp(:) = disp(:) - base
    blocklen(1:3) = 200
    blocklen(4:20) = 1
    type(1:3) = MPI_CHARACTER
    type(4:5) = MPI_INTEGER
    type(6:17) = MPI_LOGICAL
    type(18:18) = Mpi_Real
    type(19:20) = Mpi_Integer

    call Mpi_Type_Create_Struct(typesize, blocklen, disp, type, Mpi_Type_parameter_pfof_cone, mpierr)
    call Mpi_Type_Commit(Mpi_Type_parameter_pfof_cone, mpierr)

    deallocate(disp, type, blocklen)

  end subroutine Create_mpi_type_param_pfof_cone


  !=======================================================================
  !< Create MPI type corresponding to Type_parameter_conecreator_part
  subroutine Create_mpi_type_param_cone_part

    use iso_fortran_env, only : ERROR_UNIT

    type(Type_parameter_conecreator_part) :: foo
    integer(kind=4), allocatable, dimension(:) :: blocklen, type
    integer(kind=MPI_ADDRESS_KIND), allocatable, dimension(:) :: disp
    integer(kind=MPI_ADDRESS_KIND) :: base
    integer(kind=4) :: mpierr
    integer(kind=4), parameter :: typesize=18
    integer(kind=4) :: alloc_stat
    character(len=ERR_MSG_LEN) :: alloc_err_msg

    allocate(disp(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in disp allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(type(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in type allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(blocklen(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in blocklen allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if

    call Mpi_Get_Address(foo%input_path, disp(1), mpierr)
    call Mpi_Get_Address(foo%simulation_name, disp(2), mpierr)
    call Mpi_Get_Address(foo%cone_input_file, disp(3), mpierr)
    call Mpi_Get_Address(foo%info_cone_input_file, disp(4), mpierr)
    call Mpi_Get_Address(foo%info_ramses_input_file, disp(5), mpierr)
    call Mpi_Get_Address(foo%nfile, disp(6), mpierr)
    call Mpi_Get_Address(foo%first_file, disp(7), mpierr)
    call Mpi_Get_Address(foo%last_file, disp(8), mpierr)
    call Mpi_Get_Address(foo%cone_max_radius, disp(9), mpierr)
    call Mpi_Get_Address(foo%cube_size, disp(10), mpierr)
    call Mpi_Get_Address(foo%do_read_ramses_part_id, disp(11), mpierr)
    call mpi_get_address(foo%star, disp(12), mpierr)
    call mpi_get_address(foo%metal, disp(13), mpierr)
    call Mpi_Get_Address(foo%do_skip_mass, disp(14), mpierr)
    call Mpi_Get_Address(foo%do_skip_star, disp(15), mpierr)
    call Mpi_Get_Address(foo%do_skip_metal, disp(16), mpierr)
    call Mpi_Get_Address(foo%do_read_potential, disp(17), mpierr)
    call Mpi_Get_Address(foo%do_read_gravitational_field, disp(18), mpierr)

    base = disp(1)
    disp(:) = disp(:) - base
    blocklen(1:5) = 200
    blocklen(6:18) = 1
    type(1:5) = MPI_CHARACTER
    type(6:8) = MPI_INTEGER
    type(9:10) = Mpi_Double_Precision
    type(11:18) = Mpi_Logical

    call Mpi_Type_Create_Struct(typesize, blocklen, disp, type, Mpi_Type_parameter_cone_part, mpierr)
    call Mpi_Type_Commit(Mpi_Type_parameter_cone_part, mpierr)

    deallocate(disp, type, blocklen)

  end subroutine Create_mpi_type_param_cone_part


  !=======================================================================
  !< Create MPI type corresponding to Type_parameter_conecreator_grav
  subroutine Create_mpi_type_param_cone_grav

    use iso_fortran_env, only : ERROR_UNIT

    type(Type_parameter_conecreator_grav) :: foo
    integer(kind=4), allocatable, dimension(:) :: blocklen, type
    integer(kind=MPI_ADDRESS_KIND), allocatable, dimension(:) :: disp
    integer(kind=MPI_ADDRESS_KIND) :: base
    integer(kind=4) :: mpierr
    integer(kind=4), parameter :: typesize=12
    integer(kind=4) :: alloc_stat
    character(len=ERR_MSG_LEN) :: alloc_err_msg

    allocate(disp(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in disp allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(type(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in type allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if
    allocate(blocklen(typesize),stat=alloc_stat,errmsg=alloc_err_msg)
    if(alloc_stat /= 0) then
       write(ERROR_UNIT,'(A)') 'Error in blocklen allocation in Create_mpi_type_info_cone_part.'
       write(ERROR_UNIT,'(A)') alloc_err_msg
    end if

    call Mpi_Get_Address(foo%input_path, disp(1), mpierr)
    call Mpi_Get_Address(foo%simulation_name, disp(2), mpierr)
    call Mpi_Get_Address(foo%cone_input_file, disp(3), mpierr)
    call Mpi_Get_Address(foo%info_cone_input_file, disp(4), mpierr)
    call Mpi_Get_Address(foo%info_ramses_input_file, disp(5), mpierr)
    call Mpi_Get_Address(foo%nfile, disp(6), mpierr)
    call Mpi_Get_Address(foo%first_file, disp(7), mpierr)
    call Mpi_Get_Address(foo%last_file, disp(8), mpierr)
    call Mpi_Get_Address(foo%nlevel, disp(9), mpierr)
    call Mpi_Get_Address(foo%levelmin, disp(10), mpierr)
    call Mpi_Get_Address(foo%cone_max_radius, disp(11), mpierr)
    call Mpi_Get_Address(foo%cube_size, disp(12), mpierr)

    base = disp(1)
    disp(:) = disp(:) - base
    blocklen(1:5) = 200
    blocklen(6:12) = 1
    type(1:5) = MPI_CHARACTER
    type(6:10) = MPI_INTEGER
    type(11:12) = Mpi_Double_Precision

    call Mpi_Type_Create_Struct(typesize, blocklen, disp, type, Mpi_Type_parameter_cone_grav, mpierr)
    call Mpi_Type_Commit(Mpi_Type_parameter_cone_grav, mpierr)

    deallocate(disp, type, blocklen)

  end subroutine Create_mpi_type_param_cone_grav


  !=======================================================================
  !> The subroutine is used to abort the execution if something wrong is happening.
  subroutine EmergencyStop(message,errcode)

    use iso_fortran_env, only : ERROR_UNIT
    use modconstant, only : LOG_UNIT

    character(len=*), intent(in) :: message
    integer, intent(in) :: errcode

    integer(kind=4) :: mpierr

    write(*,1000) trim(message),procID

1000 format('*** ',A,' on process ',I5.5,'. ***')

    if(procID==0) close(LOG_UNIT)

    call Mpi_Abort(Mpi_Comm_World,errcode,mpierr)
    stop

  end subroutine EmergencyStop

end module modmpicommons

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Contains utils to manipulate characters
!! @brief
!! 
!! @author Fabrice Roy

!> Contains utils to manipulate characters
!------------------------------------------------------------------------------------------------------------------------------------

module char_utils_m

  implicit none

  private

  public :: Int_to_char5, &
       Int4_to_char8, &
       Int_to_char12, &
       string_t

  interface Int_to_char5
     module procedure Int4_to_char5
     module procedure Int8_to_char5
  end interface Int_to_char5

  interface Int_to_char12
     module procedure Int4_to_char12
     module procedure Int8_to_char12
  end interface Int_to_char12

  type string_t
     character(len=:), allocatable :: string
  end type string_t

contains
  !------------------------------------------------------------------------------------------------------------------------------------
  !> write an integer(4) in a character(5)
  pure function Int4_to_char5(index) result(string)

    character(len=5) :: string
    integer(kind=4), intent(in) :: index

    write(string(1:5), '(I5.5)' ) index

  end function Int4_to_char5

  !------------------------------------------------------------------------------------------------------------------------------------
  !> write an integer(8) in a character(5)
  pure function Int8_to_char5(index) result(string)

    character(len=5) :: string
    integer(kind=8), intent(in) :: index

    write(string(1:5), '(I5.5)' ) index

  end function Int8_to_char5

  !------------------------------------------------------------------------------------------------------------------------------------
  !> write an integer(4) in a character(8)
  pure function Int4_to_char8(index) result(string)

    character(len=8) :: string
    integer(kind=4), intent(in) :: index

    write(string(1:8), '(I8.8)' ) index

  end function Int4_to_char8

  !------------------------------------------------------------------------------------------------------------------------------------
  !> write an integer(4) in a character(12)
  pure function Int4_to_char12(index) result(string)

    character(len=12) :: string
    integer(kind=4), intent(in) :: index

    write(string(1:12), '(I12.12)' ) index

  end function Int4_to_char12

  !------------------------------------------------------------------------------------------------------------------------------------
  !> write an integer(8) in a character(12)
  pure function Int8_to_char12(index) result(string)

    character(len=12) :: string
    integer(kind=8), intent(in) :: index

    write(string(1:12), '(I12.12)' ) index

  end function Int8_to_char12

end module char_utils_m

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Constant parameters definition
!! @brief
!! 
!! @author Fabrice Roy
!! @author Vincent Bouillot

!> Constant parameters definition
module constants_m

  use iso_c_binding
  use mpi

  implicit none

  private
  public :: EPSILON_D, EPSILON, &
       FILENAME_LEN, &
       IDKIND, &
       LOG_UNIT, &
       MPI_DP, &
       MPI_IDKIND, &
       NAME_CONECREATOR_GRAV, &
       NAME_CONECREATOR_PART, &
       NAME_CONEMAPPER, &
       NAME_PFOF_CONE, &
       NAME_PFOF_SNAP, &
       NAME_PSOD_SNAP, &
       PR, &
       DP, &
       QDP, &
       GIT_VERSION, &
       INT_FMT, LONGINT_FMT, DOUBLE_FMT, CHAR_FMT

#include "gitversion.h"

  ! filename length
  integer, parameter :: FILENAME_LEN = 512

#ifdef LONGINT
  integer, parameter :: IDKIND = 8
  integer, parameter :: MPI_IDKIND = MPI_INTEGER8
#else
  integer, parameter :: IDKIND = 4
  integer, parameter :: MPI_IDKIND = MPI_INTEGER
#endif

  ! Output Units
  integer, parameter :: LOG_UNIT = 50   !< I/O unit for text log file

  ! Name of the codes to write as metadata in HDF5 files
  character(len=16), parameter :: NAME_CONECREATOR_GRAV = 'conecreator_grav'
  character(len=16), parameter :: NAME_CONECREATOR_PART = 'conecreator_part'
  character(len=16), parameter :: NAME_CONEMAPPER = 'conemapper'
  character(len=16), parameter :: NAME_PFOF_CONE = 'pfof_cone'
  character(len=16), parameter :: NAME_PFOF_SNAP = 'pfof_snap'
  character(len=16), parameter :: NAME_PSOD_SNAP = 'psod_snap'

#ifdef LONGREAL
  integer, parameter :: PR=8   !< Precision for real arrays read from Ramses simulations (position/velocities)
#else
  integer, parameter :: PR=4   !< Precision for real arrays read from Ramses simulations (position/velocities)
#endif

  ! constant definitions from RAMSES
#ifndef NPRE
  integer, parameter :: DP=kind(1.0_4)
  integer, parameter :: MPI_DP=MPI_REAL
#else
#if NPRE==4
  integer, parameter :: DP=kind(1.0_4)
  integer, parameter :: MPI_DP=MPI_REAL
#else  
  integer, parameter :: DP=kind(1.0_8)
  integer, parameter :: MPI_DP=MPI_DOUBLE_PRECISION
#endif
#endif

#ifdef QUADHILBERT  
  integer, parameter :: QDP=kind(1.0_16)
#else
  integer, parameter :: QDP=kind(1.0_8)
#endif

  ! epsilon for double comparisons if doubles are > 0. and < 1. 
  real(kind=DP), parameter :: EPSILON_D = 1.0d-16
  ! epsilon for real comparisons if reals are > 0. and < 1. 
  real(kind=DP), parameter :: EPSILON = 1.0d-8


  ! GIT Revision
  character(len=64), parameter :: GIT_VERSION = GITVERSION

  ! FORMAT constants
  character(*), parameter :: INT_FMT='(a13,i11)'
  character(*), parameter :: LONGINT_FMT='(a13,i20)'
  character(*), parameter :: DOUBLE_FMT='(a13,e24.15)'
  character(*), parameter :: CHAR_FMT='(a14,a80)'

end module constants_m

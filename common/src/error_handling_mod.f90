!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Contains subroutines that handle errors
!! @brief
!! 
!! @author Fabrice Roy

!> Contains subroutines that handle errors
!------------------------------------------------------------------------------------------------------------------------------------

module error_handling_mod

  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  
  implicit none

  private

  public :: Algorithmic_error, Allocate_error, IO_error

contains
  !----------------------------------------------------------------------------------------------------------------------------------
  !> Abort in case of allocation error
  subroutine Allocate_error(variable, routine, error_msg, alloc_stat, process_id)

    use mpi

    character(*), intent(in) :: variable
    character(*), intent(in) :: routine
    character(*), intent(in) :: error_msg
    integer(kind=4), intent(in) :: alloc_stat
    integer(kind=4), intent(in) :: process_id

    integer(kind=4) :: mpierr
    
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Enter Allocate_error on process', process_id
#endif
    
    write(ERROR_UNIT,*) 'Allocate failed for '//variable//' in '//routine//' on process ',process_id
    write(ERROR_UNIT,*) 'Error message:'
    write(ERROR_UNIT,*) trim(error_msg)
    write(ERROR_UNIT,*) 'Calling mpi_abort on all processes'
    call mpi_abort(MPI_COMM_WORLD, alloc_stat, mpierr)
        
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Exit Allocate_error on process', process_id
#endif
    
  end subroutine Allocate_error

  !----------------------------------------------------------------------------------------------------------------------------------
  !> Abort in case of algorithmic error
  subroutine Algorithmic_error(routine, error_msg, error_code, process_id)

    use mpi

    character(*), intent(in) :: routine
    character(*), intent(in) :: error_msg
    integer(kind=4), intent(in) :: error_code
    integer(kind=4), intent(in) :: process_id

    integer(kind=4) :: mpierr
    
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Enter Algorithmic_error on process', process_id
#endif
    
    write(ERROR_UNIT,*) 'Algorithmic error in '//routine//' on process ',process_id
    write(ERROR_UNIT,*) 'Error message:'
    write(ERROR_UNIT,*) trim(error_msg)
    write(ERROR_UNIT,*) 'Calling mpi_abort on all processes'
    call mpi_abort(MPI_COMM_WORLD, error_code, mpierr)
        
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Exit Algorithmic_error on process', process_id
#endif
    
  end subroutine Algorithmic_error

  !----------------------------------------------------------------------------------------------------------------------------------
  !> Abort in case of io error
  subroutine IO_error(routine, error_msg, error_code, process_id)

    use mpi

    character(*), intent(in) :: routine
    character(*), intent(in) :: error_msg
    integer(kind=4), intent(in) :: error_code
    integer(kind=4), intent(in) :: process_id

    integer(kind=4) :: mpierr
    
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Enter IO_error on process', process_id
#endif
    
    write(ERROR_UNIT,*) 'IO error in '//routine//' on process ',process_id
    write(ERROR_UNIT,*) 'Error message:'
    write(ERROR_UNIT,*) trim(error_msg)
    write(ERROR_UNIT,*) 'Calling mpi_abort on all processes'
    call mpi_abort(MPI_COMM_WORLD, error_code, mpierr)
        
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Exit IO_error on process', process_id
#endif
    
  end subroutine IO_error

  
end module error_handling_mod

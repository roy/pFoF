!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Contains Ramses info type declaration and reading routine
!! @brief
!!
!! @author Fabrice Roy

!> Contains Ramses info type declaration and reading routine
!------------------------------------------------------------------------------------------------------------------------------------

module ramses_info_m

  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT

  implicit none

  private
  public :: ramses_info_t

  type :: ramses_info_t !< type for information read from ramses info file
     integer(kind=4) :: ncpu
     integer(kind=4) :: ndim          !< number of dimensions
     integer(kind=4) :: levelmin          !< minimum mesh refinement level
     integer(kind=4) :: levelmax
     integer(kind=4) :: ngridmax
     integer(kind=4) :: nstep_coarse
     real(kind=8) :: boxlen
     real(kind=8) :: time
     real(kind=8) :: aexp
     real(kind=8) :: h0
     real(kind=8) :: omega_m
     real(kind=8) :: omega_l
     real(kind=8) :: omega_k
     real(kind=8) :: omega_b
     real(kind=8) :: unit_l
     real(kind=8) :: unit_d
     real(kind=8) :: unit_t
     character(len=80) :: ordering
     real(kind=8), dimension(:), allocatable :: hilbert_bound_key
   contains
     procedure :: Print => Print_ramses_info
     procedure :: Read => Read_ramses_info
     procedure :: Read_hdf5 => Read_hdf5_ramses_info
     procedure :: Write_hdf5 => Write_hdf5_ramses_info
  end type ramses_info_t

contains

  !=======================================================================
  !> Print process
  subroutine Print_ramses_info(this, unit_number, mpi_process)

    use mpi_process_m, only : mpi_process_t

    class(ramses_info_t), intent(in) :: this
    integer, intent(in) :: unit_number
    type(mpi_process_t), intent(in) :: mpi_process

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'ramses_info%print begins on process', mpi_process%rank
#endif

    write(unit_number,'(a)') ' '
    write(unit_number,'(a)') '=========================================================== '
    write(unit_number,'(a)') 'RAMSES info: '
    write(unit_number,'(a,i0)') 'number of cpu: ', this%ncpu
    write(unit_number,'(a,i0)') 'number of dimensions: ', this%ndim
    write(unit_number,'(a,i0)') 'AMR min level: ', this%levelmin
    write(unit_number,'(a,i0)') 'AMR max level: ', this%levelmax
    write(unit_number,'(a,i0)') 'max ngrid: ', this%ngridmax
    write(unit_number,'(a,i0)') 'coarse nstep: ', this%nstep_coarse
    write(unit_number,'(a,e23.16)') 'length of simulation box: ', this%boxlen
    write(unit_number,'(a,e23.16)') 'time: ', this%time
    write(unit_number,'(a,e23.16)') 'aexp: ', this%aexp
    write(unit_number,'(a,e23.16)') 'h0: ', this%h0
    write(unit_number,'(a,e23.16)') 'omega_m: ', this%omega_m
    write(unit_number,'(a,e23.16)') 'omega_b: ', this%omega_l
    write(unit_number,'(a,e23.16)') 'omega_k: ', this%omega_k
    write(unit_number,'(a,e23.16)') 'omega_b: ', this%omega_b
    write(unit_number,'(a,e23.16)') 'unit_l: ', this%unit_l
    write(unit_number,'(a,e23.16)') 'unit_d: ', this%unit_d
    write(unit_number,'(a,e23.16)') 'unit_t: ', this%unit_t
    write(unit_number,'(a,a)') 'ordering of AMR cells: ', this%ordering
    if(trim(this%ordering) == 'hilbert') then
       if(allocated(this%hilbert_bound_key)) then
          write(unit_number,'(a,e23.16)') 'max value of hilbert bound key : ', maxval(this%hilbert_bound_key)
          write(unit_number,'(a,e23.16)') 'min value of hilbert bound key : ', minval(this%hilbert_bound_key)
       end if
    end if
    write(unit_number,'(a)') '=========================================================== '
    write(unit_number,'(a)') ' '

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'ramses_info%print ends on process', mpi_process%rank
#endif

  end subroutine Print_ramses_info

!------------------------------------------------------------------------------------------------------------------------------------
  !> Read Ramses info as metadata from a HDF5 object
  subroutine Read_hdf5_ramses_info(this, hdf5_id, mpi_process)

    use fortran_hdf5_constants_m, only : H5_STR_LEN
    use fortran_hdf5_manage_groups_m, only : Hdf5_close_group, Hdf5_open_group
    use fortran_hdf5_read_attribute_m, only : Hdf5_read_attr
    use hdf5, only : HID_T
    use mpi_process_m, only : mpi_process_t

    integer(HID_T), intent(in) :: hdf5_id
    type(mpi_process_t), intent(in) :: mpi_process
    class(ramses_info_t), intent(out) :: this   !< info structure

    character(len=H5_STR_LEN) :: groupname
    character(len=H5_STR_LEN) :: aname
    integer(kind=HID_T) :: gr_id
    integer(kind=HID_T) :: gr_ramses_id

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'ramses_info%Read_hdf5 begins on process ', mpi_process%rank
#endif

  ! Ramses Info Metadata
    groupname = 'ramses_info'
    call Hdf5_open_group(hdf5_id,groupname,gr_ramses_id)
    aname = 'ncpu'
    call Hdf5_read_attr(gr_ramses_id,aname,this%ncpu)
    aname = 'ndim'
    call Hdf5_read_attr(gr_ramses_id,aname,this%ndim)
    aname = 'levelmin'
    call Hdf5_read_attr(gr_ramses_id,aname,this%levelmin)
    aname = 'levelmax'
    call Hdf5_read_attr(gr_ramses_id,aname,this%levelmax)
    aname = 'ngridmax'
    call Hdf5_read_attr(gr_ramses_id,aname,this%ngridmax)
    aname = 'nstep_coarse'
    call Hdf5_read_attr(gr_ramses_id,aname,this%nstep_coarse)
    aname = 'boxlen'
    call Hdf5_read_attr(gr_ramses_id,aname,this%boxlen)
    aname = 'time'
    call Hdf5_read_attr(gr_ramses_id,aname,this%time)
    aname = 'aexp'
    call Hdf5_read_attr(gr_ramses_id,aname,this%aexp)
    aname = 'h0'
    call Hdf5_read_attr(gr_ramses_id,aname,this%h0)
    aname = 'omega_m'
    call Hdf5_read_attr(gr_ramses_id,aname,this%omega_m)
    aname = 'omega_l'
    call Hdf5_read_attr(gr_ramses_id,aname,this%omega_l)
    aname = 'omega_k'
    call Hdf5_read_attr(gr_ramses_id,aname,this%omega_k)
    aname = 'omega_b'
    call Hdf5_read_attr(gr_ramses_id,aname,this%omega_b)
    aname = 'unit_l'
    call Hdf5_read_attr(gr_ramses_id,aname,this%unit_l)
    aname = 'unit_d'
    call Hdf5_read_attr(gr_ramses_id,aname,this%unit_d)
    aname = 'unit_t'
    call Hdf5_read_attr(gr_ramses_id,aname,this%unit_t)
    call Hdf5_close_group(gr_ramses_id)
  
#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'ramses_info%Read_hdf5 ends on process ', mpi_process%rank
#endif

  end subroutine Read_hdf5_ramses_info

  !=======================================================================
  !> Read RAMSES snapshot info file
  subroutine Read_ramses_info(this, filename, mpi_process)

    use constants_m, only : CHAR_FMT, DOUBLE_FMT, INT_FMT
    use mpi_process_m, only : mpi_process_t
    use error_handling_m, only : Allocate_error, IO_error, ERR_MSG_LEN

    type(mpi_process_t), intent(in) :: mpi_process
    character(len=*), intent(in)  :: filename     !< info file name
    class(ramses_info_t), intent(out) :: this   !< info structure

    ! Local variable
    character(len=14) :: dumchar
    integer :: unit_info
    character(len=ERR_MSG_LEN) :: error_message, alloc_err_msg
    integer :: ioerr, alloc_stat
    integer :: dumint
    integer :: icpu

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'ramses_info%read begins on process', mpi_process%rank
#endif

    open(newunit=unit_info, file=filename, status='old', action='read', form='formatted', iostat = ioerr, iomsg = error_message)
    if( ioerr /= 0 ) then
       call IO_error('open '//filename, 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    end if

    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, this%ncpu
    if(ioerr/=0) call IO_error('read this%ncpu', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, this%ndim
    if(ioerr/=0) call IO_error('read this%ndim', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, this%levelmin
    if(ioerr/=0) call IO_error('read this%levelmin', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, this%levelmax
    if(ioerr/=0) call IO_error('read this%levelmax', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, this%ngridmax
    if(ioerr/=0) call IO_error('read this%ngridmax', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=INT_FMT,    iostat=ioerr, iomsg=error_message) dumchar, this%nstep_coarse
    if(ioerr/=0) call IO_error('read this%nstep_coarse', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, *,              iostat=ioerr, iomsg=error_message) ! skip empty line
    if(ioerr/=0) call IO_error('read empty line', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, this%boxlen
    if(ioerr/=0) call IO_error('read this%boxlen', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, this%time
    if(ioerr/=0) call IO_error('read this%time', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, this%aexp
    if(ioerr/=0) call IO_error('read this%aexp', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, this%h0
    if(ioerr/=0) call IO_error('read this%h0', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, this%omega_m
    if(ioerr/=0) call IO_error('read this%omega_m', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, this%omega_l
    if(ioerr/=0) call IO_error('read this%omega_l', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, this%omega_k
    if(ioerr/=0) call IO_error('read this%omega_k', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, this%omega_b
    if(ioerr/=0) call IO_error('read this%omega_b', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, this%unit_l
    if(ioerr/=0) call IO_error('read this%unit_l', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, this%unit_d
    if(ioerr/=0) call IO_error('read this%unit_d', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=DOUBLE_FMT, iostat=ioerr, iomsg=error_message) dumchar, this%unit_t
    if(ioerr/=0) call IO_error('read this%unit_t', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, *,              iostat=ioerr, iomsg=error_message) ! skip empty line
    if(ioerr/=0) call IO_error('read empty line', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    read(unit_info, fmt=CHAR_FMT,   iostat=ioerr, iomsg=error_message) dumchar, this%ordering
    if(ioerr/=0) call IO_error('read this%ordering', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
    if(trim(this%ordering).eq.'hilbert') then
       read(unit_info, *,              iostat=ioerr, iomsg=error_message) ! skip empty line
       if(ioerr/=0) call IO_error('read empty line', 'Read_ramses_info', error_message, ioerr, mpi_process%rank)
       allocate(this%hilbert_bound_key(0:this%ncpu), stat=alloc_stat, errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          call Allocate_error('this%hilbert_bound_key','Read_ramses_info', &
               alloc_err_msg, alloc_stat, mpi_process%rank)
       end if
       do icpu = 1, this%ncpu
          read(unit_info,fmt='(i8,1x,e23.15,1x,e23.15)', iostat=ioerr, iomsg=error_message) dumint, &
               this%hilbert_bound_key(icpu-1), this%hilbert_bound_key(icpu)
          if(ioerr/=0) call IO_error('read this%hilbert_bound_key', 'Read_ramses_info', error_message, &
               ioerr, mpi_process%rank)
       end do
    end if

    close(unit_info)

#ifdef DEBUG
    write(ERROR_UNIT,'(a,i0)') 'ramses_info%read ends on process', mpi_process%rank
#endif

  end subroutine Read_ramses_info

  !------------------------------------------------------------------------------------------------------------------------------------
  !> Write Ramses info as metadata in a HDF5 object
  subroutine Write_hdf5_ramses_info(this, hdf5_id, mpi_process)

    use fortran_hdf5_constants_m, only : H5_STR_LEN
    use fortran_hdf5_manage_groups_m, only : Hdf5_close_group, Hdf5_create_group
    use fortran_hdf5_write_attribute_m, only : Hdf5_write_attr
    use hdf5, only : HID_T
    use mpi_process_m, only : mpi_process_t

    integer(HID_T), intent(in) :: hdf5_id
    type(mpi_process_t), intent(in) :: mpi_process
    class(ramses_info_t), intent(in) :: this   !< info structure

    character(len=H5_STR_LEN) :: groupname
    character(len=H5_STR_LEN) :: aname
    integer(kind=HID_T) :: gr_id
    integer(kind=HID_T) :: gr_ramses_id

#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'ramses_info%Write begins on process ', mpi_process%rank
#endif

  ! Ramses Info Metadata
    groupname = 'ramses_info'
    call Hdf5_create_group(hdf5_id,groupname,gr_ramses_id)
    aname = 'ncpu'
    call Hdf5_write_attr(gr_ramses_id,aname,this%ncpu)
    aname = 'ndim'
    call Hdf5_write_attr(gr_ramses_id,aname,this%ndim)
    aname = 'levelmin'
    call Hdf5_write_attr(gr_ramses_id,aname,this%levelmin)
    aname = 'levelmax'
    call Hdf5_write_attr(gr_ramses_id,aname,this%levelmax)
    aname = 'ngridmax'
    call Hdf5_write_attr(gr_ramses_id,aname,this%ngridmax)
    aname = 'nstep_coarse'
    call Hdf5_write_attr(gr_ramses_id,aname,this%nstep_coarse)
    aname = 'boxlen'
    call Hdf5_write_attr(gr_ramses_id,aname,this%boxlen)
    aname = 'time'
    call Hdf5_write_attr(gr_ramses_id,aname,this%time)
    aname = 'aexp'
    call Hdf5_write_attr(gr_ramses_id,aname,this%aexp)
    aname = 'h0'
    call Hdf5_write_attr(gr_ramses_id,aname,this%h0)
    aname = 'omega_m'
    call Hdf5_write_attr(gr_ramses_id,aname,this%omega_m)
    aname = 'omega_l'
    call Hdf5_write_attr(gr_ramses_id,aname,this%omega_l)
    aname = 'omega_k'
    call Hdf5_write_attr(gr_ramses_id,aname,this%omega_k)
    aname = 'omega_b'
    call Hdf5_write_attr(gr_ramses_id,aname,this%omega_b)
    aname = 'unit_l'
    call Hdf5_write_attr(gr_ramses_id,aname,this%unit_l)
    aname = 'unit_d'
    call Hdf5_write_attr(gr_ramses_id,aname,this%unit_d)
    aname = 'unit_t'
    call Hdf5_write_attr(gr_ramses_id,aname,this%unit_t)
    call Hdf5_close_group(gr_ramses_id)
  
#ifdef DEBUG
    write(OUTPUT_UNIT,*) 'ramses_info%Write ends on process ', mpi_process%rank
#endif

  end subroutine Write_hdf5_ramses_info

end module ramses_info_m

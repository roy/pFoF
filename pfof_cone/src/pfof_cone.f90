!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! This file contains the main program of the light cone implementation of pFoF.
!!
!! @author Fabrice Roy
!! @author Vincent Bouillot


program pfof_cone

  use mpi
  use modmpicommons
  use fortran_hdf5_manage_interface_m
  use modio
  use modvariables
  use modmpicom
  use modfofpara

  implicit none

  integer(kind=4) :: mpierr

  ! Initialization of MPI
  call mpi_init(mpierr)
  call mpi_comm_size(Mpi_Comm_World, procNB, mpierr)
  call mpi_comm_rank(Mpi_Comm_World, procID, mpierr)

  if(procID==0) then
     call Title()
  end if

  ! Initialization of HDF5
  call hdf5_init()

  call Readparameters()

  call H5readmap()

  call H5readshellinfo()

  call H5readparticles()

  call Computeminmax()

  call Fofparacone()

  call Deallocall()

  ! Finalize HDF5
  call hdf5_finalize()

  ! Finalize MPI
  call mpi_finalize(mpierr)

end program pfof_cone

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!!This file contains the common variables used in pFoF_cone and a subroutine used to deallocate all allocatable arrays.
!!
!! @author Fabrice Roy
!! @author Vincent Bouillot

!> This module contains the common variables used in pFoF_cone and a subroutine used to deallocate all allocatable arrays.

module modvariables

  Use modvarcommons
  Use modconstant
  use type_info_ramses_mod, only : type_info_ramses, type_info_cone_part

  implicit None

  Integer(kind=4) :: shell_nb
  Integer(kind=4), dimension(:,:), allocatable :: indexcube
  Integer(kind=4), dimension(:), allocatable :: ictable
  Integer(kind=4) :: shellcubes_size
  Real(kind=8), dimension(6) :: boundaries 
  Integer(kind=4), dimension(:), allocatable :: cubepershell

  Real(kind=PR), dimension(3) :: partmin, partmax

  Integer(kind=4) :: ncube
  Integer(kind=4), dimension(:), allocatable :: npart_tab

  Type(Type_info_ramses) :: inforamses, inforamseslast
  Type(Type_info_cone_part) :: infocone, infoconelast
  Type(Type_parameter_pfof_cone) :: param
  Type(Type_common_metadata) :: common_meta

  Private
  Public :: shell_nb, &
       indexcube, &
       ictable, &
       shellcubes_size, &
       boundaries, &
       cubepershell, &
       ncube, &
       npart_tab, &
       partmin, &
       partmax, &
       inforamses, inforamseslast, &
       infocone, infoconelast, &
       param, &
       common_meta, &
       deallocall

Contains

  Subroutine deallocall()

    Implicit None

    If(Allocated(indexcube)) Deallocate(indexcube)
    If(Allocated(ictable)) Deallocate(ictable)
    If(Allocated(cubepershell)) Deallocate(cubepershell)
    If(Allocated(position)) Deallocate(position)
    If(Allocated(velocity)) Deallocate(velocity)
    If(Allocated(pfof_id)) Deallocate(pfof_id)
    If(Allocated(ramses_id)) Deallocate(ramses_id)
    If(Allocated(potential)) Deallocate(potential)
    If(Allocated(field)) Deallocate(field)

  End Subroutine deallocall

End Module modvariables

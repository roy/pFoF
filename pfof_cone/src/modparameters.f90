!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! This file contains the parameters read from pfof_cone.nml and the version number read from pfof.version. 
!!
!! @author Fabrice Roy
!! @author Vincent Bouillot

!> This module contains the parameters read from pfof_cone.nml and the version number read from pfof.version.

Module modparameters

  Use modconstant

  Implicit None

  Type(Type_parameter_pfof_cone) :: param
    

End Module modparameters

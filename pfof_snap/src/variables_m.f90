!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! This file contains some global variables declarations.

!> This module contains some global variables declarations.
module variables_m

  use modconstant, only : IDKIND, type_common_metadata, type_parameter_pfof_snap
  use type_info_ramses_mod, only : type_info_ramses
  implicit none

  integer(kind=IDKIND) :: ngrid       !< total number of grid points: ngrid = nres ** 3
  real   (kind=4)   :: xmin, xmax, ymin, ymax, zmin, zmax  !< min and max (x,y,z) for each process
  type(type_parameter_pfof_snap) :: param !< Input parameters for pfof_snap

  ! information read from Ramses info file:
  type(type_info_ramses) :: inforamses !< Information read from RAMSES info file.

  type(type_common_metadata) :: common_meta !< Metadata common to every HDF5 file created by pfof_snap.

  private

  public :: ngrid, &
       xmin, xmax, ymin, ymax, zmin, zmax, &
       param, &
       inforamses,&
       common_meta

end module variables_m

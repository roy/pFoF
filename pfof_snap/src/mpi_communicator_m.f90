!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! This file contains the subroutines and the variables used for MPI communications.

!> This module contains the subroutines and the variables used for MPI communications.
!> Authors: F. Roy, V. Bouillot
module mpi_communicator_m

  use mpi
  use modmpicommons, only : type_info_process,&
       procid,&
       procnb

  implicit none

  private

  public :: setcommunicators, &
       build_my_process_grid, &
       info_proc

  type(type_info_process) :: info_proc
  integer(kind=4) :: dims(3) !< dimensions of the processes grid
  integer(kind=4) :: mpicube !< cartesian global communicator
  integer(kind=4) :: mpisubcubewrite !< local communicator used for hdf5 parallel output
  integer(kind=4) :: mpisubcuberead  !< local communicator used for hdf5 parallel input
  integer(kind=4) :: cubecoord(3)  !< coordinates of the process in the mpicube communicator
  integer(kind=4) :: commcolorwrite !< parameter used to create the mpisubcubewrite communicator
  integer(kind=4) :: commcolorread  !< parameter used to create the mpisubcuberead communicator
  integer(kind=4) :: scprocidwrite !< process id in the mpisubcubewrite communicator
  integer(kind=4) :: scprocidread !< process id in the mpisubcuberead communicator
  integer(kind=4) :: scprocnbwrite !< process number in the mpisubcubewrite communicator
  integer(kind=4) :: scprocnbread !< process number in the mpisubcuberead communicator
  logical(kind=4) :: periods(3) !< periodicity of the global cartesian processes grid

contains

  !=======================================================================
  subroutine build_my_process_grid(cube_to_proc, param, proc_to_cube)

    use modconstant, only : IDKIND, type_parameter_pfof_snap
    use modindex, only : coord_to_id,&
         id_to_coord

    integer(kind=4),                intent(in), dimension(:) :: cube_to_proc
    type(type_parameter_pfof_snap), intent(in)               :: param
    integer(kind=4),                intent(in), dimension(:) :: proc_to_cube

    integer(kind=IDKIND) :: my_cube_id, dist_cube_id
    integer(kind=4), dimension(3) :: my_cube_coords
    integer(kind=4), dimension(3) :: dist_coords
    integer(kind=4) :: direction
    integer(kind=4) :: mpierr

    my_cube_id = proc_to_cube(procid+1)
    call id_to_coord(my_cube_id, my_cube_coords, dims)

    ! we are looking for the neighbours with respect to the cube coordinates
    do direction = 1, 3
       dist_coords = my_cube_coords
       dist_coords(direction) = mod(my_cube_coords(direction)-1+dims(direction),dims(direction))
       call coord_to_id(dist_coords, dist_cube_id, dims)
       info_proc%global_comm%neighbours(direction*2-1) = cube_to_proc(dist_cube_id)
       dist_coords(direction) = mod(my_cube_coords(direction)+1, dims(direction))
       call coord_to_id(dist_coords, dist_cube_id, dims)
       info_proc%global_comm%neighbours(direction*2) = cube_to_proc(dist_cube_id)
    end do
#ifdef debug
    print *,'neighbours with respect to the data cubes coordinates:',procid,&
         ' ; ', info_proc%global_comm%neighbours
#endif
    info_proc%global_comm%name = mpi_comm_world
    info_proc%global_comm%dims = dims
    info_proc%global_comm%pid = procid
    info_proc%global_comm%size = procnb
    info_proc%global_comm%periods = periods
    info_proc%global_comm%color = 0
    info_proc%global_comm%coords = my_cube_coords

    if(param%gatherwrite_factor > 1) then
#ifdef DEBUG
       if(procid==0) then
          print *, '*** creating specific communicator for gathered cube output ***'
       end if
#endif
       commcolorwrite = my_cube_coords(1)/param%gatherwrite_factor + &
            (my_cube_coords(2)/param%gatherwrite_factor) * (dims(1)/param%gatherwrite_factor) &
            + (my_cube_coords(3)/param%gatherwrite_factor) * (dims(1)/param%gatherwrite_factor)*&
            (dims(1)/param%gatherwrite_factor)
       call mpi_comm_split(MPI_COMM_WORLD, commcolorwrite, procid, mpisubcubewrite, mpierr)
       call mpi_comm_rank(mpisubcubewrite, scprocidwrite, mpierr)
       call mpi_comm_size(mpisubcubewrite, scprocnbwrite, mpierr)
       info_proc%write_comm%name = mpisubcubewrite
       info_proc%write_comm%dims = 0
       info_proc%write_comm%pid = scprocidwrite
       info_proc%write_comm%size = scprocnbwrite
       info_proc%write_comm%periods = .false.
       info_proc%write_comm%color = commcolorwrite
       info_proc%write_comm%coords = 0
       info_proc%write_comm%neighbours = 0
    else
       info_proc%write_comm%name = -1
       info_proc%write_comm%dims = -1
       info_proc%write_comm%pid = -1
       info_proc%write_comm%size = -1
       info_proc%write_comm%periods = .false.
       info_proc%write_comm%color = -1
       info_proc%write_comm%coords = -1
       info_proc%write_comm%neighbours = -1
    end if

    if(param%gatherread_factor > 1) then
#ifdef DEBUG
       if(procid==0) then
          print *, '*** creating specific communicator for gathered cube input ***'
       end if
#endif
       commcolorread = my_cube_coords(1)/param%gatherread_factor +  &
            (my_cube_coords(2)/param%gatherread_factor) * (dims(1)/param%gatherread_factor) &
            + (my_cube_coords(3)/param%gatherread_factor) * (dims(1)/param%gatherread_factor)* &
            (dims(1)/param%gatherread_factor)
       call mpi_comm_split(MPI_COMM_WORLD, commcolorread, procid, mpisubcuberead, mpierr)
       call mpi_comm_rank(mpisubcuberead, scprocidread, mpierr)
       call mpi_comm_size(mpisubcuberead, scprocnbread, mpierr)
       info_proc%read_comm%name = mpisubcuberead
       info_proc%read_comm%dims = 0
       info_proc%read_comm%pid = scprocidread
       info_proc%read_comm%size = scprocnbread
       info_proc%read_comm%periods = .false.
       info_proc%read_comm%color = commcolorread
       info_proc%read_comm%coords = 0
       info_proc%read_comm%neighbours = 0
    else
       info_proc%read_comm%name = -1
       info_proc%read_comm%dims = -1
       info_proc%read_comm%pid = -1
       info_proc%read_comm%size = -1
       info_proc%read_comm%periods = .false.
       info_proc%read_comm%color = -1
       info_proc%read_comm%coords = -1
       info_proc%read_comm%neighbours = -1
    end if

  end subroutine build_my_process_grid

  !=======================================================================
  !> This subroutine creates the global cartesian grid of processes.
  !! It also creates the communicators used for the parallel hdf5 i/o if they are needed.
  subroutine setcommunicators()

    use variables_m, only : param

    integer(kind=4) :: mpierr

    ! Creation of a grid of processes.
    ! The grid is a cube, there are procNB**(1/3) processes in each dimension.
    ! The grid is periodic in each dimension.
    dims = int(procNB**(1./3.))
    periods = .true.

    call Mpi_Cart_Create(Mpi_Comm_World,3,dims,periods,.true.,MPICube,mpierr)
    ! Process ID and number
    call Mpi_Comm_Rank  (MPICube, procID, mpierr)
    call Mpi_Cart_Coords(MPICube, procID, 3, CubeCoord, mpierr)
#ifdef DEBUG
    print *,'Process ',procID, ' ; Coords:',CubeCoord
#endif
    info_proc%global_comm%name = MPICube
    info_proc%global_comm%dims = dims
    info_proc%global_comm%pid = procid
    info_proc%global_comm%size = procnb
    info_proc%global_comm%periods = periods
    info_proc%global_comm%color = 0
    info_proc%global_comm%coords = CubeCoord

    ! Definition des neighbourss pour les 6 directions avec des conditions au bord periodiques
    ! Intialization of the 'neighboor processes id'
    ! Neighboor : 1 = backward
    !             2 = forward
    !             3 = left
    !             4 = right
    !             5 = down
    !             6 = up

    call Mpi_Cart_Shift(MpiCube,0,1,info_proc%global_comm%neighbours(1),&
         info_proc%global_comm%neighbours(2),mpierr)
    call Mpi_Cart_Shift(MpiCube,1,1,info_proc%global_comm%neighbours(3),&
         info_proc%global_comm%neighbours(4),mpierr)
    call Mpi_Cart_Shift(MpiCube,2,1,info_proc%global_comm%neighbours(5),&
         info_proc%global_comm%neighbours(6),mpierr)
    if(param%gatherwrite_factor > 1) then
#ifdef DEBUG
       if(procID==0) then
          print *, '*** Creating specific communicator for gathered cube output ***'
       end if
#endif
       commcolorWrite = CubeCoord(1)/param%gatherwrite_factor + &
            (CubeCoord(2)/param%gatherwrite_factor) * (dims(1)/param%gatherwrite_factor) &
            + (CubeCoord(3)/param%gatherwrite_factor) * (dims(1)/param%gatherwrite_factor)*&
            (dims(1)/param%gatherwrite_factor)
       call Mpi_Comm_Split(MpiCube, commcolorWrite, procID, MpiSubCubeWrite, mpierr)
       call Mpi_Comm_Rank(MpiSubCubeWrite, scprocIDWrite, mpierr)
       call Mpi_Comm_Size(MpiSubCubeWrite, scprocNBWrite, mpierr)
       info_proc%write_comm%name = MpiSubCubeWrite
       info_proc%write_comm%dims = 0
       info_proc%write_comm%pid = scprocIDWrite
       info_proc%write_comm%size = scprocNBWrite
       info_proc%write_comm%periods = .false.
       info_proc%write_comm%color = commcolorWrite
       info_proc%write_comm%coords = 0
       info_proc%write_comm%neighbours = 0
    else
       info_proc%write_comm%name = -1
       info_proc%write_comm%dims = -1
       info_proc%write_comm%pid = -1
       info_proc%write_comm%size = -1
       info_proc%write_comm%periods = .false.
       info_proc%write_comm%color = -1
       info_proc%write_comm%coords = -1
       info_proc%write_comm%neighbours = -1
    end if
    if(param%gatherread_factor > 1) then
#ifdef DEBUG
       if(procID==0) then
          print *, '*** Creating specific communicator for gathered cube input ***'
       end if
#endif
       commcolorRead = CubeCoord(1)/param%gatherread_factor +  &
            (CubeCoord(2)/param%gatherread_factor) * (dims(1)/param%gatherread_factor) &
            + (CubeCoord(3)/param%gatherread_factor) * (dims(1)/param%gatherread_factor)* &
            (dims(1)/param%gatherread_factor)
       call Mpi_Comm_Split(MpiCube, commcolorRead, procID, MpiSubCubeRead, mpierr)
       call Mpi_Comm_Rank(MpiSubCubeRead, scprocIDRead, mpierr)
       call Mpi_Comm_Size(MpiSubCubeRead, scprocNBRead, mpierr)
       info_proc%read_comm%name = MpiSubCubeRead
       info_proc%read_comm%dims = 0
       info_proc%read_comm%pid = scprocIDRead
       info_proc%read_comm%size = scprocNBRead
       info_proc%read_comm%periods = .false.
       info_proc%read_comm%color = commcolorRead
       info_proc%read_comm%coords = 0
       info_proc%read_comm%neighbours = 0
    else
       info_proc%read_comm%name = -1
       info_proc%read_comm%dims = -1
       info_proc%read_comm%pid = -1
       info_proc%read_comm%size = -1
       info_proc%read_comm%periods = .false.
       info_proc%read_comm%color = -1
       info_proc%read_comm%coords = -1
       info_proc%read_comm%neighbours = -1
    end if

  end subroutine setcommunicators

end module mpi_communicator_m
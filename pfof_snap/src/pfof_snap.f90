!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! This file contains the main program of the snapshot implementation of pFoF.
!!
!! @author Fabrice Roy
!! @author Vincent Bouillot

program Friend

  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use modconstant, only : LOG_UNIT, ERR_CODE_WRONG_PARAMETER
  use parallel_fof_m, only : Parallel_fof
  use fortran_hdf5_manage_interface_m, only : hdf5_finalize, hdf5_init 
  use io_utils_m, only : Print_screen_parameters, Init_parameters, The_end, Title, Write_timings
  use mpi_communicator_m, only : Setcommunicators
  use modmpicommons, only : Emergencystop, procid, procnb
  use modreadcube, only : Readcube, Selectreadcubetype
  use modtiming, only : tfof, tfofinit, tfofloc, timeint, tobs, tout, traccord, tread, treadfile
  use modvarcommons, only : nres
  use read_ramses_output_m, only : Read_ramses_output
  use read_ascii_output_m, only : Read_ascii_output
  use variables_m, only : ngrid, param
  use write_dm_cube_m, only : Selectwritedmcube, Writedmcube
  use write_star_cube_m, only : Selectwritestarcube, Writestarcube
  use mpi
  use select_dm_m, only : Select_dm_particles
  
  implicit none

  ! Local variables
  integer(kind=4) :: mpierr

  !----------------------------------------------------

  ! Initialization of MPI
  call mpi_init(mpierr)
  call mpi_comm_size(Mpi_Comm_World, procNB, mpierr)
  call mpi_comm_rank(Mpi_Comm_World, procID, mpierr)

  ! Print "title"
  if(procID==0) then
     call Title()
  end if

  ! Read parameters
  call Init_parameters()

  ! Print parameters on screen
  if(procID==0) then
     call Print_screen_parameters()
  end if

  ! Creates the different communicators used for the analysis and the // I/O
  call Setcommunicators()

  ! Initialize HDF5 interface
  call hdf5_init()


  ! Particles read from previously created cube files
  if( param%do_read_from_cube ) then
     call Selectreadcubetype()

     if(param%do_timings) then
        timeInt = mpi_wtime()
     end if
     call Readcube()
     if(param%do_timings) then
        tRead = mpi_wtime() - timeInt
        tReadfile = tRead
     end if
  else if (param%code_index.eq.'RA2' .or. param%code_index.eq.'RA3') then
     ! code_index determines the type of cosmological simulation to be analyzed:
     ! RA2 = RAMSES v2
     ! RA3 = RAMSES v3
     ! Read the output of the cosmological simulation
     call Read_ramses_output()
     if (param%star) then
        call Select_dm_particles()
     end if
  else if(param%code_index == 'ASC') then
    call Read_ascii_output()
  else
     if(procID==0) then
        write(ERROR_UNIT, *) 'Proc', procID, '  TYPE', param%code_index
        call Emergencystop('Wrong file type parameter. Possibilities are RA2 or RA3', ERR_CODE_WRONG_PARAMETER)
        stop
     end if
  end if

  ! Print on screen and in log file
  if(procID==0) then
     write(OUTPUT_UNIT ,*) 'nz = ', nres, 'ngrid = ', ngrid
     write(LOG_UNIT,*) 'nz = ', nres, 'ngrid = ', ngrid
  end if


  ! write cube of dark matter particles files if requested
  if(param%do_write_cube) then
     if(procID==0) write(OUTPUT_UNIT ,*) 'write dark matter particles distributed in a cartesian grid'
     if(procID==0) write(LOG_UNIT,*) 'write dark matter particles distributed in a cartesian grid'
     call Selectwritedmcube()
     call Writedmcube()
     if(param%star .and. .not.param%do_skip_star) then
        if(procID==0) write(OUTPUT_UNIT ,*) 'write stars distributed in a cartesian grid'
        if(procID==0) write(LOG_UNIT,*) 'write stars distributed in a cartesian grid'
        call Selectwritestarcube()
        call Writestarcube()
     end if
  end if


  if(procID==0) then
     write(OUTPUT_UNIT ,*)  ' '
     write(OUTPUT_UNIT ,*) 'Friends of Friends halo detection'
     write(OUTPUT_UNIT ,*) ' '
     write(LOG_UNIT,*) 'Friends of Friends halo detection'
  end if


  ! Parallele Friends of Friends if requested
  if(param%do_fof) then
     call Parallel_fof()
  else
     tFoF     = 0.0
     tFoFinit = 0.0
     tFoFloc  = 0.0
     tRaccord = 0.0
     tObs     = 0.0
     tOut     = 0.0
  end if


  ! Output of timings if requested
  if(param%do_timings .and. procID==0) then
     call Write_timings()
  end if

  if(procID==0) then
     call The_end()
  end if

  ! Close hdf5 interface.
  call hdf5_finalize()

  ! Finalize MPI
  call mpi_finalize(mpierr)

end program Friend

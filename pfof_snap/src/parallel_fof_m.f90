!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Edouard Audit, Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!!This file contains the serial FOF algorithm.

!> This module contains the serial FOF algorithm.
!>
!> Authors: E. Audit, F. Roy, V. Bouillot
module parallel_fof_m

  use iso_fortran_env, only : ERROR_UNIT
  use modindex, only : coord_to_id, id_to_coord

  implicit none

  private

  public :: Parallel_fof

contains

  !=======================================================================
  !> Parallel FOF subroutine
  !! Finds haloes on each process then apply the merging process for haloes that extend across several processes.
  subroutine Parallel_fof()
    ! Halo detection is performed locally in each subdomains (by each process).
    ! Structures which are cut by a border between two subdomains are gathered.
    ! The criterium to gather two parts of a halo is simple: if one particle from in a halo is seperated from a particle in
    ! another halo (on the other side of a border) by a distance smaller than the percolation parameter, then these two halos
    ! are two parts of a single halo.

    use compute_halo_properties_mod, only : compute_halo_com, compute_halo_radius
    use modmpicommons, only : emergencystop, procid
    use mpi_communicator_m, only : info_proc

    use modconstant, only : IDKIND, LOG_UNIT
    use modfofmpi, only : mergehaloes, border, nflagloc
#ifdef OPTI
    use modhaloopti, only : gatherhalolb, halocompos, halocomvel, haloid, halomass, halonb, halonb_all, &
         halopartfor, halopartid, halopartnb, halopartpos, halopartpot, halopartvel, haloradius, halosubhalonb
#else
    use modhalo, only : final_local_npart, gatherhaloes, halocompos, halocomvel, haloid, halomass, halonb, halonb_all, &
         halopartfor, halopartid, halopartmas, halopartnb, halopartpos, halopartpot, halopartvel, &
         haloradius, halosubhalonb, selecthaloes
    use modsort, only : heapsort
    use modvarcommons, only : ffield, fmass, fpfof_id, fposition, fpotential, fstructure_id, fvelocity
#endif
    use modwritehalo, only : h5writehalopart, mpih5writehalomass, mpih5writehalopart
    use modtiming, only : tfofloc, tfofinit, tgatherhalo, time0, timeint, tobs, touthalopart, toutmass, traccord, tselecthalo, tsort
    use modvarcommons, only : global_npart, local_npart, nres, pfof_id, position, structure_id
    use variables_m, only : inforamses, param, xmax, xmin, ymax, ymin, zmax, zmin
    use mpi
    
    ! Local variables
    integer :: allocstat
    integer(kind=4), dimension(:),allocatable :: adfirst,npcase
    integer(kind=4), dimension(:), allocatable :: lamas, pile, indl
    integer(kind=4) :: i

    integer(kind=IDKIND) :: index
    integer(kind=4) :: ipart
    integer(kind=4) :: ipermut, sttmp, ind, ipile, ipilepr
    integer(kind=4) :: ix, iy, iz, ic
    integer(kind=4) :: i1, i2, i3, j1, j2, j3
    integer(kind=4) :: nbborderloc, nbborder
    integer(kind=4) :: ngrid_fof,res_fof
    integer(kind=4) :: refine_fof
    integer(kind=4) :: signx(3), signy(3), signz(3)
    integer(kind=4) :: nsign

    integer(kind=IDKIND) :: namas      ! tmp structure ID

    logical :: periodic

    real(kind=4) :: r, d, size, size_fof, xx, yy, zz
    real(kind=4) :: dx, dy, dz
    real(kind=4) :: r2
    real(kind=4) :: rT

    integer(kind=4) :: progress
    integer(kind=4) :: nh
    integer(kind=4) :: mpierr

    real(kind=4) :: epsilon
#ifdef DEBUG
    integer(kind=4), dimension(3) :: tmp1, tmp2
#endif

    ! INITIALIZATION

    ! Initialize timings if necessary
    if(param%do_timings) then
       call Mpi_Barrier(Mpi_Comm_World,mpierr)
       time0 = Mpi_Wtime()
    end if

    nflagloc = 0
    nbborderloc = 0

    ! we used a grid fof FoF
    ! this grid is more refined than the coarse grid used for the dynamic evolution
    ! the refinement factor is set to 2
    refine_fof = 2
    res_fof = refine_fof * nres / info_proc%global_comm%dims(1)
    ngrid_fof = res_fof**3
    size = real(nres,kind=4)
    size_fof = real(res_fof*info_proc%global_comm%dims(1),kind=4)

    allocate(adfirst(ngrid_fof),stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for adfirst in Fofpara', allocstat)
    allocate(npcase(ngrid_fof),stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for npcase in Fofpara', allocstat)
    allocate(border(local_npart), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for adfirst in Fofpara', allocstat)

    allocate(lamas(local_npart), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for lamas in Fofpara', allocstat)
    allocate(indl(local_npart), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for indl in Fofpara', allocstat)
    allocate(pile(local_npart), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for pile in Fofpara', allocstat)
    allocate(structure_id(local_npart), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for structure_id in Fofpara', allocstat)

    call Init(res_fof,local_npart,ngrid_fof,position,pile,adfirst,npcase,xmin,ymin,zmin,size_fof)

    nsign = 2
    if( param%percolation_length*refine_fof >= 0.5 ) then
       nsign = 3
    end if

    if( param%percolation_length*refine_fof >= 1 ) then
       if(procID == 0) then
          print *, ' '
          print *, '*********************************************************************************'
          print *, '*** percolation_length * refine_fof should be < 1                                          ***'
          print *, '*** refine_fof is set and used in modfofpara.f90, percolation_length is an input parameter ***'
          print *, '*** Pfof is exiting                                                           ***'
          print *, '*********************************************************************************'
          print *, ' '
       end if
       call Mpi_Barrier(Mpi_Comm_World, mpierr)
       call Mpi_Abort(Mpi_Comm_World, 11, mpierr)
    end if

    r = param%percolation_length
    rT = r / size
    r2 = rT*rT

    do i = 1, local_npart
       lamas(i) = i
       indl(i)  = i
    end do

    if(local_npart> 0) then
       namas = pfof_id(1) !!!!!RY
    else
       namas=0
    endif
    ipermut  = 2

    border = 0_1

    if(param%do_timings) then
       call Mpi_Barrier(Mpi_Comm_World,mpierr)
       timeInt = Mpi_Wtime()
       tFoFinit = timeInt - time0
    end if

    if(procID==0) then
       print *,' '
       print *,'Beginning of local Friend of Friend...'
       print *,' '
    end if

    !-----------!
    ! Local Friends of Friends
    !-----------!

    signx(1) = 0
    signy(1) = 0
    signz(1) = 0
    signx(3) = 1
    signy(3) = 1
    signz(3) = 1

    epsilon = 1.e-8

    progress=local_npart/10

    particules : do i=1, local_npart

       if(procID==0 .and. mod(i,progress)==0) print *,'Progress:',i/progress*10,'%'
       ipart = lamas(i)

       if (abs(position(1,ipart)-1.0e0) < epsilon) position(1,ipart) = 0.e0
       if (abs(position(2,ipart)-1.0e0) < epsilon) position(2,ipart) = 0.e0
       if (abs(position(3,ipart)-1.0e0) < epsilon) position(3,ipart) = 0.e0


       if(abs(position(1,ipart)-xmin) <= rT ) then
          border(ipart) = border(ipart) + 4_1
          nflagloc(1) = nflagloc(1) + 1
       end if
       if(abs(position(1,ipart)-xmax) <= rT ) then
          border(ipart) = border(ipart) + 8_1
          nflagloc(2) = nflagloc(2) + 1
       end if
       if(abs(position(2,ipart)-ymin) <= rT ) then
          border(ipart) = border(ipart) + 1_1
          nflagloc(3) = nflagloc(3) + 1
       end if
       if(abs(position(2,ipart)-ymax) <= rT ) then
          border(ipart) = border(ipart) + 2_1
          nflagloc(4) = nflagloc(4) + 1
       end if
       if(abs(position(3,ipart)-zmin) <= rT ) then
          border(ipart) = border(ipart) + 16_1
          nflagloc(5) = nflagloc(5) + 1
       end if
       if(abs(position(3,ipart)-zmax) <= rT ) then
          border(ipart) = border(ipart) + 32_1
          nflagloc(6) = nflagloc(6) + 1
       end if
       if(border(ipart) /= 0_1 ) nbborderloc = nbborderloc + 1

       xx = position(1,ipart)
       yy = position(2,ipart)
       zz = position(3,ipart)

       ix = int((position(1,ipart)-xmin)*size_fof)
       iy = int((position(2,ipart)-ymin)*size_fof)
       iz = int((position(3,ipart)-zmin)*size_fof)

       signx(2) = -1
       signy(2) = -1
       signz(2) = -1
       if(nsign == 2) then
          if( (position(1,ipart)-xmin)*size_fof - ix > 0.5 ) then
             signx(2) = 1
          end if
          if( (position(2,ipart)-ymin)*size_fof - iy > 0.5 ) then
             signy(2) = 1
          end if
          if( (position(3,ipart)-zmin)*size_fof - iz > 0.5 ) then
             signz(2) = 1
          end if
       end if
       structure_id(ipart) = namas

       ! nsign depends on percolation_length * refine_fof: see above
       dimx : do i1 = 1, nsign
          j1 = ix + signx(i1)
          if( (j1>= res_fof) .or. (j1 < 0) ) cycle

          dimy : do i2 = 1, nsign
             j2 = iy + signy(i2)
             if( (j2>= res_fof) .or. (j2 < 0) ) cycle

             dimz : do i3 = 1, nsign
                j3 = iz + signz(i3)
                if( (j3>= res_fof) .or. (j3 < 0) ) cycle

                !index = 1 + j1 + j2*res_fof + j3*res_fof*res_fof
#ifdef DEBUG
                tmp1 = [j1,j2,j3]
                tmp2 = res_fof
                call coord_to_id(tmp1,index,tmp2)
#else
                call coord_to_id([j1,j2,j3],index,[res_fof,res_fof,res_fof])
#endif
                ic = 0

                nonvide : if (npcase(index) >= 1) then
                   ipile   = adfirst(index)
                   ipilepr = adfirst(index)
                   feuille : do ind = 1,npcase(index)
                      nonself : if (ipart /= ipile) then
                         dx = abs(xx-position(1,ipile))
                         dy = abs(yy-position(2,ipile))
                         dz = abs(zz-position(3,ipile))

                         d  = dx*dx+dy*dy+dz*dz

                         inhalo : if(d <= r2) then
                            sttmp              = lamas(ipermut)
                            lamas(ipermut)     = ipile
                            lamas(indl(ipile)) = sttmp
                            indl(sttmp)        = indl(ipile)
                            indl(ipile)        = ipermut
                            ipermut            = ipermut+1
                            isfirst : if(ipile == adfirst(index))then
                               adfirst(index) = pile(ipile)
                               ipilepr        = pile(ipile)
                            else
                               pile(ipilepr)=pile(ipile)
                            end if isfirst
                            ic = ic + 1
                         else
                            ipilepr = ipile
                         end if inhalo
                      else
                         isfirst2 : if(ipile == adfirst(index))then
                            adfirst(index) = pile(ipile)
                            ipilepr        = pile(ipile)
                         else
                            pile(ipilepr) = pile(ipile)
                         end if isfirst2
                         ic = ic + 1
                      end if nonself
                      ipile = pile(ipile)
                   end do feuille
                end if nonvide
                npcase(index) = npcase(index) - ic
             end do dimz
          end do dimy
       end do dimx

       !-----------------------------------------------!
       ! Si l'amas est fini, on passe a l'amas suivant !
       !-----------------------------------------------!

       if (ipermut == (i+1)) then

          ipermut  = ipermut + 1
          if(i<local_npart) namas = pfof_id(lamas(i+1))
       end if

    end do particules

    if(procID==0) print *,'Local FoF ends'

    if(param%do_timings) then
       call Mpi_Barrier(Mpi_Comm_World,mpierr)
       tFoFloc = Mpi_Wtime() - timeInt
       timeInt = Mpi_Wtime()
       if(procID==0) print *,'Process 0 spent ',tFoFloc,'s performing the local FoF halo detection.'
    end if

    deallocate(lamas,pile,indl)
    deallocate(adfirst,npcase)

    !-------------------!
    ! FOF local termine !
    !-------------------!

#ifdef DEBUG
    write(ERROR_UNIT,*) 'process:', procid, ' ; nb of part with structure_id=0 =',count(structure_id==0)
#endif

    call Mpi_Reduce(nbborderloc, nbborder, 1, Mpi_Integer,Mpi_Sum,0,Mpi_Comm_World,mpierr)


    if(procID==0) then
       print *, ' '
       print *, 'Merging procedure...'
       print *,' '
    end if

    periodic = .true.
    !! Call the merging procedure with the r**2 as an argument
    call mergehaloes(r2, info_proc)

    if(param%do_timings) then
       call Mpi_Barrier(Mpi_Comm_World,mpierr)
       tRaccord = Mpi_Wtime() - timeInt
       timeInt = Mpi_Wtime()
    end if

    if(procID==0) print *,'End of the merging procedure.'


    !---------------------!
    ! FIN du raccordement !
    !---------------------!

    if(procID==0) print *,'pFoF gathers particles across the processes according their halo ID'

#ifdef OPTI
    if(procID==0) print *,'Test optimized gather'
    call gatherhaloLB(param, info_proc%global_comm%name)
#else
    call gatherhaloes(info_proc%global_comm%name, param)
#endif

    if(param%do_timings) then
       call Mpi_Barrier(Mpi_Comm_World,mpierr)
       tGatherhalo = Mpi_Wtime() - timeInt
       if(procID==0) then
          print *,' '
          print *,'Process 0 spent ',tGatherhalo,' s gathering particles across the processes'
          print *,'according their halo ID.'
          print *,' '
       end if
       timeInt = Mpi_Wtime()
    end if

#ifndef OPTI
    if(param%do_skip_mass) Then 
       if(param%do_read_potential .and. param%do_read_gravitational_field) then
          call heapsort(final_local_npart,fstructure_id,fposition,fvelocity,ffield,fpotential,fpfof_id)
       else if(param%do_read_potential .and. .not. param%do_read_gravitational_field) then
          call heapsort(final_local_npart,fstructure_id,fposition,fvelocity,fpotential,fpfof_id)
       else if(param%do_read_gravitational_field .and. .not. param%do_read_potential) then
          call heapsort(final_local_npart,fstructure_id,fposition,fvelocity,ffield,fpfof_id)
       else
          call heapsort(final_local_npart,fstructure_id,fposition,fvelocity,fpfof_id)
       end if
    else
       if(param%do_read_potential .and. param%do_read_gravitational_field) then
          call heapsort(final_local_npart,fstructure_id,fposition,fvelocity,ffield,fmass,fpotential,fpfof_id)
       else if(param%do_read_potential .and. .not. param%do_read_gravitational_field) then
          call heapsort(final_local_npart,fstructure_id,fposition,fvelocity,fpotential,fmass,fpfof_id)
       else if(param%do_read_gravitational_field .and. .not. param%do_read_potential) then
          call heapsort(final_local_npart,fstructure_id,fposition,fvelocity,ffield,fmass,fpfof_id)
       else
          call heapsort(final_local_npart,fstructure_id,fposition,fvelocity,fmass,fpfof_id)
       end if
    end if
#endif

    if(param%do_timings) then
       call Mpi_Barrier(Mpi_Comm_World,mpierr)
       tSort = Mpi_Wtime() - timeInt
       if(procID==0) print *,'Process 0 spent ', tSort,' s sorting particles according their halo ID'
       timeInt = Mpi_Wtime()
    end if

#ifndef OPTI
    !! select haloes with mass > Mmin
    call selecthaloes(param)
    call mpi_allreduce(halonb, halonb_all, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, mpierr)
#endif

    if(param%do_timings) then
       call Mpi_Barrier(Mpi_Comm_World,mpierr)
       tSelectHalo = Mpi_Wtime() - timeInt
       if(procID==0) print *,'Process 0 spent ', tSelectHalo, &
            ' s selecting haloes with Mass > ',param%Mmin
       timeInt = Mpi_Wtime()
    end if

    if(procID==0) then
       print *, ' '
       print *,'pFoF writes halo particles files.'
       print *, ' '
    end if

    if(param%gatherwrite_factor <= 1) then
       if(param%do_skip_mass) then
          if(param%do_read_potential .and. param%do_read_gravitational_field) then
             call h5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos, &
                  halopartVel, halopartID, halopartFor=halopartFor, halopartPot=halopartPot, &
                  inforamses=inforamses)
          else if(param%do_read_potential .and. .not. param%do_read_gravitational_field ) then
             call h5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos, &
                  halopartVel, halopartID, halopartPot=halopartPot,inforamses=inforamses)
          else if(param%do_read_gravitational_field .and. .not. param%do_read_potential) then
             call h5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos, &
                  halopartVel, halopartID, halopartFor=halopartFor, inforamses=inforamses)
          else
             call h5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos, &
                  halopartVel, halopartID, inforamses=inforamses)
          end if
       else
          if(param%do_read_potential .and. param%do_read_gravitational_field) then
             call h5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos, &
                  halopartVel, halopartID, halopartmas=halopartmas, halopartFor=halopartFor, &
                  halopartPot=halopartPot, inforamses=inforamses)
          else if(param%do_read_potential .and. .not. param%do_read_gravitational_field ) then
             call h5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos, &
                  halopartVel, halopartID, halopartmas=halopartmas, halopartPot=halopartPot,&
                  inforamses=inforamses)
          else if(param%do_read_gravitational_field .and. .not. param%do_read_potential) then
             call h5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos, &
                  halopartVel, halopartID, halopartmas=halopartmas, halopartFor=halopartFor, &
                  inforamses=inforamses)
          else
             call h5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos, &
                  halopartVel, halopartID, halopartmas=halopartmas, inforamses=inforamses)
          end if          
       end if
    else
       if(param%do_skip_mass) then
          if(param%do_read_potential .and. param%do_read_gravitational_field) then
             call mpih5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos,&
                  halopartVel, halopartID, halopartFor=halopartFor, halopartPot=halopartPot, &
                  inforamses=inforamses)
          else if(param%do_read_potential .and. .not. param%do_read_gravitational_field ) then
             call mpih5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos,&
                  halopartVel, halopartID, halopartPot=halopartPot,inforamses=inforamses)
          else if(param%do_read_gravitational_field .and. .not. param%do_read_potential) then
             call mpih5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos,&
                  halopartVel, halopartID, halopartFor=halopartFor, inforamses=inforamses)
          else
             call mpih5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos,&
                  halopartVel, halopartID, inforamses=inforamses)
          end if
       else
          if(param%do_read_potential .and. param%do_read_gravitational_field) then
             call mpih5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos,&
                  halopartVel, halopartID, halopartmas=halopartmas, halopartFor=halopartFor, &
                  halopartPot=halopartPot, inforamses=inforamses)
          else if(param%do_read_potential .and. .not. param%do_read_gravitational_field ) then
             call mpih5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos,&
                  halopartVel, halopartID, halopartmas=halopartmas, halopartPot=halopartPot,&
                  inforamses=inforamses)
          else if(param%do_read_gravitational_field .and. .not. param%do_read_potential) then
             call mpih5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos,&
                  halopartVel, halopartID, halopartmas=halopartmas, halopartFor=halopartFor, &
                  inforamses=inforamses)
          else
             call mpih5writehalopart(info_proc, param, global_npart, haloNB, halopartNB, haloMass, haloID, halopartPos,&
                  halopartVel, halopartID, halopartmas=halopartmas, inforamses=inforamses)
          end if          
       end if
    end if

    if(param%do_timings) then
       call Mpi_Barrier(Mpi_Comm_World,mpierr)
       tOuthalopart = Mpi_Wtime() - timeInt
       if(procID==0) print *,'Process 0 spent ',tOuthalopart,' s for the output of halo particles.'
       timeInt = Mpi_Wtime()
    end if

    deallocate(halopartid)
    if(allocated(halopartpot)) deallocate(halopartpot)
    if(allocated(halopartfor)) deallocate(halopartfor)
    if(allocated(halopartmas)) deallocate(halopartmas)
    !! compute position and velocity of the center of mass for each halo

    if(procID==0) then
       print *,' '
       print *,'pFoF computes observables and writes them.'
       print *,' '
    end if


    if(halonb==0) then
       allocate(halocompos(3,1),stat=allocstat)
       if(allocstat > 0) call Emergencystop('Allocate failed for halocompos in Fofpara', allocstat)
       allocate(halocomvel(3,1),stat=allocstat)
       if(allocstat > 0) call Emergencystop('Allocate failed for halocomvel in Fofpara', allocstat)
       allocate(haloradius(1),stat=allocstat)
       if(allocstat > 0) call Emergencystop('Allocate failed for haloradius in Fofpara', allocstat)
    else
       allocate(halocompos(3,halonb),stat=allocstat)
       if(allocstat > 0) call Emergencystop('Allocate failed for halocompos in Fofpara', allocstat)
       allocate(halocomvel(3,halonb),stat=allocstat)
       if(allocstat > 0) call Emergencystop('Allocate failed for halocomvel in Fofpara', allocstat)
       allocate(haloradius(halonb),stat=allocstat)
       if(allocstat > 0) call Emergencystop('Allocate failed for haloradius in Fofpara', allocstat)
    end if

    halocompos = 0.0d0
    halocomvel = 0.0d0
    haloradius = 0.0d0

    call compute_halo_com(halocompos, halocomvel, halomass, halopartpos, halopartvel, periodic)
    call compute_halo_radius(haloradius, halocompos, halomass, halopartpos, periodic)

    deallocate(halopartpos, halopartvel)

    if(param%do_timings) then
       call Mpi_Barrier(Mpi_Comm_World,mpierr)
       tObs = Mpi_Wtime() - timeInt
       if(procID==0) print *,'Process 0 spent',tObs,' s for the computation of observables.'
       timeInt = Mpi_Wtime()
    end if

    nh = ubound(haloMass,1)
    !    Call mpih5writehalomass(Mpi_Comm_World, param, haloNB_all, haloNB, nh, haloMass, halocomPos, &
    !         halocomVel, haloID, haloRadius, haloSubHaloNB, inforamses=inforamses)

    call mpih5writehalomass(info_proc%global_comm%name, param, global_npart, haloNB_all, haloNB, nh, &
         haloMass, halocomPos, halocomVel, haloID, haloRadius, haloSubHaloNB, inforamses=inforamses)

    if(param%do_timings) then
       call Mpi_Barrier(Mpi_Comm_World,mpierr)
       tOutmass = Mpi_Wtime() - timeInt
       if(procID==0) print *,'Process 0 spent',tOutmass,' s to write the observables.'
    end if

    if(procID==0) then
       print *,' '
       print *,'******************************************************'
       print *,'End of pFoF!'
       print *,'Number of haloes with mass > ', param%Mmin,':',haloNB_all
       print *,'******************************************************'
       print *,' '

       write(Log_Unit,*) 'Number of haloes with mass > ', param%Mmin,':',haloNB_all

    end if

    deallocate(haloMass, halocomPos, halocomVel, haloID)

end subroutine Parallel_fof


  ! ======================================================================
  !> Initialize the stacks of pointers for local friends of friends.<br>
  !! The whole domain is divided into t^3 cubes, where t is the resolution of the simulation analized times the refine_fof factor.<br>
  !! Each process will implicitly considere only its subdomains because every particles treated by the process is located
  !! in the same subdomain.<br>
  !! This routine initializes one stack of "pointers" to the index of the particles located in each cube.
  subroutine Init(n,np,ngpp,x,pile,adfirst,npcase,xmin,ymin,zmin,t)

    use iso_fortran_env, only : ERROR_UNIT
    use modconstant, only : ERR_MSG_LEN, IDKIND
    use modmpicommons, only : emergencystop, procid

    ! Input parameters
    integer :: allocstat
    integer(kind=4), intent(in) :: n                   !< number of "cube" in each dimension in a subdomain, i.e. there are
    !! n^3 cubes in a subdomain
    integer(kind=4), intent(in) :: np                  !< number of particles in the subdomain
    integer(kind=4), intent(in) :: ngpp                !< number of "FOF" cubes in the subdomain
    real(kind=4), dimension(3,np), intent(in)   :: x   !< positions of the particles
    real(kind=4), intent(in) :: xmin,ymin,zmin         !< minimum x,y and z of the subdomain
    real(kind=4), intent(in) :: t                      !< resolution of the cosmological simulation times refine_fof factor

    ! Output parameters
    integer(kind=4), dimension(ngpp), intent(out) :: npcase    !< number of particles in each cube
    integer(kind=4), dimension(ngpp), intent(out) :: adfirst   !< index of the first particle to examine in the cube
    integer(kind=4), dimension(np), intent(out) :: pile        !< stack of indices

    ! Local variables
    integer(kind=4), dimension(:), allocatable :: adlast          ! index of last particle "added" to a cube
    integer(kind=4) :: ipart, ix, iy, iz         ! temporary indices
    integer(kind=IDKIND) :: icase
    real(kind=4) :: xx, yy, zz                         ! temporary position
    character(len=ERR_MSG_LEN) :: errormsg
    character(len=5) :: procid_char
#ifdef DEBUG
    integer(kind=4), dimension(3) :: tmp1, tmp2
#endif

    ! Initialization

    allocate(adlast(ngpp), stat=allocstat)
    if(allocstat > 0) call Emergencystop('Allocate failed for adlast in Init', allocstat)

    pile = 0
    adfirst = 0
    adlast = 0
    npcase = 0

    ! loop over the particles
    do ipart = 1,np
       ! positions scaled to the resolution, i.e. between 0 and 512 for a 512^3 simulation for example
       xx = (x(1,ipart)-xmin)*t
       yy = (x(2,ipart)-ymin)*t
       zz = (x(3,ipart)-zmin)*t
       ! coordinates of the cube containing the particle: this cube is always in the subdomain of the process
       ix = int(xx)
       iy = int(yy)
       iz = int(zz)

       ! there may be some rounding problem because we switch from double precision to simple precision positions
       ! so in case we have som positions = 1.0 we place the particle in the "last" cube
       if(ix == n) ix = ix-1
       if(iy == n) iy = iy-1
       if(iz == n) iz = iz-1

       ! index of the cube
       !icase = 1 + ix + iy*n + iz*n*n
#ifdef DEBUG
       tmp1 = [ix,iy,iz]
       tmp2 = n
       call coord_to_id(tmp1,icase,tmp2)
#else
       call coord_to_id([ix,iy,iz],icase,[n,n,n])
#endif
       ! is there a bug somewhere with the index?
       if(icase>ngpp .or. icase<=0) then
          write(procid_char(1:5),'(I5.5)') procid
          write(ERROR_UNIT,*) 'ipart = ', ipart, ' : icase = ', icase, ' ; ngpp = ', ngpp
          write(ERROR_UNIT,*) 'ix = ', ix, ' ; iy = ', iy, ' ; iz = ', iz
          write(ERROR_UNIT,*) 'xx = ', xx, ' ; yy = ', yy, ' ; zz = ', zz
          write(ERROR_UNIT,*) 'position = ',x(:,ipart)
          errormsg = 'Problem in initialization of friends of friends in init() in modfofpara on process '//procid_char
          call emergencyStop(errormsg,12)
       end if
       ! there is one more particle in this cube
       npcase(icase) = npcase(icase) + 1
       ! if this particle is the first added to the cube then
       if(adfirst(icase) == 0) then
          ! we set the adfirst array to the particle index
          adfirst(icase) = ipart
          ! and the adlast as well because it's the last particle added a this time
          adlast(icase)  = ipart
       else
          ! else the stack points from the previous particle to this one
          pile(adlast(icase)) = ipart
          ! and we set the adlast array
          adlast(icase) = ipart
       end if

    end do

    deallocate(adlast)

  end subroutine Init

end module parallel_fof_m

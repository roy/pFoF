!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Select dark matter particles from hydro simulations
!! @brief
!! 
!! @author Fabrice Roy

!> Select dark matter particles from hydro simulations
!------------------------------------------------------------------------------------------------------------------------------------

module select_dm_m

  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT

  implicit none

  private

  public :: Select_dm_particles

contains
  
  subroutine Select_dm_particles()

    use mpi
    use modconstant, only : IDKIND, MPI_IDKIND
    use modsort, only : heapsort
    use variables_m, only : param
    use modvarcommons, only : position, velocity, pfof_id, mass, birth_date, metallicity, field, potential, local_npart, &
         star_position, star_velocity, star_ramses_id, star_mass, star_birth_date, star_metallicity, star_field, &
         star_potential, global_nstar, local_nstar, global_npart
    use modmpicommons, only : procid

    integer(kind=4) :: ipart, local_npart_dm
    real(kind=4), parameter :: EPSILON=1.e-7
    integer(kind=IDKIND), allocatable, dimension(:) :: is_star
    real(kind=4), dimension(:,:), allocatable :: tempreal3
    integer(kind=IDKIND), allocatable, dimension(:) :: tempinteger
    real(kind=4), dimension(:), allocatable :: tempreal1
    integer :: mpierr
    integer :: requestdm, requeststar
    logical :: is_completed
    integer, dimension(MPI_STATUS_SIZE) :: statusdm, statusstar

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Enter Select_dm_particles on process ',procid
    write(ERROR_UNIT,*) 'min and max of birth_date:',minval(birth_date),maxval(birth_date)
#endif
    
    allocate(is_star(local_npart))

    local_npart_dm = 0
    local_nstar = 0
    do ipart = 1, local_npart
       if( (abs(birth_date(ipart)) < EPSILON) .and. (pfof_id(ipart) > 0) ) then
          local_npart_dm = local_npart_dm + 1
          is_star(ipart) = 0
       else
          local_nstar = local_nstar+1
          is_star(ipart) = 1
       end if
    end do

    call mpi_iallreduce(local_nstar, global_nstar, 1, MPI_IDKIND, MPI_SUM, MPI_COMM_WORLD, requeststar, mpierr)
    call mpi_iallreduce(local_npart_dm, global_npart, 1, MPI_IDKIND, MPI_SUM, MPI_COMM_WORLD, requestdm, mpierr)

    ! We sort the particles along their group id
    if ( .not. param%do_skip_mass ) then ! Mass
       if ( param%star .and. .not.param%do_skip_star ) then ! Mass + BD
          if( param%metal .and. .not.param%do_skip_metal) then ! Mass + BD + Metal
             if ( param%do_read_gravitational_field ) then ! Mass + BD + Metal + Field
                if( param%do_read_potential) then ! Mass + BD + Metal + Field + Potential
                   call heapsort(local_npart, is_star, position, velocity, field, potential, mass, metallicity, birth_date, pfof_id)
                else ! Mass + BD + Metal + Field
                   call heapsort(local_npart, is_star, position, velocity, field, mass, metallicity, birth_date, pfof_id)
                end if
             else ! Mass + BD + Metal
                if( param%do_read_potential) then ! Mass + BD + Metal + Potential
                   call heapsort(local_npart, is_star, position, velocity, potential, mass, metallicity, birth_date, pfof_id)
                else ! Mass + BD + Metal
                   call heapsort(local_npart, is_star, position, velocity, mass, metallicity, birth_date, pfof_id)
                end if
             end if
          else ! Mass + BD
             if ( param%do_read_gravitational_field ) then ! Mass + BD + Field
                if( param%do_read_potential) then ! Mass + BD + Field + Potential
                   call heapsort(local_npart, is_star, position, velocity, field, potential, mass, birth_date, pfof_id)
                else ! Mass + BD + Field
                   call heapsort(local_npart, is_star, position, velocity, field, mass, birth_date, pfof_id)
                end if
             else ! Mass + BD
                if( param%do_read_potential) then ! Mass + BD + Potential
                   call heapsort(local_npart, is_star, position, velocity, potential, mass, birth_date, pfof_id)
                else ! Mass + BD
                   call heapsort(local_npart, is_star, position, velocity, mass, birth_date, pfof_id)
                end if
             end if
          end if
       else ! Mass
          if ( param%do_read_gravitational_field ) then ! Mass + Field
             if( param%do_read_potential) then ! Mass + Field + Potential
                call heapsort(local_npart, is_star, position, velocity, field, potential, mass, pfof_id)
             else ! Mass + Field
                call heapsort(local_npart, is_star, position, velocity, field, mass, pfof_id)
             end if
          else ! Mass
             if( param%do_read_potential) then ! Mass + Potential
                call heapsort(local_npart, is_star, position, velocity, potential, mass, pfof_id)
             else ! Mass
                call heapsort(local_npart, is_star, position, velocity, mass, pfof_id)
             end if
          end if
       end if
    else ! No mass
       if ( param%star .and. .not.param%do_skip_star ) then ! BD
          if( param%metal .and. .not.param%do_skip_metal) then ! BD + Metal
             if ( param%do_read_gravitational_field ) then ! BD + Metal + Field
                if( param%do_read_potential) then ! BD + Metal + Field + Potential
                   call heapsort(local_npart, is_star, position, velocity, field, potential, metallicity, birth_date, pfof_id)
                else ! BD + Metal + Field
                   call heapsort(local_npart, is_star, position, velocity, field, metallicity, birth_date, pfof_id)
                end if
             else ! BD + Metal
                if( param%do_read_potential) then ! BD + Metal + Potential
                   call heapsort(local_npart, is_star, position, velocity, potential, metallicity, birth_date, pfof_id)
                else ! BD + Metal
                   call heapsort(local_npart, is_star, position, velocity, metallicity, birth_date, pfof_id)
                end if
             end if
          else ! BD
             if ( param%do_read_gravitational_field ) then ! BD + Field
                if( param%do_read_potential) then ! BD + Field + Potential
                   call heapsort(local_npart, is_star, position, velocity, field, potential, birth_date, pfof_id)
                else ! BD + Field
                   call heapsort(local_npart, is_star, position, velocity, field, birth_date, pfof_id)
                end if
             else ! BD
                if( param%do_read_potential) then ! BD + Potential
                   call heapsort(local_npart, is_star, position, velocity, potential, birth_date, pfof_id)
                else ! BD
                   call heapsort(local_npart, is_star, position, velocity, birth_date, pfof_id)
                end if
             end if
          end if
       else !
          if ( param%do_read_gravitational_field ) then ! Field
             if( param%do_read_potential) then ! Field + Potential
                call heapsort(local_npart, is_star, position, velocity, field, potential, pfof_id)
             else ! Field
                call heapsort(local_npart, is_star, position, velocity, field, pfof_id)
             end if
          else !
             if( param%do_read_potential) then ! Potential
                call heapsort(local_npart, is_star, position, velocity, potential, pfof_id)
             else
                call heapsort(local_npart, is_star, position, velocity, pfof_id)
             end if
          end if
       end if
    end if

    call mpi_test(requeststar,is_completed,statusstar,mpierr)
    call mpi_test(requestdm,is_completed,statusdm,mpierr)

#ifdef DEBUG
    write(ERROR_UNIT, *) 'number of dm particles:', local_npart_dm
#endif
    
    allocate(tempreal3(3,local_npart_dm))
    allocate(star_position(3,local_nstar))
    tempreal3(:,1:local_npart_dm) = position(:, 1:local_npart_dm)
    star_position(:,1:local_nstar) = position(:, local_npart_dm+1:local_npart)
#ifdef DEBUG
    write(ERROR_UNIT, *) '1st position before move_alloc:',position(:,1), tempreal3(:,1)
#endif
    deallocate(position)
    call move_alloc(tempreal3, position)
#ifdef DEBUG
    write(ERROR_UNIT, *) '1st position after move_alloc:',position(:,1)
#endif

    allocate(tempreal3(3,local_npart_dm))
    allocate(star_velocity(3,local_nstar))
    tempreal3 = velocity(:, 1:local_npart_dm)
    star_velocity(:,1:local_nstar) = velocity(:, local_npart_dm+1:local_npart)
    call move_alloc(tempreal3, velocity)

    allocate(tempinteger(local_npart_dm))
    allocate(star_ramses_id(local_nstar))
    tempinteger = pfof_id(1:local_npart_dm)
    star_ramses_id(1:local_nstar) = pfof_id(local_npart_dm+1:local_npart)
    call move_alloc(tempinteger, pfof_id)

    if ( .not. param%do_skip_mass ) then
       allocate(tempreal1(local_npart_dm))
       allocate(star_mass(local_nstar))
       tempreal1 = mass(1:local_npart_dm)
       star_mass(1:local_nstar) = mass(local_npart_dm+1:local_npart)
       call move_alloc(tempreal1, mass)
    end if
       
    if ( param%star .and. .not.param%do_skip_star ) then
       allocate(star_birth_date(local_nstar))
       star_birth_date(1:local_nstar) = birth_date(local_npart_dm+1:local_npart)
       deallocate(birth_date)
    end if
    
    if( param%metal .and. .not.param%do_skip_metal) then
       allocate(star_metallicity(local_nstar))
       star_metallicity(1:local_nstar) = metallicity(local_npart_dm+1:local_npart)
       deallocate(metallicity)
    end if

    if ( param%do_read_gravitational_field ) then
       allocate(tempreal3(3, local_npart_dm))
       allocate(star_field(3,local_nstar))
       tempreal3 = field(:,1:local_npart_dm)
       star_field(:,1:local_nstar) = field(:, local_npart_dm+1:local_npart)
       call move_alloc(tempreal3, field)
    end if

    if( param%do_read_potential) then 
       allocate(tempreal1(local_npart_dm))
       allocate(star_potential(local_nstar))
       tempreal1 = potential(1:local_npart_dm)
       star_potential(1:local_nstar) = potential(local_npart_dm+1:local_npart)
       call move_alloc(tempreal1, potential)
    end if

    local_npart = local_npart_dm
    call mpi_wait(requeststar,statusstar,mpierr)
    call mpi_wait(requestdm,statusdm,mpierr)

    if(procid==0) then
       write(OUTPUT_UNIT,*) 'global number of dark matter particles=',global_npart
       write(OUTPUT_UNIT,*) 'global number of stars=',global_nstar
    end if

#ifdef DEBUG
    write(ERROR_UNIT,*) 'Exit Select_dm_particles on process ',procid
#endif

  end subroutine Select_dm_particles
  
end module select_dm_m

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2018 Fabrice Roy 
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! This file contains subroutines used to write HDF5 star cubes.

!> This module contains subroutines used to write HDF5 star cubes.
!> Authors: F. Roy
module write_star_cube_m

  use iso_fortran_env, only : ERROR_UNIT
  use mpi
  use modvarcommons, only : birth_date, field, &
       star_position, star_velocity, star_ramses_id, star_mass, star_birth_date, star_metallicity, star_field, &
       star_potential, global_nstar, local_nstar

  use fortran_hdf5_constants_m, only : H5_FILENAME_LEN, H5_STR_LEN
  use fortran_hdf5_manage_groups_m, only : hdf5_close_group, hdf5_create_group, hdf5_open_group
  use fortran_hdf5_manage_files_m, only : hdf5_close_file, hdf5_close_mpi_file, hdf5_create_file, hdf5_create_mpi_file
  use fortran_hdf5_write_attribute_m
  use fortran_hdf5_write_data_m
  use fortran_hdf5_write_mpi_data_m
  use hdf5, only : HID_T
  use mpi_communicator_m, only : info_proc
  use variables_m, only : inforamses, param, xmax, xmin, ymax, ymin, zmax, zmin
  use modmpicommons, only : procid, procnb
  use modwritemeta, only : Write_meta_common, Write_meta_halofinder_parameter, Write_meta_info_ramses

  implicit none

  procedure(), pointer :: Writestarcube

  private
  public :: Selectwritestarcube, Writestarcube


contains

  !=======================================================================
  subroutine Selectwritestarcube()

    if(param%gatherwrite_factor==1) then
       if(param%do_sort_cube) then
          Writestarcube => H5writesortedstarcube
       else
          Writestarcube => H5writestarcube
       end if
    else
       if(param%do_sort_cube) then
          Writestarcube => Mpih5writesortedstarcube
       else
          Writestarcube => Mpih5writestarcube
       end if
    end if
    
  end subroutine Selectwritestarcube

  !=======================================================================
  !> This subroutine writes the position, velocity and id of each particle on the process in a hdf5 file.
  !! One file is written per MPI process
  subroutine H5writestarcube()

    character(len=H5_FILENAME_LEN) :: filecube
    character(len=5)  :: pid_char

    character(len=H5_STR_LEN) :: dsetname                           ! Dataset name
    character(len=H5_STR_LEN) :: aname                              ! Attribute name
    character(len=H5_STR_LEN) :: adata
    character(len=H5_STR_LEN) :: groupname
    character(len=H5_STR_LEN) :: codename
    character(len=H5_STR_LEN) :: part_type

    integer(HID_T) :: file_id                               ! File identifier
    integer(HID_T) :: gr_id                                 ! Group identifier

    real(kind=4), dimension(6) :: boundaries
    integer(kind=8) :: npart8
    logical(kind=4) :: islast
    integer(kind=4) :: constant_mass

#ifdef DEBUG
    print *,"Enter H5writestarcube on process ",procID
#endif

    write(pid_char(1:5),'(I5.5)') procID
    filecube = 'pfof_cube_snap_star_data_'//trim(param%simulation_name)//'_'//pid_char//'.h5'

    ! create the hdf5 file
    call hdf5_create_file(filecube, file_id)

    codename='pfof_snap'
    part_type = 'star'
    npart8=int(global_nstar,kind=8)
    constant_mass = 0
    call Write_meta_common(file_id, codename, npart8, constant_mass=constant_mass, part_type=part_type)
    call Write_meta_halofinder_parameter(file_id, param)
    islast = .false.
    call Write_meta_info_ramses(file_id, inforamses, islast)

    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id,groupname, gr_id)

    ! Write type as attribute
    aname = 'file_type'
    adata = 'cube'
    call hdf5_write_attr(gr_id, aname, adata)

    ! Write the number of particles in this file as an integer(kind=8) dataset
    dsetname = 'npart_file'
    npart8 = int(local_nstar, kind=8)
    call hdf5_write_data(gr_id, dsetname, npart8)

    
    ! Write the process ID as an attribute
    aname = 'procID'
    call hdf5_write_attr(gr_id, aname, procID)

    aname = 'nfile'
    call hdf5_write_attr(gr_id, aname, procNB)

    ! Write the boundaries of the cube as an attribute
    boundaries(1) = xmin
    boundaries(2) = xmax
    boundaries(3) = ymin
    boundaries(4) = ymax
    boundaries(5) = zmin
    boundaries(6) = zmax
    aname = 'boundaries'

    call hdf5_write_attr(gr_id, aname, 6, boundaries)

    call hdf5_close_group(gr_id)

    groupname = 'data'
    call hdf5_create_group(file_id,groupname,gr_id)

    ! Write the position of the particles
    dsetname='position_part'
    call hdf5_write_data(gr_id, dsetname, 3, local_nstar, star_position)
    ! Write the velocity of the particles
    dsetname='velocity_part'
    call hdf5_write_data(gr_id, dsetname, 3, local_nstar, star_velocity)
    if(.not.param%do_skip_mass) then
       dsetname = 'mass_part'
       call hdf5_write_data(gr_id, dsetname, local_nstar, star_mass)
    end if
    ! If we use potential, write potential of the particles
    if(param%do_read_gravitational_field) then
       dsetname = 'gravitational_field_part'
       call hdf5_write_data(gr_id, dsetname, 3, local_nstar, star_field)
    end if
    ! If we use potential, write potential of the particles
    if(param%do_read_potential) then
       dsetname = 'potential_part'
       call hdf5_write_data(gr_id, dsetname, local_nstar, star_potential)
    end if
    dsetname = 'ramses_identity_part'
    call hdf5_write_data(gr_id, dsetname, local_nstar, star_ramses_id)
    dsetname = 'birth_date_part'
    call hdf5_write_data(gr_id, dsetname, local_nstar, star_birth_date)
    if(param%metal .and. .not.param%do_skip_metal) then
       dsetname = 'metallicity_part'
       call hdf5_write_data(gr_id, dsetname, local_nstar, star_metallicity)
    end if
    
    call hdf5_close_group(gr_id)

    call hdf5_close_file(file_id)

#ifdef DEBUG
    print *,'Exit H5writestarcube on process ',procID
#endif

  end subroutine H5writestarcube


  !=======================================================================
  !> This subroutine writes the position, the velocity and the id of each particle
  !! on the process in a hdf5 file.
  !! The particles are gathered
  subroutine Mpih5writestarcube()

    character(len=H5_FILENAME_LEN) :: filecube
    character(len=5)  :: pid_char

    character(len=H5_STR_LEN) :: dsetname                           ! Dataset name
    character(len=H5_STR_LEN) :: aname                              ! Attribute name
    character(len=H5_STR_LEN) :: groupname
    character(len=H5_STR_LEN) :: adata
    character(len=H5_STR_LEN) :: codename
    character(len=H5_STR_LEN) :: part_type

    integer(HID_T) :: file_id                               ! File identifier
    integer(HID_T) :: gr_id                                 ! Group identifier

    integer(kind=4) :: procperfile
    integer(kind=4) :: npart
    integer(kind=4) :: nfile
    integer(kind=4), dimension(:), allocatable :: partnb_tab

    real(kind=4), dimension(6) :: boundaries
    real(kind=4), dimension(:,:), allocatable :: bound_tab
    integer(kind=8) :: npart8
    logical(kind=4) :: islast
    integer(kind=4) :: mpierr
    integer(kind=4) :: constant_mass
#ifdef DEBUG
    print *,"Enter Mpih5writestarcube on process ",procID
#endif

    ! number of processes writing in the same file
    procperfile = param%gatherwrite_factor**3

    allocate(partnb_tab(procperfile))
    allocate(bound_tab(6,procperfile))

    write(pid_char(1:5),'(I5.5)') info_proc%write_comm%color
    filecube = 'pfof_cube_snap_star_data_'//trim(param%simulation_name)//'_'//pid_char//'.h5'

    ! create the hdf5 file
    call hdf5_create_mpi_file(filecube, info_proc%write_comm%name, file_id)

    codename='pfof_snap'
    part_type='star'
    npart8=int(global_nstar,kind=8)
    constant_mass = 0
    call Write_meta_common(file_id, codename, npart8, constant_mass=constant_mass, part_type=part_type)
    call Write_meta_halofinder_parameter(file_id, param)
    islast = .false.
    call Write_meta_info_ramses(file_id, inforamses, islast)

    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id, groupname, gr_id)

    ! Offset and global dimensions have to be computed
    call Mpi_Allgather(local_nstar, 1, Mpi_Integer, partnb_tab, 1, Mpi_Integer, info_proc%write_comm%name, mpierr)

    aname='npart_cube_array'
    call hdf5_write_attr(gr_id, aname, procperfile, partnb_tab)

    ! Write type as attribute
    aname = 'file_type'
    adata = 'mpicube'
    call hdf5_write_attr(gr_id, aname, adata)

    aname = 'gatherwrite_factor'
    call hdf5_write_attr(gr_id, aname, param%gatherwrite_factor)

    npart = sum(partnb_tab)
    npart8 = int(npart, kind=8)
    dsetname = 'npart_file'
    call hdf5_write_data(gr_id, dsetname, npart8)

    aname = 'nfile'
    nfile = procNB / procperfile
    call hdf5_write_attr(gr_id, aname, nfile)

    aname = 'procID'
    call hdf5_write_attr(gr_id, aname, info_proc%write_comm%color)

    ! Write the boundaries of the cube as an attribute
    boundaries(1) = xmin
    boundaries(2) = xmax
    boundaries(3) = ymin
    boundaries(4) = ymax
    boundaries(5) = zmin
    boundaries(6) = zmax

    call Mpi_Allgather(boundaries,6,Mpi_Real, bound_tab, 6, Mpi_Real, info_proc%write_comm%name, mpierr)

    aname = 'boundaries_array'
    call hdf5_write_attr(gr_id, aname, 6, procperfile, bound_tab)

    boundaries(1) = minval(bound_tab(1,:))
    boundaries(2) = maxval(bound_tab(2,:))
    boundaries(3) = minval(bound_tab(3,:))
    boundaries(4) = maxval(bound_tab(4,:))
    boundaries(5) = minval(bound_tab(5,:))
    boundaries(6) = maxval(bound_tab(6,:))
    aname = 'boundaries'
    call hdf5_write_attr(gr_id, aname, 6, boundaries)

    call hdf5_close_group(gr_id)

    groupname = 'data'
    call hdf5_create_group(file_id, groupname, gr_id)

    dsetname = 'position_part'
    call hdf5_write_mpi_data(gr_id, dsetname, 3, local_nstar, star_position, info_proc%write_comm%name)

    dsetname = 'velocity_part'
    call hdf5_write_mpi_data(gr_id, dsetname, 3, local_nstar, star_velocity, info_proc%write_comm%name)

    if(.not.param%do_skip_mass) then
       dsetname = 'mass_part'
       call hdf5_write_mpi_data(gr_id, dsetname, local_nstar, star_mass, info_proc%write_comm%name)
    end if
    
    if(param%do_read_potential) then
       dsetname = 'potential_part'
       call hdf5_write_mpi_data(gr_id, dsetname, local_nstar, star_potential, info_proc%write_comm%name)
    end if

    if(param%do_read_gravitational_field) then
       dsetname = 'gravitational_field_part'
       call hdf5_write_mpi_data(gr_id, dsetname, 3, local_nstar, star_field, info_proc%write_comm%name)
    end if

    dsetname = 'ramses_identity_part'
    call hdf5_write_mpi_data(gr_id, dsetname, local_nstar, star_ramses_id, info_proc%write_comm%name)
    dsetname = 'birth_date_part'
    call hdf5_write_mpi_data(gr_id, dsetname, local_nstar, star_birth_date, info_proc%write_comm%name)
    if(param%metal .and. .not.param%do_skip_metal) then
       dsetname = 'metallicity_part'
       call hdf5_write_mpi_data(gr_id, dsetname, local_nstar, star_metallicity, info_proc%write_comm%name)
    end if

    
    ! Close the root group.
    call hdf5_close_group(gr_id)

    deallocate(partnb_tab, bound_tab)

    ! Close h5 file
    call hdf5_close_mpi_file(file_id)

#ifdef DEBUG
    print *,"Exit Mpih5writestarcube on process ",procID
#endif

  end subroutine Mpih5writestarcube


  !=======================================================================

  subroutine H5writesortedstarcube()

    use modconstant, only : IDKIND
    use modsort
    use modindex

    character(len=H5_FILENAME_LEN) :: filecube
    character(len=5)  :: pid_char
    character(len=8) :: charic

    character(len=H5_STR_LEN) :: dsetname                           ! Dataset name
    character(len=H5_STR_LEN) :: aname                              ! Attribute name
    character(len=H5_STR_LEN) :: adata
    character(len=H5_STR_LEN) :: groupname
    character(len=H5_STR_LEN) :: codename
    character(len=H5_STR_LEN) :: part_type

    integer(HID_T) :: file_id                               ! File identifier
    integer(HID_T) :: gr_id
    integer(HID_T) :: gr_data_id

    real(kind=4), dimension(6) :: boundaries

    integer(kind=4) :: ix, iy, iz, ip, deb, fin, nc, ncdim, deltam1
    integer(kind=IDKIND) :: ic
    integer(kind=4), dimension(:), allocatable :: nstarcube
    integer(kind=IDKIND), dimension(:), allocatable :: ictable
    logical(kind=4) :: fileexist, fileopened

    integer(kind=8) :: npart8
    logical(kind=4) :: islast
    integer(kind=4) :: constant_mass
    
#ifdef DEBUG
    integer(kind=4), dimension(3) :: tmp1, tmp2

    print *,"Enter H5writesortedstarcube on process ",procID
#endif

    ! each FoF cube is divided into nc=512=8x8x8 groups, with ncdim=8
    ncdim = 8
    nc = ncdim*ncdim*ncdim
    allocate(ictable(local_nstar))
    allocate(nstarcube(nc))
    nstarcube = 0
    ! Ramses coarse grid is composed of nres^3 cells
    ! on a process: nres^3 / procNB cells
    ! => there is (nres/(ncdim*dims(1))^3 coarse cells in each group
    ! the size of a group is nres/(ncdim*dims(1))*(1/nres) where 1/nres is the size of 1 Ramses coarse cell
    ! => delta = 1/(ncdim*dims(1)) => 1/delta = ncdim*dims(1)
    deltam1 = ncdim*info_proc%global_comm%dims(1)

#ifdef DEBUG
    print *,procID, 'xmin, ymin, zmin=',xmin, ymin, zmin
    print *,procID, 'xmax, ymax, zmax=',xmax, ymax, zmax
#endif

    ! We compute the "group" index of each particle and the number of particle in each group
    do ip=1, local_nstar
       ix = int((star_position(1,ip) - xmin)*deltam1)
       iy = int((star_position(2,ip) - ymin)*deltam1)
       iz = int((star_position(3,ip) - zmin)*deltam1)
       ! rounding issue
       if(ix==ncdim) ix=ncdim-1
       if(iy==ncdim) iy=ncdim-1
       if(iz==ncdim) iz=ncdim-1
#ifdef DEBUG
       tmp1 = (/ix,iy,iz/)
       tmp2 = (/ncdim,ncdim,ncdim/)
       call coord_to_id(tmp1,ic,tmp2)
#else
       call coord_to_id((/ix,iy,iz/),ic,(/ncdim,ncdim,ncdim/))
#endif
       ictable(ip) = ic
       nstarcube(ic) = nstarcube(ic) + 1
    end do

    ! We sort the particles along their group id
    if ( .not. param%do_skip_mass ) then ! Mass
       if ( param%star .and. .not.param%do_skip_star ) then ! Mass + BD
          if( param%metal .and. .not.param%do_skip_metal) then ! Mass + BD + Metal
             if ( param%do_read_gravitational_field ) then ! Mass + BD + Metal + Field
                if( param%do_read_potential) then ! Mass + BD + Metal + Field + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_potential, &
                        star_mass, star_metallicity, star_birth_date, star_ramses_id)
                else ! Mass + BD + Metal + Field
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field,&
                        star_mass, star_metallicity, star_birth_date, star_ramses_id)
                end if
             else ! Mass + BD + Metal
                if( param%do_read_potential) then ! Mass + BD + Metal + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, &
                        star_mass, star_metallicity, star_birth_date, star_ramses_id)
                else ! Mass + BD + Metal
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_mass, &
                        star_metallicity, star_birth_date, star_ramses_id)
                end if
             end if
          else ! Mass + BD
             if ( param%do_read_gravitational_field ) then ! Mass + BD + Field
                if( param%do_read_potential) then ! Mass + BD + Field + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_potential, &
                        star_mass, star_birth_date, star_ramses_id)
                else ! Mass + BD + Field
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_mass, &
                        star_birth_date, star_ramses_id)
                end if
             else ! Mass + BD
                if( param%do_read_potential) then ! Mass + BD + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, &
                        star_mass, star_birth_date, star_ramses_id)
                else ! Mass + BD
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_mass, &
                        star_birth_date, star_ramses_id)
                end if
             end if
          end if
       else ! Mass
          if ( param%do_read_gravitational_field ) then ! Mass + Field
             if( param%do_read_potential) then ! Mass + Field + Potential
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_potential,&
                     star_mass, star_ramses_id)
             else ! Mass + Field
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, &
                     star_mass, star_ramses_id)
             end if
          else ! Mass
             if( param%do_read_potential) then ! Mass + Potential
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, &
                     star_mass, star_ramses_id)
             else ! Mass
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_mass, &
                     star_ramses_id)
             end if
          end if
       end if
    else ! No mass
       if ( param%star .and. .not.param%do_skip_star ) then ! BD
          if( param%metal .and. .not.param%do_skip_metal) then ! BD + Metal
             if ( param%do_read_gravitational_field ) then ! BD + Metal + Field
                if( param%do_read_potential) then ! BD + Metal + Field + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_potential, &
                        star_metallicity, star_birth_date, star_ramses_id)
                else ! BD + Metal + Field
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_metallicity, &
                        star_birth_date, star_ramses_id)
                end if
             else ! BD + Metal
                if( param%do_read_potential) then ! BD + Metal + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, star_metallicity, &
                        star_birth_date, star_ramses_id)
                else ! BD + Metal
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_metallicity, &
                        star_birth_date, star_ramses_id)
                end if
             end if
          else ! BD
             if ( param%do_read_gravitational_field ) then ! BD + Field
                if( param%do_read_potential) then ! BD + Field + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, &
                        star_potential, star_birth_date, star_ramses_id)
                else ! BD + Field
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, &
                        star_birth_date, star_ramses_id)
                end if
             else ! BD
                if( param%do_read_potential) then ! BD + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, &
                        star_birth_date, star_ramses_id)
                else ! BD
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_birth_date, &
                        star_ramses_id)
                end if
             end if
          end if
       else !
          if ( param%do_read_gravitational_field ) then ! Field
             if( param%do_read_potential) then ! Field + Potential
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, &
                     star_potential, star_ramses_id)
             else ! Field
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, &
                     star_ramses_id)
             end if
          else !
             if( param%do_read_potential) then ! Potential
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, &
                     star_ramses_id)
             else
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_ramses_id)
             end if
          end if
       end if
    end if
    
    ! We open a sortedcube file and write the groups into it
    write(pid_char(1:5),'(I5.5)') procID
    filecube = 'pfof_cube_snap_star_data_'//trim(param%simulation_name)//'_'//pid_char//'.h5'

    inquire(File=filecube,exist=fileexist, opened=fileopened)
    if(fileopened) then
       print *,'Error on process ',procID,' : File ',filecube,' already opened'
    end if

    ! create the hdf5 file
    call hdf5_create_file(filecube, file_id)

    codename='pfof_snap'
    part_type='star'
    npart8 = int(global_nstar,kind=8)
    constant_mass = 0
    call Write_meta_common(file_id, codename, npart8, constant_mass=constant_mass, part_type=part_type)
    call Write_meta_halofinder_parameter(file_id, param)
    islast = .false.
    call Write_meta_info_ramses(file_id, inforamses, islast)

    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id, groupname, gr_id)

    ! Write type as attribute
    aname = 'file_type'
    adata = 'sortedcube'
    call hdf5_write_attr(gr_id, aname, adata)

    ! Write the number of particles as an attribute
    dsetname = 'npart_file'
    npart8 = int(local_nstar, kind=8)
    call hdf5_write_data(gr_id, dsetname, npart8)

    ! Write the process ID as an attribute
    aname = 'procID'
    call hdf5_write_attr(gr_id, aname, procID)

    aname = 'nfile'
    call hdf5_write_attr(gr_id, aname, procNB)

    ! Write the boundaries of the cube as an attribute
    boundaries(1) = xmin
    boundaries(2) = xmax
    boundaries(3) = ymin
    boundaries(4) = ymax
    boundaries(5) = zmin
    boundaries(6) = zmax
    aname = 'boundaries'
    call hdf5_write_attr(gr_id, aname, 6, boundaries)

    aname='ngroup'
    call hdf5_write_attr(gr_id, aname, nc)

    aname='1/groupsize'
    call hdf5_write_attr(gr_id, aname, deltam1)

    dsetname = 'npart_grp_array'
    call hdf5_write_data(gr_id, dsetname, nc, nstarcube)

    call hdf5_close_group(gr_id)

    groupname = 'data'
    call hdf5_create_group(file_id, groupname, gr_data_id)

    deb = 1
    ! For each non empty group we create an HDF5 group and write dataset into it
    do ic = 1, nc
       if(nstarcube(ic) /= 0) then
          write(charic(1:8),'(I8.8)') ic
          groupname = 'group'//charic

          call hdf5_create_group(gr_data_id,groupname,gr_id)

          fin = deb + nstarcube(ic) - 1

          ! Write the position of the particles
          dsetname='position_part'
          call hdf5_write_data(gr_id, dsetname, 3, nstarcube(ic), star_position(:,deb:fin))

          ! Write the velocity of the particles
          dsetname='velocity_part'
          call hdf5_write_data(gr_id, dsetname, 3, nstarcube(ic), star_velocity(:,deb:fin))

          if(.not.param%do_skip_mass) then
             dsetname = 'mass_part'
             call hdf5_write_data(gr_id, dsetname, nstarcube(ic), star_mass(deb:fin))
          end if
          
          ! Write the ID of the particles
          dsetname='ramses_identity_part'
          call hdf5_write_data(gr_id, dsetname, nstarcube(ic), star_ramses_id(deb:fin))

          ! Write potential if it is used
          if(param%do_read_potential) then
             dsetname = 'potential_part'
             call hdf5_write_data(gr_id, dsetname, nstarcube(ic), star_potential(deb:fin))
          end if

          ! Write force if it is used
          if(param%do_read_gravitational_field) then
             dsetname = 'gravitational_field_part'
             call hdf5_write_data(gr_id, dsetname, 3, nstarcube(ic), star_field(:,deb:fin))
          end if

          dsetname = 'birth_date_part'
          call hdf5_write_data(gr_id, dsetname, nstarcube(ic), star_birth_date(deb:fin))
          if(param%metal .and. .not.param%do_skip_metal) then
             dsetname = 'metallicity_part'
             call hdf5_write_data(gr_id, dsetname, nstarcube(ic), star_metallicity(deb:fin))
          end if
          
          call hdf5_close_group(gr_id)
          deb = fin + 1

       end if
    end do

    call hdf5_close_group(gr_data_id)

    call hdf5_close_file(file_id)

#ifdef DEBUG
    print *,'Exit H5writesortedstarcube on process ',procID
#endif

  end subroutine H5writesortedstarcube


  !=======================================================================
  !> This subroutine writes the position, the velocity and the id of each particle
  !! on the process in a hdf5 file.
  !! The particles are gathered
  subroutine Mpih5writesortedstarcube()

    use modconstant, only : IDKIND
    use modsort

    character(len=H5_FILENAME_LEN) :: filecube
    character(len=5)  :: pid_char
    character(len=8) :: charic

    character(len=H5_STR_LEN) :: dsetname                           ! Dataset name
    character(len=H5_STR_LEN) :: aname                              ! Attribute name
    character(len=H5_STR_LEN) :: groupname
    character(len=H5_STR_LEN) :: adata
    character(len=H5_STR_LEN) :: codename
    character(len=H5_STR_LEN) :: part_type

    integer(HID_T) :: file_id                               ! File identifier
    integer(HID_T) :: gr_id
    integer(HID_T) :: gr_data_id

    integer(kind=4) :: procperfile
    integer(kind=4) :: npart
    integer(kind=4) :: nfile
    integer(kind=4) :: ic, ix, iy, iz, ip, deb, fin, nc, ncdim, deltam1, fic
    integer(kind=4), dimension(:), allocatable :: nstarcube
    integer(kind=IDKIND), dimension(:), allocatable :: ictable

    integer(kind=4), dimension(:), allocatable :: partnb_tab

    real(kind=4), dimension(6) :: boundaries
    real(kind=4), dimension(:,:), allocatable :: bound_tab

    logical :: empty

    integer(kind=4) :: mpierr
    integer(kind=8) :: npart8
    logical(kind=4) :: islast
    integer(kind=4) :: constant_mass

#ifdef DEBUG
    print *,"Enter Mpih5writesortedstarcube on process ",procID
#endif


    ! each FoF cube is divided into nc=512=8x8x8 groups, with ncdim=8
    ncdim = 8
    nc = ncdim*ncdim*ncdim
    allocate(ictable(local_nstar))
    allocate(nstarcube(nc))
    nstarcube = 0
    ! Ramses coarse grid is composed of nres^3 cells
    ! on a process: nres^3 / procNB cells
    ! => there is (nres/(ncdim*dims(1))^3 coarse cells in each group
    ! the size of a group is nres/(ncdim*dims(1))*(1/nres) where 1/nres is the size of 1 Ramses coarse cell
    ! => delta = 1/(ncdim*dims(1)) => 1/delta = ncdim*dims(1)
    deltam1 = ncdim*info_proc%global_comm%dims(1)

    ! We compute the "group" index of each particle and the number of particle in each group
    do ip=1, local_nstar
       ix = int((star_position(1,ip) - xmin)*deltam1 + 1)
       iy = int((star_position(2,ip) - ymin)*deltam1 + 1)
       iz = int((star_position(3,ip) - zmin)*deltam1 + 1)
       ! rounding issue
       if(ix>ncdim) ix=ncdim
       if(iy>ncdim) iy=ncdim
       if(iz>ncdim) iz=ncdim
       ic = ix + (iy-1)*ncdim + (iz-1)*ncdim*ncdim
       ictable(ip) = ic
       nstarcube(ic) = nstarcube(ic) + 1
    end do

    ! We sort the particles along their group id
    if ( .not. param%do_skip_mass ) then ! Mass
       if ( param%star .and. .not.param%do_skip_star ) then ! Mass + BD
          if( param%metal .and. .not.param%do_skip_metal) then ! Mass + BD + Metal
             if ( param%do_read_gravitational_field ) then ! Mass + BD + Metal + Field
                if( param%do_read_potential) then ! Mass + BD + Metal + Field + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_potential, &
                        star_mass, star_metallicity, star_birth_date, star_ramses_id)
                else ! Mass + BD + Metal + Field
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_mass, &
                        star_metallicity, star_birth_date, star_ramses_id)
                end if
             else ! Mass + BD + Metal
                if( param%do_read_potential) then ! Mass + BD + Metal + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, star_mass, &
                        star_metallicity, star_birth_date, star_ramses_id)
                else ! Mass + BD + Metal
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_mass, star_metallicity, &
                        star_birth_date, star_ramses_id)
                end if
             end if
          else ! Mass + BD
             if ( param%do_read_gravitational_field ) then ! Mass + BD + Field
                if( param%do_read_potential) then ! Mass + BD + Field + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_potential, &
                        star_mass, star_birth_date, star_ramses_id)
                else ! Mass + BD + Field
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_mass, &
                        star_birth_date, star_ramses_id)
                end if
             else ! Mass + BD
                if( param%do_read_potential) then ! Mass + BD + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, star_mass, &
                        star_birth_date, star_ramses_id)
                else ! Mass + BD
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_mass, star_birth_date, &
                        star_ramses_id)
                end if
             end if
          end if
       else ! Mass
          if ( param%do_read_gravitational_field ) then ! Mass + Field
             if( param%do_read_potential) then ! Mass + Field + Potential
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_potential, &
                     star_mass, star_ramses_id)
             else ! Mass + Field
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_mass, &
                     star_ramses_id)
             end if
          else ! Mass
             if( param%do_read_potential) then ! Mass + Potential
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, star_mass, &
                     star_ramses_id)
             else ! Mass
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_mass, star_ramses_id)
             end if
          end if
       end if
    else ! No mass
       if ( param%star .and. .not.param%do_skip_star ) then ! BD
          if( param%metal .and. .not.param%do_skip_metal) then ! BD + Metal
             if ( param%do_read_gravitational_field ) then ! BD + Metal + Field
                if( param%do_read_potential) then ! BD + Metal + Field + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_potential, &
                        star_metallicity, star_birth_date, star_ramses_id)
                else ! BD + Metal + Field
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_metallicity, &
                        star_birth_date, star_ramses_id)
                end if
             else ! BD + Metal
                if( param%do_read_potential) then ! BD + Metal + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, star_metallicity, &
                        star_birth_date, star_ramses_id)
                else ! BD + Metal
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_metallicity, &
                        star_birth_date, star_ramses_id)
                end if
             end if
          else ! BD
             if ( param%do_read_gravitational_field ) then ! BD + Field
                if( param%do_read_potential) then ! BD + Field + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_potential, &
                        star_birth_date, star_ramses_id)
                else ! BD + Field
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_birth_date, &
                        star_ramses_id)
                end if
             else ! BD
                if( param%do_read_potential) then ! BD + Potential
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, star_birth_date, &
                        star_ramses_id)
                else ! BD
                   call heapsort(local_nstar, ictable, star_position, star_velocity, star_birth_date, star_ramses_id)
                end if
             end if
          end if
       else !
          if ( param%do_read_gravitational_field ) then ! Field
             if( param%do_read_potential) then ! Field + Potential
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_potential, &
                     star_ramses_id)
             else ! Field
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_field, star_ramses_id)
             end if
          else !
             if( param%do_read_potential) then ! Potential
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_potential, star_ramses_id)
             else
                call heapsort(local_nstar, ictable, star_position, star_velocity, star_ramses_id)
             end if
          end if
       end if
    end if
    
#ifdef DEBUG
    print *,'Particles sorted'
#endif

    ! number of processes writing in the same file
    procperfile = param%gatherwrite_factor**3

    allocate(partnb_tab(procperfile))
    allocate(bound_tab(6,procperfile))

    write(pid_char(1:5),'(I5.5)') info_proc%write_comm%color
    filecube = 'pfof_cube_snap_star_data_'//trim(param%simulation_name)//'_'//pid_char//'.h5'

    ! create the hdf5 file
    call hdf5_create_mpi_file(filecube, info_proc%write_comm%name, file_id)

    codename = 'pfof_snap'
    part_type = 'star'
    npart8 = 2**(inforamses%levelmin)
    npart8 = npart8**3
    constant_mass = 0
    call Write_meta_common(file_id, codename, npart8, constant_mass=constant_mass, part_type=part_type)
    call Write_meta_halofinder_parameter(file_id, param)
    islast = .false.
    call Write_meta_info_ramses(file_id, inforamses, islast)

    ! open the root group
    groupname = 'metadata'
    call hdf5_open_group(file_id, groupname, gr_id)

    ! Offset and global dimensions have to be computed
    call Mpi_Allgather(local_nstar, 1, Mpi_Integer, partnb_tab, 1, Mpi_Integer, info_proc%write_comm%name, mpierr)

    aname='npart_cube_array'
    call hdf5_write_attr(gr_id, aname, procperfile, partnb_tab)

    aname='ngroup'
    call hdf5_write_attr(gr_id, aname, nc*procperfile)

    aname='1/groupsize'
    call hdf5_write_attr(gr_id, aname, deltam1)

    dsetname = 'npart_grp_array'
    call hdf5_write_mpi_data(gr_id, dsetname, nc, nstarcube, info_proc%write_comm%name)

    aname = 'gatherwrite_factor'
    call hdf5_write_attr(gr_id, aname, param%gatherwrite_factor)

    ! Write type as attribute
    aname = 'file_type'
    adata = 'mpisortedcube'
    call hdf5_write_attr(gr_id, aname, adata)

    npart = sum(partnb_tab)
    dsetname = 'npart_file'
    npart8 = int(npart, kind=8)
    call hdf5_write_data(gr_id, dsetname, npart8)

    aname = 'nfile'
    nfile = procNB / procperfile
    call hdf5_write_attr(gr_id, aname, nfile)

    aname = 'procID'
    call hdf5_write_attr(gr_id, aname, info_proc%write_comm%color)

    ! Write the boundaries of the cube as an attribute
    boundaries(1) = xmin
    boundaries(2) = xmax
    boundaries(3) = ymin
    boundaries(4) = ymax
    boundaries(5) = zmin
    boundaries(6) = zmax

    call Mpi_Allgather(boundaries,6,Mpi_Real, bound_tab, 6, Mpi_Real, info_proc%write_comm%name, mpierr)

    aname = 'boundaries_array'
    call hdf5_write_attr(gr_id, aname, 6, procperfile, bound_tab)

    boundaries(1) = minval(bound_tab(1,:))
    boundaries(2) = maxval(bound_tab(2,:))
    boundaries(3) = minval(bound_tab(3,:))
    boundaries(4) = maxval(bound_tab(4,:))
    boundaries(5) = minval(bound_tab(5,:))
    boundaries(6) = maxval(bound_tab(6,:))
    aname = 'boundaries'
    call hdf5_write_attr(gr_id, aname, 6, boundaries)

    call hdf5_close_group(gr_id)

    deb = 1
    fin = 1
    fic = nc*info_proc%write_comm%pid

    groupname = 'data'
    call hdf5_create_group(file_id, groupname, gr_data_id)

#ifdef DEBUG
    call mpi_barrier(MPI_COMM_WORLD, mpierr)
    write(ERROR_UNIT,*) 'synchronisation before writing data in star cube in Mpih5writesortedstarcube'
#endif
    
    ! For each non empty group we create an HDF5 group and write dataset into it
    do ic = 1, nc*procperfile

       write(charic(1:8),'(I8.8)') ic
       groupname = 'group'//charic
       call hdf5_create_group(gr_data_id, groupname, gr_id)

       fic = ic - nc*info_proc%write_comm%pid
       if(fic>=1 .and. fic<=nc) then
          if(nstarcube(fic) == 0) then
             empty = .true.
             if(deb>local_nstar) then
                deb=1
                fin=1
             else
                fin=deb
             end if
          else
             empty = .false.
             fin = deb + nstarcube(fic) - 1
#ifdef DEBUG
             print *,'group ',groupname,' opened:', procID, fic, deb, fin
#endif
          end if
       else
          empty = .true.
          if(deb>local_nstar .or. fin > local_nstar) then
             deb=1
             fin=1
          end if
       end if

       ! Write the position of the particles
       dsetname='position_part'
       call hdf5_write_mpi_data(gr_id, dsetname, 3, fin-deb+1, star_position(:,deb:fin), &
            info_proc%write_comm%name, empty)

       ! Write the velocity of the particles
       dsetname='velocity_part'
       call hdf5_write_mpi_data(gr_id, dsetname, 3, fin-deb+1, star_velocity(:,deb:fin),&
            info_proc%write_comm%name, empty)

       ! Write the ID of the particles
       dsetname='ramses_identity_part'
       call hdf5_write_mpi_data(gr_id, dsetname, fin-deb+1, star_ramses_id(deb:fin),&
            info_proc%write_comm%name,empty)

       ! write the mass of the particles
       if(.not.param%do_skip_mass) then
          dsetname = 'mass_part'
          call hdf5_write_mpi_data(gr_id, dsetname, fin-deb+1, star_mass(deb:fin), &
               info_proc%write_comm%name, empty)
       end if
       ! write the birth date of the particles
       if(param%star .and. .not.param%do_skip_star) then
          dsetname = 'birth_date_part'
          call hdf5_write_mpi_data(gr_id, dsetname, fin-deb+1, star_birth_date(deb:fin), &
               info_proc%write_comm%name, empty)
       end if
       ! write the metallicity of the particles
       if(param%metal .and. .not.param%do_skip_metal) then
          dsetname = 'metallicity_part'
          call hdf5_write_mpi_data(gr_id, dsetname, fin-deb+1, star_metallicity(deb:fin), &
               info_proc%write_comm%name, empty)
       end if
       
       ! Write potential if it is used
       if(param%do_read_potential) then
          dsetname = 'potential_part'
          call hdf5_write_mpi_data(gr_id, dsetname, fin-deb+1, star_potential(deb:fin),&
               info_proc%write_comm%name,empty)
       end if

       ! Write force if it is used
       if(param%do_read_gravitational_field) then
          dsetname = 'gravitational_field_part'
          call hdf5_write_mpi_data(gr_id, dsetname, 3, fin-deb+1, star_field(:,deb:fin),&
               info_proc%write_comm%name,empty)
       end if

       call hdf5_close_group(gr_id)

       if(fic>=1 .and. fic<=nc) then
          if(nstarcube(fic) /= 0) then
             deb = fin + 1
#ifdef DEBUG
             print *,'group ',groupname,' closed'
#endif
          end if
       end if

    end do

    call hdf5_close_group(gr_data_id)
    deallocate(partnb_tab, bound_tab)

    ! Close h5 file
    call hdf5_close_mpi_file(file_id)

#ifdef DEBUG
    print *,"Exit Mpih5writesortedstarcube on process ",procID
#endif

  end subroutine Mpih5writesortedstarcube

end module write_star_cube_m

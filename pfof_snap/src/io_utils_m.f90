!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2011 Fabrice Roy and Vincent Bouillot
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! This file contains subroutines used for I/O.

!> This module contains subroutines used for I/O.
!> Authors: F. Roy, V. Bouillot
module io_utils_m

  use mpi

  implicit none

  private

  public :: Print_screen_parameters,&
       Init_parameters,&
       The_end, &
       Title, &
       Write_timings

contains

  !=======================================================================
  !> This subroutines prints the title message on screen.
  subroutine Title()

    use iso_fortran_env, only : OUTPUT_UNIT
    use modmpicommons, only : procnb

    character(len=12) :: charpnb

    write(charpnb(1:12),*) procnb

    write(OUTPUT_UNIT,*) ' '
    write(OUTPUT_UNIT,*) '        /\ \       /\ \       /\ \         /\ \    '
    write(OUTPUT_UNIT,*) '       /  \ \     /  \ \     /  \ \       /  \ \   '
    write(OUTPUT_UNIT,*) '      / /\ \ \   / /\ \ \   / /\ \ \     / /\ \ \  '
    write(OUTPUT_UNIT,*) '     / / /\ \_\ / / /\ \_\ / / /\ \ \   / / /\ \_\ '
    write(OUTPUT_UNIT,*) '    / / /_/ / // /_/_ \/_// / /  \ \_\ / /_/_ \/_/ '
    write(OUTPUT_UNIT,*) '   / / /__\/ // /____/\  / / /   / / // /____/\    '
    write(OUTPUT_UNIT,*) '  / / /_____// /\____\/ / / /   / / // /\____\/    '
    write(OUTPUT_UNIT,*) ' / / /      / / /      / / /___/ / // / /          '
    write(OUTPUT_UNIT,*) '/ / /      / / /      / / /____\/ // / /           '
    write(OUTPUT_UNIT,*) '\/_/       \/_/       \/_________/ \/_/            '
    write(OUTPUT_UNIT,*) ' '
    write(OUTPUT_UNIT,*) 'Code written by F.Roy and V.Bouillot'
    write(OUTPUT_UNIT,*) 'based on a serial implementation written by E.Audit'
    write(OUTPUT_UNIT,*) '(see A&A 564, A13 (2014))'
    write(OUTPUT_UNIT,*) ' '
    write(OUTPUT_UNIT,*) 'Snapshot version'
    write(OUTPUT_UNIT,*) 'Number of processes: '//trim(adjustl(charpnb))
    write(OUTPUT_UNIT,*) ' '

  end subroutine Title

  !=======================================================================
  !> This subroutine print the final completion message on screen.
  subroutine The_end()

    use iso_fortran_env, only : OUTPUT_UNIT

    write(OUTPUT_UNIT,*)  ' '
    write(OUTPUT_UNIT,*) 'Run Completed!'
    write(OUTPUT_UNIT,*) ' '

  end subroutine The_end

  !=======================================================================
  !> This subroutine reads the input parameters from pfof_snap.nml file (default name) with process 0
  !! and broadcasts them to every processes.
  !! It also writes the parameters in a log file and in a backup .nml file.
  subroutine Init_parameters()

    use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
    use modconstant, only : ERR_MSG_LEN, FILENAME_LEN, LOG_UNIT
    use modmpicommons, only : create_mpi_type_param_pfof_snap, emergencystop, MPI_TYPE_PARAMETER_PFOF_SNAP, procid, procnb
    use modreadparameters, only : read_pfof_snap_parameters
    use variables_m, only : param

    character(len=ERR_MSG_LEN) :: errormessage
    character(len=FILENAME_LEN) :: filelog
    character(len=FILENAME_LEN) :: filename
    integer(kind=4) :: ioerr
    integer(kind=4) :: mpierr


    ! The process 0 read the input parameters and pack them for the broadcast.
    if(procid==0) then
       filename='pfof_snap.nml'
       call Read_pfof_snap_parameters(filename, param, ioerr, errormessage, .true.)

       if(ioerr /= 0) then
          write(ERROR_UNIT,*) errormessage
          call EmergencyStop(errormessage,ioerr)
       end if

       write(OUTPUT_UNIT,*) 'Parallel FoF snapshot version'
       write(OUTPUT_UNIT,*) procnb,' processes:'

       ! Open log file
       filelog = 'pfof_log_'//trim(param%simulation_name)//'.log'
       open(unit=LOG_UNIT,file=filelog)
       write(LOG_UNIT,*) 'Parallel FoF'
       write(LOG_UNIT,*) procnb,' processes:'

    end if

    call Create_mpi_type_param_pfof_snap()

    call mpi_bcast(param, 1, mpi_type_parameter_pfof_snap, 0, MPI_COMM_WORLD, mpierr)

  end subroutine Init_parameters


  !=======================================================================
  !> This subroutine writes the input parameters in a .nml file,
  !! print them on screen and writes them in the .log file.
  !! It should be called only from process 0.
  subroutine Print_screen_parameters()

    use iso_fortran_env, only : OUTPUT_UNIT
    use modmpicommons, only : procid
    use variables_m, only : param

    if(procid==0) then

       ! Print input parameters on screen
       write(OUTPUT_UNIT,*)  'Input parameters:'
       write(OUTPUT_UNIT,*)  ' '
       write(OUTPUT_UNIT,*)  'Type of RAMSES input files:                     ',param%code_index
       write(OUTPUT_UNIT,*)  'Path to input files:                            ',trim(param%input_path)
       write(OUTPUT_UNIT,*)  'Particle files base name:                       ',trim(param%part_input_file)
       write(OUTPUT_UNIT,*)  'Info file base name:                            ',trim(param%info_input_file)
       write(OUTPUT_UNIT,*)  'Size of groups of inputs:                       ',param%grpsize
       write(OUTPUT_UNIT,*)  'Should pFoF discard masses of particles:        ',param%do_skip_mass
       write(OUTPUT_UNIT,*)  'Were stars written in RAMSES files:             ',param%star
       write(OUTPUT_UNIT,*)  'Were metallicities written in RAMSES files:     ',param%metal
       write(OUTPUT_UNIT,*)  'Should pFoF discard stars:                      ',param%do_skip_star
       write(OUTPUT_UNIT,*)  'Should pFoF discard metallicities:              ',param%do_skip_metal
       write(OUTPUT_UNIT,*)  'Were potentials written in RAMSES files:        ',param%do_read_potential
       write(OUTPUT_UNIT,*)  'Were forces written in RAMSES files:            ',param%do_read_gravitational_field
       write(OUTPUT_UNIT,*)  'Read particles from cube files:                 ',param%do_read_from_cube
       write(OUTPUT_UNIT,*)  'Gather factor for cube input:                   ',param%gatherread_factor
       write(OUTPUT_UNIT,*) ' '
       write(OUTPUT_UNIT,*)  'Halo detection parameters:'
       write(OUTPUT_UNIT,*)  'Percolation parameter:                          ',param%percolation_length
       write(OUTPUT_UNIT,*)  'Minimum mass of halo to be analyzed:            ',param%mmin
       write(OUTPUT_UNIT,*)  'Maximum mass of halo to be analyzed:            ',param%mmax
       write(OUTPUT_UNIT,*)  'Perform friends of friends halo detection:      ',param%do_fof
       write(OUTPUT_UNIT,*)  'Perform unbinding:                              ',param%do_unbinding
       write(OUTPUT_UNIT,*)  'Perform subhalo detection:                      ',param%do_subhalo
       write(OUTPUT_UNIT,*) ' '
       write(OUTPUT_UNIT,*)  'Output parameters:'
       write(OUTPUT_UNIT,*)  'Simulation name:                                ',trim(param%simulation_name)
       write(OUTPUT_UNIT,*)  'Snapshot number:                                ',param%snapshot
       write(OUTPUT_UNIT,*)  'Write cubes of particles:                       ',param%do_write_cube
       write(OUTPUT_UNIT,*)  'Gather factor for cube output:                  ',param%gatherwrite_factor
       write(OUTPUT_UNIT,*)  'Sort particles in cube files:                   ',param%do_sort_cube
       write(OUTPUT_UNIT,*)  'Perform timings (imply extra synchronisations): ',param%do_timings
       write(OUTPUT_UNIT,*)  ' '

    end if

  end subroutine Print_screen_parameters


  !=======================================================================
  !> This subroutine writes timings in the log file and prints them on screen.
  !! It should be called only by process 0.
  !! It also closes the log file.
  subroutine Write_timings()

    use iso_fortran_env, only : OUTPUT_UNIT
    use modconstant, only : LOG_UNIT
    use modtiming

    tFoF = tFoFinit + tFoFloc + tRaccord
    tOut = tOuthalopart+tOutmass

    write(OUTPUT_UNIT,*) ''
    write(OUTPUT_UNIT,*) 'Timings:'
    write(OUTPUT_UNIT,*) 'Input:',tRead
    write(OUTPUT_UNIT,*) '        initialization        :',tInitRead
    write(OUTPUT_UNIT,*) '        read input files      :',tReadFile
    write(OUTPUT_UNIT,*) '        scatter particles     :',tTailPart
    write(OUTPUT_UNIT,*) ''
    write(OUTPUT_UNIT,*) 'Friend of Friend:',tFoF
    write(OUTPUT_UNIT,*) '        initialization:',tFoFinit
    write(OUTPUT_UNIT,*) '        local FoF     :',tFoFloc
    write(OUTPUT_UNIT,*) '        merge         :',tRaccord
    write(OUTPUT_UNIT,*) ''
    write(OUTPUT_UNIT,*) 'Observables computation and output:',tObs + tOut + tSort + tGatherhalo + tSelecthalo
    write(OUTPUT_UNIT,*) '        sort particles following haloID:',tSort
    write(OUTPUT_UNIT,*) '        gather particles following haloID: ',tGatherhalo
    write(OUTPUT_UNIT,*) '        select halo with M > Mmin: ',tSelecthalo
    write(OUTPUT_UNIT,*) '        computation of observables:',tObs
    write(OUTPUT_UNIT,*) '        write files:',tOuthalopart+tOutmass

    write(LOG_UNIT,*) ''
    write(LOG_UNIT,*) 'end of pFoF'
    write(LOG_UNIT,*) ''
    write(LOG_UNIT,*) 'Timings:'
    write(LOG_UNIT,*) 'Input:',tRead
    write(LOG_UNIT,*) '        initialization        :',tInitRead
    write(LOG_UNIT,*) '        read input files      :',tReadFile
    write(LOG_UNIT,*) '        scatter particles     :',tTailPart
    write(LOG_UNIT,*) ''
    write(LOG_UNIT,*) 'Friend of Friend:',tFoF
    write(LOG_UNIT,*) '        initialization:',tFoFinit
    write(LOG_UNIT,*) '        local FoF     :',tFoFloc
    write(LOG_UNIT,*) '        merge         :',tRaccord
    write(LOG_UNIT,*) ''
    write(LOG_UNIT,*) 'Observables computation and output:',tObs + tOut+ tSort + tGatherhalo + tSelecthalo
    write(LOG_UNIT,*) '        sort particles following haloID:',tSort
    write(LOG_UNIT,*) '        gather particles following haloID: ',tGatherhalo
    write(LOG_UNIT,*) '        select halo with M > Mmin: ',tSelecthalo
    write(LOG_UNIT,*) '        computation of observables:',tObs
    write(LOG_UNIT,*) '        write files:',tOuthalopart+tOutmass


    ! Close log file
    close(LOG_UNIT)

  end subroutine Write_timings

end module io_utils_m

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2023 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Read all particles files in ASCII format: positions and velocities only
!! @brief
!! 
!! @author Fabrice Roy

!> Read particles in ASCII files
!------------------------------------------------------------------------------------------------------------------------------------

module read_ascii_output_m

  use mpi

  implicit none

  private

  public :: Read_ascii_output

  
contains

!> This subroutine reads the particles files in ASCII (positions and velocities only) that pFOF has to analyze.
subroutine Read_ascii_output()

  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use modconstant, only : FILENAME_LEN, IDKIND, LOG_UNIT, MPI_IDKIND
  use modindex, only : coord_to_id, id_to_coord
  use modmpicommons, only : create_mpi_type_info_ramses, emergencystop, MPI_TYPE_INFO_RAMSES, procid, procnb
  use modtiming, only : time0, timeint, tinitread, tread, treadfile, ttailpart
  use modsort, only : heapsort
  use modvarcommons, only : field, global_npart, local_npart, nres, pfof_id, position, potential, velocity, &
       mass, metallicity, birth_date
  use mpi_communicator_m, only : build_my_process_grid, info_proc
  use read_ramses_part_file_m, only : Read_ramses_part_file
  use variables_m, only : inforamses, ngrid, param, xmax, xmin, ymax, ymin, zmax, zmin

  !-----------------------------------------------
  ! Lecture du fichier particules au format Ramses
  !-----------------------------------------------

  ! Local variables
  character(len=6)               :: ncharcpu
  character(len=FILENAME_LEN)    :: nomfich
  character(len=11)              :: grpchar

  integer(kind=4)                :: icpu               ! loop variable
  integer(kind=4)                :: idim
  integer(kind=4)                :: ipart
  integer(kind=4)                :: ind
  integer(kind=4)                :: mynbfile           ! number of RAMSES part files read by local process
  integer(kind=4)                :: nmod               !
  integer(kind=4)                :: firstp, lastp      ! id of 1st and last RAMSES part file read
  integer(kind=4)                :: first_id
  integer(kind=4)                :: first_part, last_part
  integer(kind=4), allocatable, dimension(:) :: local_npart_array
  integer(kind=4), allocatable, dimension(:)  :: npart_per_file
  integer(kind=4), allocatable   :: npartvloc(:), npartv(:)  ! temp and global table of particle numbers for each process
  integer(kind=4)                :: nsd  
  integer(kind=4)                :: n_i, n_j, n_k
  integer(kind=4)                :: ncpu       ! process number  read in RAMSES part files
  integer(kind=4)                :: ndim       ! dimension       read in RAMSES part files
  integer(kind=4)                :: npartloc    ! particle number read in RAMSES part files
  integer(kind=IDKIND)              :: tmplongint              ! temp integer8 variable
  integer(kind=IDKIND), allocatable :: tmpi(:)    ! TYPE VARIABLE EN FONCTION DU NB DE PART
  integer(kind=4)                :: grpnb

  real(kind=4), allocatable     :: tmpx(:,:), tmpv(:,:), tmpp(:), tmpf(:,:), tmpma(:), tmpme(:), tmpb(:)
  real(kind=4)                  :: deltasd
  integer(kind=4) :: ioerr
  character(len=500) :: errormessage

  integer(kind=4) :: mpierr

  integer(kind=4) :: max_local_npart
  integer(kind=4) :: icube, my_cube_id, dist_cube_id, dist_process, iproc
  integer(kind=4), dimension(:), allocatable :: proc_to_cube
  integer(kind=4), dimension(:), allocatable :: cube_to_proc

  integer(kind=IDKIND) :: first_element, local_pointer, local_count
  integer(kind=MPI_ADDRESS_KIND) :: lower_bound, size_of_real, size_of_integer, size_of_pri
  integer(kind=4) :: position_window, velocity_window, pfof_id_window
  integer(kind=4) :: mass_window, birth_date_window, metallicity_window
  integer(kind=4) :: potential_window, field_window, disp_window, npart_window

  integer(kind=IDKIND), dimension(:), allocatable :: cube_id
  integer(kind=IDKIND), dimension(:), allocatable :: cube_disp

  integer(kind=4), dimension(3) :: my_cube_coords
  integer(kind=4), dimension(:), allocatable :: dist_disp, dist_npart
  integer :: allocstat
  integer(kind=4) :: tmp_npart
  integer :: request
  logical :: is_completed
  integer, dimension(MPI_STATUS_SIZE) :: status

  
  ! Initialisation timer
  time0 = mpi_wtime()

  grpchar = 'group_00001'

  ! Lecture parametres et remplissage du tampon pour diffusion des parametres
  if(procid == 0) then
    write(OUTPUT_UNIT,*) 'Reading ASCII output...'

    ! Il faut mettre de fausses info ramses
    ! call readinforamses(nomfich, inforamses, ioerr, errormessage)
    inforamses%ncpu = 8
    inforamses%ndim = 3
    inforamses%levelmin = 9  !! DOIT ETRE EXACT
    inforamses%levelmax = 10 
    inforamses%ngridmax = 10
    inforamses%nstep_coarse = 10
    inforamses%boxlen = 1.e6
    inforamses%time = 1.
    inforamses%aexp = 1.
    inforamses%h0 = 1.
    inforamses%omega_m = 1.
    inforamses%omega_l = 1.
    inforamses%omega_k = 1. 
    inforamses%omega_b = 1.
    inforamses%unit_l = 1.
    inforamses%unit_d = 1.
    inforamses%unit_t = 1. 
    inforamses%ordering = 'ascii'
  end if
  call create_mpi_type_info_ramses()

  call mpi_bcast(inforamses, 1, Mpi_Type_info_ramses, 0, MPI_COMM_WORLD, mpierr)

  nres = 2**inforamses%levelmin
  if(procid==0) then

     write(*,*) 'Number of:'
     write(*,'(A25,I6)') ' - files for each output:',inforamses%ncpu
     write(*,'(A25,I6)') ' - dimensions:           ',inforamses%ndim
     write(*,'(A25,I6)') ' - grid points:          ',nres
     write(LOG_UNIT,*) 'nb_proc = ',inforamses%ncpu,'ndim = ',inforamses%ndim,'nres = ',nres

  end if

  ngrid = int(nres,kind=IDKIND)**3

  if(procid==0) write(OUTPUT_UNIT,*) 'Reading positions...'
  if(procid==0) write(LOG_UNIT,*) 'Reading positions...'

  global_npart = 0

  nmod = mod(inforamses%ncpu,procnb)
  mynbfile = inforamses%ncpu / procnb
  if(procid <= nmod-1) then
     mynbfile = mynbfile+1
     firstp   = procid * mynbfile + 1
     lastp    = (procid+1) * mynbfile
  else
     firstp   = procid * mynbfile  + nmod
     lastp    = (procid+1) * mynbfile + nmod - 1
  end if

  local_npart = 0

  allocate(npartv(procnb), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for npartv in Read_ramses', allocstat)
  allocate(npartvloc(procnb), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for npartvloc in Read_ramses', allocstat)
  allocate(local_npart_array(procnb), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for local_npart_array in Read_ramses', allocstat)

  npartv = 0
  npartvloc = 0

  nsd = int(procnb**(1./3.))
  deltasd = 1./nsd

  if(procid == 0) then
    write(*,*) 'Number of subdomains in each dimension:',nsd
    write(*,*) 'Size of each subdomain:',deltasd
  end if

  xmin =  info_proc%global_comm%coords(1)      * deltasd
  xmax = (info_proc%global_comm%coords(1) + 1) * deltasd
  ymin =  info_proc%global_comm%coords(2)      * deltasd
  ymax = (info_proc%global_comm%coords(2) + 1) * deltasd
  zmin =  info_proc%global_comm%coords(3)      * deltasd
  zmax = (info_proc%global_comm%coords(3) + 1) * deltasd
  if(info_proc%global_comm%coords(1) == info_proc%global_comm%dims(1) - 1) xmax = 1.e0
  if(info_proc%global_comm%coords(2) == info_proc%global_comm%dims(2) - 1) ymax = 1.e0
  if(info_proc%global_comm%coords(3) == info_proc%global_comm%dims(3) - 1) zmax = 1.e0

  allocate(npart_per_file(firstp:lastp) , stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for npart_per_file in Read_ramses', allocstat)

  ncharcpu = '.0.dat'
  do icpu = firstp,lastp
    write(ncharcpu(2:2),'(I1.1)') icpu
    nomfich = trim(param%input_path)//'/'//trim(param%part_input_file)//trim(ncharcpu)

    open(unit=1,file=nomfich,status='old',form='formatted')
    npartloc = 0
    do
      read(1,*, iostat=ioerr) tmpf
      if (ioerr /= 0) exit
      npartloc = npartloc + 1
    end do
    close(1)

    npart_per_file(icpu) = npartloc
    local_npart = local_npart + npartloc
  end do

  call mpi_allgather(local_npart,1,MPI_INTEGER,local_npart_array,1,MPI_INTEGER,MPI_COMM_WORLD,mpierr)

  allocate(tmpx(3,local_npart), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for tmpx in Read_ramses', allocstat)
  allocate(tmpv(3,local_npart), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for tmpv in Read_ramses', allocstat)
  allocate(tmpi(local_npart), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for tmpi in Read_ramses', allocstat)
  allocate(cube_id(local_npart), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for cube_id in Read_ramses', allocstat)


  
  if(param%do_timings) then
     call mpi_barrier(info_proc%global_comm%name,mpierr)
     timeInt = mpi_wtime()
     tInitRead = timeInt - time0
  end if

  first_id = 0
  if(procid /= 0) first_id = sum(local_npart_array(1:procid))
  write(output_unit,*) 'first_id =',first_id, ' on process ', procid

  first_part = 1
  last_part = 0

  do icpu = firstp,lastp
    write(ncharcpu(2:2),'(I1.1)') icpu
    nomfich = trim(param%input_path)//'/'//trim(param%part_input_file)//trim(ncharcpu)

    last_part = last_part + npart_per_file(icpu)

    open(unit=1,file=nomfich,status='old',form='formatted')
    
    do ipart = first_part, last_part
      read(1,*, iostat=ioerr) tmpx(1,ipart), tmpx(2,ipart),tmpx(3,ipart),tmpv(1,ipart), tmpv(2,ipart),tmpv(3,ipart)
      if (ioerr /= 0) exit
      tmpi(ipart) = ipart + first_id
    end do
    close(1)    
    first_part = last_part + 1
  end do
  ! end of the loop over RAMSES files read


  ! count the particles in each cube
  do ipart = 1, npartloc ! rescale the positions first
    tmpx(:,ipart) = tmpx(:,ipart) / inforamses%boxlen
    do idim = 1, inforamses%ndim
       if(abs(tmpx(idim,ipart)-1.0e0) < 1.e-8) tmpx(idim,ipart) = 0.e0
    end do
    n_i = int(tmpx(1,ipart)/deltasd)
    n_j = int(tmpx(2,ipart)/deltasd)
    n_k = int(tmpx(3,ipart)/deltasd)
    call coord_to_id((/n_i,n_j,n_k/),ind,(/nsd,nsd,nsd/))
    npartvloc(ind) = npartvloc(ind)+1
    cube_id(ipart) = ind
  end do
  
  call heapsort(local_npart, cube_id, tmpx, tmpv, tmpi)


#ifdef DEBUG
  write(ERROR_UNIT,*) 'process', procid, ' : position(:,1) = ', tmpx(:,1)
  write(ERROR_UNIT,*) 'process', procid, ' : position(:,last) = ', tmpx(:,local_npart)
  icube=1
  do while (abs(tmpx(1,icube)) < 1.0e-8)
     icube = icube+1
  end do
  write(ERROR_UNIT,*) 'process', procid, ' ; icube = ', icube, ' : position(:,icube) = ', tmpx(:,icube)   
#endif
  
  tmp_npart = local_npart

  allocate(cube_disp(procnb), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for cube_disp in Read_ramses', allocstat)
  ! compute displacement from the beginning to each cube from npartvloc
  cube_disp(1) = 1
  do icube = 2, procnb
     cube_disp(icube) = cube_disp(icube-1) + int(npartvloc(icube-1),kind=IDKIND)
  end do

  call mpi_allreduce(npartvloc,npartv,procnb,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,mpierr)
  tReadfile = mpi_wtime() - timeInt
  timeInt = mpi_wtime()

  ! select the cube where current process has the most particles
  max_local_npart = maxval(npartvloc)
  do icube=1, procnb
     if(max_local_npart == npartvloc(icube)) then
        my_cube_id = icube
        exit
     end if
  end do

#ifdef DEBUG
  write(OUTPUT_UNIT,*) 'Check disp on proc',procid,my_cube_id,' : ',cube_disp(1), npartvloc(1), cube_id(cube_disp(1))
#endif

  allocate(proc_to_cube(procnb), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for proc_to_cube in Read_ramses', allocstat)
  allocate(cube_to_proc(procnb), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for cube_to_proc in Read_ramses', allocstat)
  ! proc_to_cube is the array that contains, for each process id, the id of the cube
  ! that would be preffered by this process
  call mpi_gather(my_cube_id,1,MPI_INTEGER,proc_to_cube,1,MPI_INTEGER,0,MPI_COMM_WORLD,mpierr)

  if(procid==0) then
     call Distribute_cubes(proc_to_cube, cube_to_proc)
  end if

  ! now, proc_to_cube contains, for each process id, the id of the cube that has been assigned
  ! to this process
  ! cube_to_proc contains, for each cube id, the id of the process assigned to this cube
  call mpi_bcast(proc_to_cube,procnb,MPI_INTEGER,0,MPI_COMM_WORLD,mpierr)
  call mpi_bcast(cube_to_proc,procnb,MPI_INTEGER,0,MPI_COMM_WORLD,mpierr)

#ifdef DEBUG
  write(OUTPUT_UNIT,*) 'Process:',procid, ' ; Cube ID:',proc_to_cube(procid+1)
#endif

  ! set my_cube_id
  my_cube_id = proc_to_cube(procid+1)

  ! build the neighboors array corresponding to the cube coordinates
  call build_my_process_grid(cube_to_proc, param, proc_to_cube)

  call id_to_coord(int(my_cube_id,kind=IDKIND), my_cube_coords, info_proc%global_comm%dims)
  ! set min and max for x,y,z
  xmin =  my_cube_coords(1)      * deltasd
  xmax = (my_cube_coords(1) + 1) * deltasd
  ymin =  my_cube_coords(2)      * deltasd
  ymax = (my_cube_coords(2) + 1) * deltasd
  zmin =  my_cube_coords(3)      * deltasd
  zmax = (my_cube_coords(3) + 1) * deltasd
  if(my_cube_coords(1) == info_proc%global_comm%dims(1) - 1) xmax = 1.e0
  if(my_cube_coords(2) == info_proc%global_comm%dims(2) - 1) ymax = 1.e0
  if(my_cube_coords(3) == info_proc%global_comm%dims(3) - 1) zmax = 1.e0


  ! local_npart = ?
  local_npart = npartv(my_cube_id)

  ! global npart in case of dark matter only simulation
  call mpi_iallreduce(local_npart, global_npart, 1, MPI_IDKIND, MPI_SUM, MPI_COMM_WORLD, request, mpierr)

    ! Allocate arrays for positions, velocities, ids, potentials, grav. fields
  allocate (position(3,local_npart), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for position in Read_ramses', allocstat)
  allocate (velocity(3,local_npart), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for velocity in Read_ramses', allocstat)
  allocate (pfof_id(local_npart), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for pfof_id in Read_ramses', allocstat)
  
  ! Allocate array for distant displacement of data to get
  allocate(dist_disp(procnb), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for dist_disp in Read_ramses', allocstat)
  dist_disp = 0
  ! Allocate array for distant number of particles of data to get
  allocate(dist_npart(procnb), stat=allocstat)
  if(allocstat > 0) call Emergencystop('Allocate failed for dist_npart in Read_ramses', allocstat)
  dist_npart = 0
  ! everyone knows what it will deal with
  ! first: open my windows
  call mpi_type_get_extent(MPI_REAL, lower_bound, size_of_real, mpierr)
  call mpi_type_get_extent(MPI_IDKIND, lower_bound, size_of_pri, mpierr)
  call mpi_type_get_extent(MPI_INTEGER, lower_bound, size_of_integer, mpierr)


  call mpi_win_create(tmpx, 3*tmp_npart*size_of_real,int(size_of_real,kind=4), &
       MPI_INFO_NULL, info_proc%global_comm%name, position_window, mpierr)

  call mpi_win_create(tmpv, 3*tmp_npart*size_of_real,int(size_of_real,kind=4), &
       MPI_INFO_NULL, info_proc%global_comm%name, velocity_window, mpierr)

  call mpi_win_create(tmpi, tmp_npart*size_of_pri,int(size_of_pri,kind=4), &
       MPI_INFO_NULL, info_proc%global_comm%name, pfof_id_window, mpierr)

  ! the principle of the RMA will be the following:
  ! 1- loop over the processes
  !    - if I have to write something to a distant process
  !    - write my corresponding cube_disp to distant dist_disp
  !    - write my corresponding npartvloc to distant dist_npart
  ! 2- loop over the processes
  !    - if dist_disp(proc) /= 0
  !    - get data from proc at position dist_disp corresponding to dist_npart particles
  call mpi_win_create(dist_disp, procnb*size_of_integer, int(size_of_integer,kind=4), &
       MPI_INFO_NULL, info_proc%global_comm%name, disp_window, mpierr)
  call mpi_win_create(dist_npart, procnb*size_of_integer, int(size_of_integer,kind=4), &
       MPI_INFO_NULL, info_proc%global_comm%name, npart_window, mpierr)

#ifdef DEBUG
  call mpi_barrier(MPI_COMM_WORLD, mpierr)
  write(OUTPUT_UNIT,*) 'Read_ramses: all windows have been created on process ',procid
  write(OUTPUT_UNIT,*) 'proc',procid, ', tmp_npart=',tmp_npart
#endif

  ! open the windows for displacement and particles number
  call mpi_win_fence(0, disp_window,mpierr)
  call mpi_win_fence(0, npart_window,mpierr)

#ifdef DEBUG
  call mpi_barrier(MPI_COMM_WORLD, mpierr)
  write(OUTPUT_UNIT,*) 'Read_ramses: fence open disp and npart ok ',procid
#endif

  ! loop over the process id
  do iproc = 1, procnb-1
     dist_process = mod(procid+iproc, procnb)
     dist_cube_id = proc_to_cube(dist_process+1)

     ! if we have particles to send to this process
     if(npartvloc(dist_cube_id)/=0) then
        ! put the part number that dist_process will have to get from me
        call mpi_put(npartvloc(dist_cube_id), 1, MPI_INTEGER, &
             dist_process, int(procid,kind=MPI_ADDRESS_KIND), 1, MPI_INTEGER, npart_window, mpierr)
        ! put the displacement where dist_process will find these particles on me
        call mpi_put(cube_disp(dist_cube_id), 1, MPI_INTEGER, &
             dist_process, int(procid,kind=MPI_ADDRESS_KIND), 1, MPI_INTEGER, disp_window, mpierr)
     end if
  end do

  ! close the windows for displacement and particles number
  call mpi_win_fence(0, disp_window,mpierr)
  call mpi_win_fence(0, npart_window,mpierr)

#ifdef DEBUG
  call mpi_barrier(MPI_COMM_WORLD, mpierr)
  write(OUTPUT_UNIT,*) 'Read_ramses: fence close disp and npart ok ',procid
#endif

#ifdef DEBUG
  call mpi_barrier(MPI_COMM_WORLD, mpierr)
  write(OUTPUT_UNIT,*) 'Read_ramses: fence open properties ok ',procid
#endif


  ! open the windows for particles properties
  call mpi_win_fence(0, position_window,mpierr)
  call mpi_win_fence(0, velocity_window,mpierr)
  call mpi_win_fence(0, pfof_id_window,mpierr)
  if(param%do_read_gravitational_field) call mpi_win_fence(0, field_window,mpierr)
  if(param%do_read_potential) call mpi_win_fence(0, potential_window,mpierr)
  if(.not.param%do_skip_mass) call mpi_win_fence(0, mass_window, mpierr)
  if(param%star .and. .not.param%do_skip_star) call mpi_win_fence(0, birth_date_window, mpierr)
  if(param%metal .and. .not.param%do_skip_metal) call mpi_win_fence(0, metallicity_window, mpierr)
  
  first_element = 1
  ! loop again over the process
  do iproc = 1, procnb-1

     ! test iallreduce for global_npart
     call mpi_test(request,is_completed,status,mpierr)


     dist_process = mod(procid+iproc, procnb)
     ! if the distant process has some particles that I need, I will read them
     if(dist_npart(dist_process+1) /= 0) then
        call mpi_get(position(1,first_element), 3*dist_npart(dist_process+1), MPI_REAL,&
             dist_process, int(3*(dist_disp(dist_process+1)-1),kind=MPI_ADDRESS_KIND), &
             3*dist_npart(dist_process+1), MPI_REAL, position_window, mpierr)

        call mpi_get(velocity(1,first_element), 3*dist_npart(dist_process+1), MPI_REAL,&
             dist_process, int(3*(dist_disp(dist_process+1)-1),kind=MPI_ADDRESS_KIND), &
             3*dist_npart(dist_process+1), MPI_REAL, velocity_window, mpierr)

        call mpi_get(pfof_id(first_element), dist_npart(dist_process+1), MPI_IDKIND,&
             dist_process, int(dist_disp(dist_process+1)-1,kind=MPI_ADDRESS_KIND), &
             dist_npart(dist_process+1), MPI_IDKIND, pfof_id_window, mpierr)
        
        first_element = first_element + dist_npart(dist_process+1)
     end if
  end do

  ! close the windows for particles properties
  call mpi_win_fence(0, position_window,mpierr)
  call mpi_win_fence(0, velocity_window,mpierr)
  call mpi_win_fence(0, pfof_id_window,mpierr)

  ! free the windows
  call mpi_win_free(position_window, mpierr)
  call mpi_win_free(velocity_window, mpierr)
  call mpi_win_free(pfof_id_window, mpierr)
  
  call mpi_win_free(disp_window, mpierr)
  call mpi_win_free(npart_window, mpierr)

  ! when the windows are free we can safely copy the local data to the working arrays
  local_pointer = cube_disp(my_cube_id)
  local_count = npartvloc(my_cube_id)
  position(:,first_element:local_npart) = tmpx(:,local_pointer:local_pointer+local_count-1)
  velocity(:,first_element:local_npart) = tmpv(:,local_pointer:local_pointer+local_count-1)
  pfof_id(first_element:local_npart) = tmpi(local_pointer:local_pointer+local_count-1)

  ! deallocate some temporary arrays
  deallocate(cube_disp)
  deallocate(tmpx,tmpv,tmpi)
  deallocate(npartv, npartvloc)
  deallocate(cube_id)
  
  tTailPart = mpi_wtime() - timeInt
  tRead = mpi_wtime() - time0

#ifdef DEBUG
  write(ERROR_UNIT,*) 'number of part with position(:)=0.0 = ',count((position(1,:)<=1.e-6).and. &
       (position(1,:)<=1.e-6).and.(position(1,:)<=1.e-6))
  write(ERROR_UNIT,*) 'process:', procid, ' ; nb of part with pfof_id=0 =',count(pfof_id==0)
#endif
  
  ! wait iallreduce for global_npart
  call mpi_wait(request,status,mpierr)

end subroutine Read_ascii_output

!=======================================================================
subroutine Distribute_cubes(id_array, id_array_inv)

  use iso_fortran_env, only : ERROR_UNIT
  use modmpicommons, only : procnb


  integer(kind=4), intent(inout), dimension(*) :: id_array
  integer(kind=4), intent(inout), dimension(*) :: id_array_inv

  integer(kind=4) :: iproc, icube, n_proc_to_assign
  logical(kind=1), dimension(procnb) :: assigned, chosen
  integer(kind=4), dimension(procnb) :: proc_to_assign
  integer(kind=4) :: mpierr

  chosen = .false.
  assigned = .false.
  n_proc_to_assign = 0
  ! loop over the array of "prefered cube id per process"
  do iproc = 1, procnb
     icube = id_array(iproc) ! preferd cube for process iproc-1
     ! if this cube has not already been chosen, then it is assigned to process iproc-1
     ! and it is marked as chosen
     if(.not.chosen(icube)) then
        chosen(icube) = .true.
        assigned(iproc) = .true.
     else
        n_proc_to_assign = n_proc_to_assign + 1
        proc_to_assign(n_proc_to_assign) = iproc
     end if
  end do

  if(n_proc_to_assign /= count(.not.chosen)) then
     write(ERROR_UNIT,*) 'Problem in distribute_cubes:'
     write(ERROR_UNIT,*) 'number of unassigned processes /= number of not chosen cubes'
     write(ERROR_UNIT,*) 'abort'
     call mpi_abort(MPI_COMM_WORLD, 2, mpierr)
  end if

  iproc = 1
  do icube = 1, procnb
     if(.not.chosen(icube)) then
        id_array(proc_to_assign(iproc)) = icube
        iproc = iproc + 1
     end if
  end do


  do iproc = 1, procnb
     icube = id_array(iproc)
     id_array_inv(icube) = iproc - 1
  end do

end subroutine Distribute_cubes



end module read_ascii_output_m

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2015 Fabrice Roy 
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Global variables declarations.
!! @brief
!! 
!! @author Fabrice Roy

!> Global variables declarations.
!------------------------------------------------------------------------------------------------------------------------------------

Module conemapper_var_m
  use modconstant

  implicit none

  Type(Type_parameter_pfof_cone) :: parameters !< Input parameters read from the pfof_cone.nml file.

  Integer(kind=4) :: shell_nb  !< Number of shell files read.
  Integer(kind=4), dimension(:), allocatable :: npartcube  !< Number of particles in each shell cube.
  Integer(kind=8), dimension(:), allocatable :: npartcubefof  ! nb of particles in each pfof cube/process
  Integer(kind=4), dimension(:,:), allocatable :: pictable  ! list of shell cube indices in each pfof cube/process
  Integer(kind=4), dimension(:,:), allocatable :: neigh  ! process ID of each neighbour for each pfof cube/process
  Integer(kind=8), dimension(:), allocatable :: my_npart_tab  ! array of nb of particles in each pfof cube/process
  Integer(kind=4), dimension(3) :: nctab  ! nb of shell cubes in each dimension in the global mapping
  Integer(kind=4) :: ncube  ! nb of shell cubes
  Integer(kind=8) :: npart  ! total number of particles in the cone
  Real(kind=8) :: cubesize  ! length of the shell cube edge
  Real(kind=8), dimension(:,:), allocatable :: boundaries ! (x,y,z)min, (x,y,z)max of each pfof cube
  Logical :: fullsky  ! is it a fullsky cone?

end module conemapper_var_m

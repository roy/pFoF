!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2015 Fabrice Roy 
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Conemapper computes the optimal processes placement for a Ramses cone analysis
!! @brief
!! 
!! @author Fabrice Roy

!> Conemapper computes the optimal processes placement for a Ramses cone analysis
!------------------------------------------------------------------------------------------------------------------------------------

program conemapper

  use conemapper_io_m
  use fortran_hdf5_manage_interface_m
  use map_m

  implicit none

  ! Initialization of HDF5
  call hdf5_init()

  call readparameters()

  call h5readshellinfo()

  call exploremap()

  ! Finalize HDF5
  call hdf5_finalize()

end program conemapper

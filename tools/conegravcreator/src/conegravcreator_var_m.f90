!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2015 Fabrice Roy 
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Variables for conegravcreator
!! @brief
!! 
!! @author Fabrice Roy

!> Variables for conegravcreator
!------------------------------------------------------------------------------------------------------------------------------------

Module conegravcreator_var_m
  Use mpi
  Use modconstant
  use type_info_ramses_mod


  Implicit none

  ! Input parameters
  Type(Type_parameter_conecreator_grav) :: param

  ! Variables
  Real(kind=4), dimension(:,:), allocatable :: pos  ! position of the cells in the halo just read
  Real(kind=4), dimension(:,:), allocatable :: acc  ! acceleration in the cells in the halo just read 
  Real(kind=4), dimension(:), allocatable :: phi  ! potential in the cells in the halo just read
  Real(kind=4), dimension(:), allocatable :: rho  ! density in the cells in the halo just read
  Integer(kind=4), dimension(:), allocatable :: refined ! is the cell refined or not

  Character(len=200), dimension(:), allocatable :: filelist    ! list of the filenames that we must read

  Integer :: procID
  Integer :: procNB
  Integer :: mpierr

  Integer(kind=4), dimension(:,:), allocatable :: ncellcubeloc, ncellcube
  Integer(kind=4), dimension(:), allocatable :: idcube

  Integer(kind=4) :: ncx
  Integer(kind=4) :: ncy
  Integer(kind=4) :: ncz
  Integer(kind=4) :: ncube

  Integer(kind=4), dimension(:), allocatable :: ncellperlevel

  Real(kind=8) :: hy
  Real(kind=8) :: hz

  !! MPI Variables
  Integer :: req_sumnpc
  Integer(kind=4), dimension(Mpi_Status_Size) :: mpistat

  Type(Type_info_cone_grav) :: infocone
  Type(Type_info_ramses) :: inforamses

End Module conegravcreator_var_m

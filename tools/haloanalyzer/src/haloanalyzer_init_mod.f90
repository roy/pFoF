module haloanalyzer_init_mod
  
  implicit none

  private

  public :: haloanalyzer_init

contains

  subroutine haloanalyzer_init(particles, info_proc, size, var_list)

    use iso_fortran_env, only : ERROR_UNIT
    use haloanalyzer_var_mod, only : type_info_proc, type_particles, type_var_list
    use modconstant, only : ERR_MSG_LEN
    
    type(type_particles), intent(out) :: particles
    type(type_info_proc), intent(in) :: info_proc
    integer(kind=4), intent(in) :: size
    type(type_var_list), intent(in) :: var_list

    integer(kind=4) :: alloc_stat
    character(len=ERR_MSG_LEN) :: alloc_err_msg

    particles%nb_part = size
    
    if(var_list%need_position) then
       allocate(particles%position(3,size),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in particles%position allocation in haloanalyzer_init on process ',info_proc%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    end if

    if(var_list%need_velocity) then
       allocate(particles%velocity(3,size),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in particles%velocity allocation in haloanalyzer_init on process ',info_proc%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    end if

    if(var_list%need_pfof_id) then
       allocate(particles%pfof_id(size),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in particles%pfof_id allocation in haloanalyzer_init on process ',info_proc%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    end if
    
    if(var_list%need_mass) then
       allocate(particles%mass(size),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in particles%mass allocation in haloanalyzer_init on process ',info_proc%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    end if
    
    if(var_list%need_birth_date) then
       allocate(particles%birth_date(size),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in particles%birth_date allocation in haloanalyzer_init on process ',info_proc%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    end if
    
    if(var_list%need_metallicity) then
       allocate(particles%metallicity(size),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in particles%metallicity allocation in haloanalyzer_init on process ',info_proc%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    end if
    
    if(var_list%need_potential) then
       allocate(particles%potential(size),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in particles%potential allocation in haloanalyzer_init on process ',info_proc%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    end if
    
    if(var_list%need_field) then
       allocate(particles%field(3, size),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in particles%field allocation in haloanalyzer_init on process ',info_proc%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    end if
    
    if(var_list%need_structure_id) then
       allocate(particles%structure_id(size),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in particles%structure_id allocation in haloanalyzer_init on process ',info_proc%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    end if
    
    if(var_list%need_ramses_id) then
       allocate(particles%ramses_id(size),stat=alloc_stat,errmsg=alloc_err_msg)
       if(alloc_stat /= 0) then
          write(ERROR_UNIT,'(A,I6)') 'Error in particles%ramses_id allocation in haloanalyzer_init on process ',info_proc%pid
          write(ERROR_UNIT,'(A)') alloc_err_msg
       end if
    end if
    
  end subroutine haloanalyzer_init
  
end module haloanalyzer_init_mod

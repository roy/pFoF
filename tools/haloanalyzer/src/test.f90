program test

  use iso_fortran_env, only : OUTPUT_UNIT
  use mpi
  use read_star_cube_mod
  use sort_particles_mod
  use type_particles_mod
  use modhdf5 !, only : H5_FILENAME_LEN
  
  implicit none

  integer :: mpierr
  integer :: npart
  integer :: process_id
  type(type_stars) :: stars
  character(len=H5_FILENAME_LEN), allocatable :: filename 
  type(type_star_cube_metadata) :: star_cube_metadata
  integer(hid_t) :: file_id
  
  call mpi_init(mpierr)
  call mpi_comm_rank(MPI_COMM_WORLD, process_id, mpierr)
  call Hdf5_init()

  if(process_id==0) filename='/home/roy/Travail/Devel/Cosmologie/test/branch/pfof_cube_snap_star_data_dev_00000.h5'
  if(process_id==1) filename='/home/roy/Travail/Devel/Cosmologie/test/branch/pfof_cube_snap_star_data_dev_00001.h5'

  call Select_read_star_cube(filename, process_id)

  stars%has_ramses_identity=.true.
  call Read_star_cube(star_cube_metadata, stars, filename, process_id)

  write(OUTPUT_UNIT,*) 'Avant tri:', stars%ramses_identity(1), stars%ramses_identity(stars%size) 
  call Heapsort_particles(stars%size, stars, 'ramses_identity', process_id)
  write(OUTPUT_UNIT,*) 'Apres tri:', stars%ramses_identity(1), stars%ramses_identity(stars%size)

  if(allocated(stars%position)) then
     write(OUTPUT_UNIT,*) 'allocation de stars%position ok, taille=',size(stars%position,2)
  end if

  call Hdf5_finalize()
  call mpi_finalize(mpierr)
  
end program test

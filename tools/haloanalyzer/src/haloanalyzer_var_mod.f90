!=======================================================================
! Author: Fabrice Roy (LUTH/CNRS/Observatoire de Paris)
! Fabrice.Roy@obspm.fr
!=======================================================================
module haloanalyzer_var_mod

  use modconstant, only : IDKIND
  ! If you do not know the type of integer used for the ID, use h5dump on the first halo file.
  ! h5dump -d haloID test_halo_00000.h5
  ! This will print the dataset haloID containing the ID of each halo in the file. 
  ! You should see something like
  ! HDF5 "test_halo_00000.h5" {
  ! DATASET "haloID" {
  !    DATATYPE  H5T_STD_I64LE
  ! The datatype should contain the number of bits used, i.e. 32 for kind=4 or 64 for kind=8.

  implicit none
   
  private

  public :: type_particles, &
    type_var_list, &
    type_info_proc, &
    currenthalo, &
    currenthaloID, &
    nbhaloanalyzed



  type :: type_particles
     integer(kind=4) :: nb_part
     integer(kind=IDKIND), dimension(:), allocatable :: pfof_id        
     real(kind=4), dimension(:,:), allocatable :: position  
     real(kind=4), dimension(:,:), allocatable :: velocity  
     real(kind=4), dimension(:), allocatable :: mass
     real(kind=4), dimension(:,:), allocatable :: field
     real(kind=4), dimension(:), allocatable :: potential
     real(kind=4), dimension(:), allocatable :: birth_date
     real(kind=4), dimension(:), allocatable :: metallicity
     integer(kind=IDKIND), dimension(:), allocatable :: structure_id
     integer(kind=IDKIND), dimension(:), allocatable :: ramses_id
  end type type_particles

  type type_var_list
     logical :: need_position = .true.
     logical :: need_velocity = .true.
     logical :: need_pfof_id = .true.
     logical :: need_mass = .false.
     logical :: need_birth_date = .false.
     logical :: need_metallicity = .false.
     logical :: need_field = .false.
     logical :: need_potential = .false.
     logical :: need_structure_id = .false.
     logical :: need_ramses_id = .false.
  end type type_var_list

  type type_info_proc
     integer :: pid
     integer :: pnb
  end type type_info_proc

  integer(kind=IDKIND) :: currenthaloID ! haloID of the halo analyzed in your function   
  integer(kind=4) :: currenthalo     ! index of the halo which varies from 1 to N where N is the total number of haloes you are analyzing   
  integer(kind=4) :: nbhaloanalyzed  ! total number of haloes that you want to analyze 
  

end module haloanalyzer_var_mod

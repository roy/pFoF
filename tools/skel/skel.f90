!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2018 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Contains subroutines that do something
!! @brief
!! 
!! @author Fabrice Roy

!> Contains subroutines that do something
!------------------------------------------------------------------------------------------------------------------------------------

module do_something_mod

  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  
  implicit none

  private

  public :: Do_something

contains
  !----------------------------------------------------------------------------------------------------------------------------------
  !> Do something
  subroutine Do_something(some_arg_out, some_arg_in)

    use some_module, only : SOME_CONSTANT, some_routine
    
    real(kind=4), intent(out) :: some_arg_out
    integer(kind=4), intent(in) :: some_arg_in
    
    integer :: some_local_arg


#ifdef DEBUG
    write(ERROR_UNIT,*) 'Enter Do_something on process', process_id
#endif
    
    
    
#ifdef DEBUG
    write(ERROR_UNIT,*) 'Exit Read_amr_file on process', 
#endif
    
  end subroutine Do_something

end module do_something_mod

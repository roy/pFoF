include ../../../Make.inc

EXE = conepartcreator.exe

COMMONDIR = ../../../common/src/
OBJDIR=../../../obj/
MODDIR=../../../mod/
VPATH= $(COMMONDIR):.

SRC = modconstant.f90 \
	type_info_ramses_mod.f90 \
	modreadinfo.f90 \
	conepartcreator_var_m.f90 \
	modmpicommons.f90 \
	modwritemeta.f90 \
	conepartcreator_io_m.f90 \
	conepartcreator_sort_m.f90 \
	conepartcreator.f90

OBJS_NOPREFIX=$(SRC:%.f90=%.o)
OBJS=$(addprefix $(OBJDIR), $(OBJS_NOPREFIX))

# variable containing if git is used or not
ifeq ($(wildcard ../../../.git/HEAD ../../../.git/index),)
	OBJ_GIT =
	USE_GIT =
else
	USE_GIT = $(shell which git)
	OBJ_GIT = ../../../.git/HEAD ../../../.git/index
endif


$(OBJDIR)%.o:%.f90
	@echo "--------------------------------"
	@echo building $*.o with parameters \
	FC=$(FC), FCFLAGS=$(FCFLAGS)
	$(FC) -c $(FCFLAGS) $< -o $@

$(MODDIR)%.mod:%.f90
	@echo "--------------------------------"
	@echo building $*.mod with parameters \
	FC=$(FC), FCFLAGS=$(FCFLAGS)
	$(FC) -c $(FCFLAGS) $<


all: directories release
release: $(EXE)
debug: FCFLAGS = $(DEBUGFLAGS)
debug: $(EXE)

$(EXE) : gitversion $(OBJS)
	@echo "--------------------------------"
	@echo building $(EXE) 
	$(FC) -o $(EXE) $(OBJS)  $(LDFLAGS)

cleanall :
	@echo "--------------------------------"
	@echo cleaning compiled objects, modules and executable
	rm -f $(OBJDIR)*.o $(MODDIR)*.mod *~ $(EXE) $(COMMONDIR)/gitversion.h

clean :
	@echo "--------------------------------"
	@echo cleaning compiled objects, modules and executable 
	rm -f $(EXE)

# create file for git version
gitversion: $(OBJ_GIT)
ifneq ($(USE_GIT), )
	@cd $(COMMONDIR) ;\
	echo "#define GITVERSION \"$(shell git rev-parse HEAD)\"" > gitversion.h
else
	@cd $(COMMONDIR) ;\
	echo "#define GITVERSION \"no_version_found\"" > gitversion.h
endif

directories: ${MODDIR} ${OBJDIR}

${OBJDIR}:
	${MKDIR_P} $@

${MODDIR}:
	${MKDIR_P} $@

.PHONY: gitversion clean cleanall directories all


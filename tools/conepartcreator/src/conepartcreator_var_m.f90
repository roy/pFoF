!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2015 Fabrice Roy 
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Variables for conepartcreator
!! @brief
!! 
!! @author Fabrice Roy

!> Variables for conepartcreator
!------------------------------------------------------------------------------------------------------------------------------------

module conepartcreator_var_m

  Use modconstant
  Use mpi
  use type_info_ramses_mod

  Type(Type_parameter_conecreator_part) :: param

  Integer(kind=IDKIND), dimension(:), allocatable :: id        ! id of the particles in the halo just read
  Real(kind=4), dimension(:,:), allocatable :: pos  ! position of the particles in the halo just read
  Real(kind=4), dimension(:,:), allocatable :: vel  ! velocities of the particles in the halo just read
  Integer(kind=IDKIND), dimension(:), allocatable :: ramsespartid
  Real(kind=4), dimension(:), allocatable :: pot
  Real(kind=4), dimension(:,:), allocatable :: field


  Integer(kind=4), dimension(:), allocatable :: npartcubeloc, npartcube
  Integer(kind=4), dimension(:), allocatable :: idcube

  Integer(kind=4) :: ncx
  Integer(kind=4) :: ncy
  Integer(kind=4) :: ncz
  Integer(kind=4) :: ncube

  Real(kind=8) :: hy
  Real(kind=8) :: hz

  !! MPI Variables
  Integer :: req_sumnpc
  Integer(kind=4), dimension(Mpi_Status_Size) :: mpistat

  Type(Type_info_cone_part) :: infocone
  Type(Type_info_ramses) :: inforamses

End Module conepartcreator_var_m

!------------------------------------------------------------------------------------------------------------------------------------
! Copyright  2015 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Program that convert particles binary files from a RAMSES cone output to HDF5 files
!! @brief
!! 
!! @author Fabrice Roy

!> Program that convert particles binary files from a RAMSES cone output to HDF5 files
!------------------------------------------------------------------------------------------------------------------------------------

Program conepartcreator

  Use conepartcreator_io_m
  Use conepartcreator_sort_m
  Use conepartcreator_var_m
  Use modmpicommons, only : procID, procNB

  Use mpi
  use fortran_hdf5_manage_interface_m

  Implicit None

  Integer(kind=4) :: mpierr

  Call Mpi_Init(mpierr)
  Call Mpi_Comm_Rank(Mpi_Comm_World, procID, mpierr)
  Call Mpi_Comm_Size(Mpi_Comm_World, procNB, mpierr)

  Call hdf5_init()

  Call readparameters

  Call readramsesfiles

  Call dividespace()

  If(param%do_read_ramses_part_id) Then
     If(.not.param%do_read_potential) Then
!        Call tritas(npartloc,idcube, pos, vel,ramsespartid)
        Call heapsort(npartloc, idcube, pos, vel, tid=ramsespartid)
     Else
        If(.not.param%do_read_gravitational_field) Then
           Call heapsort(npartloc, idcube, pos, vel, tp=pot, tid=ramsespartid)
        Else
           Call heapsort(npartloc, idcube, pos, vel, tf=field, tp=pot, tid=ramsespartid)
        End If
     End If
  Else
!     Call tritas(npartloc,idcube, pos, vel)
     If(.not.param%do_read_potential) Then
        Call heapsort(npartloc, idcube, pos, vel)
     Else
        If(.not.param%do_read_gravitational_field) Then
           Call heapsort(npartloc, idcube, pos, vel, tp=pot)
        Else
           Call heapsort(npartloc, idcube, pos, vel, tf=field, tp=pot)
        End If
     End If
     
  End If

#ifdef WITHMPI3
  Call Mpi_Wait(req_sumnpc, mpistat, mpierr)
#endif

  Call h5writecone()
     
  Deallocate(pos, vel, id)
  Deallocate(nparttab)

  Call hdf5_finalize()
  Call Mpi_Finalize(mpierr)

End Program conepartcreator

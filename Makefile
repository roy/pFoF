include Make.inc

all: pfof_snap.exe pfof_cone.exe amr2cell_hdf5.exe conepartcreator.exe conegravcreator.exe conemapper.exe

pfof_snap.exe: 
	cd pfof_snap/src ; make

pfof_cone.exe:
	cd pfof_cone/src ; make

amr2cell_hdf5.exe:
	cd amr2cell_hdf5/src ; make

conepartcreator.exe:
	cd tools/conepartcreator/src ; make

conegravcreator.exe:
	cd tools/conegravcreator/src ; make

conemapper.exe:
	cd tools/conemapper/src ; make

clean:
	cd pfof_snap/src ; make clean
	cd pfof_cone/src ; make clean
	cd amr2cell_hdf5/src ; make clean
	cd tools/conepartcreator/src ; make clean
	cd tools/conegravcreator/src ; make clean
	cd tools/conemapper/src ; make clean
	rm -f mod/* obj/*
